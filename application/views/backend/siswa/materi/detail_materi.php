<div class="row clearfix">
  <?php if ($materi->num_rows()>0): ?>
      
  <div class="col-lg-12">
    <div class="card">
      <div class="body">

          <h2>
            <?php print_r($materi->result()[0]->judul) ?>
          </h2>

          <span><?php echo $materi->result()[0]->isi ?></span>


      </div>
    </div>
  </div>

  <?php endif ?>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#materi').addClass('active');
  });
</script>
