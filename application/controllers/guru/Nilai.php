<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }

  // Untuk View Uts
  public function index()
  {
    $isi['judul']   = "Nilai Uts Pilgan";
    $isi['content'] = "backend/guru/dt_nilai/uts/pilgan/v_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function uts_eesay()
  {
    $isi['judul']   = "Nilai Uts Eesay";
    $isi['content'] = "backend/guru/dt_nilai/uts/eesay/v_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function ulangan_pilgan()
  {
    $isi['judul']   = "Nilai Ulangan Pilgan";
    $isi['content'] = "backend/guru/dt_nilai/ulangan/pilgan/v_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function ulangan_eesay()
  {
    $isi['judul']   = "Nilai Ulangan Eesay";
    $isi['content'] = "backend/guru/dt_nilai/ulangan/eesay/v_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function t_uts_pilgan()
  {
    $isi['judul']   = "Tambah Nilai Uts";
    $isi['content'] = "backend/guru/dt_nilai/uts/t_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function t_uts_eesay()
  {
    $isi['judul']   = "Tambah Nilai Uts";
    $isi['content'] = "backend/guru/dt_nilai/uts/t_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function insert_uts_pilgan()
  {
    // code...
  }

  public function insert_uts_eesay()
  {
    # code...
  }

  public function u_uts_pilgan()
  {
    $isi['judul']   = "Update Nilai Uts";
    $isi['content'] = "backend/guru/dt_nilai/uts/u_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function u_uts_eesay()
  {
    $isi['judul']   = "Update Nilai Uts";
    $isi['content'] = "backend/guru/dt_nilai/uts/u_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function update_uts_pilgan()
  {
    // code...
  }

  public function update_uts_eesay()
  {
    # code...
  }


  public function delete_uts_pilgan()
  {
    // code...
  }

  public function delete_uts_eesay()
  {
    # code...
  }

  public function t_ulangan_pilgan()
  {
    $isi['judul']   = "Tambah Nilai Ulangan";
    $isi['content'] = "backend/guru/dt_nilai/ulangan/t_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function t_ulangan_eesay()
  {
    $isi['judul']   = "Tambah Nilai Ulangan";
    $isi['content'] = "backend/guru/dt_nilai/ulangan/t_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function insert_ulangan_pilgan()
  {
    // code...
  }

  public function insert_ulangan_eesay()
  {
    // code...
  }

  public function u_ulangan_pilgan()
  {
    $isi['judul']   = "Update Nilai Ulangan";
    $isi['content'] = "backend/guru/dt_nilai/ulangan/u_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function u_ulangan_eesay()
  {
    $isi['judul']   = "Update Nilai Ulangan";
    $isi['content'] = "backend/guru/dt_nilai/ulangan/u_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function update_ulangan_pilgan()
  {
    // code...
  }

  public function update_ulangan_eesay()
  {
    # code...
  }

  public function delete_ulangan_pilgan()
  {
    // code...
  }

  public function delete_ulangan_eesay()
  {
    # code...
  }
}
