<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
      	<div class="pull-right">

      		<form action="" method="get">
      			      		<input type="date" name="tanggal" class="form-control" style="float: right;" value="<?php echo date('Y-m-d') ?>">
      		<br>
      		<br>
      		<button type="submit" style="float: right;" class="btn btn-primary">Filter</button>
      		</form>
      	</div>
          <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
      		<thead>
      			<th>NO</th>
      			<th>Kelas</th>
      			<th>Jumlah Murid</th>
      			<th>Jumlah Absen Hari Ini</th>
      			<th>Opsi</th>
      		</thead>
      		<tbody>
      			<?php if ($data->num_rows()>0): ?>
      				
      				<?php $n=1; ?>
      			<?php foreach ($data->result_object() as $dt): ?>
      				<tr>
      					<td><?php echo $n++ ?></td>
      					<td><?php echo $dt->nama_kelas ?></td>
      					<td><?php echo $dt->total ?></td>
      					<td><?php echo $dt->absen ?></td>
      					<td>
      						<a href="<?php echo base_url('guru/Absensi/detail?kelas='.$dt->id_kelas.'&from='.$this->input->get('tanggal')) ?>" class="btn btn-primary">DETAIL</a>
      					</td>
      				</tr>
      			<?php endforeach ?>
      			<?php else: ?>
      				<tr>
      					<td colspan="5"><center>Tidak Ada Data</center></td>
      				</tr>
      			<?php endif ?>
      		</tbody>
      	</table>

      </div>
    </div>
  </div>
</div>