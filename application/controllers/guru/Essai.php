<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Essai extends CI_Controller {



  protected $table="t_modul_soal";
  protected $folder = "backend/guru/dt_soal/";
  protected $primaryKey= "id";
  protected $base = "Soal";

  function __construct()
  {
    parent::__construct();
    $this->load->model('model_select');
  }

  // Untuk View Uts
  public function index()
  {
    $this->model_security->getsecurity();
    $soal = $this->input->get('soal');

    if ($soal=="ULANGAN") {
    	# code...
    	$s = "Ulangan Harian";
    }
    else{
        	$s = "UTS";	
    }
    
    $judul   = "Soal Essai ".$s ;
    $content = "backend/guru/essai/table";

    $forms = $this->forms();

    // $data    = $this->model_all->join2table_group('tb_uts_pil');

    // print_r($soal);


    $sql = "SELECT * from t_modul_soal a inner join tb_mapel b on a.id_mapel = b.id_mapel inner join tb_kelas c on a.kelas = c.id_kelas inner join tb_jurusan aj on aj.id_jurusan = a.jurusan WHERE  jenis='".$soal."' AND essai='1'";
    $data = $this->db->query($sql);
    $this->load->view("backend/guru/home", compact('judul','content','forms','data'));

  }

public function daftar($id){

    $judul   = "Daftar Partisipan Ujian";
    $content = "backend/guru/essai/list";

    $info = $this->db->query("SELECT * FROM t_modul_soal a inner join tb_mapel b on a.id_mapel = b.id_mapel WHERE  a.id='$id'");
    if ($info->num_rows()>0) {


    	$data = $this->db->query("select a.id,d.nama_siswa,a.soal_id from t_soal_user a inner join t_modul_soal b on a.id_modul = b.id inner join tb_user c on a.id_user = c.id_user INNER join tb_siswa d on c.id_user = d.id_user  WHERE b.id='".$id."' GROUP by a.id_user");


    	# code...

    	// print_r($info)
	$this->load->view("backend/guru/home",compact('content','judul','info','data'));
    }
    else{
    	echo "<h1>Halaman Yang Anda Cari Tidak Ada</h1>";
    }
}




  public function delete($id){
    // echo $id;
    $sql = $this->db->delete('t_modul_soal',array('id'=>$id));
    if ($sql) {

      alert_success('Berhasil Menghapus Data');
    }
      return redirect($_SERVER['HTTP_REFERER']);
  }

  

	public function koreksi($id){
		$info = $this->db->query("SELECT * from t_soal_user a  inner join tb_user b on b.id_user = a.id_user inner join tb_siswa c on c.id_user = b.id_user  inner join t_modul_soal d on a.id_modul = d.id WHERE a.soal_id = '".$id."'");

		$data = $this->db->query("SELECT *,a.id as idne,a.benar as benare from t_soal_user a inner join t_soal_ganda b on a.id_soal = b.id inner join t_jawaban_user c on c.id_soal_user = a.id inner join t_jawaban j on b.id = j.id_soal  WHERE a.soal_id='".$id."'");


		$judul = "Koreksi Jawaban Essai";

    	$content = "backend/guru/essai/koreksi";


    	$this->load->view("backend/guru/home", compact('judul','content','forms','info','data'));

	}

public function info($id){

  $info = $this->db->query("SELECT * from t_soal_user a  inner join tb_user b on b.id_user = a.id_user inner join tb_siswa c on c.id_user = b.id_user  inner join t_modul_soal d on a.id_modul = d.id inner join tb_mapel x on d.id_mapel = x.id_mapel WHERE a.soal_id = '".$id."'");

    $data = $this->db->query("SELECT *,a.id as idne,a.benar as benare from t_soal_user a inner join t_soal_ganda b on a.id_soal = b.id inner join t_jawaban_user c on c.id_soal_user = a.id WHERE a.soal_id='".$id."'");



// print_r($info->result_object());
    $data = $this->db->query("SELECT *,a.id as idne,a.benar as benare from t_soal_user a inner join t_soal_ganda b on a.id_soal = b.id WHERE a.soal_id='".$id."'  ");




    $judul = "Detail Jawaban Siswa";

      $content = "backend/guru/essai/info";


      $nilai = $this->db->get_where('tb_nilai',['kode_soal'=>$id]);

      $this->load->view("backend/guru/home", compact('judul','content','forms','info','data','nilai'));




}

	public function set_poin(){
		$soal = $this->input->post('soal');
		$benar = $this->input->post('benar');
		$salah = $this->input->post('salah');
		$poin = $this->input->post('poin');
		$idne = $this->input->post('idne');


		$d = $this->db->get_where('t_soal_user', ['soal_id'=>$idne])->result();

		// print_r($d);
		$id_siswa = $d[0]->id_user;
		$modul = $d[0]->id_modul;
		$siswa = $this->db->get_where('tb_siswa',['id_user'=>$id_siswa])->result();
		$jurusan = $siswa[0]->jurusan;
		$kelas = $siswa[0]->id_kelas;

		$tgl = date('Y-m-d');


		$data = $this->db->get_where('tb_nilai', ['id_siswa'=>$id_siswa,'id_modul'=>$modul,'kode_soal'=>$idne]);
		if ($data->num_rows()==0) {
		$this->db->insert('tb_nilai', ['id_siswa'=>$id_siswa,'id_kelas'=>$kelas,'point'=>$poin,'id_jurusan'=>$jurusan,'tgl'=>$tgl,'kode_soal'=>$idne,'benar'=>$benar,'salah'=>$salah,'soal'=>$soal,'id_modul'=>$modul,'jenis_soal'=>'ESSAI']);

		}
		else{
			$this->db->update('tb_nilai', ['benar'=>$benar,'salah'=>$salah,'point'=>$poin,'soal'=>$soal,'jenis_soal'=>'ESSAI'],['id_modul'=>$modul,'id_siswa'=>$id_siswa]);

		}

		// print_r($siswa);



	}

	public function set_koreksi(){
		// print_r($this->input->post());
		$id = $this->input->post('id');
		$state = $this->input->post('state');

		$this->db->update('t_soal_user',['is_click'=>'1','benar'=>$state],['id'=>$id]);

		$data = $this->db->get_where('t_soal_user',['id'=>$id])->result();
		$soalid = $data[0]->soal_id;


		$dt = $this->db->get_where('t_soal_user',['soal_id'=>$soalid]);

		$total_soal = $dt->num_rows();


		$modulid= $dt->result()[0]->id_modul;
		$modul = $this->db->get_where('t_modul_soal',['id'=>$modulid]);

		$max = $modul->result()[0]->max_nilai;

		$poin_per_soal = $max/$total_soal;


		$total = 0;	
		$salah = 0;
		$benar = 0;
		$belum = 0;
		$poin = 0;
		foreach ($dt->result() as $d) {
			# code...
			$total +=1;
			if ($d->is_click ==1) {
				# code...

				if ($d->benar==0) {
					# code...
					$salah += 1;
				}

			}
			else{
				$belum +=1;
			}

			if ($d->benar==1) {
				# code...
				$benar += 1;
								$poin += 1* $poin_per_soal;
			}

		}


		echo json_encode(['total'=>$total,'salah'=>$salah,'benar'=>$benar,'belum'=>$belum,'poin'=>$poin]);

	}

	public function getState($soalid){

		// $data = $this->db->get_where('t_soal_user',['id'=>$id])->result();
		// $soalid = $data[0]->soal_id;


		$dt = $this->db->get_where('t_soal_user',['soal_id'=>$soalid]);

		$modulid= $dt->result()[0]->id_modul;
		$modul = $this->db->get_where('t_modul_soal',['id'=>$modulid]);

		$total_soal = $dt->num_rows();



		$max = $modul->result()[0]->max_nilai;

		$poin_per_soal = $max/$total_soal;

		$total = 0;	
		$salah = 0;
		$benar = 0;
		$poin=0;
		$belum = 0;

		foreach ($dt->result() as $d) {
			# code...
			$total +=1;
			if ($d->is_click ==1) {
				# code...

				if ($d->benar==0) {
					# code...
					$salah += 1;
				}

			}
			else{
				$belum +=1;
			}

			if ($d->benar==1) {
				# code...
				$benar += 1;
				$poin += 1* $poin_per_soal;
			}

		}

		echo json_encode(['total'=>$total,'salah'=>$salah,'benar'=>$benar,'belum'=>$belum,'poin'=>$poin]);

	}
  public function dump_update(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$where = array("'.$this->primaryKey.'"=>$id);<br>';
    echo '$this->db->update("'.$table.'",$datane,$where);';

  }

  public function edit($id){

    if ($this->input->post()) {
      # code...

   
    $kode_soal = $this->input->post("kode_soal");
$nama_modul = $this->input->post("nama_modul");
$kelas = $this->input->post("kelas");
$jurusan = $this->input->post("jurusan");
$jumlah_soal = $this->input->post("jumlah_soal");
$jumlah_jawaban = $this->input->post("jumlah_jawaban");
$min_nilai = $this->input->post("min_nilai");
$max_nilai = $this->input->post("max_nilai");
$waktu = $this->input->post("waktu");
$id_mapel = $this->input->post("id_mapel");
$jenis = $this->input->post("jenis");

$datane = array(
"kode_soal" => $kode_soal,
"nama_modul" => $nama_modul,
"kelas" => $kelas,
"jurusan" => $jurusan,
"jumlah_soal" => $jumlah_soal,
"jumlah_jawaban" => 1,
"min_nilai" => $min_nilai,
"max_nilai" => $max_nilai,
"waktu" => $waktu,
"id_mapel" => $id_mapel,
"jenis" => $jenis,
);
$where = array("id"=>$id);
$this->db->update("t_modul_soal",$datane,$where);



      alert_success('Berhasil Mengubah Data');
      return redirect('guru/Soal');

    }
    else{


    $data  = $this->model_all->get_where("t_modul_soal",["id"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];

    $judul   = "Ubah Modul Soal Pilihan Ganda";
    $content = "backend/guru/dt_soal/edit";

    $forms = $this->forms();
    $this->load->view("backend/guru/home", compact('judul','content','forms','data'));
    }
    else{
      echo "Tidak Ada Data";
    }

  }

    // print_r($data);
  }


public function detail($id){
 $forms = $this->forms();


    $data  = $this->model_all->get_where("t_modul_soal",["id"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Detail Data ";
      $content = $this->folder.'detail';
      $this->load->view("backend/guru/home", compact('content','judul','forms','data'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }


}

function shuffle_assoc($list) { 
  if (!is_array($list)) return $list; 

  $keys = array_keys($list); 
  shuffle($keys); 
  $random = array(); 
  foreach ($keys as $key) { 
    $random[$key] = $list[$key]; 
  }
  return $random; 
} 

public function unique_code_generator()
    {
        $t=time();
        return ( rand(000,111).$t);
    }



public function kirim(){
  // print_r($this->input->post());
  $soal = $this->input->post('soal') ;
  $jwb = $this->input->post('jawaban') ;
  $modul = $this->input->post('modul');

  // print_r($jwb);
  $num = 0;
  $soale = [];

  // print_r($soal);
  $x = 1;
  foreach ($jwb as $j) {
    # code...
    if($num++>0){
      // print_r($j);
      // print_r(count($j));

      $data = [];
      $n= 0;
      foreach ($j as $jawaban) {
        $data[] = [
          'jawaban'=>$jawaban,
          'benar'=>$n++==0? true : false
        ];

      }

      // echo $n++;
      $soale[] = [
        'soal'=>$soal[$x++],
        'jawaban'=>$data
      ];
      // print_r($this->shuffle_assoc($data

      // echo $x++;
    }

  }

// print_r($soale);
  foreach ($soale as $se) {
    # code...
    // print_r($se);
    $soal = $se['soal'];
    $jawaban= $se['jawaban'];


    if (!empty($soal)) {
    //   # code...
    $this->db->insert('t_soal_ganda',['id_modul'=>$modul,'soal'=>$soal]);
    $idne=  $this->db->insert_id();
    foreach ($jawaban as $jw) {
      $idx = $this->db->query("SELECT FLOOR(RAND() * 99999) AS random_num FROM t_jawaban WHERE 'random_num' NOT IN (SELECT uid FROM t_jawaban) LIMIT 1");

      if ($idx->num_rows()==0) {
        # code...
        $idx = time();
      }
      else{

      $idx = $idx->result()[0]->random_num;
      }
      $jawabann = $jw['jawaban'];
      $benar = $jw['benar'];
    $this->db->insert('t_jawaban',['id_soal'=>$idne,'uid'=>$idx,'jawaban'=>$jawabann,'benar'=>($benar? 1 :0)]);


    }
    }

  }


  echo json_encode(['status'=>1]);
  // foreach ($jwb as $j) {
  //   # code...
  //   print_r($j);
  // }
}


  public function data($id){
    $data = $this->db->get_where('t_soal_ganda',['id_modul'=>$id]);
    $this->load->view('backend/guru/dt_soal/data',compact('jml','data'));


  }
public function wrap_soal($id){

    $data  = $this->model_all->get_where("t_modul_soal",["id"=>$id]);

    $data = $data->result_object()[0];
    // print_r($data);

    $jml = $data->jumlah_jawaban;


  $this->load->view('backend/guru/dt_soal/wrap-form-soal',compact('jml'));
  }
  public function forms(){

    $kelas = $this->db->get('tb_kelas');

    $jurusan = $this->db->get('tb_jurusan');
    
    $mapel = $this->db->get('tb_mapel');


    $arrMapel = [];

    foreach ($mapel->result_array() as $m) {
      $arrMapel[] = [
        'value'=>$m['id_mapel'],
        'text'=>$m['nama_mapel']
      ];
    }
    $arrKelas = [];


    foreach ($kelas->result_array() as $k) {
      # code...
      $arrKelas[] = [
        'value'=>$k['id_kelas'],
        'text'=>$k['nama_kelas']
      ];
    }

    $arrJurusan = [];
    foreach ($jurusan->result_array() as $k) {
      # code...
      $arrJurusan[] = [
        'value'=>$k['id_jurusan'],
        'text'=>$k['nama_jurusan']
      ];
    }







    $forms  = [

      1=>[


        [

        'name'=>'Jenis Soal',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'jenis',
          'data'=>[
            ['value'=>'ULANGAN','text'=>'ULANGAN'],
            ['value'=>'UTS','text'=>'UTS']
          ]
        ]
        ],

        [

        'name'=>'Kode Soal',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'kode_soal'
        ]

        ],
        [

        'name'=>'Nama Modul Soal',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'nama_modul'
        ]

        ],
     
        [

        'name'=>'Mata Pelajaran',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'id_mapel',
          'data'=>$arrMapel
        ]
        ],
        [

        'name'=>'Kelas',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'kelas',
          'data'=>$arrKelas
        ]
        ],


        [

        'name'=>'Jurusan',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'jurusan',
          'data'=>$arrJurusan
        ]
        ],

      ],

      2=>
        [



        [

        'name'=>'Jumlah Soal',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'number',
          'field'=>'jumlah_soal'
        ]

        ],
        
        [


        'name'=>'Min Nilai',
        'list'=>[
          'type'=>'number',
          'field'=>'min_nilai'
        ]

        ],


        [


        'name'=>'Max Nilai',
        'list'=>[
          'type'=>'number',
          'field'=>'max_nilai'
        ]

        ],

        [


        'name'=>'Waktu (Menit)',
        'list'=>[
          'type'=>'number',
          'field'=>'waktu'
        ]

        ],


        ]
    ];

    // print_r($forms);
    return $forms;

    // print_r($forms);

    // echo json_encode($forms);


  }


  public function dump_insert(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$this->db->insert("'.$table.'",$datane);';

  }

  public function create(){

    $this->model_security->getsecurity();

    if ($this->input->post()) {
      # code...
      $kode_soal = $this->input->post("kode_soal");
$nama_modul = $this->input->post("nama_modul");
$kelas = $this->input->post("kelas");
$jurusan = $this->input->post("jurusan");
$jumlah_soal = $this->input->post("jumlah_soal");
$jumlah_jawaban = $this->input->post("jumlah_jawaban");
$min_nilai = $this->input->post("min_nilai");
$max_nilai = $this->input->post("max_nilai");
$waktu = $this->input->post("waktu");
$id_mapel = $this->input->post("id_mapel");
$jenis = $this->input->post("jenis");


$datane = array(
"kode_soal" => $kode_soal,
"nama_modul" => $nama_modul,
"kelas" => $kelas,
"jurusan" => $jurusan,
"jumlah_soal" => $jumlah_soal,
"jumlah_jawaban" => 1,
"min_nilai" => $min_nilai,
"max_nilai" => $max_nilai,
"waktu" => $waktu,
"id_mapel" => $id_mapel,
"jenis" => $jenis,
"essai"=>1
);
$this->db->insert("t_modul_soal",$datane);

      alert_success('Berhasil Menambah Data');
      return redirect('guru/Essai?soal='.$jenis);

    }
    else{
    $judul   = "Tambah Soal Essai";
    $content = "backend/guru/essai/create";

    $forms = $this->forms();
    $this->load->view("backend/guru/home", compact('judul','content','forms'));
    }

  }

  public function t_uts_eesay()
  {
    $this->model_security->getsecurity();
    $isi['judul']   = "Tambah Soal Uts";
    $isi['content'] = "backend/guru/dt_soal/uts/eesay/t_uts";
    $isi['data']    =  $this->model_all->get_all('tb_jurusan')->result();
    $isi['data_2']  =  $this->model_all->get_all('tb_kelas')->result();
    $isi['data_3']  =  $this->model_all->get_all('tb_mapel')->result();
    $this->load->view("backend/guru/home", $isi);
  }

  public function insert_uts_pilgan()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('kode_soal', 'Kode Soal', 'required',
      array('required' => 'Kode Soal Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('mapel', 'Mata Pelajaran', 'required',
      array('required' => 'Mata Pelajaran Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
      array('required' => 'Jurusan Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
      array('required' => 'Kelas Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {
      return $this->t_uts_pilgan();
    } else {

      $kode_soal  = $this->input->post('kode_soal');
      $guru       = $this->input->post('guru');
      $mapel      = $this->input->post('mapel');
      $jurusan    = $this->input->post('jurusan');
      $kelas      = $this->input->post('kelas');
      $soal       = $this->input->post('soal');
      $data       = array();

      foreach ($soal as $key => $value) {
        $data[] = array(
          'kode_soal'   => $kode_soal,
          'mapel_id'    => $mapel,
          'kelas_id'    => $kelas,
          'jurusan_id'  => $jurusan,
          'guru_id'     => $guru,
          'jenis_soal'  => 'pilgan',
          'soal'        => $_POST['soal'][$key],
          'option_a'    => $_POST['option_a'][$key],
          'option_b'    => $_POST['option_b'][$key],
          'option_c'    => $_POST['option_c'][$key],
          'option_d'    => $_POST['option_d'][$key],
          'option_e'    => $_POST['option_e'][$key],
          'jawaban'     => $_POST['kunci'][$key],
          'tgl'         => date('Y-m-d')

        );
      }
      $this->db->insert_batch('tb_uts_pil', $data);
      alert_success('Soal Uts Berhasil Ditambahkan');
      return redirect('guru/Soal');
    }
  }

  public function insert_uts_eesay()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('kode_soal', 'Kode Soal', 'required',
      array('required' => 'Kode Soal Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('mapel', 'Mata Pelajaran', 'required',
      array('required' => 'Mata Pelajaran Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
      array('required' => 'Jurusan Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
      array('required' => 'Kelas Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {
      return $this->t_uts_eesay();
    } else {

      $kode_soal  = $this->input->post('kode_soal');
      $guru       = $this->input->post('guru');
      $mapel      = $this->input->post('mapel');
      $jurusan    = $this->input->post('jurusan');
      $kelas      = $this->input->post('kelas');
      $soal       = $this->input->post('soal');
      $data       = array();

      foreach ($soal as $key => $value) {
        $data[] = array(
          'kode_soal'   => $kode_soal,
          'mapel_id'    => $mapel,
          'kelas_id'    => $kelas,
          'jurusan_id'  => $jurusan,
          'guru_id'     => $guru,
          'jenis_soal'  => 'eesay',
          'soal'        => $_POST['soal'][$key],
          'jawaban'     => $_POST['kunci'][$key],
          'tgl'         => date('Y-m-d')

        );
      }
      $this->db->insert_batch('tb_uts_eesay', $data);
      alert_success('Soal Eesay Berhasil Ditambahkan');
      return redirect('guru/Soal/uts_eesay');
    }
  }

  public function u_uts_pilgan()
  {
    $isi['judul']   = "Update Soal Uts";
    $isi['content'] = "backend/guru/dt_soal/uts/pilgan/u_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function u_uts_eesay()
  {
    $isi['judul']   = "Update Soal Uts";
    $isi['content'] = "backend/guru/dt_soal/uts/eesay/u_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function update_uts_pilgan()
  {
    // code...
  }

  public function update_uts_eesay()
  {
    // code...
  }

  public function delete_uts_pilgan()
  {
    // code...
  }

  public function delete_uts_eesay()
  {
    // code...
  }

  public function t_ulangan_pilgan()
  {
    $isi['judul']   = "Tambah Soal Ulangan";
    $isi['content'] = "backend/guru/dt_soal/ulangan/pilgan/t_ulangan";
    $isi['data']    =  $this->model_all->get_all('tb_jurusan')->result();
    $isi['data_2']  =  $this->model_all->get_all('tb_kelas')->result();
    $isi['data_3']  =  $this->model_all->get_all('tb_mapel')->result();
    $this->load->view("backend/guru/home", $isi);
  }

  public function t_ulangan_eesay()
  {
    $isi['judul']   = "Tambah Soal Ulangan";
    $isi['content'] = "backend/guru/dt_soal/ulangan/eesay/t_ulangan";
    $isi['data']    =  $this->model_all->get_all('tb_jurusan')->result();
    $isi['data_2']  =  $this->model_all->get_all('tb_kelas')->result();
    $isi['data_3']  =  $this->model_all->get_all('tb_mapel')->result();
    $this->load->view("backend/guru/home", $isi);
  }

  public function insert_ulangan_pilgan()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('kode_soal', 'Kode Soal', 'required',
      array('required' => 'Kode Soal Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('mapel', 'Mata Pelajaran', 'required',
      array('required' => 'Mata Pelajaran Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
      array('required' => 'Jurusan Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
      array('required' => 'Kelas Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {
      return $this->t_ulangan_pilgan();
    } else {

      $kode_soal  = $this->input->post('kode_soal');
      $guru       = $this->input->post('guru');
      $mapel      = $this->input->post('mapel');
      $jurusan    = $this->input->post('jurusan');
      $kelas      = $this->input->post('kelas');
      $soal       = $this->input->post('soal');
      $data       = array();

      foreach ($soal as $key => $value) {
        $data[] = array(
          'kode_soal'   => $kode_soal,
          'mapel_id'    => $mapel,
          'kelas_id'    => $kelas,
          'jurusan_id'  => $jurusan,
          'guru_id'     => $guru,
          'jenis_soal'  => 'pilgan',
          'soal'        => $_POST['soal'][$key],
          'option_a'    => $_POST['option_a'][$key],
          'option_b'    => $_POST['option_b'][$key],
          'option_c'    => $_POST['option_c'][$key],
          'option_d'    => $_POST['option_d'][$key],
          'option_e'    => $_POST['option_e'][$key],
          'jawaban'     => $_POST['kunci'][$key],
          'tgl'         => date('Y-m-d')

        );
      }
      $this->db->insert_batch('tb_ulangan_pil', $data);
      alert_success('Soal Ulangan Berhasil Ditambahkan');
      return redirect('guru/Soal/ulangan_pilgan');
    }
  }

  public function insert_ulangan_eesay()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('kode_soal', 'Kode Soal', 'required',
      array('required' => 'Kode Soal Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('mapel', 'Mata Pelajaran', 'required',
      array('required' => 'Mata Pelajaran Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
      array('required' => 'Jurusan Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
      array('required' => 'Kelas Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {
      return $this->t_ulangan_eesay();
    } else {

      $kode_soal  = $this->input->post('kode_soal');
      $guru       = $this->input->post('guru');
      $mapel      = $this->input->post('mapel');
      $jurusan    = $this->input->post('jurusan');
      $kelas      = $this->input->post('kelas');
      $soal       = $this->input->post('soal');
      $data       = array();

      foreach ($soal as $key => $value) {
        $data[] = array(
          'kode_soal'   => $kode_soal,
          'mapel_id'    => $mapel,
          'kelas_id'    => $kelas,
          'jurusan_id'  => $jurusan,
          'guru_id'     => $guru,
          'jenis_soal'  => 'eesay',
          'soal'        => $_POST['soal'][$key],
          'jawaban'     => $_POST['kunci'][$key],
          'tgl'         => date('Y-m-d')

        );
      }
      $this->db->insert_batch('tb_ulangan_eesay', $data);
      alert_success('Soal Eesay Berhasil Ditambahkan');
      return redirect('guru/Soal/ulangan_eesay');
    }
  }

  public function u_ulangan_pilgan()
  {
    $isi['judul']   = "Update Soal Ulangan";
    $isi['content'] = "backend/guru/dt_soal/ulangan/pilgan/u_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function u_ulangan_eesay()
  {
    $isi['judul']   = "Update Soal Ulangan";
    $isi['content'] = "backend/guru/dt_soal/ulangan/eesay/u_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function update_ulangan_pilgan()
  {
    // code...
  }

  public function update_ulangan_eesay()
  {
    // code...
  }

  public function delete_ulangan_pilgan()
  {
    // code...
  }

  public function delete_ulangan_eesay()
  {
    // code...
  }



  public function set_password($id){

    // print_r($this->input->post());
    if ($this->input->post()) {
      $pass = $this->input->post('pass');

      $upd = $this->db->update('t_modul_soal',['password'=>$pass],['id'=>$id]);

      if ($upd) {
        # code...
        $status = 1;
      }
      else{
        $status = 0;
      }

      echo json_encode(['status'=>$status]);

    }
    else{
          $data = $this->db->get_where('t_modul_soal',['id'=>$id]);
    $this->load->view('backend/guru/dt_soal/set_password',compact('data'));
    }

  }
}
