<div class="container-fluid">
  <div class="block-header">
      <h2><?= $judul; ?></h2>
  </div>
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
            <h2>
                Jawaban Ulangan
            </h2>
        </div>
        <div class="body">
          <a href="<?= base_url(); ?>guru/Jawaban/">
              <button name="button" class="btn btn-md btn-primary"> <span>Tambah Jawaban</span> <i class="material-icons">add_circle</i> </button>
          </a>
          <br>
          <br>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Level</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td align="center">
                    <a href="#">
                      <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit Jawaban">
                        <i class="material-icons">edit</i>
                      </button>
                    </a>
                    <a href="#">
                      <button type="button" name="button" onclick="return confirm('Yakin Ingin Menghapus Jawaban...???')" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus Jawaban">
                        <i class="material-icons">delete</i>
                      </button>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#jawaban').addClass('active');
    $('#jawaban_ulangan').addClass('active');
  });
</script>
