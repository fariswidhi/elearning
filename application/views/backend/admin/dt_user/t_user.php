<!-- Input Group -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Tambah User
                </h2>

            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <form action="<?= base_url(); ?>admin/User/insert_user" method="POST">

                            <label for="username">Username</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person_add</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control date" value="<?= set_value('username'); ?>" name="username" placeholder="Username">
                                </div>
                                <b class="text-danger"><?= form_error('username'); ?></b>
                            </div>

                            <label for="password">Password</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control date" name="password" placeholder="Password">
                                </div>
                                <b class="text-danger"><?= form_error('password'); ?></b>
                            </div>

                            <label for="level">Level</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                  <select class="form-control show-tick" name="level" data-live-search="true">
                                      <option value="">-- Pilih Level --</option>
                                      <option value="1">Admin</option>
                                      <option value="2">Guru</option>
                                      <option value="3">Siswa</option>
                                  </select>
                                </div>
                                <b class="text-danger"><?= form_error('level'); ?></b>
                            </div>

                            <div class="input-group">
                                <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                                <button type="submit" name="button" class="btn btn-primary waves-effect waves-light pull-right"><span>Tambah User</span> <i class="material-icons">send</i> </button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_user').addClass('active');
  });
</script>
<!-- #END# Input Group -->
