<!-- Input Group -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Update Guru
                </h2>

            </div>
            <div class="body">
                <div class="row clearfix">
                    <form action="" method="post">
                        <div class="col-md-6">
                            <label for="nig">Nomor Induk Guru</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control date" required="required" placeholder="Nomor Induk Guru" name="nig">
                                </div>
                            </div>

                            <label for="nama">Nama Guru</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person_add</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control date" placeholder="Nama Guru" name="nama">
                                </div>
                            </div>

                            <label for="jk">Jenis Kelamin</label>
                            <div class="input-group">
                                <select class="form-control show-tick" required="required" data-live-search="true" name="jk">
                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                    <option value="L">Laki - Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>

                            <label for="bidang_studi">Bidang Studi</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person_add</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control date" required="required" placeholder="Bidang Studi" name="bidang_studi">
                                </div>
                            </div>

                            <label for="tmpt">Tempat Lahir</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person_add</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control date" required="required" placeholder="Tempat Lahir" name="tmpt">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                          <label for="tgl">Tanggal Lahir</label>
                          <div class="input-group">
                              <span class="input-group-addon">
                                  <i class="material-icons">person</i>
                              </span>
                              <div class="form-line">
                                  <input type="date" class="datepicker form-control date" required="required" placeholder="Pilih Tanggal" name="tgl">
                              </div>
                          </div>

                          <label for="agama">Agama</label>
                          <div class="input-group">
                              <select class="form-control show-tick" required="required" data-live-search="true" name="agama">
                                  <option value="">-- Pilih Agama --</option>
                                  <option value="L">Laki - Laki</option>
                                  <option value="P">Perempuan</option>
                              </select>
                          </div>

                          <label for="almt">Alamat</label>
                          <div class="input-group">
                              <span class="input-group-addon">
                                  <i class="material-icons">lock</i>
                              </span>
                              <div class="form-line">
                                  <textarea name="almt" class="form-control" required="required" rows="6" placeholder="Alamat"></textarea>
                              </div>
                          </div>

                          <label for="level">Level</label>
                          <div class="input-group">
                              <img src="" alt="Foto" width="300">
                              <input type="file" class="form-control" required="required" name="foto" value="">
                          </div>

                        </div>
                        <div class="col-md-12">
                          <div class="input-group">
                              <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                              <button type="submit" name="button" class="btn btn-success waves-effect waves-light"><span>Update Guru</span> <i class="material-icons">update</i> </button>
                          </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#siswa').addClass('active');
  });
</script>
<!-- #END# Input Group -->
