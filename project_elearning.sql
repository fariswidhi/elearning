-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 28, 2018 at 10:57 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_absen`
--

CREATE TABLE `tb_absen` (
  `id_absen` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `id_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_user` int(11) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` enum('Islam','Kristen','Katholik','Budha','Hindu','Kong Hu Chu') NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_user`, `nama_admin`, `jk`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `foto`) VALUES
(11, 'Atfalstyle', '', '-', '0000-00-00', '', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru`
--

CREATE TABLE `tb_guru` (
  `nig` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `bidang_studi` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` enum('Islam','Kristen','Budha','Katholik','Hindu','Kong Hu Chu') NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_guru`
--

INSERT INTO `tb_guru` (`nig`, `id_user`, `nama_guru`, `jk`, `bidang_studi`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `foto`) VALUES
('asdasdasds', 12, 'asdasdasds', '', '-', '-', '0000-00-00', '', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'IPA'),
(4, 'IPS');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `nama_kelas`) VALUES
(1, '10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mapel`
--

INSERT INTO `tb_mapel` (`id_mapel`, `nama_mapel`) VALUES
(3, 'Bahasa Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `tb_materi_tugas`
--

CREATE TABLE `tb_materi_tugas` (
  `id_materi` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `isi` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai`
--

CREATE TABLE `tb_nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `benar` varchar(10) NOT NULL,
  `salah` varchar(10) NOT NULL,
  `point` int(11) NOT NULL,
  `soal` varchar(100) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id_pengumuman`, `tgl`, `judul`, `isi`) VALUES
(1, '2018-11-07', 'Penting Nemen Cuyasdasdasd2222', '                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n'),
(4, '2018-11-07', 'asdasd', 'asdasdasd');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `nis` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `agama` enum('Islam','Kristen','Katholik','Hindu','Budha','Kong Hu Chu') NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL,
  `id_kelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`nis`, `id_user`, `nama_siswa`, `jk`, `jurusan`, `agama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `foto`, `id_kelas`) VALUES
('dopalstyle', 16, 'dopalstyle', '-', '-', '', '-', '0000-00-00', '-', '-', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ulangan_eesay`
--

CREATE TABLE `tb_ulangan_eesay` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ulangan_eesay`
--

INSERT INTO `tb_ulangan_eesay` (`id_ulangan`, `kode_soal`, `mapel_id`, `kelas_id`, `jurusan_id`, `guru_id`, `jenis_soal`, `soal`, `jawaban`, `tgl`) VALUES
(1, 123, 3, 1, 1, 12, 'eesay', 'jkk', 'k', '2018-11-26'),
(2, 123, 3, 1, 1, 12, 'eesay', 'kb', 'b', '2018-11-26'),
(3, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(4, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(5, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(6, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(7, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(8, 123, 3, 1, 1, 12, 'eesay', 'n', 'nn', '2018-11-26'),
(9, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(10, 123, 3, 1, 1, 12, 'eesay', 'nn', 'n', '2018-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ulangan_pil`
--

CREATE TABLE `tb_ulangan_pil` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `option_a` varchar(100) NOT NULL,
  `option_b` varchar(100) NOT NULL,
  `option_c` varchar(100) NOT NULL,
  `option_d` varchar(100) NOT NULL,
  `option_e` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pass1` text NOT NULL,
  `pass2` text NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `pass1`, `pass2`, `level`) VALUES
(11, 'atfalstyle', '$2y$09$pkAKSSQxXxPMUwtSAjX4N.oQPRjWl23tiOsLTnsg7PNR17EBxOOZ2', 'atfal123', 1),
(12, 'gopalstyle', '$2y$09$kMPDvVzk8bKrbg2Q7f/2uuLKIV9xDyMVABAKIuCXYYtbxQ98e4.mS', 'atfal123', 2),
(16, 'dopalstyle', '$2y$09$ZvPaSb.MGaCOoWOduXKfZeD3koniG5JsLOj5j7GmQXqqALHeqcxpC', 'atfal123', 3);

--
-- Triggers `tb_user`
--
DELIMITER $$
CREATE TRIGGER `triger_user` AFTER INSERT ON `tb_user` FOR EACH ROW BEGIN
	IF NEW.level = 1 THEN
    	INSERT INTO tb_admin VALUES(NEW.id_user,NEW.username,'-','-','-','-','-','-');
    ELSEIF NEW.level = 2 THEN
    	INSERT INTO tb_guru VALUES(NEW.username,NEW.id_user,NEW.username,'-','-','-','-','-','-','-');
    ELSEIF NEW.level = 3 THEN
    	INSERT INTO tb_siswa VALUES(NEW.username,NEW.id_user,NEW.username,'-','-','-','-','-','-','-','-');
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_uts_eesay`
--

CREATE TABLE `tb_uts_eesay` (
  `id_uts` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_uts_eesay`
--

INSERT INTO `tb_uts_eesay` (`id_uts`, `kode_soal`, `mapel_id`, `jurusan_id`, `kelas_id`, `guru_id`, `jenis_soal`, `soal`, `jawaban`, `tgl`) VALUES
(1, 123123123, 3, 1, 1, 12, 'eesay', 'asdas', 'asdad', '2018-11-26'),
(2, 123123123, 3, 1, 1, 12, 'eesay', 'asdasdad', 'asdasd', '2018-11-26'),
(3, 123123123, 3, 1, 1, 12, 'eesay', 'asdad', 'asasdasd', '2018-11-26'),
(4, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(5, 123123123, 3, 1, 1, 12, 'eesay', 'asdadasd', 'asdad', '2018-11-26'),
(6, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdas', '2018-11-26'),
(7, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'dasdasd', '2018-11-26'),
(8, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(9, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(10, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasda', '2018-11-26'),
(11, 0, 3, 1, 1, 12, 'eesay', 'asdad', 'kjj', '2018-11-26'),
(12, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(13, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(14, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(15, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(16, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(17, 0, 3, 1, 1, 12, 'eesay', 'jj', 'j', '2018-11-26'),
(18, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(19, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(20, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(21, 132, 3, 4, 1, 12, 'eesay', 'asdad', 'jh', '2018-11-26'),
(22, 132, 3, 4, 1, 12, 'eesay', 'hjh', 'jh', '2018-11-26'),
(23, 132, 3, 4, 1, 12, 'eesay', 'jh', 'h', '2018-11-26'),
(24, 132, 3, 4, 1, 12, 'eesay', 'hh', 'h', '2018-11-26'),
(25, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(26, 132, 3, 4, 1, 12, 'eesay', 'h', 'h', '2018-11-26'),
(27, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(28, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(29, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(30, 132, 3, 4, 1, 12, 'eesay', 'h', 'h', '2018-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_uts_pil`
--

CREATE TABLE `tb_uts_pil` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `option_a` varchar(100) NOT NULL,
  `option_b` varchar(100) NOT NULL,
  `option_c` varchar(100) NOT NULL,
  `option_d` varchar(100) NOT NULL,
  `option_e` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_absen`
--
ALTER TABLE `tb_absen`
  ADD PRIMARY KEY (`id_absen`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD KEY `tb_admin_ibfk_1` (`id_user`);

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  ADD PRIMARY KEY (`id_materi`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_jurusan` (`jurusan_id`),
  ADD KEY `id_kelas` (`kelas_id`);

--
-- Indexes for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_siswa` (`id_siswa`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_jurusan` (`id_jurusan`),
  ADD KEY `tb_nilai_ibfk_2` (`kode_soal`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `guru_id` (`guru_id`),
  ADD KEY `jurusan_id` (`jurusan_id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `mapel_id` (`mapel_id`);

--
-- Indexes for table `tb_ulangan_pil`
--
ALTER TABLE `tb_ulangan_pil`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_kelas` (`kelas_id`),
  ADD KEY `id_mapel` (`mapel_id`),
  ADD KEY `id_jurusan` (`jurusan_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  ADD PRIMARY KEY (`id_uts`),
  ADD KEY `guru_id` (`guru_id`),
  ADD KEY `jurusan_id` (`jurusan_id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `mapel_id` (`mapel_id`);

--
-- Indexes for table `tb_uts_pil`
--
ALTER TABLE `tb_uts_pil`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_kelas` (`kelas_id`),
  ADD KEY `id_mapel` (`mapel_id`),
  ADD KEY `id_jurusan` (`jurusan_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_absen`
--
ALTER TABLE `tb_absen`
  MODIFY `id_absen` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  MODIFY `id_materi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_ulangan_pil`
--
ALTER TABLE `tb_ulangan_pil`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  MODIFY `id_uts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_uts_pil`
--
ALTER TABLE `tb_uts_pil`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_absen`
--
ALTER TABLE `tb_absen`
  ADD CONSTRAINT `tb_absen_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `tb_siswa` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD CONSTRAINT `tb_admin_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD CONSTRAINT `tb_guru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  ADD CONSTRAINT `tb_materi_tugas_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_materi_tugas_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_materi_tugas_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD CONSTRAINT `tb_nilai_ibfk_1` FOREIGN KEY (`kode_soal`) REFERENCES `tb_ulangan_pil` (`id_ulangan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_2` FOREIGN KEY (`kode_soal`) REFERENCES `tb_uts_pil` (`id_ulangan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_3` FOREIGN KEY (`id_siswa`) REFERENCES `tb_siswa` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_4` FOREIGN KEY (`id_kelas`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_5` FOREIGN KEY (`id_jurusan`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD CONSTRAINT `tb_siswa_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_4` FOREIGN KEY (`mapel_id`) REFERENCES `tb_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  ADD CONSTRAINT `tb_uts_eesay_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_4` FOREIGN KEY (`mapel_id`) REFERENCES `tb_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- CREATE TRIGGER `triger_user` AFTER INSERT ON `tb_user`
--  FOR EACH ROW BEGIN
--   IF NEW.level = 1 THEN
--       INSERT INTO tb_admin VALUES(NEW.id_user,NEW.username,'-','-','-','-','-','-');
--     ELSEIF NEW.level = 2 THEN
--       INSERT INTO tb_guru VALUES(NEW.username,NEW.id_user,NEW.username,'-','-','-','-','-','-','-');
--     ELSEIF NEW.level = 3 THEN
--       INSERT INTO tb_siswa VALUES(NEW.username,NEW.id_user,NEW.username,'-','-','-','-','-','-','-','-');
--     END IF;
-- END
