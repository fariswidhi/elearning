<!-- Favicon-->
<link rel="icon" href="<?= base_url(); ?>assets/back_end/favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->
<link href="<?= base_url(); ?>assets/back_end/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="<?= base_url(); ?>assets/back_end/plugins/node-waves/waves.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="<?= base_url(); ?>assets/back_end/plugins/animate-css/animate.css" rel="stylesheet" />

<!-- Bootstrap Select Css -->
<link href="<?= base_url(); ?>assets/back_end/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<!-- JQuery DataTable Css -->
<link href="<?= base_url(); ?>assets/back_end/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

<!-- Morris Chart Css-->
<link href="<?= base_url(); ?>assets/back_end/plugins/morrisjs/morris.css" rel="stylesheet" />

<!-- Custom Css -->
<link href="<?= base_url(); ?>assets/back_end/css/style.css" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="<?= base_url(); ?>assets/back_end/css/themes/all-themes.css" rel="stylesheet" />
