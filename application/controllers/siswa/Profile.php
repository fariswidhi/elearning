<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		$this->model_security->getsecurity();
    	$isi['judul']   = "Profile";
    	$isi['content'] = "backend/siswa/profile/profile";
    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');
    	// echo $this->session->userdata('id');
    	$userid = $this->session->userdata('id');

    	$isi['siswa'] = $this->db->query("SELECT * FROM tb_siswa a inner join tb_kelas b on a.id_kelas = b.id_kelas inner join tb_jurusan c on c.id_jurusan = a.jurusan inner join tb_user d on a.id_user = d.id_user  WHERE a.id_user='$userid'");

    	$isi['dataNilai'] = $this->db->query("select *,a.kode_soal as kdsoal from tb_nilai a inner join  t_modul_soal b on a.id_modul  = b.id WHERE a.id_siswa ='$userid'");
    	$this->load->view("backend/siswa/home", $isi);
	}
	public function update($id){

		$dd = $this->db->get_where('tb_siswa',['id_user'=>$id])->result()[0];
		$config['upload_path']          = 'gambar/profile';
		$config['allowed_types']        = 'gif|jpg|png';
		// $config['max_size']             = 100;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
		// $ext = $this->upload->data()['file_ext'];
		// $filename= 'profile-'.time().$ext;

			// print_r($this->upload->data());
		// $config['file_name'] = $filename;
 
		$this->load->library('upload', $config);
  		$this->upload->initialize($config);

	// print_r($this->input->post())  		;
  		if (!empty($_FILES)) {
		if (!  $this->upload->do_upload('berkas')){
			$error = array('error' => $this->upload->display_errors());
			// $this->load->view('v_upload', $error);
			print_r($error);
		}
		else{
			$data = array('upload_data' => $this->upload->data());


			$filename = 	'gambar/profile/'.$this->upload->data()['file_name'];
			// print_r($filename);
			// exit();

			// $sessid 
			$this->db->update('tb_siswa',['foto'=>$filename],['id'=>$dd->id]);
			// print_r($filename);

		}
  		}

  		if (!empty($this->input->post('password'))) {
  			# code...
  			$this->db->update('tb_user',['pass1'=>hash_string($this->input->post('password')),'pass2'=>$this->input->post('password')],['id_user'=>$id]);
  		}

  		      alert_success('Profil Berhasil Diubah');
      return redirect('siswa/Profile');


	}

}
