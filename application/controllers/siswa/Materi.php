<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {

	public function index()
	{
		$this->model_security->getsecurity();
    	$isi['judul']   = "Materi";
    	$isi['content'] = "backend/siswa/materi/v_materi";
    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');
    	$userid = $this->session->userdata('id');

    	$siswa = $this->db->get_where('tb_siswa',['id_user'=>$userid]);
    	$siswa = $siswa->result()[0];
    	$jurusan = $siswa->jurusan;
    	$kelas= $siswa->id_kelas;


        // print_r()
        // echo $kelas;
    	$isi['materi'] = $this->db->get_where('tb_materi_tugas',['jurusan_id'=>$jurusan,'kelas_id'=>$kelas]);
    	// print_r($kelas);


    	$this->load->view("backend/siswa/home", $isi);
	}
		public function detail($id)
	{
		$this->model_security->getsecurity();
    	$isi['judul']   = "Materi";
    	$isi['content'] = "backend/siswa/materi/detail_materi";
    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');
    	$userid = $this->session->userdata('id');

    	$siswa = $this->db->get_where('tb_siswa',['id_user'=>$userid]);
    	$siswa = $siswa->result()[0];
    	$jurusan = $siswa->jurusan;
    	$kelas= $siswa->id_kelas;


    	$isi['materi'] = $this->db->get_where('tb_materi_tugas',['id_materi'=>$id]);
    	// print_r($kelas);


    	$this->load->view("backend/siswa/home", $isi);
	}
}
