<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="row">
          <div class="col-lg-6">
            <table class="table table-bordered">
              <tr>
                <td>Nama Siswa</td><td><?php echo $info->result()[0]->nama_siswa ?></td>
              </tr>
              <tr>
                <td>Nama Modul</td><td><?php echo $info->result()[0]->nama_modul ?></td>
              </tr>
              <tr>
                <td>Min Nilai</td><td><?php echo $info->result()[0]->min_nilai ?></td>
              </tr>
              <tr>
                <td>Max Nilai</td><td><?php echo $info->result()[0]->max_nilai ?></td>
              </tr>

            </table>

          </div>
          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-6">
                
                <div class="info-box bg-red">
                        <div class="icon">
                            <i class="material-icons">close</i>
                        </div>
                        <div class="content">
                            <div class="text">SALAH</div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20"><?php echo $nilai->result_object()[0]->salah ?></div>
                        </div>
                    </div>
                    
              </div>
              <div class="col-lg-6">
                
                <div class="info-box bg-green">
                        <div class="icon">
                            <i class="material-icons">check</i>
                        </div>
                        <div class="content">
                            <div class="text">BENAR</div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20"><?php echo $nilai->result_object()[0]->benar ?></div>
                        </div>
                    </div>
                    
              </div>

            </div>

              <div class="col-lg-6" style="float: none;margin: 0 auto;">
                
                <div class="info-box bg-purple">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">NILAI</div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20"><?php echo $nilai->result_object()[0]->point ?></div>
                        </div>
                    </div>
                    
              </div>


          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
            Jawaban 
          </h2>

      </div>
      <div class="body">


         <?php foreach ($data->result() as $d): 

          // print_r($d);
          $id = $d->idne;
          $jawabane = $this->db->query("SELECT * FROM t_jawaban_user a inner JOIN t_jawaban b on a.id_jawaban =b.uid WHERE a.id_soal_user ='$id'");




          // print_r($d);
          $jwb = $jawabane->result()[0];

          $id_soal_user = $jwb->id_soal_user;
          // print_r($jawabane->result_object());
          // $jawabane_user = $this->db->get_where('tb')
          ?>
  
      <div style="border: 1px solid #eee;margin: 10px;padding: 10px;">
       <div class="pull-right">
          <?php echo $d->benare==1? '<label class="badge bg-green">BENAR<label>':'<label class="badge bg-red">SALAH<label>' ?>
       </div>
        <h3>        <?php           echo $d->soal; ?></h3>
        <h6  style="color: green">Jawaban</h6>
        <?php echo $jawabane->result()[0]->jawaban ?>
        <h6  style="color: orange">Jawaban Siswa</h6>
        <?php echo $jawabane->result()[0]->jawaban_essai ?>

</div>
         <?php endforeach ?>

<br>
<br>

</center>
         </div>
      </div>

  </div>
</div>



</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_eesay').addClass('active');
  });

  $(document).on('click','.btn-click',function(){
    var id = $(this).attr('data-id');

    var salah = $(this).attr('data-false');

    var state = 0;
      if (salah=="true") {
        state =0;
      }
      else{
        state =1;
      }

      $.ajax({
        url: "<?php echo base_url('guru/Essai/set_koreksi') ?>",
        data: {
          id :id,
          state:state
        },
        method: "POST",
        dataType:"JSON",
        success:function(res){
          if (state==0) {
            $(".btn-benar-"+id).removeAttr('disabled');
            $(".btn-salah-"+id).attr('disabled','disabled');
          }
          else{

            $(".btn-salah-"+id).removeAttr('disabled');

            $(".btn-benar-"+id).attr('disabled','disabled');
          }

          $("#total").val(res.total);
          $("#salah").val(res.salah);
          $("#benar").val(res.benar);

          $("#poin").val(res.poin);
          $("#belum").val(res.belum);
        }
      })

  })


      $.ajax({
        url: "<?php echo base_url('guru/Essai/getState/'.$this->uri->segment(4)) ?>",
        method: "GET",
        dataType:"JSON",
        success:function(res){
          $("#poin").val(res.poin);
          $("#total").val(res.total);
          $("#salah").val(res.salah);
          $("#benar").val(res.benar);
          $("#belum").val(res.belum);
        }
      })

      $(document).on('click','.btn-save-koreksi',function(){
        var soal = $("#total").val();
        var benar = $("#benar").val();
        var salah = $("#salah").val();

        var poin = $("#poin").val();
        $.ajax({
          url: "<?php echo base_url('guru/Essai/set_poin') ?>",
          data: {
            soal:soal,
            benar:benar,
            salah:salah,
            poin:poin,
            idne : "<?php echo $this->uri->segment(4) ?>"
          },
          method:"POST",
          success:function(res){
            alert("Berhasil Mengoreksi Soal Essai");
          }
        })
      });
</script>



