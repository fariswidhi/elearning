<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('alert-success'))
{
	function alert_success($pesan)
	{
		$CI=& get_instance();
		$CI->session->set_flashdata('info', '<div class="alert alert-success alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>'.$pesan.'</strong>
		</div>');
	}
}

if(!function_exists('alert-warning'))
{
	function alert_warning($pesan)
	{
		$CI=& get_instance();
		$CI->session->set_flashdata('info', '<div class="alert alert-warning alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>'.$pesan.'</strong>
		</div>');
	}
}

if(!function_exists('alert_info'))
{
	function alert_info($pesan)
	{
		$CI=& get_instance();
		$CI->session->set_flashdata('info', '<div class="alert alert-info alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>'.$pesan.'</strong>
		</div>');
	}
}

if(!function_exists('alert_danger'))
{
	function alert_danger($pesan)
	{
		$CI=& get_instance();
		$CI->session->set_flashdata('info', '<div class="alert alert-danger alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>'.$pesan.'</strong>
		</div>');
	}
}