<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="row clearfix">
          <div class="col-md-6">
            <form enctype="multipart/form-data" action="<?= base_url(); ?>guru/Materi/insert_materi" method="post" >
              <input type="hidden" name="guru" value="<?= $this->session->userdata('id'); ?>">
              <div class="">
              <label for="judul">Judul</label>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required"  name="judul">
              </div>

              </div>

              <label for="jurusan">Mata Pelajaran</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->

                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="mapel_id" data-live-search="true">
                      <option value="">-- Pilih Mata Pelajaran --</option>
                      <?php
                        foreach ($mp as $dtx) {
                          echo "<option value='".$dtx->id_mapel."'>".$dtx->nama_mapel."</option>";
                        }
                      ?>
                  </select>
                </div>

              </div>

              <label for="jurusan">Jurusan</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->

                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="jurusan" data-live-search="true">
                      <option value="">-- Pilih Jurusan --</option>
                      <?php
                        foreach ($data as $dt) {
                          echo "<option value='".$dt->id_jurusan."'>".$dt->nama_jurusan."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('jurusan'); ?></b>
              </div>

              <label for="kelas">Kelas</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="kelas" data-live-search="true">
                      <option value="">-- Pilih Kelas --</option>
                      <?php
                        foreach ($data_2 as $dt) {
                          echo "<option value='".$dt->id_kelas."'>".$dt->nama_kelas."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('kelas'); ?></b>
              </div>

          </div>


          <div class="col-lg-12">
            
              <label for="isi">File</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">local_offer</i>
                  </span>
                  <input type="file" name="file" class="form-control">

                  <b class="text-danger"><?= form_error('isi'); ?></b>
              </div>

              <div class="input-group">
                  <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                  <button type="submit" name="button" class="btn btn-primary waves-effect waves-light pull-right"><span>Tambah Materi</span> <i class="material-icons">send</i> </button>
              </div
          </div>
            </form>
        </div>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">


  $(function() {
    $('#materi').addClass('active');



  });
</script>
