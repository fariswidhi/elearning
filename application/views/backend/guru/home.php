<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>E-Learning - Guru</title>
    <?php $this->load->view('backend/layout/header'); ?>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 
    <!-- Include Editor style. -->
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />

</head>

<body class="theme-indigo">
    <!-- Page Loader -->
    <?php if (($this->uri->segment(2)!='Streaming') || $this->uri->segment(2) !='Soal'): ?>
<!--         <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Loading...</p>
        </div>
    </div> -->
    <?php endif ?>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar" >
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">E-Learning - SMA Muhammadiyah Jayapura</a>
            </div>
            <!-- <div class="collapse navbar-collapse" id="navbar-collapse"></div> -->
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <?php $this->load->view('backend/guru/menu'); ?>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="block-header">
            <h2><?= $judul; ?></h2>
        </div>
        <?php $this->load->view($content); ?>
      </div>
    </section>
    <?php $this->load->view('backend/layout/footer'); ?>

<!-- |<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>  -->


<!-- include summernote css/js -->
<?php if ($this->uri->segment(2)=='Soal'||$this->uri->segment(2)=='Essai'): ?>
    

<!-- <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>  -->

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<script type="text/javascript" src="https://mindmup.github.io/bootstrap-wysiwyg/bootstrap-wysiwyg.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
 
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/js/froala_editor.pkgd.min.js"></script>
 
    <!-- Initialize the editor. -->
<!--     <script> $(function() { $('.jwb').froalaEditor() }); </script> -->

<script>
$(document).ready(function() {
  // $('#summernote').summernote({
  //   height:300
  // });
});


var n= 0;
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


function readURL(input,target,id) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $(target).attr('src', e.target.result);
      $('.jwb'+id).val(e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

  $(document).on('click','.tambah_soal',function(){
    // if ($(this).attr('data-id') !='') {
    //     console.log("OKOK");
    // }

    // console.log($(this).attr('data-id').length());
    $(this).attr('data-id',makeid());
    var forme = $("#forme").html();
    // alert("OKe");

     n += 1;

    // n+=1;
    $("#forme").find('.summernote').summernote('destroy');


    // $('#forme').find('.summernote').remove();

// $("#append").html('');
console.log(n);
    if (n==1) {
        console.log("WIS");


$('#append').append(forme);
    $('.summernote').summernote({
      height:300
    });        

    }
    else{
// $('#forme').find('.summernote').summernote();
$('#append').append(forme);
    $('.summernote').summernote({
      height:300
    });        


    // alert("oka");
    $(".note-editor:last").remove();
    // $('#forme').find('.note-editor:last').remove();
    }

    var uid = $(".img-upload").attr('data-uid');

    $(".img-upload").each(function(i){
        $(this).addClass('id-'+i);
        $(this).attr('data-ku',+i)
    })

    $(".prev").each(function(s){

        $(this).addClass('img-'+s);
    });

    $(".jwb").each(function(x){
        $(this).addClass('jwb'+x);
    })

    // console.log(n-1);
    var kurang = n-1;
    var tambah = n+1;
    var ooo = tambah-1;
    console.log('tdata-'+tambah);
    console.log('kdata-'+ooo);
        // $(".img-upload").removeClass('data-'+ooo);
        // $(".img-upload").addClass('data-'+tambah);



//     $('.summernote:last').summernote('code', '');
    // $('.jawaban').wysiwyg();


// $("#append").append(forme);
  });

  $(document).on("change",'.img-upload',function(){
    var id  = $(this).attr('data-ku');

        readURL(this,".img-"+id,id);
    // console.log(id);
  })

</script>
<?php endif ?>
</body>

</html>
