<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->model_security->getsecurity();
	}

	public function index()
	{
		$lvl = $this->session->userdata('level');
		if($lvl == 1)
		{
			$isi['judul']    = "Beranda";
			$isi['content']  = "backend/admin/content";
			$id = array('id_user'	=> $this->session->userdata('id'));
			$data = $this->model_all->get_where('tb_admin', $id)->result();
			foreach ($data as $dt) {
				$nama_admin		= $dt->nama_admin;
				$jk_admin		= $dt->jk;
				$tempat_admin	= $dt->tempat_lahir;
				$tgl_lhr_admin	= $dt->tgl_lahir;
				$agama_admin	= $dt->agama;
				$alamat_admin	= $dt->alamat;
				$foto_admin		= $dt->foto;
			}
			$sess = array(
				'nama_admin'	=> $nama_admin,
				'jk_admin'		=> $jk_admin,
				'tempat_admin'	=> $tempat_admin,
				'tgl_lhr_admin' => $tgl_lhr_admin,
				'agama_admin'	=> $agama_admin,
				'alamat_admin'	=> $alamat_admin,
				'foto_admin'	=> $foto_admin
			);
			$this->session->set_userdata($sess);
			$this->load->view('backend/admin/home', $isi);
		}
		elseif($lvl == 2)
		{
			$isi['judul']    = "Beranda";
			$isi['content']  = "backend/guru/content";
			$id = array('id_user' => $this->session->userdata('id'));
			$data = $this->model_all->get_where('tb_guru', $id)->result();
			foreach ($data as $dt) {
				$nig					= $dt->nig;
				$nama_guru		= $dt->nama_guru;
				$jk_guru		= $dt->jk;
				$bd_std			= $dt->bidang_studi;
				$tempat_guru	= $dt->tempat_lahir;
				$tgl_lhr_guru	= $dt->tgl_lahir;
				$agama_guru		= $dt->agama;
				$alamat_guru	= $dt->alamat;
				$foto_guru		= $dt->foto;
			}

			$sess = array(
				'nig'			=> $nig,
				'nama_guru'		=> $nama_guru,
				'jk_guru'		=> $jk_guru,
				'bd_std'		=> $bd_std,
				'tempat_guru'	=> $tempat_guru,
				'tgl_lhr_guru' 	=> $tgl_lhr_guru,
				'agama_guru'	=> $agama_guru,
				'alamat_guru'	=> $alamat_guru,
				'foto_guru'		=> $foto_guru
			);
			$this->session->set_userdata($sess);
			$this->load->view('backend/guru/home', $isi);
		}
		elseif($lvl == 3)
		{
			$isi['judul']    = "Beranda";
			$isi['content']  = "backend/siswa/content";
			$id = array('id_user'	=> $this->session->userdata('id'));
			$data = $this->model_all->get_where('tb_siswa', $id)->result();
			foreach ($data as $dt) {
				$nis 			= $dt->nis;
				$nama_siswa		= $dt->nama_siswa;
				$jk_siswa		= $dt->jk;
				$jurusan_siswa	= $dt->jurusan;
				$tempat_siswa	= $dt->tempat_lahir;
				$tgl_lhr_siswa	= $dt->tgl_lahir;
				$agama_siswa	= $dt->agama;
				$alamat_siswa	= $dt->alamat;
				$foto_siswa		= $dt->foto;
				$kelas_siswa	= $dt->id_kelas;
			}

			$sess = array(
				'nama_siswa'	=> $nama_siswa,
				'jk_siswa'		=> $jk_siswa,
				'tempat_siswa'	=> $tempat_siswa,
				'tgl_lhr_siswa' => $tgl_lhr_siswa,
				'agama_siswa'	=> $agama_siswa,
				'alamat_siswa'	=> $alamat_siswa,
				'foto_siswa'	=> $foto_siswa
			);
			$this->session->set_userdata($sess);
			$this->load->view('backend/siswa/home', $isi);
		}
	}
}
