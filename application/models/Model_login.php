<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model {

	function __construct() {
    parent::__construct();
  }

	public function getlogin($u,$p)
	{
		$this->db->where('username',$u);
		$query = $this->db->get('tb_user');
		if($query->num_rows()>0):
			foreach ($query->result() as $row):
				$id 		= $row->id_user;
				$user 	= $row->username;
				$pass		= $row->pass1;
				$pass2	= $row->pass2;
				$level 	= $row->level;
			endforeach;
			$sess = array(
				'id'				=> $id,
				'pass'			=> $pass,
				'pass2'			=> $pass2,
				'username'	=> $user,
				'level'			=> $level);
			if(!hash_verify($p,$pass)):
				alert_danger("Password Yang Anda Masukan Salah");
				redirect('Login');
			else:
				if($level == '1' || $level == '2' || $level == '3'):
					$this->session->set_userdata($sess);
					// echo "oK";
					// print_r($level);
					if ($level == "3") {
						# code...


						$date = date('Y-m-d');
						$data = $this->db->query("SELECT * FROM tb_absen WHERE date('$date') AND id_siswa ='$id'");
						if ($data->num_rows()==0) {
							# code...
													$this->db->insert("tb_absen",['tgl'=>date('Y-m-d H:i:s'),'id_siswa'=>$id]);
						}

					}
					redirect('Home');
					// echo date('Y-m-d H:i:s');
					// exit();
				endif;
			endif;
		else:
			alert_danger('Username Atau Password Salah');
			redirect('Login');
		endif;
	}
}
