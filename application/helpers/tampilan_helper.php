<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('lihat_super_admin'))
{
  function lihat_super_admin($nama_super_admin, $id, $gbr)
  {
    $data = '
    <div class="col-md-4">
      <div class="box box-widget widget-user">
        <div class="widget-user-header bg-blue-active">
          <h3 class="widget-user-username">'.$nama_super_admin.'</h3>
          <h5 class="widget-user-desc">Super Admin</h5>
        </div>
        <div class="widget-user-image">
          <img class="img-circle" src="'.base_url().'assets/img/img_user/'.$gbr.'" alt="User Avatar">
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-4 border-right">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/v_u_user/'.$id.'?name='.base64_encode(base64_encode("Super Admin")).'"><button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i> Edit</button></a>
              </div>
            </div>
            <div class="col-sm-4 border-right">
              <div class="description-block">
              </div>
            </div>
            <div class="col-sm-4">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/d_user_a/'.$id.'">
                  <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-search"></i> Detail</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
    return $data;
  }
}

if(!function_exists('lihat_admin_kesehatan'))
{
  function lihat_admin_kesehatan($nama_admin_kesehatan, $id, $gbr)
  {
    $data = '
    <div class="col-md-4">
      <div class="box box-widget widget-user">
        <div class="widget-user-header bg-yellow-active">
          <h3 class="widget-user-username">'.$nama_admin_kesehatan.'</h3>
          <h5 class="widget-user-desc">Admin Kesehatan</h5>
        </div>
        <div class="widget-user-image">
          <img class="img-circle" src="'.base_url().'assets/img/img_user/'.$gbr.'" alt="User Avatar">
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-4 border-right">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/v_u_user/'.$id.'?name='.base64_encode(base64_encode("Admin Kesehatan")).'"><button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i> Edit</button></a>
              </div>
            </div>
            <div class="col-sm-4 border-right">
              <div class="description-block">
                
              </div>
            </div>
            <div class="col-sm-4">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/d_user_ak/'.$id.'">
                  <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-search"></i> Detail</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
    return $data;
  }
}

if(!function_exists('lihat_user_prodi'))
{
  function lihat_user_prodi($nama_user, $id, $gbr)
  {
    $data = '
    <div class="col-md-4">
      <div class="box box-widget widget-user">
        <div class="widget-user-header bg-aqua-active">
          <h3 class="widget-user-username">'.$nama_user.'</h3>
          <h5 class="widget-user-desc">Prodi</h5>
        </div>
        <div class="widget-user-image">
          <img class="img-circle" src="'.base_url().'assets/img/img_user/'.$gbr.'" alt="User Avatar">
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-4 border-right">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/v_u_user/'.$id.'?name='.base64_encode(base64_encode("User Prodi")).'"><button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i> Edit</button></a>
              </div>
            </div>
            <div class="col-sm-4 border-right">
            </div>
            <div class="col-sm-4">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/d_user_pr/'.$id.'">
                  <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-search"></i> Detail</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
    return $data;
  }
}

if(!function_exists('lihat_user_pendamping'))
{
  function lihat_user_pendamping($nama_pendamping, $id, $gbr)
  {
    $data = '
    <div class="col-md-4">
      <div class="box box-widget widget-user">
        <div class="widget-user-header bg-green-active">
          <h3 class="widget-user-username">'.$nama_pendamping.'</h3>
          <h5 class="widget-user-desc">Pendamping</h5>
        </div>
        <div class="widget-user-image">
          <img class="img-circle" src="'.base_url().'assets/img/img_user/'.$gbr.'" alt="User Avatar">
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-4 border-right">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/v_u_user/'.$id.'?name='.base64_encode(base64_encode("User Pendamping")).'"><button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i> Edit</button></a>
              </div>
            </div>
            <div class="col-sm-4 border-right">
              
            </div>
            <div class="col-sm-4">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/d_user_pdm/'.$id.'">
                  <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-search"></i> Detail</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
    return $data;
  }
}

if(!function_exists('lihat_user_mahasiswa'))
{
  function lihat_user_mahasiswa($nama_mhs, $id, $gbr)
  {
    $data = '
    <div class="col-md-4">
      <div class="box box-widget widget-user">
        <div class="widget-user-header bg-red-active">
          <h3 class="widget-user-username">'.$nama_mhs.'</h3>
          <h5 class="widget-user-desc">Mahasiswa</h5>
        </div>
        <div class="widget-user-image">
          <img class="img-circle" src="'.base_url().'assets/img/img_user/'.$gbr.'" alt="User Avatar">
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-4 border-right">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/v_u_user/'.$id.'?name='.base64_encode(base64_encode("User Mahasiswa")).'"><button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i> Edit</button></a>
              </div>
            </div>
            <div class="col-sm-4 border-right">
              
            </div>
            <div class="col-sm-4">
              <div class="description-block">
                <a href="'.base_url().'s_admin/User/d_user_mhs/'.$id.'">
                  <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-search"></i> Detail</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
    return $data;
  }
}

if(!function_exists('jmlh_mhs'))
{
  function jmlh_mhs($table, $where, $id)
  {
    $CI=& get_instance();
    $id_mhs = array($where => $id);
    $data = $CI->model_all->get_where($table, $id_mhs)->num_rows();
    return $data;
  }
}

if(!function_exists('jmlh_mhs_jk'))
{
  function jmlh_mhs_jk($table, $where, $id, $jk, $sex)
  {
    $CI=& get_instance();
    $id_mhs = array($where => $id, $jk => $sex);
    $data = $CI->model_all->get_where($table, $id_mhs)->num_rows();
    return $data;
  }
}

if(!function_exists('prodi'))
{
  function prodi($id)
  {
     $CI=& get_instance();
    $id_prodi = array('id_prodi' => $id);
    $data = $CI->model_all->get_where('prodi', $id_prodi);
    foreach ($data->result() as $dt) {
      $nama_prodi = $dt->nama_prodi;
    }
    if(empty($nama_prodi))
    {
      echo "Prodi Belum Diisi";
    } else 
    {
      return $nama_prodi;
    }
  }
}

if(!function_exists('lihat_kompi'))
{
  function lihat_kompi($id)
  {
    $CI=& get_instance();
    $id_kompi = array('id_kompi' => $id);
    $data = $CI->model_all->get_where('kompi', $id_kompi);
    foreach ($data->result() as $dt) {
      $nama_kompi = $dt->nama_kompi;
    }
    return $nama_kompi;
  }
}

if(!function_exists('lihat_pleton'))
{
  function lihat_pleton($id)
  {
    $CI=& get_instance();
    $id_pleton = array('id_pleton' => $id);
    $data = $CI->model_all->get_where('pleton', $id_pleton);
    foreach ($data->result() as $dt) {
      $nama_pleton = $dt->nama_pleton;
    }
    return $nama_pleton;
  }
}


if(!function_exists('detail_mhs'))
{
  function detail_mhs($nama, $nim, $gbr)
  {
    $data = '<li>
            <img src="'.base_url().'assets/img/img_user/'.$gbr.'" alt="User Image" width="120">
            <a class="users-list-name" href="'.base_url().'s_admin/Mhs/d_mhs_3/'.$nim.'">'.$nama.'</a>
            <span class="users-list-date">'.base64_decode(base64_decode($nim)).'</span>
          </li>';
    return $data;
  }
}