<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
            <?= $judul; ?>
          </h2>
      </div>
      <div class="body">
        <?php
          $info = $this->session->flashdata('info');
          if (!empty($info)) {
            echo $info;
          }
        ?>
        <a href="<?= base_url(); ?>guru/Materi/t_materi">
            <button name="button" class="btn btn-md btn-primary"> <span>Tambah Materi</span> <i class="material-icons">add_circle</i> </button>
        </a>
        <br>
        <br>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
            <thead>
              <tr>
                <th width="10">No</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th>Kelas</th>
                <th>Mata Pelajaran</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php  
                $no = 1;
                foreach ($data->result_object() as $dt) {
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?php echo $dt->judul ?></td>
                <td><?php echo $dt->nama_jurusan ?></td>
                <td><?php echo $dt->nama_kelas ?></td>
                <td><?php echo $dt->nama_mapel ?></td>


                <td align="center">
                  <a href="<?= base_url(); ?>guru/Materi/detail_materi/<?= $dt->id_materi; ?>">
                    <button type="button" name="button" class="btn btn-circle btn-info waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Detail Materi">
                      <i class="material-icons">search</i>
                    </button>
                  </a>
                  <a href="<?= base_url(); ?>guru/Materi/u_materi/<?= $dt->id_materi; ?>">
                    <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit Materi">
                      <i class="material-icons">edit</i>
                    </button>
                  </a>
                  <a href="<?= base_url(); ?>guru/Materi/delete_materi/<?= $dt->id_materi; ?>">
                    <button type="button" name="button" onclick="return confirm('Yakin Ingin Menghapus Materi...???')" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus Materi">
                      <i class="material-icons">delete</i>
                    </button>
                  </a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#materi').addClass('active');
  });
</script>
