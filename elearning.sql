-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 09, 2018 at 08:13 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_absen`
--

CREATE TABLE `tb_absen` (
  `id_absen` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `id_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_user` int(11) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` enum('Islam','Kristen','Katholik','Budha','Hindu','Kong Hu Chu') NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_user`, `nama_admin`, `jk`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `foto`) VALUES
(11, 'Atfalstyle', '', '-', '0000-00-00', '', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru`
--

CREATE TABLE `tb_guru` (
  `id` int(11) NOT NULL,
  `nig` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `bidang_studi` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` enum('Islam','Kristen','Budha','Katholik','Hindu','Kong Hu Chu') NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_guru`
--

INSERT INTO `tb_guru` (`id`, `nig`, `id_user`, `nama_guru`, `jk`, `bidang_studi`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `foto`) VALUES
(1, 'asdasdasds', 12, 'asdasdasds', '', '-', '-', '0000-00-00', '', '-', '-'),
(3, '', 25, '', 'L', '', '', '0000-00-00', 'Islam', 'OK', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'IPA'),
(4, 'IPS');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `nama_kelas`) VALUES
(1, '10'),
(2, '11'),
(3, '12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mapel`
--

INSERT INTO `tb_mapel` (`id_mapel`, `nama_mapel`) VALUES
(3, 'Bahasa Indonesia'),
(5, 'BHS INGGRIS'),
(6, 'Matematika');

-- --------------------------------------------------------

--
-- Table structure for table `tb_materi_tugas`
--

CREATE TABLE `tb_materi_tugas` (
  `id_materi` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `isi` text NOT NULL,
  `tgl` date NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_materi_tugas`
--

INSERT INTO `tb_materi_tugas` (`id_materi`, `jurusan_id`, `kelas_id`, `guru_id`, `isi`, `tgl`, `mapel_id`, `judul`) VALUES
(2, 1, 1, 25, '<h1>TOLONG DIKEJAKAN SEBAIK MUNGKIN!!!!</h1>\r\n', '2018-12-09', 0, NULL),
(3, 1, 1, 25, '<p>OK</p>\r\n', '2018-12-09', 3, 'Modul Materi DIubah');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai`
--

CREATE TABLE `tb_nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `benar` varchar(10) NOT NULL,
  `salah` varchar(10) NOT NULL,
  `point` float NOT NULL,
  `soal` varchar(100) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `id_modul` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nilai`
--

INSERT INTO `tb_nilai` (`id_nilai`, `id_siswa`, `id_kelas`, `id_jurusan`, `tgl`, `kode_soal`, `benar`, `salah`, `point`, `soal`, `jenis_soal`, `id_modul`) VALUES
(1, 30, 1, 1, '2018-12-08', 1544279293, '1', '1', 50, '2', 'ESSAI', 2),
(2, 31, 1, 1, '2018-12-08', 53397, '1', '1', 50, '2', 'ESSAI', 2),
(3, 30, 1, 1, '2018-12-08', 81597, '3', '0', 100, '3', 'PILIHAN GANDA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id_pengumuman`, `tgl`, `judul`, `isi`) VALUES
(1, '2018-11-07', 'Penting Nemen Cuyasdasdasd2222', '                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n'),
(4, '2018-11-07', 'asdasd', 'asdasdasd'),
(5, '2018-12-06', 'PENGUMUMAN', 'Dimohon untuk semua Petugas');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id` int(11) NOT NULL,
  `nis` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `agama` enum('Islam','Kristen','Katholik','Hindu','Budha','Kong Hu Chu') NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL,
  `id_kelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`id`, `nis`, `id_user`, `nama_siswa`, `jk`, `jurusan`, `agama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `foto`, `id_kelas`) VALUES
(1, 'dopalstyle', 16, 'dopalstyle', '-', '-', '', '-', '0000-00-00', '-', '-', 0),
(2, '123', 22, 'BAGUS', 'L', 'IPS', 'Islam', 'PATI', '2018-12-03', 'REMBANG', 'TES', 0),
(3, '1281728', 26, 'Baba', 'L', '1', 'Islam', 'Rembag', '2018-12-07', 'Rembang', 'TES', 1),
(4, '9127812', 30, 'Suharto', 'L', '1', 'Islam', 'Rembang', '2000-12-12', 'Rembang', 'Screenshot_from_2018-12-08_17-11-425.png', 1),
(5, '01291291299', 31, 'YONO', 'L', '1', 'Islam', 'PATI', '2018-12-05', 'Semarang', 'TES', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ulangan_eesay`
--

CREATE TABLE `tb_ulangan_eesay` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ulangan_eesay`
--

INSERT INTO `tb_ulangan_eesay` (`id_ulangan`, `kode_soal`, `mapel_id`, `kelas_id`, `jurusan_id`, `guru_id`, `jenis_soal`, `soal`, `jawaban`, `tgl`) VALUES
(1, 123, 3, 1, 1, 12, 'eesay', 'jkk', 'k', '2018-11-26'),
(2, 123, 3, 1, 1, 12, 'eesay', 'kb', 'b', '2018-11-26'),
(3, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(4, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(5, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(6, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(7, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(8, 123, 3, 1, 1, 12, 'eesay', 'n', 'nn', '2018-11-26'),
(9, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(10, 123, 3, 1, 1, 12, 'eesay', 'nn', 'n', '2018-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ulangan_pil`
--

CREATE TABLE `tb_ulangan_pil` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `option_a` varchar(100) NOT NULL,
  `option_b` varchar(100) NOT NULL,
  `option_c` varchar(100) NOT NULL,
  `option_d` varchar(100) NOT NULL,
  `option_e` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pass1` text NOT NULL,
  `pass2` text NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `pass1`, `pass2`, `level`) VALUES
(11, 'atfalstyle', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 1),
(12, 'gopalstyle', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(16, 'dopalstyle', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(19, 'ahmad', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(20, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(21, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(22, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(23, 'Rembang', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(24, 'guru', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(25, 'bambang', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(26, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(27, 'vava', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(28, 'vava', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(30, 'suharto', '$2y$09$TuVxQKXpObdjSiTkY9yDtOV3O3T6vYj4/50SIW33vAqzdmEsaovbu', 'suhadi', 3),
(31, 'yono', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_uts_eesay`
--

CREATE TABLE `tb_uts_eesay` (
  `id_uts` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_uts_eesay`
--

INSERT INTO `tb_uts_eesay` (`id_uts`, `kode_soal`, `mapel_id`, `jurusan_id`, `kelas_id`, `guru_id`, `jenis_soal`, `soal`, `jawaban`, `tgl`) VALUES
(1, 123123123, 3, 1, 1, 12, 'eesay', 'asdas', 'asdad', '2018-11-26'),
(2, 123123123, 3, 1, 1, 12, 'eesay', 'asdasdad', 'asdasd', '2018-11-26'),
(3, 123123123, 3, 1, 1, 12, 'eesay', 'asdad', 'asasdasd', '2018-11-26'),
(4, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(5, 123123123, 3, 1, 1, 12, 'eesay', 'asdadasd', 'asdad', '2018-11-26'),
(6, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdas', '2018-11-26'),
(7, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'dasdasd', '2018-11-26'),
(8, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(9, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(10, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasda', '2018-11-26'),
(11, 0, 3, 1, 1, 12, 'eesay', 'asdad', 'kjj', '2018-11-26'),
(12, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(13, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(14, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(15, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(16, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(17, 0, 3, 1, 1, 12, 'eesay', 'jj', 'j', '2018-11-26'),
(18, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(19, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(20, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(21, 132, 3, 4, 1, 12, 'eesay', 'asdad', 'jh', '2018-11-26'),
(22, 132, 3, 4, 1, 12, 'eesay', 'hjh', 'jh', '2018-11-26'),
(23, 132, 3, 4, 1, 12, 'eesay', 'jh', 'h', '2018-11-26'),
(24, 132, 3, 4, 1, 12, 'eesay', 'hh', 'h', '2018-11-26'),
(25, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(26, 132, 3, 4, 1, 12, 'eesay', 'h', 'h', '2018-11-26'),
(27, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(28, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(29, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(30, 132, 3, 4, 1, 12, 'eesay', 'h', 'h', '2018-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_uts_pil`
--

CREATE TABLE `tb_uts_pil` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `option_a` varchar(100) NOT NULL,
  `option_b` varchar(100) NOT NULL,
  `option_c` varchar(100) NOT NULL,
  `option_d` varchar(100) NOT NULL,
  `option_e` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jawaban`
--

CREATE TABLE `t_jawaban` (
  `id` int(11) NOT NULL,
  `uid` varchar(11) NOT NULL,
  `jawaban` text,
  `id_soal` int(11) NOT NULL,
  `benar` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jawaban`
--

INSERT INTO `t_jawaban` (`id`, `uid`, `jawaban`, `id_soal`, `benar`, `created`) VALUES
(1, '1544152326', 'Jakarta', 4, 1, '2018-12-07 03:12:06'),
(2, '95560', 'Semarang', 4, 0, '2018-12-07 03:12:06'),
(3, '98303', 'Denpasar', 4, 0, '2018-12-07 03:12:06'),
(4, '4836', 'Balikpapan', 4, 0, '2018-12-07 03:12:06'),
(5, '64223', 'Bangkok', 5, 1, '2018-12-07 04:40:07'),
(6, '35442', 'Makassar', 5, 0, '2018-12-07 04:40:08'),
(7, '84543', 'Singapura', 5, 0, '2018-12-07 04:40:08'),
(8, '16391', 'Kuala Lumpur', 5, 0, '2018-12-07 04:40:08'),
(9, '82647', '0', 6, 1, '2018-12-07 04:40:55'),
(10, '93421', '1', 6, 0, '2018-12-07 04:40:56'),
(11, '19167', '2', 6, 0, '2018-12-07 04:40:56'),
(12, '15569', '3', 6, 0, '2018-12-07 04:40:56'),
(13, '50042', '2', 7, 1, '2018-12-07 06:44:30'),
(14, '48912', '3', 7, 0, '2018-12-07 06:44:30'),
(15, '94434', '4', 7, 0, '2018-12-07 06:44:30'),
(16, '25437', '6', 7, 0, '2018-12-07 06:44:30'),
(17, '43882', '4', 8, 1, '2018-12-07 06:44:30'),
(18, '43101', '5', 8, 0, '2018-12-07 06:44:30'),
(19, '83861', '6', 8, 0, '2018-12-07 06:44:30'),
(20, '90002', '7', 8, 0, '2018-12-07 06:44:31'),
(21, '98431', '11', 9, 1, '2018-12-07 06:44:31'),
(22, '22152', '10', 9, 0, '2018-12-07 06:44:31'),
(23, '15465', '12', 9, 0, '2018-12-07 06:44:31'),
(24, '10870', '15', 9, 0, '2018-12-07 06:44:31'),
(25, '25671', '2', 10, 1, '2018-12-07 06:47:13'),
(26, '45336', '3', 10, 0, '2018-12-07 06:47:13'),
(27, '49667', '4', 10, 0, '2018-12-07 06:47:13'),
(28, '12329', '6', 10, 0, '2018-12-07 06:47:13'),
(29, '79440', '12', 11, 1, '2018-12-07 06:47:36'),
(30, '32565', '13', 11, 0, '2018-12-07 06:47:36'),
(31, '24505', '15', 11, 0, '2018-12-07 06:47:36'),
(32, '24829', '20', 11, 0, '2018-12-07 06:47:36'),
(33, '79552', '2', 12, 1, '2018-12-07 08:20:14'),
(34, '86529', '3', 12, 0, '2018-12-07 08:20:15'),
(35, '93990', '1', 12, 0, '2018-12-07 08:20:15'),
(36, '10367', '5', 12, 0, '2018-12-07 08:20:15'),
(37, '69862', '11', 13, 1, '2018-12-07 08:20:15'),
(38, '18211', '2', 13, 0, '2018-12-07 08:20:15'),
(39, '81468', '21', 13, 0, '2018-12-07 08:20:15'),
(40, '52709', '1', 13, 0, '2018-12-07 08:20:15'),
(41, '19142', '1', 14, 1, '2018-12-07 08:20:15'),
(42, '37582', '2', 14, 0, '2018-12-07 08:20:15'),
(43, '30485', '4', 14, 0, '2018-12-07 08:20:15'),
(44, '39680', '5', 14, 0, '2018-12-07 08:20:16'),
(45, '61531', '10', 15, 1, '2018-12-07 08:20:49'),
(46, '44314', '12', 15, 0, '2018-12-07 08:20:49'),
(47, '36979', '12', 15, 0, '2018-12-07 08:20:50'),
(48, '51952', '31', 15, 0, '2018-12-07 08:20:50'),
(49, '48826', '800', 16, 1, '2018-12-07 08:20:50'),
(50, '88275', '1210', 16, 0, '2018-12-07 08:20:50'),
(51, '94899', '120', 16, 0, '2018-12-07 08:20:50'),
(52, '9672', '900', 16, 0, '2018-12-07 08:20:50'),
(53, '65600', '2', 17, 1, '2018-12-08 00:55:39'),
(54, '50670', '1', 17, 0, '2018-12-08 00:55:39'),
(55, '56551', '3', 17, 0, '2018-12-08 00:55:39'),
(56, '30746', '6', 17, 0, '2018-12-08 00:55:39'),
(57, '84076', '7', 18, 1, '2018-12-08 00:55:39'),
(58, '28145', '12', 18, 0, '2018-12-08 00:55:39'),
(59, '88497', '10', 18, 0, '2018-12-08 00:55:39'),
(60, '58053', '9', 18, 0, '2018-12-08 00:55:39'),
(61, '24776', '99', 19, 1, '2018-12-08 00:55:40'),
(62, '49721', '91', 19, 0, '2018-12-08 00:55:40'),
(63, '74279', '93', 19, 0, '2018-12-08 00:55:40'),
(64, '22234', '29', 19, 0, '2018-12-08 00:55:40'),
(65, '88333', '110', 20, 1, '2018-12-08 00:55:40'),
(66, '74963', '20', 20, 0, '2018-12-08 00:55:40'),
(67, '9820', '30', 20, 0, '2018-12-08 00:55:40'),
(68, '24211', '10', 20, 0, '2018-12-08 00:55:40'),
(69, '22365', 'Paris', 21, 1, '2018-12-08 04:39:07'),
(70, '91894', 'Berlin', 22, 1, '2018-12-08 04:39:07'),
(71, '92377', 'Jakarta', 23, 1, '2018-12-08 04:39:08');

-- --------------------------------------------------------

--
-- Table structure for table `t_jawaban_user`
--

CREATE TABLE `t_jawaban_user` (
  `id` int(11) NOT NULL,
  `id_soal_user` int(11) NOT NULL,
  `id_jawaban` varchar(50) NOT NULL,
  `jawaban_essai` text,
  `dipilih` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jawaban_user`
--

INSERT INTO `t_jawaban_user` (`id`, `id_soal_user`, `id_jawaban`, `jawaban_essai`, `dipilih`, `created`) VALUES
(1, 1, '22365', 'Paris', 0, '2018-12-08 14:28:13'),
(2, 2, '91894', 'Berlin', 0, '2018-12-08 14:28:13'),
(3, 3, '91894', 'Berlin', 0, '2018-12-08 14:29:56'),
(4, 4, '92377', 'London', 0, '2018-12-08 14:29:57'),
(5, 5, '24776', NULL, 1, '2018-12-08 15:35:35'),
(6, 5, '22234', NULL, 0, '2018-12-08 15:35:35'),
(7, 5, '74279', NULL, 0, '2018-12-08 15:35:35'),
(8, 5, '49721', NULL, 0, '2018-12-08 15:35:35'),
(9, 6, '28145', NULL, 0, '2018-12-08 15:35:36'),
(10, 6, '84076', NULL, 1, '2018-12-08 15:35:36'),
(11, 6, '88497', NULL, 0, '2018-12-08 15:35:36'),
(12, 6, '58053', NULL, 0, '2018-12-08 15:35:36'),
(13, 7, '74963', NULL, 0, '2018-12-08 15:35:36'),
(14, 7, '24211', NULL, 0, '2018-12-08 15:35:36'),
(15, 7, '9820', NULL, 0, '2018-12-08 15:35:36'),
(16, 7, '88333', NULL, 1, '2018-12-08 15:35:36');

-- --------------------------------------------------------

--
-- Table structure for table `t_livestream`
--

CREATE TABLE `t_livestream` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `url` text,
  `id_guru` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_livestream`
--

INSERT INTO `t_livestream` (`id`, `nama`, `keterangan`, `created`, `url`, `id_guru`) VALUES
(5, 'as', 'a', NULL, 's', 0),
(6, 'pembahasan 1', 'ok', NULL, 'https://www.youtube.com/watch?v=coE3BF8vsEc\\', 25);

-- --------------------------------------------------------

--
-- Table structure for table `t_modul_soal`
--

CREATE TABLE `t_modul_soal` (
  `id` int(11) NOT NULL,
  `kode_soal` varchar(50) DEFAULT NULL,
  `jurusan` int(11) NOT NULL DEFAULT '0',
  `kelas` int(11) NOT NULL DEFAULT '0',
  `nama_modul` varchar(100) DEFAULT NULL,
  `max_nilai` int(11) NOT NULL DEFAULT '0',
  `id_mapel` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `min_nilai` int(11) NOT NULL DEFAULT '0',
  `jumlah_jawaban` int(11) NOT NULL,
  `waktu` int(11) NOT NULL DEFAULT '0',
  `status_aktif` int(11) NOT NULL DEFAULT '0',
  `jumlah_soal` int(11) NOT NULL,
  `keterangan` text,
  `password` varchar(50) DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `essai` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_modul_soal`
--

INSERT INTO `t_modul_soal` (`id`, `kode_soal`, `jurusan`, `kelas`, `nama_modul`, `max_nilai`, `id_mapel`, `created`, `min_nilai`, `jumlah_jawaban`, `waktu`, `status_aktif`, `jumlah_soal`, `keterangan`, `password`, `jenis`, `essai`) VALUES
(1, 'A0123', 1, 1, 'Matematika kelas 10', 100, 6, '2018-12-08 00:52:44', 75, 4, 10, 0, 3, NULL, 'password', 'UTS', 0),
(2, '01201', 1, 1, 'ESSAI MATEMATIKA', 100, 6, '2018-12-08 03:02:24', 75, 1, 100, 0, 2, NULL, 'password', 'ULANGAN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_soal_ganda`
--

CREATE TABLE `t_soal_ganda` (
  `id` int(11) NOT NULL,
  `id_modul` int(11) NOT NULL,
  `soal` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_soal_ganda`
--

INSERT INTO `t_soal_ganda` (`id`, `id_modul`, `soal`, `created`) VALUES
(1, 4, 'Ibukota Indonesai?', '2018-12-07 03:11:08'),
(2, 4, 'Ibukota Indonesai?', '2018-12-07 03:11:13'),
(3, 4, 'Ibukota Indonesai?', '2018-12-07 03:11:20'),
(4, 4, 'Ibukota Indonesai?', '2018-12-07 03:12:05'),
(5, 4, 'Ibukota Thailand?', '2018-12-07 04:40:06'),
(6, 4, '1 X 0 +0 =...', '2018-12-07 04:40:55'),
(7, 4, '1+1=...', '2018-12-07 06:44:30'),
(8, 4, '2+2=', '2018-12-07 06:44:30'),
(9, 4, '10+1=', '2018-12-07 06:44:31'),
(10, 6, '1+1', '2018-12-07 06:47:12'),
(11, 6, '2+10=....', '2018-12-07 06:47:35'),
(12, 6, '1+1=....', '2018-12-07 08:20:14'),
(13, 6, '10+1', '2018-12-07 08:20:15'),
(14, 6, '20', '2018-12-07 08:20:15'),
(15, 6, '10 X 1', '2018-12-07 08:20:49'),
(16, 6, '900-100', '2018-12-07 08:20:50'),
(17, 1, '1+1=.', '2018-12-08 00:55:39'),
(18, 1, '6+1=....', '2018-12-08 00:55:39'),
(19, 1, '100-1', '2018-12-08 00:55:40'),
(20, 1, '100+10=', '2018-12-08 00:55:40'),
(21, 2, 'Ibukota Prancis Adalah', '2018-12-08 04:39:07'),
(22, 2, 'Ibukota Jerman', '2018-12-08 04:39:07'),
(23, 2, 'Ibukota Indonesia', '2018-12-08 04:39:08');

-- --------------------------------------------------------

--
-- Table structure for table `t_soal_user`
--

CREATE TABLE `t_soal_user` (
  `id` int(11) NOT NULL,
  `id_modul` int(11) NOT NULL,
  `soal_id` varchar(40) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  `benar` int(11) NOT NULL DEFAULT '0',
  `dijawab` int(11) NOT NULL DEFAULT '0',
  `status_pengerjaan` int(11) NOT NULL DEFAULT '0',
  `is_click` int(11) NOT NULL DEFAULT '0',
  `stoped_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_soal_user`
--

INSERT INTO `t_soal_user` (`id`, `id_modul`, `soal_id`, `id_soal`, `created`, `id_user`, `benar`, `dijawab`, `status_pengerjaan`, `is_click`, `stoped_at`) VALUES
(1, 2, '1544279293', 21, '2018-12-08 14:28:13', 30, 1, 1, 2, 1, NULL),
(2, 2, '1544279293', 22, '2018-12-08 14:28:13', 30, 0, 1, 2, 1, NULL),
(3, 2, '53397', 22, '2018-12-08 14:29:56', 31, 1, 1, 2, 1, NULL),
(4, 2, '53397', 23, '2018-12-08 14:29:56', 31, 0, 1, 2, 1, NULL),
(5, 1, '81597', 19, '2018-12-08 15:35:35', 30, 1, 1, 2, 0, NULL),
(6, 1, '81597', 18, '2018-12-08 15:35:35', 30, 1, 1, 2, 0, NULL),
(7, 1, '81597', 20, '2018-12-08 15:35:36', 30, 1, 1, 2, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_absen`
--
ALTER TABLE `tb_absen`
  ADD PRIMARY KEY (`id_absen`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD KEY `tb_admin_ibfk_1` (`id_user`);

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  ADD PRIMARY KEY (`id_materi`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_jurusan` (`jurusan_id`),
  ADD KEY `id_kelas` (`kelas_id`);

--
-- Indexes for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_siswa` (`id_siswa`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_jurusan` (`id_jurusan`),
  ADD KEY `tb_nilai_ibfk_2` (`kode_soal`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `guru_id` (`guru_id`),
  ADD KEY `jurusan_id` (`jurusan_id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `mapel_id` (`mapel_id`);

--
-- Indexes for table `tb_ulangan_pil`
--
ALTER TABLE `tb_ulangan_pil`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_kelas` (`kelas_id`),
  ADD KEY `id_mapel` (`mapel_id`),
  ADD KEY `id_jurusan` (`jurusan_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  ADD PRIMARY KEY (`id_uts`),
  ADD KEY `guru_id` (`guru_id`),
  ADD KEY `jurusan_id` (`jurusan_id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `mapel_id` (`mapel_id`);

--
-- Indexes for table `tb_uts_pil`
--
ALTER TABLE `tb_uts_pil`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_kelas` (`kelas_id`),
  ADD KEY `id_mapel` (`mapel_id`),
  ADD KEY `id_jurusan` (`jurusan_id`);

--
-- Indexes for table `t_jawaban`
--
ALTER TABLE `t_jawaban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jawaban_user`
--
ALTER TABLE `t_jawaban_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_livestream`
--
ALTER TABLE `t_livestream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_modul_soal`
--
ALTER TABLE `t_modul_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_soal_ganda`
--
ALTER TABLE `t_soal_ganda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_soal_user`
--
ALTER TABLE `t_soal_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_absen`
--
ALTER TABLE `tb_absen`
  MODIFY `id_absen` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_guru`
--
ALTER TABLE `tb_guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  MODIFY `id_materi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_ulangan_pil`
--
ALTER TABLE `tb_ulangan_pil`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  MODIFY `id_uts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tb_uts_pil`
--
ALTER TABLE `tb_uts_pil`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_jawaban`
--
ALTER TABLE `t_jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `t_jawaban_user`
--
ALTER TABLE `t_jawaban_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `t_livestream`
--
ALTER TABLE `t_livestream`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_modul_soal`
--
ALTER TABLE `t_modul_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_soal_ganda`
--
ALTER TABLE `t_soal_ganda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `t_soal_user`
--
ALTER TABLE `t_soal_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_absen`
--
ALTER TABLE `tb_absen`
  ADD CONSTRAINT `tb_absen_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `tb_siswa` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD CONSTRAINT `tb_admin_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD CONSTRAINT `tb_guru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  ADD CONSTRAINT `tb_materi_tugas_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_materi_tugas_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_materi_tugas_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD CONSTRAINT `tb_nilai_ibfk_3` FOREIGN KEY (`id_siswa`) REFERENCES `tb_siswa` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_4` FOREIGN KEY (`id_kelas`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_5` FOREIGN KEY (`id_jurusan`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD CONSTRAINT `tb_siswa_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_4` FOREIGN KEY (`mapel_id`) REFERENCES `tb_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  ADD CONSTRAINT `tb_uts_eesay_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_4` FOREIGN KEY (`mapel_id`) REFERENCES `tb_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
