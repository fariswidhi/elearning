<?php
//============================================================+
// File name   : example_002.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 002 for TCPDF class
//               Removing Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Removing Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
// require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('E-Learning');
$pdf->SetTitle('Laporan Data Jurusan');
$pdf->SetSubject('Data Jurusan');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 12);

// add a page
$pdf->AddPage();

// set some text to print
$heading =
    '<h4>
    	LAPORAN DATA JURUSAN
    </h4>';


$pdf->writeHTMLCell(0, 0, '', '', $heading, 0, 1, 0, true, 'C', true);
$pdf->Ln(5);
$table = '<style type="text/css">
        .tabel { border-collapse:collapse; border:1px solid black; }
        .tabel th { padding:8px 5px; background-color:#ccc;  color:#000;  border:1px solid black; }
        .tabel td { padding:3px;  border:1px solid black; }
        </style>';
$table .= '<table class="tabel" cellspacing="0" cellpadding="5">';
$table .= '<tr>
            <th align="center" width="5%">No</th>
            <th align="center" width="95%">Nama Jurusan</th>
        </tr>';
        $no = 1;
    foreach($data as $row):
$table .= '<tr>
            <td align="center">'.$no++.'</td>
            <td>'.$row->nama_jurusan.'</td>
        </tr>';
    endforeach;
$table .= '</table>';
$pdf->writeHTMLCell(0, 0, '', '', $table, 0, 1, 0, true, '', true);


// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('Laporan_Data_Jurusan.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
