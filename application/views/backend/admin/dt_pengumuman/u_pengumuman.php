<!-- Input Group -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Update Pengumuman
                </h2>

            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <form action="<?= base_url(); ?>admin/Pengumuman/update_pengumuman" method="post">

                            <label for="judul">Nama Pengumuman</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">local_offer</i>
                                </span>
                                <div class="form-line">
                                    <input type="hidden" name="id_pengumuman" value="<?= $id_pengumuman; ?>">
                                    <input type="text" class="form-control date" required="required" name="judul" placeholder="Judul Pengumunan" value="<?= $jdl; ?>">
                                </div>
                            </div>

                            <label for="isi">Isi Pengumuman</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">feedback</i>
                                </span>
                                <div class="form-line">
                                    <textarea name="isi" class="form-control date" required="required" rows="6" placeholder="Isi Pengumuman"><?= $isi; ?></textarea>
                                </div>
                            </div>

                            <div class="input-group">
                                <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                                <button type="submit" name="button" class="btn btn-success waves-effect waves-light pull-right"><span>Update Pengumuman</span> <i class="material-icons">update</i> </button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#pengumuman').addClass('active');
  });
</script>
<!-- #END# Input Group -->
