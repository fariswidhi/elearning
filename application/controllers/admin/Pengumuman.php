<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->model_security->getsecurity();
    $isi['data']    = $this->model_all->get_all('tb_pengumuman')->result();
    $isi['judul']   = "Pengumuman";
    $isi['content'] = "backend/admin/dt_pengumuman/v_pengumuman";
    $this->load->view("backend/admin/home", $isi);
  }

  public function detail_pengumuman()
  {
    $this->model_security->getsecurity();
    $where = array('id_pengumuman' => $this->uri->segment(4));
    $data = $this->model_all->get_where('tb_pengumuman', $where)->result_array();

    $judul   = "Pengumuman / Update Pengumuman";
    $content = "backend/admin/dt_pengumuman/detail_pengumuman";



    // print_r($data);


    $this->load->view("backend/admin/home", compact('data','judul','content'));
  }

  public function t_pengumuman()
  {
    $this->model_security->getsecurity();
    $isi['judul']   = "Pengumuman / Tambah Pengumuman";
    $isi['content'] = "backend/admin/dt_pengumuman/t_pengumuman";
    $this->load->view("backend/admin/home", $isi);
  }

  public function insert_pengumuman()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('judul', 'Judul', 'required',
      array('required' => 'Judul Pengumuman Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('isi', 'Isi', 'required',
      array('required' => 'Isi Pengumuman Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {
      return $this->t_pengumuman();
    } else {
      $data = array(
        'tgl'   => date('Y-m-d'),
        'judul' => $this->input->post('judul'),
        'isi'   => $this->input->post('isi')
      );
      $this->model_all->insert('tb_pengumuman', $data);
      alert_success('Pengumuman Berhasil Ditambahkan');
      return redirect('admin/Pengumuman');
    }
  }

  public function u_pengumuman()
  {
    $this->model_security->getsecurity();
    $where = array('id_pengumuman' => $this->uri->segment(4));
    $data = $this->model_all->get_where('tb_pengumuman', $where)->result();
    foreach ($data as $dt) {
      $isi['id_pengumuman']   = $dt->id_pengumuman;
      $isi['tgl']   = $dt->tgl;
      $isi['jdl'] = $dt->judul;
      $isi['isi']   = $dt->isi;
    }
    $isi['judul']   = "Pengumuman / Update Pengumuman";
    $isi['content'] = "backend/admin/dt_pengumuman/u_pengumuman";
    $this->load->view("backend/admin/home", $isi);
  }

  public function update_pengumuman()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('judul', 'Judul', 'required',
      array('required' => 'Judul Pengumunan Tidak Boleh Kosong')
    );
    $this->form_validation->set_rules('isi', 'Isi', 'required',
      array('required' => 'Isi Pengumuman Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {
      return $this->u_pengumuman();
    } else {
      $where = array('id_pengumuman' => $this->input->post('id_pengumuman'));
      $data = array(
        'tgl'   => date('Y-m-d'),
        'judul' => $this->input->post('judul'),
        'isi'   => $this->input->post('isi')
      );
      $this->model_all->update('tb_pengumuman', $data, $where);
      alert_success('Pengumunan Berhasil Diupdate');
      return redirect('admin/Pengumuman');
    }
  }

  public function delete_pengumuman()
  {
    $this->model_security->getsecurity();
    $where = array('id_pengumuman' => $this->uri->segment(4));
    $this->model_all->del_id('tb_pengumuman', $where);
    alert_danger('Pengumuman Berhasil Dihapus');
    return redirect('admin/Pengumuman');
  }

}
