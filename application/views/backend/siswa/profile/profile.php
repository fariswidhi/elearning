<div class="row clearfix">
                <div class="col-xs-12 col-sm-3">
                    <div class="card profile-card">
                        <div class="profile-header">&nbsp;</div>
                        <div class="profile-body">
                            <div class="image-area">
                              <center>
                                    <img style="width: 100px;height: 100px;" src="<?php echo base_url($siswa->result()[0]->foto)  ?>" alt="AdminBSB - Profile Image">
                              </center>
                            </div>
                            <div class="content-area">
                              <center>                                <h3><?php echo $siswa->result()[0]->nama_siswa ?></h3>
                                <p>Siswa</p>

                                </center>

                            </div>
                        </div>
                    </div>

                    
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="card">
                        <div class="body">

                <?php
                    $info = $this->session->flashdata('info');
                    if (!empty($info)) {
                      echo $info;
                    }
                ?>
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Profil</a></li>
                                    <li role="presentation" class=""><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">Data Nilai</a></li>
                                    <li role="presentation" class=""><a href="#change_password_settings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">Ubah Profil</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="home">
<table class="table table-striped">
                                        <tr>
                                          <td>Nama</td><td><?php echo $siswa->result()[0]->nama_siswa ?></td>
                                        </tr>
                                        <tr>
                                          <td>Tanggal Lahir</td><td><?php echo date('d-m-Y',strtotime($siswa->result()[0]->tgl_lahir)) ?></td>
                                        </tr>
                                        <tr>
                                          <td>Tempat Lahir</td><td><?php echo $siswa->result()[0]->tempat_lahir ?></td>
                                        </tr>
                                        <tr>
                                          <td>NIS</td><td><?php print_r($siswa->result()[0]->nis); ?></td>
                                        </tr>
                                        <tr>
                                          <td>Agama</td><td><?php print_r($siswa->result()[0]->agama); ?></td>
                                        </tr>
                                        <tr>
                                          <td>Kelas</td><td><?php echo $siswa->result()[0]->nama_kelas ?></td>
                                        </tr>
                                        <tr>
                                          <td>Jurusan</td><td><?php print_r($siswa->result()[0]->nama_jurusan); ?></td>
                                        </tr>
                                      </table>

                                       
                                                                           </div>
                                    <div role="tabpanel" class="tab-pane fade" id="profile_settings">
                                        
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">

                                       <thead>
                                         <th>NO</th>
                                         <th>Nama Modul</th>
                                         <th>Nilai</th>
                                         <th>Aksi</th>
                                       </thead>
                                       <tbody>
                                         <?php if ($dataNilai->num_rows()>0):
                                          $n =1;
                                          ?>
                                           <?php foreach ($dataNilai->result() as $dn): 
                                            // print_r($dn);
                                              ?>
                                             <tr>
                                               <td><?php echo $n++ ?></td>
                                               <td><?php echo $dn->nama_modul ?></td>
                                               <td><?php echo $dn->point ?></td>
                                               <td><a href="<?php echo base_url('siswa/modul/data/'.$dn->kdsoal) ?>" class="btn btn-info">INFO</a></td>
                                             </tr>
                                           <?php endforeach ?>
                                         <?php endif ?>
                                       </tbody>
                                     </table>

                                    </div>

<!--                                     <?php 
                                    print_r($this->session->userdata());
                                     ?> -->
                                    <div role="tabpanel" class="tab-pane fade" id="change_password_settings">
                                        <form class="form-horizontal" action="<?php echo base_url("siswa/Profile/update/".$this->session->userdata('id')) ?>" enctype="multipart/form-data" method="post">

                                            <div class="form-group">
                                                <label for="NewPassword" class="col-sm-3 control-label">Ubah Foto</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="file" class="form-control" id="NewPassword"  placeholder="New Password" name="berkas">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NewPassword" class="col-sm-3 control-label">Username</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="NewPassword" name="username" placeholder="New Password" disabled value="<?php echo $siswa->result()[0]->username ?>">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="NewPasswordConfirm" class="col-sm-3 control-label">Password</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="password" class="form-control" id="NewPasswordConfirm" name="password" placeholder="Ketik untuk mengubah password " >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="submit" class="btn btn-success">UBAH</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#home').addClass('active');
  });
</script>
