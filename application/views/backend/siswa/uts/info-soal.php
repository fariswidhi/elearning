<?php if ($data->num_rows()>0): ?>
	
<table class="table table-striped">
	<tr>
		<td>Nama Modul</td><td><?php echo $data->result()[0]->nama_modul ?></td>
	</tr>
	<tr>
		<td>Min Nilai</td><td><?php echo $data->result()[0]->min_nilai ?></td>
	</tr>
	<tr>
		<td>Max Nilai</td><td><?php echo $data->result()[0]->max_nilai ?></td>
	</tr>
	<tr>
		<td>Jumlah Soal</td><td><?php echo $data->result()[0]->jumlah_soal ?></td>
	</tr>
	<tr>
		<td>Waktu</td><td><?php echo $data->result()[0]->waktu ?> Menit</td>
	</tr>
	<tr>
		<td>Mata Pelajaran</td><td><label class="badge bg-cyan"><?php echo $data->result()[0]->nama_mapel ?></label></td>
	</tr>
	
</table>

<div class="form-line" style="width: 100%;">
	<center>
		<div class="alert alert-success">
			Masukan Password Modul Untuk Melanjutkan
		</div>
	</center>
	<input type="password" name="" class="form-control" id="pss">
	<input type="hidden" name="" value="<?php echo $id ?>" id="id">
</div>

<?php else: ?>
	<center>Data Tidak Ditemukan</center>
<?php endif ?>