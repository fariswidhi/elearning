<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data Siswa
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>Foto</th>
                                <th>Nomor Induk</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Jurusan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                              $no = 1;
                              foreach ($data as $dt) {
                          ?>
                          <tr>
                              <td><?= $no++; ?></td>
                              <td align="center">
                                <div class="image">
                                  <?php
                                    $foto = $dt->foto;
                                    if ($foto == '-') {
                                      echo '<img src="'.base_url().'assets/back_end/images/user.png" width="48" height="48" alt="User" />';
                                    } else {
                                      echo '<img src="'.base_url().'assets/back_end/img_user/guru/'.$dt->foto.'" width="48" height="48" alt="User" />';
                                    }
                                  ?>
                                </div>
                              </td>
                              <td><?= $dt->nis ?></td>
                              <td><?= $dt->nama_siswa; ?></td>
                              <td>
                                  <?php
                                    $jk = $dt->jk;
                                    if ($jk == 'L') {
                                      echo "Laki - Laki";
                                    } else if ($jk == 'P') {
                                      echo "Perempuan";
                                    } else {
                                      echo "-";
                                    }
                                  ?>
                              </td>
                              <td><?= $dt->jurusan; ?></td>
                              <td align="center">
                                  <a href="<?= base_url(); ?>admin/Guru/detail_guru/<?= $dt->id_user; ?>">
                                    <button type="button" name="button" class="btn btn-circle btn-info waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Detail Guru">
                                      <i class="material-icons">search</i>
                                    </button>
                                  </a>
                                  <a href="<?= base_url(); ?>admin/Guru/u_guru/<?= $dt->id_user; ?>">
                                    <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit Guru">
                                      <i class="material-icons">edit</i>
                                    </button>
                                  </a>
                              </td>
                          </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#siswa').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
