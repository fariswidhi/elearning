<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data <?php echo $judul ?>
                </h2>
            </div>
            <div class="body">
               <?php
                $info = $this->session->flashdata('info');
                if (!empty($info)) {
                  echo $info;
                }
            ?>
                                      <a type="button" name="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Tambah Data" href="<?php echo base_url(uri_string()); ?>/create">
                                        <i class="material-icons">add</i> Tambah Data
                                      </a>


                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>


<?php 
    foreach ($forms as $f) {
      # code...

      // print_r($f);

      foreach ($f as $x) {
        if (!empty($x['showAsTable'])) {
          # code...
          if ($x['showAsTable']) {
            # code...
            ?>
            <th><?php echo $x['name'] ?></th>
            <?php
          }
        }
  }


}


        ?>
        <td>Opsi</td>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                                foreach ($data as $dt) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                              <?php 
    foreach ($forms as $f) {
      # code...

      // print_r($f);

      foreach ($f as $x) {
        if (!empty($x['showAsTable'])) {
          # code...
          if ($x['showAsTable']) {
            # code...
            $obj = $x['list']['field'];
            ?>
            <td><?php echo $dt->$obj ?></td>
            <?php
          }
        }
  }


}


        ?>

  <td align="center">
                                    <a href="<?= base_url(); ?>guru/<?php echo $this->uri->segment(2) ?>/detail/<?= $dt->id; ?>">
                                      <button type="button" name="button" class="btn btn-circle btn-info waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Detail ">
                                        <i class="material-icons">search</i>
                                      </button>
                                    </a>
                            

                      <a href="<?= base_url(); ?>guru/<?php echo $this->uri->segment(2) ?>/delete/<?= $dt->id; ?>" onclick="confirm('okay?')">
                                      <button type="button" name="button" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit ">
                                        <i class="material-icons">delete</i>
                                      </button>
                                    </a>

                                </td>


                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#guru').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
