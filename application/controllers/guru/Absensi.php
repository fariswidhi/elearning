<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {



  protected $table="t_modul_soal";
  protected $folder = "backend/guru/dt_soal/";
  protected $primaryKey= "id";
  protected $base = "Soal";

  function __construct()
  {
    parent::__construct();
    $this->load->model('model_select');
  }

  // Untuk View Uts
  public function index()
  {

    $this->model_security->getsecurity();

    $tanggal = $this->input->get('tanggal');


    if (!empty($tanggal)) {
      # code...
     $sql = "SELECT a.id_kelas,a.nama_kelas,count(*) as total,SUM(CASE WHEN c.id_absen is null then 0 else 1 end) as absen from tb_kelas a inner join tb_siswa b on a.id_kelas = b.id_kelas LEFT join tb_absen c on b.id_user = c.id_siswa  WHERE date(c.tgl)='".$tanggal."' OR c.tgl is null GROUP by b.id_kelas ";

    }
    else{
      $tanggal = date('Y-m-d');
     $sql = "SELECT a.id_kelas,a.nama_kelas,count(*) as total,SUM(CASE WHEN c.id_absen is null then 0 else 1 end) as absen from tb_kelas a inner join tb_siswa b on a.id_kelas = b.id_kelas LEFT join tb_absen c on b.id_user = c.id_siswa WHERE date(c.tgl)='".$tanggal."' OR c.tgl is null GROUP by b.id_kelas ";

    }


     $data = $this->db->query($sql);
     $content = "backend/guru/absensi/index";

     $judul = "Data Absensi";



    $this->load->view("backend/guru/home", compact('judul','content','forms','data'));

  }

  public function detail(){
    $kelas = $this->input->get('kelas');
     $content = "backend/guru/absensi/detail";

     $kelass = $this->db->get_where('tb_kelas',['id_kelas'=>$kelas]);

     if ($kelass->num_rows()==0) {
       # code...
      echo "Tidak Ada Data";
      die;
     }
     $judul = "Data Absensi Kelas ".$kelass->result()[0]->nama_kelas;

     // print_r($kelass->result());


     $from = $this->input->get('from');

     if (!empty($from)) {
      $tgl = $from;
     $sql = "SELECT * from tb_siswa a LEFT join tb_absen b on a.id_user =b.id_siswa WHERE a.id_kelas='".$kelas."' AND (date(b.tgl)='$tgl'  OR b.tgl is null)";
     }
     else{
      $tgl = date('Y-m-d');
     $sql = "SELECT * from tb_siswa a LEFT join tb_absen b on a.id_user =b.id_siswa WHERE a.id_kelas='".$kelas."' AND (date(b.tgl)='$tgl'  OR b.tgl is null)";

     }

     $dt = $this->db->query($sql);


    $this->load->view("backend/guru/home", compact('judul','content','forms','data','dt'));

  }

}
