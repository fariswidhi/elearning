<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
                  <form action="<?php echo base_url(uri_string()); ?>" method="post" enctype="multipart/form-data">

        <div class="row clearfix">


<?php 
    foreach ($forms as $f) {
      # code...
      ?>
<div class="col-md-6">
      <?php
      // print_r($f);
      foreach ($f as $x) {
        # code...


?>

 <label for="nig"><?php echo $x['name'] ?></label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person</i>
                  </span>

<?php 
$obj = $x['list']['field'];
 ?>
                    <?php if ($x['list']['type']=="select"): ?>
                       <select class="form-control show-tick" data-live-search="true" name="<?php echo $x['list']['field'] ?>">
                      <option value="">-- Pilih <?php echo $x['name'] ?> --</option>
                      <?php foreach ($x['list']['data'] as $o ): ?>
                        <option <?php echo $o['value']==$data->$obj ?'selected':'' ?> value="<?php echo $o['value'] ?>"><?php echo $o['text'] ?></option>
                      <?php endforeach ?>
                  </select>
                    <?php elseif($x['list']['type']=="textarea"): ?>

                      <textarea name="<?php echo $x['list']['field'] ?>" class="form-control" rows="6" required="required" placeholder="Alamat"><?php echo $data->$obj ?></textarea>

                      <?php else: ?>
                  <div class="form-line">
                      <input value="<?php echo $data->$obj ?>" type="<?php echo $x['list']['type'] ?>" class="form-control date"  placeholder="<?php echo $x['name'] ?>" name="<?php echo $x['list']['field'] ?>">
              </div>
                    <?php endif ?>
                  </div>

<?php
      }
      ?>
</div>

<?php

    }



 ?>



            <div class="col-md-12">
              <div class="input-group">
                  <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                  <button type="submit" name="button" class="btn btn-success waves-effect waves-light"><span>Ubah </span> <i class="material-icons">update</i> </button>
              </div>
            </div>
 
        </div>
    </form>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#lembar_soal').hide();

    $('#tmb_soal').click(function() {
      $('#lembar_soal').slideDown();
      $('#muat_soal').fadeOut();
    });
    
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_eesay').addClass('active');
  });
</script>
