<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}

	public function getLogin()
	{
		$user = $this->input->post('user');
		$pass = $this->input->post('pass');

		if (isset($user)) {
			if ($user == NULL) {
					alert_danger('Username Tidak Boleh Kosong');
					redirect('Login');
			}
			elseif ($pass == NULL) {
					alert_danger('Password Tidak Boleh Kosong');
					redirect('Login');
			}
			else {
				$this->model_login->getlogin($user,$pass);
			}
		}
	}

	public function logout()
	{
			$this->session->sess_destroy();
			redirect('Login');
	}
}
