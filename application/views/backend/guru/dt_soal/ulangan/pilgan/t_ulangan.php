<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="row clearfix">
          <form action="<?= base_url(); ?>guru/Soal/insert_ulangan_pilgan" method="post">
            <div class="col-md-6">

              <input type="hidden" name="guru" value="<?= $this->session->userdata('id') ?>">

              <label for="kode_soal">Kode Soal</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">local_offer</i>
                  </span>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required" name="kode_soal" placeholder="Kode Soal">
                  </div>
                  <b class="text-danger"><?= form_error('kode_soal'); ?></b>
              </div>

              <label for="mapel">Mapel</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="mapel" data-live-search="true">
                      <option value="">-- Pilih Mapel --</option>
                      <?php
                        foreach ($data_3 as $dt) {
                          echo "<option value='".$dt->id_mapel."'>".$dt->nama_mapel."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('mapel'); ?></b>
              </div>

            </div>

            <div class="col-md-6">

              <label for="jurusan">Jurusan</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="jurusan" data-live-search="true">
                      <option value="">-- Pilih Jurusan --</option>
                      <?php
                        foreach ($data as $dt) {
                          echo "<option value='".$dt->id_jurusan."'>".$dt->nama_jurusan."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('jurusan'); ?></b>
              </div>

              <label for="jurusan">Kelas</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="kelas" data-live-search="true">
                      <option value="">-- Pilih Kelas --</option>
                      <?php
                        foreach ($data_2 as $dt) {
                          echo "<option value='".$dt->id_kelas."'>".$dt->nama_kelas."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('kelas'); ?></b>
              </div>

            </div>
            <div class="col-md-12" id="muat_soal">
              <button type="button" class="btn btn-lg btn-primary waves-effect" id="tmb_soal">Muat Soal</button>
            </div>
            <div id="lembar_soal" class="col-md-12">
              <?php for ($i=1; $i <= 10; $i++) { ?>
              <label for="soal">Soal No <?= $i; ?></label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">feedback</i>
                </span>
                <div class="form-line">
                    <input type="text" required="required" class="form-control date" name="soal[]" placeholder="Masukan Soal">
                </div>
                <b class="text-danger"><?= form_error('soal'); ?></b>
              </div>

              <label for="jwbn">Jawaban</label>
              <div class="input-group">
                  <div class="col-md-2">
                    <span class="input-group-">A</span>
                    <div class="form-line">
                        <input type="text" class="form-control date" required="required" name="option_a[]" placeholder="Jawaban">
                    </div>
                  </div>

                  <div class="col-md-2">
                    <span class="input-group-">B</span>
                    <div class="form-line">
                        <input type="text" class="form-control date" required="required" name="option_b[]" placeholder="Jawaban">
                    </div>
                  </div>

                  <div class="col-md-2">
                    <span class="input-group-">C</span>
                    <div class="form-line">
                        <input type="text" class="form-control date" required="required" name="option_c[]" placeholder="Jawaban">
                    </div>
                  </div>

                  <div class="col-md-2">
                    <span class="input-group-">D</span>
                    <div class="form-line">
                        <input type="text" class="form-control date" required="required" name="option_d[]" placeholder="Jawaban">
                    </div>
                  </div>

                  <div class="col-md-2">
                    <span class="input-group-">E</span>
                    <div class="form-line">
                        <input type="text" class="form-control date" required="required" name="option_e[]" placeholder="Jawaban">
                    </div>
                  </div>

                  <div class="col-md-2">
                    <span class="input-group-">Kunci</span>
                    <div class="form-line">
                        <input type="text" class="form-control date" required="required" name="kunci[]" placeholder="Jawaban">
                    </div>
                  </div>
              </div>
              <?php } ?>
            </div>
            <div class="col-md-12">
              <div class="input-group">
                  <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                  <button type="submit" name="button" class="btn btn-primary waves-effect waves-light pull-right"><span>Tambah Soal Uts</span> <i class="material-icons">send</i> </button>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#lembar_soal').hide();

    $('#tmb_soal').click(function() {
      $('#lembar_soal').slideDown();
      $('#muat_soal').fadeOut();
    });
    
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_pilgan').addClass('active');
  });
</script>
