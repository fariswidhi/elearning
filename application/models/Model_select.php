<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_select extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function mapel($id)
	{
		$mapel = "<option value=''>- Pilih Mapel -</option>";

		$this->db->order_by('nama_mapel','ASC');
		$map = $this->db->get_where('tb_mapel', array('id_jurusan' => $id));

		foreach ($map->result_array() as $data):
			$mapel .= "<option value='$data[id_mapel]'>$data[nama_mapel]</option>";
		endforeach;

		return $mapel;
	}
}
