<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <?php 
    $userid = $this->session->userdata('id');
    $data = $this->db->get_where('tb_guru',['id_user'=>$userid]);
     ?>
    <div class="user-info">
        <div class="image">
            <img src="<?php echo base_url($data->result()[0]->foto) ?>" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->session->userdata('nama_guru'); ?></div>
            <div class="email">Guru</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="<?= base_url(); ?>guru/Profile"><i class="material-icons">person</i>Profile</a></li>
                    <li role="seperator" class="divider"></li>
                    <li><a href="<?= base_url(); ?>Login/logout"><i class="material-icons">input</i>Log Out</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li id="home">
                <a href="<?= base_url(); ?>Home">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li id="soal">
                <a href="#" class="menu-toggle">
                    <i class="material-icons">assessment</i>
                    <span>Soal</span>
                </a>
                <ul class="ml-menu">
                    <li id="soal_uts"><a href="#" class="menu-toggle">UTS</a>
                      <ul class="ml-menu">
                          <li id="soal_ulangan_pilgan"><a href="<?= base_url(); ?>guru/Soal/?soal=UTS">Pilihan Ganda</a></li>
                          <li id="soal_ulangan_eesay"><a href="<?= base_url(); ?>guru/Essai/?soal=UTS">Essai</a></li>
                      </ul>
                    </li>
                    <li id="soal_ulangan"><a href="#" class="menu-toggle">Ulangan Harian</a>
                      <ul class="ml-menu ">
                          <li id="soal_ulangan_pilgan"><a href="<?= base_url(); ?>guru/Soal/?soal=ULANGAN">Pilgan</a></li>
                          <li id="soal_ulangan_eesay"><a href="<?= base_url(); ?>guru/Essai/?soal=ULANGAN">Eesay</a></li>
                      </ul>
                    </li>
                </ul>
            </li>
            <li id="materi">
                <a href="<?= base_url(); ?>guru/Materi">
                    <i class="material-icons">assistant_photo</i>
                    <span>Materi / Tugas</span>
                </a>
            </li>


            <li id="streaming">
                <a href="<?= base_url(); ?>guru/Streaming">
                    <i class="material-icons">assistant_photo</i>
                    <span>Live Streaming</span>
                </a>
            </li>


            <li id="streaming">
                <a href="<?= base_url(); ?>guru/Absensi">
                    <i class="material-icons">assistant_photo</i>
                    <span>Absensi Siswa</span>
                </a>
            </li>
            <!-- <li id="jawaban">
                <a href="#" class="menu-toggle">
                    <i class="material-icons">bookmark</i>
                    <span>Jawaban</span>
                </a>
                <ul class="ml-menu">
                    <li id="jawaban_uts"><a href="<?= base_url(); ?>guru/Jawaban">UTS</a></li>
                    <li id="jawaban_ulangan"><a href="<?= base_url(); ?>guru/Jawaban/ulangan">Ulangan Harian</a></li>
                </ul>
            </li> -->
          <!--   <li id="nilai">
                <a href="#" class="menu-toggle">
                    <i class="material-icons">assignment</i>
                    <span>Nilai</span>
                </a>
                <ul class="ml-menu">
                    <li id="nilai_uts"><a href="#" class="menu-toggle">UTS</a>
                      <ul class="ml-menu">
                          <li id="nilai_uts_pilgan"><a href="<?= base_url(); ?>guru/Nilai">Pilgan</a></li>
                          <li id="nilai_uts_eesay"><a href="<?= base_url(); ?>guru/Nilai/uts_eesay">Eesay</a></li>
                      </ul>
                    </li>
                    <li id="nilai_ulangan"><a href="#" class="menu-toggle">Ulangan Harian</a>
                      <ul class="ml-menu">
                          <li id="nilai_ulangan_pilgan"><a href="<?= base_url(); ?>guru/Nilai/ulangan_pilgan">Pilgan</a></li>
                          <li id="nilai_ulangan_eesay"><a href="<?= base_url(); ?>guru/Nilai/ulangan_eesay">Eesay</a></li>
                      </ul>
                    </li>
                </ul>
            </li> -->
        </ul>
    </div>
    <!-- #Menu -->
    <?php $this->load->view('backend/layout/copyright'); ?>
</aside>
<!-- #END# Left Sidebar -->
