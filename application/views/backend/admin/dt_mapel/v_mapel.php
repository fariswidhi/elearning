<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data Mata Pelajaran
                </h2>
            </div>
            <div class="body">
                <?php
                  $info = $this->session->flashdata('info');
                  if (!empty($info)) {
                    echo $info;
                  }
                ?>
                <a href="<?= base_url(); ?>admin/Mapel/t_mapel">
                    <button type="button" class="btn btn-primary waves-effect" name="button"> <span>Tambah Mapel</span> <i class="material-icons">add_circle</i> </button>
                </a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>nama Mapel</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                              $no = 1;
                              foreach ($data as $dt) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $dt->nama_mapel; ?></td>
                                <td align="center">
                                    <a href="<?= base_url(); ?>admin/Mapel/detail_mapel/<?= $dt->id_jurusan; ?>">
                                      <button type="button" name="button" class="btn btn-circle btn-info waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Lihat Mapel">
                                        <i class="material-icons">search</i>
                                      </button>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_mapel').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
