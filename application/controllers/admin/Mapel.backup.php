<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->model_security->getsecurity();
    $isi['data']    = $this->model_all->get_all('tb_mapel')->result();
    $isi['judul']   = "Mata Pelajaran";
    $isi['content'] = "backend/admin/dt_mapel/v_mapel";
    $this->load->view("backend/admin/home", $isi);
  }

  public function t_mapel()
  {
    $this->model_security->getsecurity();
    $isi['data']    = $this->model_all->get_all('tb_jurusan')->result();
    $isi['judul']   = "Mata Pelajaran / Tambah Mata Pelajaran";
    $isi['content'] = "backend/admin/dt_mapel/t_mapel";
    $this->load->view("backend/admin/home", $isi);
  }

  public function insert_mapel()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('mapel', 'Mapel', 'required',
      array('required' => 'Mata Pelajaran Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {
      return $this->t_mapel();
    } else {

      $data = array(
        'nama_mapel' => $this->input->post('mapel')
      );
      $this->model_all->insert('tb_mapel', $data);
      alert_success('Mata Pelajaran Berhasil Ditambahkan');
      return redirect('admin/Mapel');
    }
  }

  public function u_mapel()
  {
    $this->model_security->getsecurity();
    $isi['data']    = $this->model_all->get_all('tb_jurusan')->result();
    $id    = $this->uri->segment(4);
    $where = array('id_mapel' => $id);
    $data  = $this->model_all->get_where('tb_mapel', $where)->result();
    foreach ($data as $dt) {
      $isi['id_mapel']    = $dt->id_mapel;
      $isi['nama_mapel']  = $dt->nama_mapel;
    }
    $isi['judul']   = "Mata Pelajaran / Update Mata Pelajaran";
    $isi['content'] = "backend/admin/dt_mapel/u_mapel";
    $this->load->view("backend/admin/home", $isi);
  }

  public function update_mapel()
  {
    $this->model_security->getsecurity();

    $this->form_validation->set_rules('mapel', 'Mapel', 'required',
      array('required' => 'Mapel Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {

      return $this->u_mapel();
    } else {
      $where = array('id_mapel' => $this->input->post('id_mapel'));
      $data  = array(
        'nama_mapel'  => $this->input->post('mapel')
      );
      $this->model_all->update('tb_mapel', $data, $where);
      alert_success('Mata Pelajaran Berhasil Diupdate');
      return redirect('admin/Mapel');
    }
  }

  public function delete_mapel()
  {
    $this->model_security->getsecurity();
    $id = $this->uri->segment(4);
    $where = array('id_mapel' => $id);
    $this->model_all->del_id('tb_mapel', $where);
    alert_danger('Mapel Berhasil Dihapus');
    return redirect('admin/Mapel');
  }

}
