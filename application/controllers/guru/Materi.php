<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
    	$this->model_security->getsecurity();
		$isi['judul']	= 'Materi / Tugas';
		$isi['content'] = 'backend/guru/dt_materi/v_materi';
		// $isi['data']	= $this->model_all->get_all('tb_materi_tugas')->result();

		// $isi['data']
		$data = $this->db->query("SELECT * FROM tb_materi_tugas a inner join tb_jurusan b on a.jurusan_id = b.id_jurusan inner join tb_mapel c on a.mapel_id = c.id_mapel inner join tb_kelas  d on a.kelas_id = d.id_kelas");
		$isi['data'] = $data;
		// print_r($data->result());
		$this->load->view('backend/guru/home', $isi);
	}

	public function t_materi()
	{
    	$this->model_security->getsecurity();
		$isi['judul']	= 'Tambah Materi / Tugas';
		$isi['content'] = 'backend/guru/dt_materi/t_materi';
		$isi['data']	= $this->model_all->get_all('tb_jurusan')->result();
		$isi['data_2']	= $this->model_all->get_all('tb_kelas')->result();
		$isi['mp'] = $this->model_all->get_all('tb_mapel')->result();

		$this->load->view('backend/guru/home', $isi);
	}

	public function insert_materi()
	{
    	$this->model_security->getsecurity();

	    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
	      array('required' => 'Jurusan Tidak Boleh Kosong')
	    );
	    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
	      array('required' => 'Kelas Tidak Boleh Kosong')
	    );

	    if ($this->form_validation->run() == FALSE) {
	    	return $this->t_materi();
	    } else {




	    	$location = './gambar/materi/';
		$config['upload_path']          = $location;
		$config['allowed_types']        = '*';

 
		 $this->upload->initialize($config);

 
		if ( ! $this->upload->do_upload('file')){
			// $error = array('error' => $this->upload->display_errors());

			// print_r($this->upload->display_errors());
			echo "Gagal Upload";
			// $this->load->view('v_upload', $error);
		}else{
			$data = $this->upload->data();
			// $this->load->view('v_upload_sukses', $data);
			$file_name = 'gambar/materi/'.$data['file_name'];
	    	$data = array(
	    		'jurusan_id' => $this->input->post('jurusan'),
	    		'kelas_id'	 => $this->input->post('kelas'),
	    		'judul'=>$this->input->post('judul'),
	    		'mapel_id'=>$this->input->post('mapel_id'),
	    		'guru_id'	 => $this->input->post('guru'),
	    		'tgl'		 => date('Y-m-d'),
	    		'file'=>$file_name
	    	);

			$this->model_all->insert('tb_materi_tugas', $data);
	    	alert_success('Materi / Tugas Berhasil Ditambahkan');
	    	return redirect('guru/Materi');


		}


    
    // return "default.jpg";




	    }
	}

	public function u_materi()
	{
    	$this->model_security->getsecurity();
		$id = array('id_materi' => $this->uri->segment(4));
		$isi['judul']		= 'Update Materi';
		$isi['content'] = 'backend/guru/dt_materi/u_materi';
		$isi['data']	= $this->model_all->get_all('tb_jurusan')->result();
		$isi['data_2']	= $this->model_all->get_all('tb_kelas')->result();
		$data = $this->model_all->get_where('tb_materi_tugas',$id)->result();
		foreach ($data as $dt) {
			$isi['id_materi'] = $dt->id_materi;
			$isi['jurusan'] = $dt->jurusan_id;
			$isi['kelas'] 	= $dt->kelas_id;
			$isi['isi'] 	= $dt->isi;
		}

		$isi['mp'] = $this->model_all->get_all('tb_mapel')->result();
		$isi['info'] = $this->db->get_where('tb_materi_tugas',$id)->result()[0];
		$this->load->view('backend/guru/home', $isi);
	}

	public function update_materi()
	{
    	$this->model_security->getsecurity();

	    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
	      array('required' => 'Jurusan Tidak Boleh Kosong')
	    );
	    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
	      array('required' => 'Kelas Tidak Boleh Kosong')
	    );

	    if ($this->form_validation->run() == FALSE) {
	    	return $this->t_materi();
	    } else {
	    	$id_materi = $this->input->post('id_materi');
	     	



	    $location = './gambar/materi/';
		$config['upload_path']          = $location;
		$config['allowed_types']        = '*';

 
		 $this->upload->initialize($config);

		 $where = array(
		 	'id_materi'=>$this->input->post('id_materi')
		 );
 
		if ( ! $this->upload->do_upload('file')){
			// $error = array('error' => $this->upload->display_errors());

			// print_r($this->upload->display_errors());


	    	$data = array(
	    		'jurusan_id' => $this->input->post('jurusan'),
	    		'kelas_id'	 => $this->input->post('kelas'),
	    		'judul'=>$this->input->post('judul'),
	    		'mapel_id'=>$this->input->post('mapel_id'),
	    		'guru_id'	 => $this->input->post('guru'),
	    		'tgl'		 => date('Y-m-d'),
	    	);

	    	$this->model_all->update('tb_materi_tugas', $data, $where);

			// $this->load->view('v_upload', $error);
		}else{
			$data = $this->upload->data();
			// $this->load->view('v_upload_sukses', $data);
			$file_name = 'gambar/materi/'.$data['file_name'];
	    	$data = array(
	    		'jurusan_id' => $this->input->post('jurusan'),
	    		'kelas_id'	 => $this->input->post('kelas'),
	    		'judul'=>$this->input->post('judul'),
	    		'mapel_id'=>$this->input->post('mapel_id'),
	    		'guru_id'	 => $this->input->post('guru'),
	    		'tgl'		 => date('Y-m-d'),
	    		'file'=>$file_name
	    	);

	    	$this->model_all->update('tb_materi_tugas', $data, $where);



		}


    



	    	alert_success('Materi / Tugas Berhasil Diupdate');
	    	return redirect('guru/Materi');
	    }
	}

	public function detail_materi()
	{
    	$this->model_security->getsecurity();
		$id =  $this->uri->segment(4);
		$judul		= 'Detail Materi';
		$content = 'backend/guru/dt_materi/d_materi';

		$data = $this->db->query("SELECT * from tb_materi_tugas a inner join tb_jurusan b on a.jurusan_id = b.id_jurusan inner join tb_kelas c on a.kelas_id = c.id_kelas inner join tb_user u on a.guru_id = u.id_user inner join tb_guru v on v.id_user = u.id_user WHERE a.id_materi='$id'");
		if ($data->num_rows()>0) {
			# code...
			$dt = $data->result();


		$this->load->view('backend/guru/home', compact('content','judul','dt'));
		}
		else{
			echo "<h1>HALAMAN TIDAK DITEMUKAN</h1>";
		}
	}

	public function delete_materi()
	{
    	$this->model_security->getsecurity();
		$id_materi = $this->uri->segment(4);
	    $where    = array('id_materi'  => $id_materi);
	    $this->model_all->del_id('tb_materi_tugas', $where);
	    alert_danger('Materi / Tugas Berhasil Dihapus');
	    return redirect('guru/Materi');
	}

}
