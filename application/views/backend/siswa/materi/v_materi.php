<div class="row clearfix">
  <?php if ($materi->num_rows()>0): ?>
    <?php foreach ($materi->result() as $m): ?>
      
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    <div class="card">
      <div class="body">

          <h2>
            <?php print_r($m->judul) ?>


          </h2>

          <a class="btn btn-primary" href="<?php echo base_url($m->file) ?>">Download</a>
      </div>
    </div>
  </div>
    <?php endforeach ?>
  <?php endif ?>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#materi').addClass('active');
  });
</script>
