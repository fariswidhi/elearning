<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ujian extends CI_Controller {

	public function index()
	{
		$this->model_security->getsecurity();
    	$isi['judul']   = "Ulangan";
    	$isi['content'] = "backend/siswa/ulangan/v_ulangan";
	    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');

    	// print_r($this->session->userdata());


		$modul = $this->session->userdata('modul');

		$dtModul = $this->db->query("SELECT * FROM t_modul_soal WHERE id='$modul'");

		// print_r($dtModul->result()[0]);

		
    	$this->load->view("backend/siswa/ujian",compact('dtModul'));

	}

	public function list_soal(){
		$modul = $this->session->userdata('modul');
		$userid = $this->session->userdata('id');

		// $data = $this->db->get_where('t_soal_user',['id_modul'=>$modul,'id_user'=>$userid]);
		$data = $this->db->query("SELECT a.id,a.dijawab FROM t_soal_user a inner JOIN t_soal_ganda b on a.id_soal = b.id WHERE a.id_modul='".$modul."' and a.id_user='".$userid."' AND a.status_pengerjaan='1'");
		// print_r($data->result());	
		echo json_encode($data->result());
		// print_r($this->session->userdata());
		// print_r($modul);
	}

	public function jawaban($id){
		if ($this->input->is_ajax_request()) {
		$data = $this->db->query("SELECT *,a.id as idx FROM t_jawaban_user a inner JOIN t_jawaban b on a.id_jawaban = b.uid WHERE a.id_soal_user='$id' ORDER BY a.id desc");

		$soal  = $this->db->query("SELECT b.soal,c.essai FROM t_soal_user a INNER join t_soal_ganda b on a.id_soal = b.id inner join t_modul_soal c on a.id_modul = c.id WHERE a.id ='$id' AND a.status_pengerjaan='1'");

		// echo "SELECT b.soal FROM t_soal_user a INNER join t_soal_ganda b on a.id_soal = b.id WHERE a.id ='$id' AND a.status_pengerjaan='1'";

		// print_r($soal->result());
		$this->load->view('backend/siswa/ujian/jawaban',compact('data','soal','id'));
		}
	}

	public function jwbn(){
		if ($this->input->is_ajax_request()) {
			$idx = $this->input->post('idx');
			$soal = $this->input->post('soal');
			
			$data1 =[
				'id_soal_user'=>$soal
			];

			$this->db->update('t_jawaban_user',['dipilih'=>'0'],$data1);

			$data = [
				'id_jawaban'=>$idx,
				'id_soal_user'=>$soal
			];



		$jawaban = $this->db->get_where('t_jawaban',['uid'=>$idx])->result();
	
		// print_r($jawaban);
		$benar = $jawaban[0]->benar;

		$this->db->update('t_jawaban_user',['dipilih'=>'1'],$data);

		$this->db->update('t_soal_user',['dijawab'=>'1','benar'=>$benar],['id'=>$soal]);

			// print_r($data);
		}
	}

	 public function time(){
        $modul = $this->session->userdata('modul');
        $user = $this->session->userdata('id');

        $data= $this->db->get_where('t_soal_user',['id_modul'=>$modul,'id_user'=>$user,'status_pengerjaan'=>'1']);


        $lama = $this->db->get_where('t_modul_soal',['id'=>$modul]);

			// print_r($lama->result())        ;

        $waktu = $lama->result()[0]->waktu * 60;
        $tglMulai =$data->result()[0]->created;

        $str = strtotime($tglMulai);
        $result = $waktu + $str;
        // echo $result;


        $now = strtotime(date('Y-m-d H:i:s'));

        echo json_encode(['waktu'=>		 date('Y/m/d H:i:s',$result),'timeout'=>($now> $result ? 'true':'false')]);

        if ($now>$result) {



        $data = $this->db->get_where('t_soal_user',['status_pengerjaan'=>1]);

        $this->session->set_flashdata('selesai', 'Terima Kasih Atas Partisipasi Anda :)');


        // print_r($this->session->userdata());
        $userid = $this->session->userdata('id');

        $siswa = $this->db->get_where('tb_siswa',['id_user'=>$userid]);

        // print_r($siswa);

        if ($siswa->num_rows()>0) {
        	# code...

        	$max_nilai = $this->db->query("SELECT * from t_soal_user a inner join t_modul_soal b on a.id_modul = b.id WHERE a.id_modul='".$modul."'");
        	$max_nilai = $max_nilai->result()[0]->max_nilai;

        	$dtSiswa = $siswa->result();

        	$siswaid= $dtSiswa[0]->id;
        	// print_r($dtSiswa);
        	$jurusan = $dtSiswa[0]->jurusan;
        	$kelas = $dtSiswa[0]->id_kelas;

        	// print_r($siswaid);
        	// print_r($dtSiswa);



        	$total_soal = $data->num_rows();
        	$nilai_per_soal = $max_nilai/$total_soal;


        	$benar = 0;
        	$salah = 0;
        	$jml = 0;
        	$poin = 0;
        	foreach ($data->result() as $d) {
        		# code...
        		if ($d->benar==1) {
        			# code...

        			$poin += 1* $nilai_per_soal;
        			$benar += 1;
        		}
        		if ($d->benar==0) {
        			$salah +=1;
        			$poin +=0 ;
        		}

        		$jml += 1;
        	}


        }

        if ($lama->result()[0]->essai==0) {
        $this->db->insert('tb_nilai',['id_siswa'=>$userid,'id_jurusan'=>$jurusan,'id_kelas'=>$kelas,'tgl'=>date('Y-m-d'),'kode_soal'=>$data->result()[0]->soal_id,'jenis_soal'=>'PILIHAN GANDA','benar'=>$benar,'soal'=>$jml,'salah'=>$salah,'point'=>$poin,'id_modul'=>$modul]);


        }


        	$this->db->update('t_soal_user',['status_pengerjaan'=>2,'stoped_at'=>date('Y-m-d H:i:s')],['id_modul'=>$modul,'id_user'=>$user]);

        }
        

        // echo $user;
    }


    public function berhenti(){

        $modul = $this->session->userdata('modul');
        $user = $this->session->userdata('id');

        $data = $this->db->get_where('t_soal_user',['status_pengerjaan'=>1]);


        $lama = $this->db->get_where('t_modul_soal',['id'=>$modul]);

        $this->session->set_flashdata('selesai', 'Terima Kasih Atas Partisipasi Anda :)');

        // print_r($this->session->userdata());
        $userid = $this->session->userdata('id');

        $siswa = $this->db->get_where('tb_siswa',['id_user'=>$userid]);

        // print_r($siswa);
        if ($siswa->num_rows()>0) {
        	# code...

        	$max_nilai = $this->db->query("SELECT * from t_soal_user a inner join t_modul_soal b on a.id_modul = b.id WHERE a.id_modul='".$modul."'");
        	$max_nilai = $max_nilai->result()[0]->max_nilai;

        	$dtSiswa = $siswa->result();

        	$siswaid= $dtSiswa[0]->id;
        	// print_r($dtSiswa);
        	$jurusan = $dtSiswa[0]->jurusan;
        	$kelas = $dtSiswa[0]->id_kelas;

        	// print_r($siswaid);
        	// print_r($dtSiswa);

        	$benar = 0;
        	$salah = 0;
        	$jml = 0;

        	$total_soal = $data->num_rows();
        	$nilai_per_soal = $max_nilai/$total_soal;

        	$poin = 0;
        	foreach ($data->result() as $d) {
        		# code...
        		if ($d->benar==1) {
        			# code...
        			$poin += 1* $nilai_per_soal;
        			$benar += 1;
        		}
        		if ($d->benar==0) {
        			$salah +=1;
        			$poin += 1* 0;
        		}

        		$jml += 1;
        	}


        }

                if ($lama->result()[0]->essai==0) {

        $this->db->insert('tb_nilai',['id_siswa'=>$userid,'id_jurusan'=>$jurusan,'id_kelas'=>$kelas,'tgl'=>date('Y-m-d'),'kode_soal'=>$data->result()[0]->soal_id,'jenis_soal'=>'PILIHAN GANDA','benar'=>$benar,'soal'=>$jml,'salah'=>$salah,'point'=>$poin,'id_modul'=>$modul]);
    }


        // print_r($modul);
        $this->db->update('t_soal_user',['status_pengerjaan'=>2,'stoped_at'=>date('Y-m-d H:i:s')],['id_modul'=>$modul,'id_user'=>$user]);

    }

    public function send_essai(){
		
		$jwbn = $this->input->post('jwbn');
		$uid = $this->input->post('uid');
		$soal = $this->input->post('soal');


		$this->db->update('t_soal_user',['dijawab'=>'1'],['id'=>$soal]);


		$this->db->update('t_jawaban_user',['jawaban_essai'=>$jwbn],['id'=>$uid]);


    }

}
