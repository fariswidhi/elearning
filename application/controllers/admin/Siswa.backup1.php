<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->model_security->getsecurity();
    $isi['data']    = $this->model_all->get_all('tb_siswa')->result();
    $isi['judul']   = "Siswa";
    $isi['content'] = "backend/admin/dt_siswa/v_siswa";
    $this->load->view("backend/admin/home", $isi);
  }

  public function u_siswa()
  {
    $this->model_security->getsecurity();
    $isi['judul']   = "Siswa / Update Siswa";
    $isi['content'] = "backend/admin/dt_siswa/u_siswa";
    $this->load->view("backend/admin/home");
  }
}
