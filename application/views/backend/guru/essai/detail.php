


<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="pull-right">
          <button data-source="<?php echo base_url().'guru/Soal/set_password/'.$this->uri->segment(4) ?>" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-modal">Set Password</button>

          
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Set Password Modul</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
        <button type="button" class="btn btn-primary set_password">ATUR</button>
      </div>
    </div>
  </div>
</div>

                  <form action="<?php echo base_url(uri_string()); ?>" method="post" enctype="multipart/form-data">

        <div class="row ">

<?php 
    foreach ($forms as $f) {
      # code...
      ?>
<div class="col-lg-3">
      <?php
      // print_r($f);
      foreach ($f as $x) {
        # code...


?>

 <label for="nig"><?php echo $x['name'] ?></label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person</i>
                  </span>

<?php 
$obj = $x['list']['field'];
 ?>

 <?php echo $data->$obj ?>
                  </div>

<?php
      }
      ?>
</div>

<?php

    }



 ?>
 
        </div>
    </form>

      </div>
    </div>

      <div class="card">
      <div class="header">
Soal
      </div>
      <div class="body">
<ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a class="tbh" data-source="<?php echo base_url() ?>guru/Soal/data/<?php echo $this->uri->segment(4) ?>" href="#home" data-toggle="tab" aria-expanded="true">Data Soal</a></li>
                                <li role="presentation" class=""><a data-source="<?php echo base_url() ?>guru/Soal/wrap_soal/<?php echo $this->uri->segment(4) ?>" href="#profile" data-toggle="tab" aria-expanded="false" class="tbh">Tambah Soal</a></li>

    </ul>
    <div class="oke">
      <br>

   
    </div>

</div>
</div>


  </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js"></script>
<script type="text/javascript">



  $(document).ready(function() {

    $('#lembar_soal').hide();

    $('#tmb_soal').click(function() {
      $('#lembar_soal').slideDown();
      $('#muat_soal').fadeOut();
    });
    
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_eesay').addClass('active');



  });

  $(document).on('click','.tbh',function(){
    var ds = $(this).attr('data-source');

    $.get(ds,function(res){
      $(".oke").html(res);
    })
  })

  $(document).on('click','.tambah_soal',function(){
    var forme = $("#forme").html();


    $("#append").append(forme);
  });



function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}



  $(document).on("click",".save",function(){
    var form = $("#form").serialize();

    // console.log(form);
    $(".soal").each(function(){
      // console.log($(this).val());
    });

    var data = [];
    var soal = [];
    $(".oke-form").each(function(){
      // console.log($(this).html());

        var jwbn = [];
        $(this).find('.jawaban').each(function(){
          // console.log($(this).val());
          // jwbn[] = $(this).val();
          jwbn.push($(this).val());
        });

          var sl = $(this).find('.soal').val();
          soal.push(sl);
          data.push(jwbn);
    });

    // console.log(soal);

    // console.log(data);


    $.ajax({
      url:"<?php echo base_url() ?>/guru/Soal/kirim",
      data: {
        jawaban: data,
        soal:soal,
        modul:<?php echo $this->uri->segment(4) ?>
      },
      method: "POST",
      dataType:"JSON",
      success:function(res){
        // console.log(res);
        if (res.status==1) {

          alert("Data Berhasil Disimpan");
        }
        else{
          alert("Data Gagal Disimpan");
        }
      }
    });
  });

  $(document).on('click','.btn-modal',function(){
    var ds = $(this).attr('data-source');

    $.get(ds,function(res){
      $(".modal-body").html(res);
    })
  });

  $(document).on('click','.set_password',function(){
    var form = $("#form").serialize();
    var action = $("#form").attr('data-action');
    $.ajax({
      url: action,
      method: "POST",
      data: form,
      dataType: "JSON",
      success:function(res){
        if (res.status==1) {
          alert("Berhasil Set Password Modul");
          $("#exampleModal").modal('hide');
        }
        else{
                    $("#exampleModal").modal('hide');
        }
      }
    })
  })
</script>
