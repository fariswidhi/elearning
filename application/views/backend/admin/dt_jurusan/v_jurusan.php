<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data Jurusan
                </h2>
            </div>
            <div class="body">
                <?php
                  $info = $this->session->flashdata('info');
                  if (!empty($info)) {
                    echo $info;
                  }
                ?>
                <a href="<?= base_url(); ?>admin/Jurusan/t_jurusan">
                    <button name="button" class="btn btn-md btn-primary"> <span>Tambah Jurusan</span> <i class="material-icons">add_circle</i> </button>
                </a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>Nama Jurusan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                              $no = 1;
                              foreach ($data as $dt) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $dt->nama_jurusan; ?></td>
                                <td align="center">
                                    <a href="<?= base_url(); ?>admin/Jurusan/u_jurusan/<?= $dt->id_jurusan; ?>">
                                      <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit Jurusan">
                                        <i class="material-icons">edit</i>
                                      </button>
                                    </a>
                                    <a href="<?= base_url(); ?>admin/Jurusan/delete_jurusan/<?= $dt->id_jurusan; ?>">
                                      <button onclick="return confirm('Yakin Ingin Menghapus Jurusan...???')" type="button" name="button" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus Jurusan">
                                        <i class="material-icons">delete</i>
                                      </button>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_jurusan').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
