<div class="row clearfix">
  <?php if ($data->num_rows()>0): ?>
      <?php foreach ($data->result_object() as $d): ?>
        
  <div class="col-lg-4">
    <div class="card">
      <div class="header">
          <h2>
            <?php echo $d->nama ?>
          </h2>

      </div>
      <div class="body">
        <?php echo $d->keterangan ?>
        <br>
        <br>
        <a class="btn btn-primary" href="<?php echo base_url('siswa/Live/detail/'.$d->id) ?>">LIHAT</a>
      </div>
    </div>
  </div>
      <?php endforeach ?>
  <?php endif ?>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#live').addClass('active');
  });
</script>
