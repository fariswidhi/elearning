<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Streaming extends CI_Controller
{


  protected $table="t_livestream";
  protected $folder = "backend/guru/streamings/";
  protected $primaryKey= "id";
  protected $base = "Streaming";

  function __construct()
  {
    parent::__construct();
  }


  public function forms(){



  $forms  = [

      1=>[
        [

        'name'=>'Nama/Judul',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'nama'
        ]

        ],
        [

        'name'=>'Keterangan',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'keterangan'
        ]

        ],
        [

        'name'=>'URL YouTube',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'url'
        ]

        ],
      

       
        
      ],

    ];

    // print_r($forms);
    return $forms;

    // echo json_encode($forms);


  }
  public function index()
  {
    $this->model_security->getsecurity();
    $data    = $this->model_all->get_all($this->table)->result();
    $judul   = "Streaming";
    $content = $this->folder."table";
    $forms = $this->forms();

    $this->load->view("backend/guru/home", compact('data','judul','content','forms'));
  }
  
  public function edit($id){

    if ($this->input->post()) {
      # code...

$nama_mapel = $this->input->post("nama_mapel");

$datane = array(
"nama_mapel" => $nama_mapel,
);
$where = array("id_mapel"=>$id);
$this->db->update("tb_mapel",$datane,$where);



      alert_success('Live Streaming Berhasil Diubah');
      return redirect('guru/'.$this->base);
    }
    else{
    $judul   = "Update Data".$this->base;
    $content = $this->folder."edit";
    $forms = $this->forms();


    $data  = $this->model_all->get_where($this->table,["id_mapel"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Edit Data ";
      $content = $this->folder.'edit';
    $this->load->view("backend/admin/home", compact('content','judul','forms','data'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }

    }

    // print_r($data);
  }


  public function detail($id){

    $forms = $this->forms();


    $data  = $this->model_all->get_where($this->table,["id"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Detail Data ";
      $content = $this->folder.'detail';
    $this->load->view("backend/admin/home", compact('content','judul','forms','data'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }



  }
  public function dump_insert(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$this->db->insert("'.$table.'",$datane);';

  }


  public function dump_update(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$where = array("'.$this->primaryKey.'"=>$id);<br>';
    echo '$this->db->update("'.$table.'",$datane,$where);';

  }
  public function create(){

    if ($this->input->post()) {
      # code... 
      $post = $this->input->post();

      // foreach ($post as $key => $value) {
      //   # code...
      //   echo $key."<br>";
      // }


      // print_r($userid);
$nama = $this->input->post("nama");
$keterangan = $this->input->post("keterangan");
$url = $this->input->post("url");

$datane = array(
"nama" => $nama,
"keterangan" => $keterangan,
"url" => $url,
"id_guru"=>$this->session->userdata('id')
);
$this->db->insert("t_livestream",$datane);


      // // $data  =  $this->model_all->
      alert_success('Data Berhasil Ditambahkan');
      return redirect('guru/'.$this->base);


    }
    else{

    $content = $this->folder."create";
      $judul = "Tambah Data ";


  $forms = $this->forms();
   $this->load->view('backend/admin/home',compact('content','judul','forms')) ;
    }
  }

  public function delete($id){

    $where  = array("id"=>$id);
    $this->model_all->del_id($this->table,$where);

          return redirect('guru/'.$this->base);

  }

}
