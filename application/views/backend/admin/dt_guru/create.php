<!-- Input Group -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?php echo $judul ?>
          </h2>

      </div>
      <div class="body">
        <div class="row clearfix">
          <form action="<?php echo base_url(uri_string()); ?>" method="post" enctype="multipart/form-data">

<?php 
    foreach ($forms as $f) {
      # code...
      ?>
<div class="col-md-6">
      <?php
      // print_r($f);
      foreach ($f as $x) {
        # code...


?>

 <label for="nig"><?php echo $x['name'] ?></label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person</i>
                  </span>

                    <?php if ($x['list']['type']=="select"): ?>
                       <select class="form-control show-tick" data-live-search="true" name="<?php echo $x['list']['field'] ?>">
                      <option value="">-- Pilih <?php echo $x['name'] ?> --</option>
                      <?php foreach ($x['list']['data'] as $o ): ?>
                        <option value="<?php echo $o['value'] ?>"><?php echo $o['text'] ?></option>
                      <?php endforeach ?>
                  </select>
                    <?php elseif($x['list']['type']=="textarea"): ?>

                      <textarea name="<?php echo $x['list']['field'] ?>" class="form-control" rows="6" required="required" placeholder="Alamat"></textarea>

                      <?php else: ?>
                  <div class="form-line">
                      <input type="<?php echo $x['list']['type'] ?>" class="form-control date" required="required"  name="<?php echo $x['list']['field'] ?>">
              </div>
                    <?php endif ?>
                  </div>

<?php
      }
      ?>
</div>

<?php

    }



 ?>
<!-- 
            <div class="col-md-6">
              <label for="nig">Nomor Induk Guru</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person</i>
                  </span>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required" placeholder="Nomor Induk Guru" name="nig">
                  </div>
              </div>

              <label for="nama">Nama Guru</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person_add</i>
                  </span>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required" placeholder="Nama Guru" name="nama">
                  </div>
              </div>

              <label for="jk">Jenis Kelamin</label>
              <div class="input-group">
                  <select class="form-control show-tick" data-live-search="true" name="jk">
                      <option value="">-- Pilih Jenis Kelamin --</option>
                      <option value="L">Laki - Laki</option>
                      <option value="P">Perempuan</option>
                  </select>
              </div>

              <label for="bidang_studi">Bidang Studi</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person_add</i>
                  </span>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required" placeholder="Bidang Studi" name="bidang_studi">
                  </div>
              </div>

              <label for="tmpt">Tempat Lahir</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person_add</i>
                  </span>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required" placeholder="Tempat Lahir" name="tmpt">
                  </div>
              </div>

              <label for="tgl">Tanggal Lahir</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person</i>
                  </span>
                  <div class="form-line">
                      <input type="date" class="datepicker form-control date" required="required" placeholder="Pilih Tanggal" name="tgl">
                  </div>
              </div>
            </div>
            <div class="col-md-6">

              <label for="agama">Agama</label>
              <div class="input-group">
                  <select class="form-control show-tick" data-live-search="true" name="agama">
                      <option value="">-- Pilih Agama --</option>
                      <option value="Islam">Islam</option>
                      <option value="Katolik">Katolik</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Budha">Budha</option>
                  </select>
              </div>

              <label for="almt">Alamat</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">lock</i>
                  </span>
                  <div class="form-line">
                      <textarea name="almt" class="form-control" rows="6" required="required" placeholder="Alamat"></textarea>
                  </div>
              </div>

              <label for="level">Foto</label>
              <div class="input-group">
                  <img src="" alt="Foto" width="300">
                  <input type="file" class="form-control" required="required" name="foto" value="">
              </div>


               <label for="tmpt">Username</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person_add</i>
                  </span>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required" placeholder="Tempat Lahir" name="username">
                  </div>
              </div>

             <label for="tmpt">Password</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">person_add</i>
                  </span>
                  <div class="form-line">
                      <input type="password" class="form-control date" required="required" placeholder="Tempat Lahir" name="password">
                  </div>
              </div>
            </div>
 -->


            <div class="col-md-12">
              <div class="input-group">
                  <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                  <button type="submit" name="button" class="btn btn-success waves-effect waves-light"><span>Simpan</span> <i class="material-icons">update</i> </button>
              </div>
            </div>
            </div>




          </form>
        </div>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#guru').addClass('active');
  });
</script>
<!-- #END# Input Group -->
