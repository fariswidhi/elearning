<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Laporan Data Guru
                </h2>
            </div>
            <div class="body">
                <a href="<?= base_url(); ?>admin/Laporan/cetak_guru">
                    <button type="button" class="btn btn-primary waves-effect"> <span>Cetak Laporan</span> <i class="material-icons">picture_as_pdf</i> </button>
                </a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>NIP</th>
                                <th>Foto</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Bidang Studi</th>
                                <th>Tempat Lahir</th>                              
                                <th>Tanggal Lahir</th>
                                <th>Agama</th>
                                <th>Alamat</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                              $no = 1;
                              foreach ($data as $dt) {
                          ?>
                          <tr>
                              <td><?= $no++; ?></td>
                                                            <td><?= $dt->nig ?></td>

                              <td align="center">
                                <div class="image">
                                  <?php
                                    $foto = $dt->foto;
                                    if ($foto == '-') {
                                      echo '<img src="'.base_url().'assets/back_end/images/user.png" width="48" height="48" alt="User" />';
                                    } else {
                                      echo '<img src="'.base_url().'/'.$dt->foto.'" width="48" height="48" alt="User" />';
                                    }
                                  ?>
                                </div>
                              </td>
                              <td><?= $dt->nama_guru; ?></td>
                              <td>
                                  <?php
                                    $jk = $dt->jk;
                                    if ($jk == 'L') {
                                      echo "Laki - Laki";
                                    } else if ($jk == 'P') {
                                      echo "Perempuan";
                                    } else {
                                      echo "-";
                                    }
                                  ?>
                              </td>
                              <td><?= $dt->bidang_studi; ?></td>
                              <td><?php echo $dt->tempat_lahir ?></td>
                              <td><?php echo $dt->tgl_lahir ?></td>
                              <td><?php echo $dt->agama ?></td>
                              <td><?php echo $dt->alamat ?></td>


                          </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#laporan').addClass('active');
    $('#lap_guru').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
