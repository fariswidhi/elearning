<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>
      </div>
      <div class="body">
        <a href="<?= base_url(); ?>guru/Soal/t_ulangan_pilgan">
            <button name="button" class="btn btn-md btn-primary"> <span>Tambah Soal</span> <i class="material-icons">add_circle</i> </button>
        </a>
        <br>
        <br>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
            <thead>
              <tr>
                <th width="10">No</th>
                <th>Tanggal</th>
                <th>Mata Pelajaran</th>
                <th>Kelas</th>
                <th>Jenis Soal</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="center">
                  <a href="<?= base_url(); ?>guru/Soal/detail_ulangan_pilgan">
                    <button type="button" name="button" class="btn btn-circle btn-info waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Detail Soal">
                      <i class="material-icons">search</i>
                    </button>
                  </a>
                  <a href="<?= base_url(); ?>guru/Soal/u_ulangan_pilgan">
                    <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit Soal">
                      <i class="material-icons">edit</i>
                    </button>
                  </a>
                  <a href="<?= base_url(); ?>guru/Soal/delete_ulangan_pilgan">
                    <button type="button" name="button" onclick="return confirm('Yakin Ingin Menghapus Soal Uts...???')" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus Soal">
                      <i class="material-icons">delete</i>
                    </button>
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_pilgan').addClass('active');
  });
</script>
