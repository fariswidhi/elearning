<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->

      <?php 
    $userid = $this->session->userdata('id');
    $data = $this->db->get_where('tb_siswa',['id_user'=>$userid]);
     ?>
    <div class="user-info">
        <div class="image">
            <img src="<?php echo base_url($data->result()[0]->foto) ?>" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->session->userdata('nama_siswa'); ?></div>
            <div class="email">Siswa</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
 
                    <li role="seperator" class="divider"></li>

                    <li><a href="<?= base_url(); ?>siswa/Profile"><i class="material-icons">input</i>Info</a></li>
                    <li><a href="<?= base_url(); ?>Login/logout"><i class="material-icons">input</i>Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            <li id="home">
                <a href="<?= base_url(); ?>Home">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>

            <li id="materi">
                <a href="<?= base_url(); ?>siswa/Materi">
                    <i class="material-icons">book</i>
                    <span>Materi</span>
                </a>
            </li>

            <li id="ulangan">
                <a href="<?= base_url(); ?>siswa/Ulangan">
                    <i class="material-icons">local_offer</i>
                    <span>Ulangan</span>
                </a>
            </li>

            <li id="uts">
                <a href="<?= base_url(); ?>siswa/Uts">
                    <i class="material-icons">local_offer</i>
                    <span>Uts</span>
                </a>
            </li>

            <li id="live">
                <a href="<?= base_url(); ?>siswa/Live">
                    <i class="material-icons">camera</i>
                    <span>Live Streaming</span>
                </a>
            </li>
            
            <li id="live">
                <a href="<?= base_url(); ?>siswa/Pengumuman">
                    <i class="material-icons">camera</i>
                    <span>Pengumuman</span>
                </a>
            </li>
            
        </ul>
    </div>
    <!-- #Menu -->
    <?php $this->load->view('backend/layout/copyright'); ?>
</aside>
<!-- #END# Left Sidebar -->
