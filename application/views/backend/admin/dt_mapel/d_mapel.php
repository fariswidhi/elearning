<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data Mata Pelajaran Jurusan
                </h2>
            </div>
            <div class="body">
                <button type="button" onclick="window.history.go(-1)" class="btn btn-default waves-effect" name="button"> <i class="material-icons">arrow_back</i> <span>Kembali</span></button>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>Nama Mata Pelajaran</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                              $no = 1;
                              foreach ($data as $dt) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $dt->nama_mapel; ?></td>
                                <td align="center">
                                  <a href="<?= base_url(); ?>admin/Mapel/u_mapel/<?= $dt->id_mapel; ?>">
                                    <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit Mapel">
                                      <i class="material-icons">edit</i>
                                    </button>
                                  </a>
                                  <a href="<?= base_url(); ?>admin/Mapel/delete_mapel/<?= $dt->id_mapel; ?>">
                                    <button onclick="return confirm('Yakin Ingin Menghapus Mapel...???')" type="button" name="button" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus Mapel">
                                      <i class="material-icons">delete</i>
                                    </button>
                                  </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_mapel').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
