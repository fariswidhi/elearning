<!-- Input Group -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Detaiil Pengumuman
                </h2>

            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <form action="<?= base_url(); ?>admin/Pengumuman/update_pengumuman" method="post">

                            <label for="judul">Judul Pengumuman</label><br>
                          
                          <?php echo $data[0]['judul'] ?><br>

                            <label for="isi">Isi Pengumuman</label><br>
                            <?php echo $data[0]['isi'] ?>
                            <div class="input-group">
                                <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#pengumuman').addClass('active');
  });
</script>
<!-- #END# Input Group -->
