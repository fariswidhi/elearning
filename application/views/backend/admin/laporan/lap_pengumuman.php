<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Laporan Data Pengumuman
                </h2>
            </div>
            <div class="body">
                <a href="<?= base_url(); ?>admin/Laporan/cetak_pengumuman">
                    <button type="button" class="btn btn-primary waves-effect"> <span>Cetak Laporan</span> <i class="material-icons">picture_as_pdf</i> </button>
                </a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>Tanggal Pengumuman</th>
                                <th>Judul Pengumuman</th>
                                <th>Isi Pengumuman</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                            $no = 1;
                            foreach ($data as $dt) {
                          ?>
                          <tr>
                              <td><?= $no++; ?></td>
                              <td><?= date_indo($dt->tgl); ?></td>
                              <td><?= $dt->judul; ?></td>
                              <td><?= substr($dt->isi, 0, 180).'....'; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#laporan').addClass('active');
    $('#lap_pengumuman').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
