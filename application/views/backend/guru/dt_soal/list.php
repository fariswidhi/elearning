<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="row">
        	<div class="col-lg-6">
        		<table class="table table-bordered">
        			<tr>
        				<td>Nama Modul</td><td><?php echo $info->result()[0]->nama_modul ?></td>
        			</tr>
        			<tr>
        				<td>Mata Pelajaran</td><td><?php echo $info->result()[0]->nama_mapel ?></td>
        			</tr>
        			<tr>
        				<td>Min Nilai</td><td><?php echo $info->result()[0]->min_nilai ?></td>
        			</tr>
        			<tr>
        				<td>Max Nilai</td><td><?php echo $info->result()[0]->max_nilai ?></td>
        			</tr>
        			<tr>
        				<td>Waktu</td><td><?php echo $info->result()[0]->waktu ?> Menit</td>
        			</tr>

        		</table>
        	</div>
        </div>
      </div>
    </div>
  </div>


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
          	List 
          </h2>

      </div>
      <div class="body">
      	<table class="table table-striped">
      		<thead>
      			<th>No</th>
      			<th>Nama Siswa</th>
      			<th>Nilai</th>
      			<th>OPSI</th>
      		</thead>
      		<tbody>
      			<?php if ($data->num_rows()>0): ?>
      				<?php 
      				$n=1;
      				 ?>
      					<?php foreach ($data->result() as $d): 

      						$nilai = $this->db->get_where('tb_nilai',['kode_soal'=>$d->soal_id]);

							$num = $nilai->num_rows();
      						// print_r($nilai->num_rows());
      						?>
      						<tr>
      							<td><?php echo $n++ ?></td>
      							<td><?php echo $d->nama_siswa ?></td>
      							<td>
      								<?php if ($num==0): ?>
      									-
      								<?php else: ?>
      									<?php echo $nilai->result()[0]->point ?>
      								<?php endif ?>
      							</td>
      							<td><a href="<?php echo base_url('guru/Soal/info/'.$d->soal_id) ?>" class="btn btn-success">Info</a></td>
      						</tr>
      					<?php endforeach ?>
      			<?php endif ?>
      		</tbody>
      	</table>
      </div>
  </div>
</div>


</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_eesay').addClass('active');
  });
</script>



