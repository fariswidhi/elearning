<!-- Input Group -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Update Kelas
                </h2>

            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <form action="<?= base_url(); ?>admin/Kelas/update_kelas" method="post">

                            <label for="kelas">Nama Kelas</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">local_offer</i>
                                </span>
                                <div class="form-line">
                                    <input type="hidden" name="id_kelas" value="<?= $id_kelas; ?>">
                                    <input type="text" class="form-control date" required="required" name="kelas" value="<?= $nama_kelas; ?>" placeholder="Nama Kelas">
                                </div>
                            </div>

                            <div class="input-group">
                                <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                                <button type="submit" name="button" class="btn btn-success waves-effect waves-light pull-right"><span>Update Kelas</span> <i class="material-icons">update</i> </button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_kelas').addClass('active');
  });
</script>
<!-- #END# Input Group -->
