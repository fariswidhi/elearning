<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
          <thead>
            <th>No</th>
            <th>Nama Siswa</th>
            <th>Status Absen</th>
          </thead>
          <tbody>
            <?php if ($dt->num_rows()>0): ?>
              
                <?php $n=1; ?>
              <?php foreach ($dt->result() as $dx): ?>
                <tr>
                  <td><?php echo $n++ ?></td>
                  <td><?php echo $dx->nama_siswa ?></td>
                  <td><?php echo $dx->id_absen==null ? '<label class="label label-danger">Belum</label>':'<label class="label label-success">Sudah</label>' ?></td>
                </tr>
              <?php endforeach ?>
            <?php endif ?>
          </tbody>
          
        </table>
      

      </div>
    </div>
  </div>
</div>