<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Ujian</title>
    <?php $this->load->view('backend/layout/header'); ?>
</head>

<body class="theme-black">
    <!-- Page Loader -->
    
    <!-- #END# Page Loader -->
    <!-- Top Bar -->
    <nav class="navbar "  style="background-color: #3bafda;">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#"><?php echo $dtModul->result()[0]->nama_modul; ?></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    
                    <li><a href="javascript:void(0);" class="js-search" data-close="true" style="font-size: 30px;" id="getting-started"></a></li>
                    <li><button style="margin-top: 15px;"  class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal"><i class="material-icons">stop</i> SELESAI</button></li>

                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <!-- #END# Tasks -->

                </ul>
            </div>
            <!-- <div class="collapse navbar-collapse" id="navbar-collapse"></div> -->
        </div>
    </nav>
    <!-- #Top Bar -->

    <section class="content" style="margin: 0 auto;">
        <div class="container" style="margin-top: 100px;">
            <div class="block-header">

                <div class="row">
                    <div class="col-lg-8" id="soal-wrap">

                        <div class="card">

    <div class="body" style="height: 300px;">
    <center><h1>Klik Tombol Di Kanan Untuk Memilih Soal</h1></center>
    </div>
                </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                    <div class="header">
                <h2>SOAL</h2>
                        </div>
                        <div class="body">



                            <div id="row-soal">
                                
                            </div>
                                
   <span style="clear: both;display: block;" class="clear"></span>
<br>
<br>

<!-- <div style="width: 50%;float: left;">
    <button type="button" class="btn btn-warning waves-effect">
                                    <i class="material-icons">navigate_before</i>
                                </button>
</div>

<div style="width: 50%;float: left;">
    <button type="button" class="btn btn-warning waves-effect" style="float: right;">
                                    <i class="material-icons">navigate_next</i>
                                </button>
</div> -->
   <span style="clear: both;display: block;" class="clear"></span>

                        </div>
                    </div>
                    </div>
                </div>
            </div>


        </div>
    </section>


    <div class="modal" tabindex="-1" role="dialog"  id="exampleModal">
  <div class="modal-dialog" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mdl-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<div id="mdl-body">
    Apakah anda Ingin Berhenti
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-ok">Ya</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
      </div>
    </div>
  </div>
</div>
    <?php $this->load->view('backend/layout/footer'); ?>

</body>
<style type="text/css">
    .oke-btn{
        float: left;margin: 5px;width: 20%;
    }
</style>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>

    <script type="text/javascript">


$(document).ready(function(){

    setInterval(function(){
        $.ajax({
        url:"<?php echo base_url('siswa/ujian/time') ?>",            
                method: "GET",
        dataType: "JSON",
        success:function(res){
            if (res.timeout=="true") {
                // $("#exampleModal").modal("show");
                // $("#mdl-title").text("Pemberitahuan");
                // $("#mdl-body").text("<h1>Waktu Mengerjakan Anda Telah Habis</h1>");
                // alert("Waktu Mengerjakan Anda Sudah Habis");
                if (confirm("waktu Mengerjakan anda sudah habis")) {
                    location.href ="<?php echo base_url('siswa/'.ucfirst(strtolower($dtModul->result()[0]->jenis))) ?>"
                }
                else{
                                        location.href ="<?php echo base_url('siswa/'.ucfirst(strtolower($dtModul->result()[0]->jenis))) ?>"
                }

            }
            else{
                //     $("#exampleModal").modal("show");
                // $("#mdl-title").text("Pemberitahuan");
                // $("#mdl-body").text("<h1>Waktu Mengerjakan Anda Telah Habis</h1>");
            }

        }
        })
    },2000)

    $.ajax({
        url:"<?php echo base_url('siswa/ujian/time') ?>",
        method: "GET",
        dataType: "JSON",
        success:function(res){

             $("#getting-started").countdown(res.waktu, function(event) {
    $(this).text(
      event.strftime('%H:%M:%S')
    );
  });

        }
    })

});
load();
function load(){


$.ajax({
    url:"<?php echo base_url() ?>/siswa/ujian/list_soal",
    method: "GET",
    dataType: "JSON",
    success:function(res){
        var html = '';
        for (var i = 0; i < res.length ; i++) {
            if (res[i].dijawab=="1") {
            html += '<button  data-idne="'+res[i].id+'" class="btn btn-'+res[i].id+' sudah btn-primary btn-lg oke-btn" >'+parseInt(i+1)+'</button>';
                
            }
            else{
                html += '<button  data-idne="'+res[i].id+'" class="btn  btn-'+res[i].id+' belum btn-default btn-lg oke-btn" >'+parseInt(i+1)+'</button>';
            }
        }
        $("#row-soal").html(html);
    }
});

}

$(document).on('click','.oke-btn',function(){
    var idne = $(this).attr('data-idne');



$(".oke-btn").removeClass("btn-success");
$(".belum").addClass("btn-default");

    $(this).removeClass('btn-default');


    $(this).addClass('btn-success');
    // load();

    $.ajax({
        url: "<?php echo base_url() ?>/siswa/ujian/jawaban/"+idne,
        method: "GET",
        success:function(res){
            $("#soal-wrap").html(res);
        }
    })
    console.log(idne);
});

$(document).on('click','.check',function(){
    var idx = $(this).attr('data-idx');
    // alert(idx);
            var sl = $(this).attr('data-soal');
    $.ajax({
        url: "<?php echo base_url() ?>/siswa/ujian/jwbn",
        method: "POST",
        data:{
            idx:idx,
            soal: $(this).attr('data-soal')
        },
        success:function(res){
            $(".btn-"+sl).removeClass("btn-success");
            $(".btn-"+sl).addClass("btn-primary");
            $(".btn-"+sl).removeClass("belum");
            $(".btn-"+sl).addClass("sudah");
            
               
        }
    })
});

$(document).on('click','.btn-ok',function(){
    // alert("OK");
    $.ajax({
        url: "<?php echo base_url('siswa/ujian/berhenti') ?>",
        method: "POST",
        success:function(res){
            location.href= '<?php echo base_url('siswa/'.ucfirst(strtolower($dtModul->result()[0]->jenis))) ?>';
        }
    })
});

$(document).on('click','#kirim_jawaban',function(){
    var uid = $(this).attr('data-idx');

    var jwbn = $("#text_jawaban").val();
    var sl = $(this).attr('data-soal');

    $.ajax({
        url: "<?php echo base_url('siswa/ujian/send_essai') ?>",
        method: "POST",
        data: {
            jwbn: jwbn,
            uid: uid,
            soal: sl
        },
        success:function(){
            $(".btn-"+sl).removeClass("btn-success");
            $(".btn-"+sl).addClass("btn-primary");
            $(".btn-"+sl).removeClass("belum");
            $(".btn-"+sl).addClass("sudah");
            
               


        }
    })
})

    </script>


</html>
