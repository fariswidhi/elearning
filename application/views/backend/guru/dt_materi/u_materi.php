<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="row clearfix">
            <form enctype="multipart/form-data" action="<?= base_url(); ?>guru/Materi/update_materi" method="post">
          <div class="col-md-6">
              <input type="hidden" name="id_materi" value="<?= $id_materi; ?>">
              <input type="hidden" name="guru" value="<?= $this->session->userdata('id'); ?>">
     <div class="">
              <label for="judul">Judul</label>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required"  name="judul"  value="<?php echo $info->judul ?>">
              </div>

              </div>

              <label for="jurusan">Mata Pelajaran</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->

                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="mapel_id" data-live-search="true">
                      <option value="">-- Pilih Mata Pelajaran --</option>
                      <?php
                        foreach ($mp as $dtx) {
                          ?>

                          <option <?php echo $info->mapel_id==$dtx->id_mapel? 'selected':'' ?> value="<?php echo $dtx->id_mapel ?>"><?php echo $dtx->nama_mapel ?></option>
                      <?php  }
                      ?>
                  </select>
                </div>

              </div>


              <label for="jurusan">Jurusan</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="jurusan" data-live-search="true">
                      <option value="">-- Pilih Jurusan --</option>
                      <?php
                        foreach ($data as $dt) {
                          $id_jrsn = $dt->id_jurusan;
                          if ($id_jrsn == $jurusan) {
                            echo "<option value='".$dt->id_jurusan."' selected>".$dt->nama_jurusan."</option>";
                          }
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('jurusan'); ?></b>
              </div>

              <label for="kelas">Kelas</label>
              <div class="">
                <!-- <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span> -->
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="kelas" data-live-search="true">
                      <option value="">-- Pilih Kelas --</option>
                      <?php
                        foreach ($data_2 as $dt) {
                          $id_kelas = $dt->id_kelas;
                          if ($id_kelas == $kelas) {
                            echo "<option value='".$dt->id_kelas."' selected>".$dt->nama_kelas."</option>";
                          }
                          else{
        echo "<option value='".$dt->id_kelas."' >".$dt->nama_kelas."</option>"; 
                          }
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('kelas'); ?></b>
              </div>

          </div>
<div class="col-lg-12">
  
             
              <label for="isi">File</label>
              <a class="btn btn-xs btn-info" href="<?php echo base_url($info->file) ?>"><?php echo "Download" ?></a>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">local_offer</i>
                  </span>
                  <input type="file" name="file" class="form-control">

                  <b class="text-danger"><?= form_error('isi'); ?></b>
              </div>


              <div class="input-group">
                  <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                  <button type="submit" name="button" class="btn btn-success waves-effect waves-light pull-right"><span>Update Materi</span> <i class="material-icons">update</i> </button>
              </div>
</div>

            </form>
        </div>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">


  $(function() {
    $('#materi').addClass('active');
  });
</script>
