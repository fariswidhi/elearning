<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Live extends CI_Controller {

	public function index()
	{
		$this->model_security->getsecurity();
    	$isi['judul']   = "Live Streaming";
    	$isi['content'] = "backend/siswa/live/live";
    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');
    	$isi['data'] = $this->db->get('t_livestream');

    	$this->load->view("backend/siswa/home", $isi);
	}

	public function detail($id){
    	$isi['content'] = "backend/siswa/live/detail";
    	    	$isi['judul']   = "Live Streaming";
    	$isi['data'] = $this->db->get_where('t_livestream',['id'=>$id]);

		$this->load->view("backend/siswa/home",$isi);
	}
}


// <iframe width="560" height="315" src="https://www.youtube.com/embed/coE3BF8vsEc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>