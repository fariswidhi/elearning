<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_all extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function insert($table = '', $data = '')
	{
		$db = $this->db->insert($table, $data);
		return;
	}

	public function get_all($table)
	{
		$this->db->from($table);
		return $this->db->get();
	}

	public function cari($table, $data)
	{
		$this->db->from($table)->like($data);
		return $this->db->get();
	}

	public function cari_where($table, $data, $where)
	{
		$this->db->from($table)->like($data)->where($where);
		return $this->db->get();
	}

	public function get_all_order($table,$order)
	{
		$this->db->from($table)->order_by($order,'ASC');
		return $this->db->get();
	}

	public function get_where_order($table = null, $where = null,$order = null)
	{
		$this->db->from($table)->where($where)->order_by($order,'DESC');
		return $this->db->get();
	}

	public function get_all_group($table,$id)
	{
		$this->db->from($table)->group_by($id);
		return $this->db->get();
	}

	public function get_all_group_order($table = null,$id = null, $ord = null)
	{
		$this->db->from($table)->group_by($id)->order_by($ord,'DESC');
		return $this->db->get();
	}

	public function get_where($table = null, $where = null)
	{
		$this->db->from($table)->where($where);
		return $this->db->get();
	}

	public function update($table = null, $data = null, $where = null)
	{
		$db = $this->db->update($table, $data, $where);
		return;
	}

	public function del_id_g($table = null, $id, $where = null, $gbr = null)
	{
    $query = $this->db->get($table);
    $row = $query->row();
		$this->db->delete($table, array($where => $id));
		return;
	}

	public function del_cek($table = null, $id, $where = null, $gbr = null)
	{
		$this->db->where($where, $id);
    $query = $this->db->get($table);
    $row = $query->row();
    unlink($gbr.$row->gbr);
		$this->db->delete($table, array($where => $id));
		return;
	}

	public function del_all($table = null)
	{
		$this->db->empty_table($table);
		return true;
	}

	public function del_id($table = null, $where = null)
	{
		$this->db->delete($table, $where);
		return;
	}

	public function join2table($t1,$t2,$w1,$w2)
	{
		$this->db->select('*')
		->from($t1)
		->join($t2,"$t2.$w1=$t1.$w2");
		return $this->db->get()->result();
	}

	public function join2table_group($t1,$t2,$w1,$w2,$group)
	{
		$this->db->select('*')
		->from($t1)
		->join($t2,"$t2.$w1=$t1.$w2")
		->group_by($group);
		return $this->db->get()->result();
	}

	public function join2table_where($id,$t1,$t2,$w1,$w2)
	{
		$this->db->select('*')
		->from($t1)
		->join($t2,"$t2.$w1=$t1.$w2")
		->where($id);
		return $this->db->get()->result();
	}

	public function join3table_group($t1,$t2,$t3,$w1,$w2,$w3,$group)
	{
		$this->db->select('*')
		->from($t1)
		->join($t2,"$t2.$w1=$t1.$w2")
		->join($t3,"$t3.$w1=$t1.$w3")
		->group_by($group);
		return $this->db->get()->result();
	}

	public function join4table($t1,$t2,$t3,$t4,$t5,$i1,$i2,$i3,$i4,$w2,$w3,$w4,$w5)
	{
		$this->db->select('*')
		->from($t1)
		->join($t2,"$t2.$i1=$t1.$w2")
		->join($t3,"$t3.$i2=$t1.$w3")
		->join($t4,"$t4.$i3=$t1.$w4")
		->join($t5,"$t5.$i4=$t1.$w5");
		return $this->db->get()->result();
	}
}
