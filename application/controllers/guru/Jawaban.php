<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawaban extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }

  // Untuk View Uts
  public function index()
  {
    $isi['judul']   = "Kunci Jawaban Uts";
    $isi['content'] = "backend/guru/dt_jawaban/uts/v_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function ulangan()
  {
    $isi['judul']   = "Kunci Jawaban Ulangan";
    $isi['content'] = "backend/guru/dt_jawaban/ulangan/v_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function t_uts()
  {
    $isi['judul']   = "Tambah Kunci Jawaban";
    $isi['content'] = "backend/guru/dt_jawaban/uts/t_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function insert_uts()
  {
    // code...
  }

  public function u_uts()
  {
    $isi['judul']   = "Update Kunci Jawaban";
    $isi['content'] = "backend/guru/dt_jawaban/uts/u_uts";
    $this->load->view("backend/guru/home", $isi);
  }

  public function update_uts()
  {
    // code...
  }

  public function delete_uts()
  {
    // code...
  }

  public function t_ulangan()
  {
    $isi['judul']   = "Tambah Kunci Jawaban";
    $isi['content'] = "backend/guru/dt_jawaban/ulangan/t_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function insert_ulangan()
  {
    // code...
  }

  public function u_ulangan()
  {
    $isi['judul']   = "Update Kunci Jawaban";
    $isi['content'] = "backend/guru/dt_jawaban/ulangan/u_ulangan";
    $this->load->view("backend/guru/home", $isi);
  }

  public function update_ulangan()
  {
    // code...
  }

  public function delete_ulangan()
  {
    // code...
  }
}
