<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="row clearfix">
          <form action="<?= base_url(); ?>guru/Soal/t_uts_pilgan" method="post">
            <div class="col-md-6">

              <input type="hidden" name="guru" value="<?= $this->session->userdata('id') ?>">

              <label for="kode_soal">Kode Soal</label>
              <div class="input-group">
                  <span class="input-group-addon">
                      <i class="material-icons">local_offer</i>
                  </span>
                  <div class="form-line">
                      <input type="text" class="form-control date" required="required" name="kode_soal" placeholder="Nama Jurusan">
                  </div>
                  <b class="text-danger"><?= form_error('kode_soal'); ?></b>
              </div>

              <label for="mapel">Mapel</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span>
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="mapel" data-live-search="true">
                      <option value="">-- Pilih Mapel --</option>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('mapel'); ?></b>
              </div>

            </div>

            <div class="col-md-6">

              <label for="jurusan">Jurusan</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span>
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="jurusan" data-live-search="true">
                      <option value="">-- Pilih Jurusan --</option>
                      <?php
                        foreach ($data as $dt) {
                          echo "<option value='".$dt->id_jurusan."'>".$dt->nama_jurusan."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('jurusan'); ?></b>
              </div>

              <label for="kelas">Kelas</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span>
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="kelas" data-live-search="true">
                      <option value="">-- Pilih Kelas --</option>
                      <?php
                        foreach ($data as $dt) {
                          echo "<option value='".$dt->id_kelas."'>".$dt->nama_kelas."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('kelas'); ?></b>
              </div>

            </div>
            
            <div id="lembar_soal" class="col-md-12">
              <?php for ($i=1; $i <= 10; $i++) { ?>
              <label for="soal">Soal No <?= $i; ?></label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">feedback</i>
                </span>
                <div class="form-line">
                    <input type="text" required="required" class="form-control date" name="soal[]" placeholder="Masukan Soal">
                </div>
              </div>

              <label for="jurusan">Kunci Jawaban</label>
              <div class="input-group">

                  <div class="col-md-6">
                    <span class="input-group-"></span>
                    <div class="form-line">
                        <input type="text" class="form-control date" required="required" name="kunci[]" placeholder="Jawaban">
                    </div>
                  </div>
              </div>
              <?php } ?>
            </div>
            <div class="col-md-12">
              <div class="input-group">
                  <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                  <button type="submit" name="button" class="btn btn-success waves-effect waves-light pull-right"><span>Update Soal Uts</span> <i class="material-icons">update</i> </button>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_eesay').addClass('active');
  });
</script>
