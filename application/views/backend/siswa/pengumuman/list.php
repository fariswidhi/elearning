<div class="row clearfix">

  <?php if ($data->num_rows()>0): ?>
    <?php foreach ($data->result() as $dt): ?>
      
                <div class="col-lg-4">
                    <div class="card">
                        <div class="body">
                                                      <div class="pull-right">
                                                          <span><?php echo date('d-m-Y',strtotime($dt->tgl)) ?></span>
                            </div>
                            <h2><?php echo $dt->judul ?></h2>

                            <a class="btn btn-primary"  href="<?php echo base_url('siswa/Pengumuman/detail/'.$dt->id_pengumuman) ?>">DETAIL</a>
                        </div>
                    </div>
                </div>
    <?php endforeach ?>
  <?php endif ?>

            </div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#home').addClass('active');
  });
</script>
