<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller
{


  protected $table="tb_siswa";
  protected $folder = "backend/admin/siswa/";
  protected $primaryKey= "id";
  protected $base = "Siswa";

  function __construct()
  {
    parent::__construct();
  }


  public function forms(){


    $jurusanData = $this->db->get('tb_jurusan');

    if ($jurusanData->num_rows()>0) {
      # code...
      $jurusan = [];

      foreach ($jurusanData->result_array() as $jr) {
        # code...
        $jurusan[]= [
          'value'=>$jr['id_jurusan'],
          'text'=>$jr['nama_jurusan']
        ];
      }
    }
  





    $kelasData = $this->db->get('tb_kelas');

    if ($kelasData->num_rows()>0) {
      # code...
      $kelas = [];

      foreach ($kelasData->result_array() as $jr) {
        # code...
        $kelas[]= [
          'value'=>$jr['id_kelas'],
          'text'=>$jr['nama_kelas']
        ];
      }
    }




  $forms  = [

      1=>[
        [

        'name'=>'NIS',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'nis'
        ]

        ],
        [

        'name'=>'Nama Siswa',
                'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'nama_siswa'
        ]

        ],

        [

        'name'=>'Jenis Kelamin',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'jk',
          'data'=>[
            ['value'=>'L','text'=>'Laki - Laki'],
            ['value'=>'P','text'=>'Perempuan'],
          ]
        ]
        ],


        [

        'name'=>'Jurusan',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'jurusan',
          'data'=>$jurusan
        ]
        ],

        [

        'name'=>'Kelas',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'id_kelas',
          'data'=>$kelas
        ]
        ],

        
      ],

      2=>
        [
[

        'name'=>'Tempat Lahir',
        'list'=>[
          'type'=>'text',
          'field'=>'tempat_lahir'
        ]

        ],

        [

        'name'=>'Tanggal Lahir',
        'list'=>[
          'type'=>'date',
          'field'=>'tgl_lahir'
        ]

        ],


        [

        'name'=>'Agama',
        'list'=>[
          'type'=>'select',
          'field'=>'agama',
          'data'=>[
            ['value'=>'Islam','text'=>'Islam'],
            ['value'=>'Kristen','text'=>'Kristen'],
            ['value'=>'Katholik','text'=>'Katholik'],
            ['value'=>'Hindu','text'=>'Hindu'],
            ['value'=>'Budha','text'=>'Budha']
          ]
        ]
        ],
  [

        'name'=>'Alamat',
        'list'=>[
          'type'=>'textarea',
          'field'=>'alamat'
        ]

        ],
        [

        'name'=>'Foto',
        'list'=>[
          'type'=>'file',
          'field'=>'foto'
        ]

        ],


        [

        'name'=>'Username',
        'list'=>[
          'type'=>'username',
          'field'=>'username'
        ]

        ],
        [

        'name'=>'Password',
        'list'=>[
          'type'=>'password',
          'field'=>'password'
        ]

        ],
        ]
    ];

    // print_r($forms);
    return $forms;

    // echo json_encode($forms);


  }
  public function index()
  {
    $this->model_security->getsecurity();
    $data    = $this->model_all->get_all($this->table)->result();

    $sql  =  "SELECT * FROM tb_siswa a inner join tb_kelas b on a.id_kelas = b.id_kelas inner JOIN tb_jurusan c on a.jurusan = c.id_jurusan";
    $data = $this->db->query($sql)->result();
    $judul   = "Siswa";
    $content = "backend/admin/siswa/table";
    $forms = $this->forms();

    $this->load->view("backend/admin/home", compact('data','judul','content','forms'));
  }
  
  public function edit($id){

    if ($this->input->post()) {
      # code...

$nis = $this->input->post("nis");
$nama_siswa = $this->input->post("nama_siswa");
$jk = $this->input->post("jk");
$jurusan = $this->input->post("jurusan");

$kelas = $this->input->post("id_kelas");

$tempat_lahir = $this->input->post("tempat_lahir");
$tgl_lahir = $this->input->post("tgl_lahir");
$agama = $this->input->post("agama");
$alamat = $this->input->post("alamat");
$foto = $this->input->post("foto");
$username = $this->input->post("username");
$password = $this->input->post("password");


$datane = array(
"nis" => $nis,
"nama_siswa" => $nama_siswa,
"jk" => $jk,
"jurusan" => $jurusan,
"id_kelas" => $kelas,
"tempat_lahir" => $tempat_lahir,
"tgl_lahir" => $tgl_lahir,
"agama" => $agama,
"alamat" => $alamat,
// "foto" => $foto,
);


$userid = $this->db->get_where('tb_siswa',['id'=>$id])->result()[0]->id_user;

if (!empty($password)) {
  $this->db->update('tb_user',['pass1'=>hash_string($password),'pass2'=>$password],['id_user'=>$userid]);
}


$where = array("id"=>$id);


if (!empty($password)) {

}
$this->db->update($this->table,$datane,$where);


      alert_success($this->base.' Berhasil Diubah');
      return redirect('admin/'.$this->base);
    }
    else{
    $judul   = "Guru / Update Guru";
    $content = $this->folder."edit";
    $forms = $this->forms();


    $data  = $this->model_all->get_where($this->table,["id"=>$id]);
    if ($data->num_rows()==1) {
      $userid= $data->result()[0]->id_user;
      # code...

      $data = $data->result_object()[0];
      $judul = "Edit Data ";
      $content = $this->folder.'edit';

      $user = $this->db->get_where('tb_user',['id_user'=>$userid]);

      // print_r($user->result());
      // print_r($user->num_rows());
    $this->load->view("backend/admin/home", compact('content','judul','forms','data','user'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }

    }

    // print_r($data);
  }


  public function detail($id){

    $forms = $this->forms();


    $data  = $this->model_all->get_where($this->table,["id"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Detail Data ";
      $content = $this->folder.'detail';

          $sql  =  "SELECT *,c.nama_jurusan as jurusan,b.nama_kelas as id_kelas FROM tb_siswa a inner join tb_kelas b on a.id_kelas = b.id_kelas inner JOIN tb_jurusan c on a.jurusan = c.id_jurusan WHERE a.id='".$id."'";

//           echo $sql;
// exit();

          $data = $this->db->query($sql)->result_object()[0];
          // print_r($data);
    $this->load->view("backend/admin/home", compact('content','judul','forms','data'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }



  }
  public function dump_insert(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$this->db->insert("'.$table.'",$datane);';

  }


  public function dump_update(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$where = array("'.$this->primaryKey.'"=>$id);<br>';
    echo '$this->db->update("'.$table.'",$datane,$where);';

  }
  public function create(){

    if ($this->input->post()) {
      # code... 
      $post = $this->input->post();

      // foreach ($post as $key => $value) {
      //   # code...
      //   echo $key."<br>";
      // }


      $nig = $this->input->post("nig");
      $nama = $this->input->post("nama_guru");
      $jk = $this->input->post("jk");
      $bidang_studi = $this->input->post("bidang_studi");
      $tmpt = $this->input->post("tempat_lahir");
      $tgl = $this->input->post("tgl_lahir");
      $agama = $this->input->post("agama");
      $almt = $this->input->post("alamat");
      $username = $this->input->post("username");
      $password = $this->input->post("password");
      $button = $this->input->post("button");



      $userdata = array(
        'username'=>$username,
        'pass1'=>hash_string($password),
        'pass2'=>$password,
        'level'=>3
      );


      $this->db->insert('tb_user',$userdata);

      // GET USERID
      $userid=  $this->db->insert_id();

      // print_r($userid);
$nis = $this->input->post("nis");
$nama_siswa = $this->input->post("nama_siswa");
$jk = $this->input->post("jk");
$jurusan = $this->input->post("jurusan");
$tempat_lahir = $this->input->post("tempat_lahir");
$tgl_lahir = $this->input->post("tgl_lahir");
$agama = $this->input->post("agama");
$alamat = $this->input->post("alamat");
$foto = $this->input->post("foto");
$kelas = $this->input->post('id_kelas');



    $config['upload_path']          = 'gambar/profile';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 100;
    $config['max_width']            = 1024;
    $config['max_height']           = 768;
    $this->load->library('upload', $config);
      $this->upload->initialize($config);
    // var_dump($_REQUEST);
    if ( ! $this->upload->do_upload('foto')){

      $error = array('error' => $this->upload->display_errors());

$datane = array(
"nis" => $nis,
'id_user' =>$userid,
"nama_siswa" => $nama_siswa,
"jk" => $jk,
"id_kelas" => $kelas,
"jurusan" => $jurusan,
"tempat_lahir" => $tempat_lahir,
"tgl_lahir" => $tgl_lahir,
"agama" => $agama,
"alamat" => $alamat,
);

// exit();

    }else{
      $dataFoto = $this->upload->data();

      $filename =   $this->upload->data()['file_name'];


$datane = array(
"nis" => $nis,
'id_user' =>$userid,
"nama_siswa" => $nama_siswa,
"jk" => $jk,
"id_kelas" => $kelas,
"jurusan" => $jurusan,
"tempat_lahir" => $tempat_lahir,
"tgl_lahir" => $tgl_lahir,
"agama" => $agama,
"alamat" => $alamat,
"foto" => 'gambar/profile/'.$filename,
);

    }

$this->db->insert("tb_siswa",$datane);
      // $data  =  $this->model_all->
      alert_success('Data Berhasil Ditambahkan');
      return redirect('admin/'.$this->base);


    }
    else{

    $content = $this->folder."create";
      $judul = "Tambah Data ";


  $forms = $this->forms();
   $this->load->view('backend/admin/home',compact('content','judul','forms')) ;
    }
  }

  public function delete($id){

    $where  = array("id"=>$id);
    $this->model_all->del_id($this->table,$where);

          return redirect('admin/'.$this->base);

  }

}
