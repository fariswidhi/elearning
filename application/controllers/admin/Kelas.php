<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->model_security->getsecurity();
    $isi['data']    = $this->model_all->get_all('tb_kelas')->result();
    $isi['judul']   = "Kelas";
    $isi['content'] = "backend/admin/dt_kelas/v_kelas";
    $this->load->view("backend/admin/home", $isi);
  }

  public function t_kelas()
  {
    $this->model_security->getsecurity();
    $isi['judul']   = "Kelas / Tambah Kelas";
    $isi['content'] = "backend/admin/dt_kelas/t_kelas";
    $this->load->view("backend/admin/home", $isi);
  }

  public function insert_kelas()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
      array('required'  => 'Kelas Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE)
    {
      return $this->t_kelas();
    }
    else
    {
      $data = array('nama_kelas'  => $this->input->post('kelas'));
      $this->model_all->insert('tb_kelas', $data);
      alert_success('Kelas Berhasil Ditambahkan');
      return redirect('admin/Kelas');
    }
  }

  public function u_kelas()
  {
    $this->model_security->getsecurity();
    $id_kelas = $this->uri->segment(4);
    $where    = array('id_kelas'  => $id_kelas);
    $data     = $this->model_all->get_where('tb_kelas', $where)->result();
    foreach ($data as $dt) {
      $isi['id_kelas']    = $dt->id_kelas;
      $isi['nama_kelas']  = $dt->nama_kelas;
    }
    $isi['judul']   = "Kelas / Update Kelas";
    $isi['content'] = "backend/admin/dt_kelas/u_kelas";
    $this->load->view("backend/admin/home", $isi);
  }

  public function update_kelas()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('kelas', 'Kelas', 'required',
      array('required'  =>  'Kelas Tidak Boleh Kosong')
    );

    if($this->form_validation->run() == FALSE)
    {
        return $this->u_kelas();
    }
    else
    {
        $id_kelas = $this->input->post('id_kelas');
        $where    = array('id_kelas'  => $id_kelas);
        $data     = array('nama_kelas'  => $this->input->post('kelas'));
        $this->model_all->update('tb_kelas', $data, $where);
        alert_success('Kelas Berhasil Diupdate');
        return redirect('admin/Kelas');
    }
  }

  public function delete_kelas()
  {
    $this->model_security->getsecurity();
    $id_kelas = $this->uri->segment(4);
    $where    = array('id_kelas'  => $id_kelas);
    $this->model_all->del_id('tb_kelas', $where);
    alert_danger('Kelas Berhasil Dihapus');
    return redirect('admin/Kelas');
  }
}
