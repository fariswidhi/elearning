<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">
        <div class="row">
        	<div class="col-lg-6">
        		<table class="table table-bordered">
        			<tr>
        				<td>Nama Siswa</td><td><?php echo $info->result()[0]->nama_siswa ?></td>
        			</tr>
        			<tr>
        				<td>Nama Modul</td><td><?php echo $info->result()[0]->nama_modul ?></td>
        			</tr>
              <tr>
                <td>Min Nilai</td><td><?php echo $info->result()[0]->min_nilai ?></td>
              </tr>
              <tr>
                <td>Max Nilai</td><td><?php echo $info->result()[0]->max_nilai ?></td>
              </tr>

        		</table>
        	</div>
        </div>
      </div>
    </div>
  </div>


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
          	Jawaban 
          </h2>

      </div>
      <div class="body">
      	 <?php foreach ($data->result() as $d): 


          ?>
  
      <div style="border: 1px solid #eee;margin: 10px;padding: 10px;">
        <div class="pull-right">
          <button data-false="false" data-id="<?php echo $d->idne ?>" <?php echo $d->benare==1 ? 'disabled':' ' ?>  class="btn btn-success  btn-click btn-benar-<?php echo $d->idne ?> ">SET BENAR</button>
          <button data-false="true" data-id="<?php echo $d->idne ?>" <?php echo $d->is_click==1 ? ($d->benare==0 ? 'disabled':''):' ' ?> class="btn btn-danger   btn-click btn-salah-<?php  echo $d->idne ?> ">SET SALAH</button>
        </div>
        <h3>        <?php           echo $d->soal; ?></h3>
        <h6  style="color: green">Jawaban Benar:</h6>
        <?php echo $d->jawaban ?>
        <h6 style="color: orange">Jawaban Siswa:</h6>
          <?php echo $d->jawaban_essai ?>
      </div>           
         <?php endforeach ?>
         <div class="row">
           <div class="col-lg-3">
            <label>Total Soal</label>
  <input type="text" name="" class="form-control" id="total">
</div>
<div class="col-lg-3">
  <label>Salah</label>
  <input type="text" name="" class="form-control" id="salah">
</div>
<div class="col-lg-3">
  <label>Benar</label>
  <input type="text" name="" class="form-control" id="benar">
</div>
  <div class="col-lg-3" >
  <label>Belum Dikoreksi</label>
  <input type="text" name="" class="form-control" id="belum">
</div>




</div>
<div class="col-lg-3" style="margin:  0 auto;float: none;">
  <label>Nilai</label>
  <input type="text" name="" class="form-control" id="poin">
  <br>
  <center>  <button class="btn btn-primary btn-save-koreksi">SIMPAN</button></center>
</div>
<br>
<br>

</center>
         </div>
      </div>

  </div>
</div>



</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#soal').addClass('active');
    $('#soal_ulangan').addClass('active');
    $('#soal_ulangan_eesay').addClass('active');
  });

  $(document).on('click','.btn-click',function(){
    var id = $(this).attr('data-id');

    var salah = $(this).attr('data-false');

    var state = 0;
      if (salah=="true") {
        state =0;
      }
      else{
        state =1;
      }

      $.ajax({
        url: "<?php echo base_url('guru/Essai/set_koreksi') ?>",
        data: {
          id :id,
          state:state
        },
        method: "POST",
        dataType:"JSON",
        success:function(res){
          if (state==0) {
            $(".btn-benar-"+id).removeAttr('disabled');
            $(".btn-salah-"+id).attr('disabled','disabled');
          }
          else{

            $(".btn-salah-"+id).removeAttr('disabled');

            $(".btn-benar-"+id).attr('disabled','disabled');
          }

          $("#total").val(res.total);
          $("#salah").val(res.salah);
          $("#benar").val(res.benar);

          $("#poin").val(res.poin);
          $("#belum").val(res.belum);
        }
      })

  })


      $.ajax({
        url: "<?php echo base_url('guru/Essai/getState/'.$this->uri->segment(4)) ?>",
        method: "GET",
        dataType:"JSON",
        success:function(res){
          $("#poin").val(res.poin);
          $("#total").val(res.total);
          $("#salah").val(res.salah);
          $("#benar").val(res.benar);
          $("#belum").val(res.belum);
        }
      })

      $(document).on('click','.btn-save-koreksi',function(){
        var soal = $("#total").val();
        var benar = $("#benar").val();
        var salah = $("#salah").val();

        var poin = $("#poin").val();
        $.ajax({
          url: "<?php echo base_url('guru/Essai/set_poin') ?>",
          data: {
            soal:soal,
            benar:benar,
            salah:salah,
            poin:poin,
            idne : "<?php echo $this->uri->segment(4) ?>"
          },
          method:"POST",
          success:function(res){
            alert("Berhasil Mengoreksi Soal Essai");
          }
        })
      });
</script>



