<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="<?= base_url(); ?>assets/back_end/images/user.png" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->session->userdata('nama_admin'); ?></div>
            <div class="email">Administrator</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">

                    <li><a href="<?= base_url(); ?>Login/logout"><i class="material-icons">input</i>Log Out</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li id="home">
                <a href="<?= base_url(); ?>Home">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li id="data_master">
                <a href="#" class="menu-toggle">
                    <i class="material-icons">view_module</i>
                    <span>Data Master</span>
                </a>
                <ul class="ml-menu">
                    <li id="data_user"><a href="<?= base_url(); ?>admin/User">Data user</a></li>
                    <li id="data_kelas"><a href="<?= base_url(); ?>admin/Kelas">Data Kelas</a></li>
                    <li id="data_mapel"><a href="<?= base_url(); ?>admin/Mapel">Data Mata Pelajaran</a></li>
                    <li id="data_jurusan"><a href="<?= base_url(); ?>admin/Jurusan">Data Jurusan</a></li>
                </ul>
            </li>
            <li id="guru">
                <a href="<?= base_url(); ?>admin/Guru">
                    <i class="material-icons">people</i>
                    <span>Guru</span>
                </a>
            </li>
            <li id="siswa">
                <a href="<?= base_url(); ?>admin/Siswa">
                    <i class="material-icons">people_outline</i>
                    <span>Siswa</span>
                </a>
            </li>
            <li id="pengumuman">
                <a href="<?= base_url(); ?>admin/Pengumuman">
                    <i class="material-icons">notifications</i>
                    <span>Pengumuman</span>
                </a>
            </li>
            <li id="laporan">
                <a href="#" class="menu-toggle">
                    <i class="material-icons">picture_as_pdf</i>
                    <span>Laporan</span>
                </a>
                <ul class="ml-menu">
                    <li id="lap_guru"><a href="<?= base_url(); ?>admin/Laporan">Laporan Data Guru</a></li>
                    <li id="lap_siswa"><a href="<?= base_url(); ?>admin/Laporan/lap_siswa">Laporan Data Siswa</a></li>
                    <li id="lap_kelas"><a href="<?= base_url(); ?>admin/Laporan/lap_kelas">Laporan Data Kelas</a></li>
                    <li id="lap_mapel"><a href="<?= base_url(); ?>admin/Laporan/lap_mapel">Laporan Data Mata Pelajaran</a></li>
                    <li id="lap_jurusan"><a href="<?= base_url(); ?>admin/Laporan/lap_jurusan">Laporan Data Jurusan</a></li>
                    <li id="lap_pengumuman"><a href="<?= base_url(); ?>admin/Laporan/lap_pengumuman">Laporan Data Pengumuman</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- #Menu -->
    <?php $this->load->view('backend/layout/copyright'); ?>
</aside>
<!-- #END# Left Sidebar -->
