<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>
      </div>
      <div class="body">
        <!-- <a href="#">
            <button name="button" class="btn btn-md btn-primary"> <span>Tambah Nilai</span> <i class="material-icons">add_circle</i> </button>
        </a>
        <br>
        <br> -->
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
            <thead>
              <tr>
                <th width="10">No</th>
                <th>Nama Siswa</th>
                <th>Kelas</th>
                <th>Jurusan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="center">
                  <a href="<?= base_url(); ?>guru/Nilai/detail_uts">
                    <button type="button" name="button" class="btn btn-circle btn-info waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Lihat Nilai">
                      <i class="material-icons">search</i>
                    </button>
                  </a>
                  <a href="<?= base_url(); ?>guru/Nilai/delete_uts">
                    <button type="button" name="button" onclick="return confirm('Yakin Ingin Menghapus Nilai...???')" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus Nilai">
                      <i class="material-icons">delete</i>
                    </button>
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#nilai').addClass('active');
    $('#nilai_uts').addClass('active');
    $('#nilai_uts_pilgan').addClass('active');
  });
</script>
