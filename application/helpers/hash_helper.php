<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('encode'))
{
    function encode($data)
    {
        $CI=& get_instance();
        $key = $CI->config->item('encryption_key');
        $CI->encryption->initialize(
            array(
                    'cipher' => 'aes-256',
                    'mode' => 'ctr',
                    'key' => $key
            )
        );
        return $CI->encryption->encrypt($data);
    }
}

if(!function_exists('decode'))
{
    function decode($data)
    {
        $CI=& get_instance();
        return $CI->encryption->decrypt($data);
    }
}

if(!function_exists('hash_string'))
{
    function hash_string($string)
    {
        $hashed_string = password_hash($string, PASSWORD_BCRYPT, ['cost' => 9]);
        return $hashed_string;
    }
}

if(!function_exists('hash_verify'))
{
    function hash_verify($plain_text, $hashed_string)
    {
        $hashed_string = password_verify($plain_text, $hashed_string);
        return $hashed_string;
    }
}