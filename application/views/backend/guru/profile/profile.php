<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              <?= $judul; ?>
          </h2>

      </div>
      <div class="body">

        <div class="col-lg-4" style="float: none;margin: 0 auto;">
          
        <center><img style="width: 100px;height: 100px;border-radius: 50%;" src="<?php echo base_url($data->result()[0]->foto) ?>">
          <form action="<?php echo base_url('guru/Profile/upload') ?>" method="post" enctype="multipart/form-data">
            <input type="file" name="file"  class="form-control">
            <br>
            <button type="submit" class="btn btn-primary">GANTI FOTO PROFIL</button>
            <br>
            <br>
          </form>
        </center>
        </div>
        <?php if ($data->num_rows()>0): ?>
          <table class="table table-striped">
            <tr>
              <td>Nama</td><td><?php echo $data->result()[0]->nama_guru ?></td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td><td><?php echo $data->result()[0]->jk ?></td>
            </tr>
            <tr>
              <td>Bidang Studi</td><td><?php echo $data->result()[0]->bidang_studi ?></td>
            </tr>
            <tr>
              <td>Tempat Lahir</td><td><?php echo $data->result()[0]->tempat_lahir ?></td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td><td><?php echo date('d-m-Y',strtotime($data->result()[0]->tgl_lahir)) ?></td>
            </tr>
            <tr>
              <td>Agama</td><td><?php echo $data->result()[0]->agama ?></td>
            </tr>
            <tr>
              <td>Alamat</td><td><?php echo $data->result()[0]->alamat ?></td>
            </tr>
          </table>
        <?php endif ?>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#home').addClass('active');
  });
</script>
