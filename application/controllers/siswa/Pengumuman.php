<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {

	public function index()
	{
		$this->model_security->getsecurity();
    	$isi['judul']   = "Pengumuman";
    	$isi['content'] = "backend/siswa/pengumuman/list";
    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');
    	$userid = $this->session->userdata('id');

    	$siswa = $this->db->get_where('tb_siswa',['id_user'=>$userid]);
    	$siswa = $siswa->result()[0];
    	$jurusan = $siswa->jurusan;
    	$kelas= $siswa->id_kelas;


    	$isi['data'] = $this->db->get('tb_pengumuman');
    	// print_r($kelas);


    	$this->load->view("backend/siswa/home", $isi);
	}
		public function detail($id)
	{
		$this->model_security->getsecurity();
    	$isi['judul']   = "Pengumuman";
    	$isi['content'] = "backend/siswa/pengumuman/detail";
    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');
    	$userid = $this->session->userdata('id');

    	$siswa = $this->db->get_where('tb_siswa',['id_user'=>$userid]);
    	$siswa = $siswa->result()[0];
    	$jurusan = $siswa->jurusan;
    	$kelas= $siswa->id_kelas;


    	$isi['data'] = $this->db->get_where('tb_pengumuman',['id_pengumuman'=>$id]);
    	// print_r($kelas);


    	$this->load->view("backend/siswa/home", $isi);
	}
}
