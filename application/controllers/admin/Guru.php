<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller
{


  protected $table="tb_guru";
  protected $folder = "backend/admin/dt_guru/";
  protected $primaryKey= "id";
  protected $base = "Guru";

  function __construct()
  {
    parent::__construct();
  }


  public function forms(){
    $forms  = [

      1=>[
        [

        'name'=>'Nomor Induk Guru',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'nig'
        ]

        ],
        [

        'name'=>'Nama Guru',
                'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'nama_guru'
        ]

        ],

        [

        'name'=>'Jenis Kelamin',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'select',
          'field'=>'jk',
          'data'=>[
            ['value'=>'L','text'=>'Laki - Laki'],
            ['value'=>'P','text'=>'Perempuan'],
          ]
        ]
        ],


        [

        'name'=>'Bidang Studi',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'bidang_studi'
        ]

        ],
        
      ],

      2=>
        [
[

        'name'=>'Tempat Lahir',
        'list'=>[
          'type'=>'text',
          'field'=>'tempat_lahir'
        ]

        ],

        [

        'name'=>'Tanggal Lahir',
        'list'=>[
          'type'=>'date',
          'field'=>'tgl_lahir'
        ]

        ],


        [

        'name'=>'Agama',
        'list'=>[
          'type'=>'select',
          'field'=>'agama',
          'data'=>[
            ['value'=>'Islam','text'=>'Islam'],
            ['value'=>'Kristen','text'=>'Kristen'],
            ['value'=>'Katholik','text'=>'Katholik'],
            ['value'=>'Hindu','text'=>'Hindu'],
            ['value'=>'Budha','text'=>'Budha']
          ]
        ]
        ],
  [

        'name'=>'Alamat',
        'list'=>[
          'type'=>'textarea',
          'field'=>'alamat'
        ]

        ],
        [

        'name'=>'Foto',
        'list'=>[
          'type'=>'file',
          'field'=>'foto'
        ]

        ],


        [

        'name'=>'Username',
        'list'=>[
          'type'=>'username',
          'field'=>'username'
        ]

        ],
        [

        'name'=>'Password',
        'list'=>[
          'type'=>'password',
          'field'=>'password'
        ]

        ],
        ]
    ];

    // print_r($forms);
    return $forms;

    // echo json_encode($forms);


  }
  public function index()
  {
    $this->model_security->getsecurity();
    $data    = $this->model_all->get_all($this->table)->result();
    $judul   = "Guru";
    $content = "backend/admin/dt_guru/table";
    $forms = $this->forms();

    $this->load->view("backend/admin/home", compact('data','judul','content','forms'));
  }

  public function edit($id){

    if ($this->input->post()) {
      # code...

   
      $nig = $this->input->post("nig");
      $nama = $this->input->post("nama_guru");
      $jk = $this->input->post("jk");
      $bidang_studi = $this->input->post("bidang_studi");
      $tempat_lahir = $this->input->post("tempat_lahir");
      $tgl_lahir = $this->input->post("tgl_lahir");
      $agama = $this->input->post("agama");
      $almt = $this->input->post("alamat");
      $username = $this->input->post("username");
      $password = $this->input->post("password");
      $button = $this->input->post("button");

// $datane = array(
// "nig" => $nig,
// "nama_guru" => $nama,
// "jk" => $jk,
// "bidang_studi" => $bidang_studi,
// "tempat_lahir" => $tempat_lahir,
// "tgl_lahir" => $tgl_lahir,
// "agama" => $agama,
// "alamat" => $almt,
// "foto" => $foto,
// );

$userid = $this->input->post('userid');



      $username = $this->input->post("username");
      $password = $this->input->post("password");




    $config['upload_path']          = 'gambar/profiles';
    $config['allowed_types']        = 'gif|jpg|png';

    $this->load->library('upload', $config);
      $this->upload->initialize($config);
    // var_dump($_REQUEST);
    if ( ! $this->upload->do_upload('foto')){

      $error = array('error' => $this->upload->display_errors());

      print_r($error);
      $gurudata = array(

"nig" => $nig,
"nama_guru" => $nama,
"jk" => $jk,
"bidang_studi" => $bidang_studi,
"tempat_lahir" => $tempat_lahir,
"tgl_lahir" => $tgl_lahir,
"agama" => $agama,
"alamat" => $almt,

      );

// exit();

    }else{
      $dataFoto = $this->upload->data();

      $filename =   $this->upload->data()['file_name'];


   $gurudata = array(

"nig" => $nig,
"nama_guru" => $nama,
"jk" => $jk,
"bidang_studi" => $bidang_studi,
"tempat_lahir" => $tempat_lahir,
"tgl_lahir" => $tgl_lahir,
"agama" => $agama,
"alamat" => $almt,
"foto" => 'gambar/profiles/'.$filename,

      );

    }


// print_r($gurudata);



// exit();
  $where = array("id"=>$id);
    $this->db->update("tb_guru",$gurudata,$where);


// print_r($datane);


  if (!empty($password)) {
    # code...
    $this->db->update("tb_guru",["pass1"=>hash_string($password),"pass2"=>$password],["id_user"=>$userid]);
  }
      alert_success('Guru Berhasil Diubah');
      return redirect('admin/Guru');
    }
    else{
    $judul   = "Guru / Update Guru";
    $content = $this->folder."edit";
    $forms = $this->forms();


    $data  = $this->model_all->get_where("tb_guru",["id"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Edit Data ";
      $content = $this->folder.'edit';
      $userid= $data->id_user;

      // print_r($userid);
      $user = $this->db->get_where('tb_user',['id_user'=>$userid]);

      // print_r($user->num_rows());

  // print_r($user->result())      ;
    $this->load->view("backend/admin/home", compact('content','judul','forms','data','user'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }

    }

    // print_r($data);
  }


  public function detail($id){

    $forms = $this->forms();


    $data  = $this->model_all->get_where("tb_guru",["id"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Detail Data ";
      $content = $this->folder.'detail';
    $this->load->view("backend/admin/home", compact('content','judul','forms','data'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }



  }
  public function dump_insert(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$this->db->insert("'.$table.'",$datane);';

  }


  public function dump_update(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$where = array("'.$this->primaryKey.'"=>$id);<br>';
    echo '$this->db->update("'.$table.'",$datane,$where);';

  }
  public function create(){

    if ($this->input->post()) {
      # code... 
      $post = $this->input->post();

      // foreach ($post as $key => $value) {
      //   # code...
      //   echo $key."<br>";
      // }


      $nig = $this->input->post("nig");
      $nama = $this->input->post("nama_guru");
      $jk = $this->input->post("jk");
      $bidang_studi = $this->input->post("bidang_studi");
      $tmpt = $this->input->post("tempat_lahir");
      $tgl = $this->input->post("tgl_lahir");
      $agama = $this->input->post("agama");
      $almt = $this->input->post("alamat");
      $username = $this->input->post("username");
      $password = $this->input->post("password");
      $button = $this->input->post("button");



      $userdata = array(
        'username'=>$username,
        'pass1'=>hash_string($password),
        'pass2'=>$password,
        'level'=>2
      );


      $this->db->insert('tb_user',$userdata);

      // GET USERID
      $userid=  $this->db->insert_id();

      // print_r($userid);


    $config['upload_path']          = 'gambar/profiles';
    $config['allowed_types']        = 'gif|jpg|png';
    $this->load->library('upload', $config);
      $this->upload->initialize($config);
    // var_dump($_REQUEST);
    if ( ! $this->upload->do_upload('foto')){

      $error = array('error' => $this->upload->display_errors());
      $gurudata = array(
'nig' =>$nig,
'id_user' =>$userid,
'nama_guru' =>$nama,
'jk' =>$jk,
'bidang_studi' =>$bidang_studi,
'tempat_lahir' =>$tmpt,
'tgl_lahir' =>$tgl,
'agama' =>$agama,
'alamat' =>$almt,
      );

// exit();

    }else{
      $dataFoto = $this->upload->data();

      $filename =   $this->upload->data()['file_name'];


   $gurudata = array(
'nig' =>$nig,
'id_user' =>$userid,
'nama_guru' =>$nama,
'jk' =>$jk,
'bidang_studi' =>$bidang_studi,
'tempat_lahir' =>$tmpt,
'tgl_lahir' =>$tgl,
'agama' =>$agama,
'alamat' =>$almt,
"foto" => 'gambar/profiles/'.$filename,

      );

    }

// print_r($gurudata);

// exit();

      $this->db->insert('tb_guru',$gurudata);
      // $data  =  $this->model_all->
      alert_success('Data Berhasil Ditambahkan');
      return redirect('admin/'.$this->base);


    }
    else{

    $content = $this->folder."create";
      $judul = "Tambah Data ";


  $forms = $this->forms();
   $this->load->view('backend/admin/home',compact('content','judul','forms')) ;
    }
  }

  public function delete($id){

    $where  = array("id"=>$id);
    $this->model_all->del_id($this->table,$where);

          return redirect('admin/'.$this->base);

  }

}
