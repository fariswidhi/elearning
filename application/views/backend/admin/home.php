<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>E-Learning - Admin</title>
    <?php $this->load->view('backend/layout/header'); ?>
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Loading...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">E-Learning - SMA Muhammadiyah Jayapura</a>
            </div>
            <!-- <div class="collapse navbar-collapse" id="navbar-collapse"></div> -->
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <?php $this->load->view('backend/admin/menu'); ?>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2><?= $judul; ?></h2>
            </div>
            <?php $this->load->view($content); ?>
        </div>
    </section>
    <?php $this->load->view('backend/layout/footer'); ?>
</body>

</html>
