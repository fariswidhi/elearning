<!-- Jquery Core Js -->
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?= base_url(); ?>assets/back_end/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="<?= base_url(); ?>assets/back_end/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?= base_url(); ?>assets/back_end/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<!-- Custom Js -->
<script src="<?= base_url(); ?>assets/back_end/js/admin.js"></script>
<script src="<?= base_url(); ?>assets/back_end/js/ajax.js"></script>
<script src="<?= base_url(); ?>assets/back_end/js/pages/examples/sign-in.js"></script>
<script src="<?= base_url(); ?>assets/back_end/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url(); ?>assets/back_end/js/pages/ui/tooltips-popovers.js"></script>
<script src="<?= base_url(); ?>assets/back_end/js/pages/forms/advanced-form-elements.js"></script>

<!-- Demo Js -->
<script src="<?= base_url(); ?>assets/back_end/js/demo.js"></script>

