<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data Pengumuman
                </h2>
            </div>
            <div class="body">
                <?php
                  $info = $this->session->flashdata('info');
                  if (!empty($info)) {
                    echo $info;
                  }
                ?>
                <a href="<?= base_url(); ?>admin/Pengumuman/t_pengumuman">
                    <button type="button" class="btn btn-primary waves-effect" name="button"> <span>Tambah Pengumuman</span> <i class="material-icons">add_circle</i> </button>
                </a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>Tanggal Pengumuman</th>
                                <th>Judul Pengumuman</th>
                                <th>Isi Pengumuman</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                              $no = 1;
                              foreach ($data as $dt) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= date_indo($dt->tgl); ?></td>
                                <td><?= $dt->judul; ?></td>
                                <td><?= substr($dt->isi, 0, 180).'....'; ?></td>
                                <td align="center">

                                    <a href="<?= base_url(); ?>admin/Pengumuman/detail_pengumuman/<?= $dt->id_pengumuman ?>">
                                      <button type="button" name="button" class="btn btn-circle btn-info waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Lihat Pengumuman">
                                        <i class="material-icons">search</i>
                                      </button>
                                    </a>
                                    <a href="<?= base_url(); ?>admin/Pengumuman/u_pengumuman/<?= $dt->id_pengumuman ?>">
                                      <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit Pengumuman">
                                        <i class="material-icons">edit</i>
                                      </button>
                                    </a>
                                    <a href="<?= base_url(); ?>admin/Pengumuman/delete_pengumuman/<?= $dt->id_pengumuman ?>">
                                      <button onclick="return confirm('Yakin Ingin Menghapus Pengumunan...???')" type="button" name="button" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus Pengumuman">
                                        <i class="material-icons">delete</i>
                                      </button>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#pengumuman').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
