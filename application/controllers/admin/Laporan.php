<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('pdf_library');
  }

  // Laporan Guru
  public function index()
  {
    $isi['data']    = $this->model_all->get_all('tb_guru')->result();
    $isi['judul']   = "Laporan Data Guru";
    $isi['content'] = "backend/admin/laporan/lap_guru";
    $this->load->view("backend/admin/home", $isi);
  }

  public function lap_siswa()
  {

      $sql  =  "SELECT * FROM tb_siswa a inner join tb_kelas b on a.id_kelas = b.id_kelas inner JOIN tb_jurusan c on a.jurusan = c.id_jurusan";
    $isi['data']    = $this->db->query($sql)->result();
    $isi['judul']   = "Laporan Data Siswa";
    $isi['content'] = "backend/admin/laporan/lap_siswa";
    $this->load->view("backend/admin/home", $isi);
  }

  public function lap_kelas()
  {
    $isi['data']    = $this->model_all->get_all('tb_kelas')->result();
    $isi['judul']   = "Laporan Data Kelas";
    $isi['content'] = "backend/admin/laporan/lap_kelas";
    $this->load->view("backend/admin/home", $isi);
  }

  public function lap_mapel()
  {
    $isi['judul']   = "Laporan Data Mata Pelajaran";
    $isi['content'] = "backend/admin/laporan/lap_mapel";
    $isi['data']    = $this->model_all->get_all('tb_mapel');
    $this->load->view("backend/admin/home", $isi);
  }

  public function lap_jurusan()
  {
    $isi['data']    = $this->model_all->get_all('tb_jurusan')->result();
    $isi['judul']   = "Laporan Data Jurusan";
    $isi['content'] = "backend/admin/laporan/lap_jurusan";
    $this->load->view("backend/admin/home", $isi);
  }

  public function lap_pengumuman()
  {
    $isi['data']    = $this->model_all->get_all('tb_pengumuman')->result();
    $isi['judul']   = "Laporan Data Pengumuman";
    $isi['content'] = "backend/admin/laporan/lap_pengumuman";
    $this->load->view("backend/admin/home", $isi);
  }

  public function cetak_guru()
  {
    $isi['data']  = $this->model_all->get_all('tb_guru')->result();
    $this->load->view("backend/admin/laporan/cetak/guru", $isi);
  }

  public function cetak_jurusan()
  {
    $isi['data']  = $this->model_all->get_all('tb_jurusan')->result();
    $this->load->view("backend/admin/laporan/cetak/jurusan", $isi);
  }

  public function cetak_kelas()
  {
    $isi['data']  = $this->model_all->get_all('tb_kelas')->result();
    $this->load->view("backend/admin/laporan/cetak/kelas", $isi);
  }

  public function cetak_mapel()
  {
    $isi['data']    = $this->model_all->get_all('tb_mapel');

    $this->load->view("backend/admin/laporan/cetak/mapel", $isi);
  }

  public function cetak_pengumuman()
  {
    $isi['data']  = $this->model_all->get_all('tb_pengumuman')->result();
    $this->load->view("backend/admin/laporan/cetak/pengumuman", $isi);
  }

  public function cetak_siswa()
  {

    $sql  =  "SELECT * FROM tb_siswa a inner join tb_kelas b on a.id_kelas = b.id_kelas inner JOIN tb_jurusan c on a.jurusan = c.id_jurusan";


    $isi['data']  = $this->db->query($sql)->result();
    $this->load->view("backend/admin/laporan/cetak/siswa", $isi);
  }

}
