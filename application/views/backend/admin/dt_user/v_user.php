<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Data User
                </h2>
            </div>
            <div class="body">
                <?php
                    $info = $this->session->flashdata('info');
                    if (!empty($info)) {
                      echo $info;
                    }
                ?>
                <a href="<?= base_url(); ?>admin/User/t_user">
                    <button name="button" class="btn btn-md btn-primary"> <span>Tambah User</span> <i class="material-icons">add_circle</i> </button>
                </a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                        <thead>
                            <tr>
                                <th width="10">No</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Level</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                                foreach ($data as $dt) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $dt->username; ?></td>
                                <td><?= $dt->pass2; ?></td>
                                <td>
                                    <?php
                                        $level = $dt->level;
                                        if ($level == 1) {
                                          echo "Admin";
                                        } else if($level == 2) {
                                          echo "Guru";
                                        } else if($level == 3) {
                                          echo "Siswa";
                                        }
                                    ?>
                                </td>
                                <td align="center">
                                    <a href="<?= base_url(); ?>admin/User/u_user/<?= $dt->id_user; ?>">
                                      <button type="button" name="button" class="btn btn-circle btn-success waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Edit User">
                                        <i class="material-icons">edit</i>
                                      </button>
                                    </a>
                                    <a href="<?= base_url(); ?>admin/User/delete_user/<?= $dt->id_user; ?>">
                                      <button type="button" name="button" onclick="return confirm('Yakin Ingin Menghapus User...???')" class="btn btn-circle btn-danger waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Hapus User">
                                        <i class="material-icons">delete</i>
                                      </button>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_user').addClass('active');
  });
</script>
<!-- #END# Exportable Table -->
