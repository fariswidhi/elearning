-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 23, 2018 at 03:00 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 5.6.39-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_absen`
--

CREATE TABLE `tb_absen` (
  `id_absen` int(11) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_absen`
--

INSERT INTO `tb_absen` (`id_absen`, `tgl`, `id_siswa`) VALUES
(5, '2018-12-21 13:46:11', 30);

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_user` int(11) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` enum('Islam','Kristen','Katholik','Budha','Hindu','Kong Hu Chu') NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_user`, `nama_admin`, `jk`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `foto`) VALUES
(11, 'Atfalstyle', '', '-', '0000-00-00', '', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru`
--

CREATE TABLE `tb_guru` (
  `id` int(11) NOT NULL,
  `nig` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `bidang_studi` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` enum('Islam','Kristen','Budha','Katholik','Hindu','Kong Hu Chu') NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_guru`
--

INSERT INTO `tb_guru` (`id`, `nig`, `id_user`, `nama_guru`, `jk`, `bidang_studi`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `foto`) VALUES
(1, 'asdasdasds', 12, 'asdasdasds', '', '-', '-', '0000-00-00', '', '-', '-'),
(3, '', 25, 'Bambang', 'L', '', '', '0000-00-00', 'Islam', 'OK', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'IPA'),
(4, 'IPS');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `nama_kelas`) VALUES
(1, '10'),
(2, '11'),
(3, '12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mapel`
--

INSERT INTO `tb_mapel` (`id_mapel`, `nama_mapel`) VALUES
(3, 'Bahasa Indonesia'),
(5, 'BHS INGGRIS'),
(6, 'Matematika');

-- --------------------------------------------------------

--
-- Table structure for table `tb_materi_tugas`
--

CREATE TABLE `tb_materi_tugas` (
  `id_materi` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `isi` text NOT NULL,
  `tgl` date NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_materi_tugas`
--

INSERT INTO `tb_materi_tugas` (`id_materi`, `jurusan_id`, `kelas_id`, `guru_id`, `isi`, `tgl`, `mapel_id`, `judul`, `file`) VALUES
(2, 1, 2, 25, '<h1>TOLONG DIKEJAKAN SEBAIK MUNGKIN!!!!</h1>\r\n', '2018-12-20', 3, 'TES', 'gambar/materi/edit_(1).pdf'),
(3, 1, 2, 25, '<p>OK</p>\r\n', '2018-12-20', 3, 'TES', 'gambar/materi/edit_(1).pdf'),
(4, 1, 2, 25, '', '2018-12-20', 3, 'TES', 'gambar/materi/edit_(1).pdf'),
(5, 1, 1, 25, '', '2018-12-20', 3, 'TES', 'gambar/materi/edit_(1).pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai`
--

CREATE TABLE `tb_nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `benar` varchar(10) NOT NULL,
  `salah` varchar(10) NOT NULL,
  `point` float NOT NULL,
  `soal` varchar(100) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `id_modul` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nilai`
--

INSERT INTO `tb_nilai` (`id_nilai`, `id_siswa`, `id_kelas`, `id_jurusan`, `tgl`, `kode_soal`, `benar`, `salah`, `point`, `soal`, `jenis_soal`, `id_modul`) VALUES
(1, 30, 1, 1, '2018-12-08', 1544279293, '1', '1', 50, '2', 'ESSAI', 2),
(2, 31, 1, 1, '2018-12-08', 53397, '1', '1', 50, '2', 'ESSAI', 2),
(3, 30, 1, 1, '2018-12-08', 81597, '3', '0', 100, '3', 'PILIHAN GANDA', 1),
(4, 30, 1, 1, '2018-12-19', 53545, '2', '0', 100, '2', 'PILIHAN GANDA', 3),
(5, 30, 1, 1, '2018-12-19', 58873, '0', '1', 0, '1', 'PILIHAN GANDA', 8),
(6, 30, 1, 1, '2018-12-20', 72348, '2', '0', 10, '2', 'ESSAI', 9),
(7, 30, 1, 1, '2018-12-20', 51854, '4', '0', 100, '4', 'PILIHAN GANDA', 10),
(8, 30, 1, 1, '2018-12-20', 24453, '2', '0', 100, '2', 'PILIHAN GANDA', 10),
(9, 30, 1, 1, '2018-12-20', 90962, '2', '0', 100, '2', 'PILIHAN GANDA', 11);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id_pengumuman`, `tgl`, `judul`, `isi`) VALUES
(1, '2018-11-07', 'Penting Nemen Cuyasdasdasd2222', '                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n'),
(4, '2018-11-07', 'asdasd', 'asdasdasd'),
(5, '2018-12-06', 'PENGUMUMAN', 'Dimohon untuk semua Petugas');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id` int(11) NOT NULL,
  `nis` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `jk` enum('L','P','-') NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `agama` enum('Islam','Kristen','Katholik','Hindu','Budha','Kong Hu Chu') NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL,
  `id_kelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`id`, `nis`, `id_user`, `nama_siswa`, `jk`, `jurusan`, `agama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `foto`, `id_kelas`) VALUES
(1, 'dopalstyle', 16, 'dopalstyle', '-', '-', '', '-', '0000-00-00', '-', '-', 0),
(2, '123', 22, 'BAGUS', 'L', '1', 'Islam', 'PATI', '2018-12-03', 'REMBANG', 'TES', 1),
(3, '1281728', 26, 'Baba', 'L', '1', 'Islam', 'Rembag', '2018-12-07', 'Rembang', 'TES', 1),
(4, '9127812', 30, 'Suharto', 'L', '1', 'Islam', 'Rembang', '2000-12-12', 'Rembang', 'Screenshot_from_2018-12-08_17-11-425.png', 1),
(5, '01291291299', 31, 'YONO', 'L', '1', 'Islam', 'PATI', '2018-12-05', 'Semarang', 'TES', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ulangan_eesay`
--

CREATE TABLE `tb_ulangan_eesay` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ulangan_eesay`
--

INSERT INTO `tb_ulangan_eesay` (`id_ulangan`, `kode_soal`, `mapel_id`, `kelas_id`, `jurusan_id`, `guru_id`, `jenis_soal`, `soal`, `jawaban`, `tgl`) VALUES
(1, 123, 3, 1, 1, 12, 'eesay', 'jkk', 'k', '2018-11-26'),
(2, 123, 3, 1, 1, 12, 'eesay', 'kb', 'b', '2018-11-26'),
(3, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(4, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(5, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(6, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(7, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(8, 123, 3, 1, 1, 12, 'eesay', 'n', 'nn', '2018-11-26'),
(9, 123, 3, 1, 1, 12, 'eesay', 'n', 'n', '2018-11-26'),
(10, 123, 3, 1, 1, 12, 'eesay', 'nn', 'n', '2018-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ulangan_pil`
--

CREATE TABLE `tb_ulangan_pil` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `option_a` varchar(100) NOT NULL,
  `option_b` varchar(100) NOT NULL,
  `option_c` varchar(100) NOT NULL,
  `option_d` varchar(100) NOT NULL,
  `option_e` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pass1` text NOT NULL,
  `pass2` text NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `pass1`, `pass2`, `level`) VALUES
(11, 'atfalstyle', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 1),
(12, 'gopalstyle', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(16, 'dopalstyle', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(19, 'ahmad', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(20, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(21, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(22, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(23, 'Rembang', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(24, 'guru', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(25, 'bambang', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 2),
(26, 'user', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(27, 'vava', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(28, 'vava', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3),
(30, 'suharto', '$2y$09$TuVxQKXpObdjSiTkY9yDtOV3O3T6vYj4/50SIW33vAqzdmEsaovbu', 'suhadi', 3),
(31, 'yono', '$2y$09$.wqldLfTiajckh6a0sXUxO5odn8s5ZUHdjiSLGlmlYzIyNUkcRqXq', 'sama', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_uts_eesay`
--

CREATE TABLE `tb_uts_eesay` (
  `id_uts` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_uts_eesay`
--

INSERT INTO `tb_uts_eesay` (`id_uts`, `kode_soal`, `mapel_id`, `jurusan_id`, `kelas_id`, `guru_id`, `jenis_soal`, `soal`, `jawaban`, `tgl`) VALUES
(1, 123123123, 3, 1, 1, 12, 'eesay', 'asdas', 'asdad', '2018-11-26'),
(2, 123123123, 3, 1, 1, 12, 'eesay', 'asdasdad', 'asdasd', '2018-11-26'),
(3, 123123123, 3, 1, 1, 12, 'eesay', 'asdad', 'asasdasd', '2018-11-26'),
(4, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(5, 123123123, 3, 1, 1, 12, 'eesay', 'asdadasd', 'asdad', '2018-11-26'),
(6, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdas', '2018-11-26'),
(7, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'dasdasd', '2018-11-26'),
(8, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(9, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasd', '2018-11-26'),
(10, 123123123, 3, 1, 1, 12, 'eesay', 'asdasd', 'asdasda', '2018-11-26'),
(11, 0, 3, 1, 1, 12, 'eesay', 'asdad', 'kjj', '2018-11-26'),
(12, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(13, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(14, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(15, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(16, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(17, 0, 3, 1, 1, 12, 'eesay', 'jj', 'j', '2018-11-26'),
(18, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(19, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(20, 0, 3, 1, 1, 12, 'eesay', 'j', 'j', '2018-11-26'),
(21, 132, 3, 4, 1, 12, 'eesay', 'asdad', 'jh', '2018-11-26'),
(22, 132, 3, 4, 1, 12, 'eesay', 'hjh', 'jh', '2018-11-26'),
(23, 132, 3, 4, 1, 12, 'eesay', 'jh', 'h', '2018-11-26'),
(24, 132, 3, 4, 1, 12, 'eesay', 'hh', 'h', '2018-11-26'),
(25, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(26, 132, 3, 4, 1, 12, 'eesay', 'h', 'h', '2018-11-26'),
(27, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(28, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(29, 132, 3, 4, 1, 12, 'eesay', 'h', 'hh', '2018-11-26'),
(30, 132, 3, 4, 1, 12, 'eesay', 'h', 'h', '2018-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_uts_pil`
--

CREATE TABLE `tb_uts_pil` (
  `id_ulangan` int(11) NOT NULL,
  `kode_soal` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `jenis_soal` varchar(100) NOT NULL,
  `soal` text NOT NULL,
  `option_a` varchar(100) NOT NULL,
  `option_b` varchar(100) NOT NULL,
  `option_c` varchar(100) NOT NULL,
  `option_d` varchar(100) NOT NULL,
  `option_e` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jawaban`
--

CREATE TABLE `t_jawaban` (
  `id` int(11) NOT NULL,
  `uid` varchar(11) NOT NULL,
  `jawaban` text,
  `id_soal` int(11) NOT NULL,
  `benar` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jawaban`
--

INSERT INTO `t_jawaban` (`id`, `uid`, `jawaban`, `id_soal`, `benar`, `created`) VALUES
(1, '1544152326', 'Jakarta', 4, 1, '2018-12-07 03:12:06'),
(2, '95560', 'Semarang', 4, 0, '2018-12-07 03:12:06'),
(3, '98303', 'Denpasar', 4, 0, '2018-12-07 03:12:06'),
(4, '4836', 'Balikpapan', 4, 0, '2018-12-07 03:12:06'),
(5, '64223', 'Bangkok', 5, 1, '2018-12-07 04:40:07'),
(6, '35442', 'Makassar', 5, 0, '2018-12-07 04:40:08'),
(7, '84543', 'Singapura', 5, 0, '2018-12-07 04:40:08'),
(8, '16391', 'Kuala Lumpur', 5, 0, '2018-12-07 04:40:08'),
(9, '82647', '0', 6, 1, '2018-12-07 04:40:55'),
(10, '93421', '1', 6, 0, '2018-12-07 04:40:56'),
(11, '19167', '2', 6, 0, '2018-12-07 04:40:56'),
(12, '15569', '3', 6, 0, '2018-12-07 04:40:56'),
(13, '50042', '2', 7, 1, '2018-12-07 06:44:30'),
(14, '48912', '3', 7, 0, '2018-12-07 06:44:30'),
(15, '94434', '4', 7, 0, '2018-12-07 06:44:30'),
(16, '25437', '6', 7, 0, '2018-12-07 06:44:30'),
(17, '43882', '4', 8, 1, '2018-12-07 06:44:30'),
(18, '43101', '5', 8, 0, '2018-12-07 06:44:30'),
(19, '83861', '6', 8, 0, '2018-12-07 06:44:30'),
(20, '90002', '7', 8, 0, '2018-12-07 06:44:31'),
(21, '98431', '11', 9, 1, '2018-12-07 06:44:31'),
(22, '22152', '10', 9, 0, '2018-12-07 06:44:31'),
(23, '15465', '12', 9, 0, '2018-12-07 06:44:31'),
(24, '10870', '15', 9, 0, '2018-12-07 06:44:31'),
(25, '25671', '2', 10, 1, '2018-12-07 06:47:13'),
(26, '45336', '3', 10, 0, '2018-12-07 06:47:13'),
(27, '49667', '4', 10, 0, '2018-12-07 06:47:13'),
(28, '12329', '6', 10, 0, '2018-12-07 06:47:13'),
(29, '79440', '12', 11, 1, '2018-12-07 06:47:36'),
(30, '32565', '13', 11, 0, '2018-12-07 06:47:36'),
(31, '24505', '15', 11, 0, '2018-12-07 06:47:36'),
(32, '24829', '20', 11, 0, '2018-12-07 06:47:36'),
(33, '79552', '2', 12, 1, '2018-12-07 08:20:14'),
(34, '86529', '3', 12, 0, '2018-12-07 08:20:15'),
(35, '93990', '1', 12, 0, '2018-12-07 08:20:15'),
(36, '10367', '5', 12, 0, '2018-12-07 08:20:15'),
(37, '69862', '11', 13, 1, '2018-12-07 08:20:15'),
(38, '18211', '2', 13, 0, '2018-12-07 08:20:15'),
(39, '81468', '21', 13, 0, '2018-12-07 08:20:15'),
(40, '52709', '1', 13, 0, '2018-12-07 08:20:15'),
(41, '19142', '1', 14, 1, '2018-12-07 08:20:15'),
(42, '37582', '2', 14, 0, '2018-12-07 08:20:15'),
(43, '30485', '4', 14, 0, '2018-12-07 08:20:15'),
(44, '39680', '5', 14, 0, '2018-12-07 08:20:16'),
(45, '61531', '10', 15, 1, '2018-12-07 08:20:49'),
(46, '44314', '12', 15, 0, '2018-12-07 08:20:49'),
(47, '36979', '12', 15, 0, '2018-12-07 08:20:50'),
(48, '51952', '31', 15, 0, '2018-12-07 08:20:50'),
(49, '48826', '800', 16, 1, '2018-12-07 08:20:50'),
(50, '88275', '1210', 16, 0, '2018-12-07 08:20:50'),
(51, '94899', '120', 16, 0, '2018-12-07 08:20:50'),
(52, '9672', '900', 16, 0, '2018-12-07 08:20:50'),
(53, '65600', '2', 17, 1, '2018-12-08 00:55:39'),
(54, '50670', '1', 17, 0, '2018-12-08 00:55:39'),
(55, '56551', '3', 17, 0, '2018-12-08 00:55:39'),
(56, '30746', '6', 17, 0, '2018-12-08 00:55:39'),
(57, '84076', '7', 18, 1, '2018-12-08 00:55:39'),
(58, '28145', '12', 18, 0, '2018-12-08 00:55:39'),
(59, '88497', '10', 18, 0, '2018-12-08 00:55:39'),
(60, '58053', '9', 18, 0, '2018-12-08 00:55:39'),
(61, '24776', '99', 19, 1, '2018-12-08 00:55:40'),
(62, '49721', '91', 19, 0, '2018-12-08 00:55:40'),
(63, '74279', '93', 19, 0, '2018-12-08 00:55:40'),
(64, '22234', '29', 19, 0, '2018-12-08 00:55:40'),
(65, '88333', '110', 20, 1, '2018-12-08 00:55:40'),
(66, '74963', '20', 20, 0, '2018-12-08 00:55:40'),
(67, '9820', '30', 20, 0, '2018-12-08 00:55:40'),
(68, '24211', '10', 20, 0, '2018-12-08 00:55:40'),
(69, '22365', 'Paris', 21, 1, '2018-12-08 04:39:07'),
(70, '91894', 'Berlin', 22, 1, '2018-12-08 04:39:07'),
(71, '92377', 'Jakarta', 23, 1, '2018-12-08 04:39:08'),
(72, '32913', '2', 24, 1, '2018-12-19 12:38:22'),
(73, '32062', '3', 24, 0, '2018-12-19 12:38:22'),
(74, '61571', '1', 24, 0, '2018-12-19 12:38:22'),
(75, '11673', '10', 24, 0, '2018-12-19 12:38:22'),
(76, '73650', '5', 25, 1, '2018-12-19 12:38:22'),
(77, '33235', '100', 25, 0, '2018-12-19 12:38:22'),
(78, '45222', '10', 25, 0, '2018-12-19 12:38:22'),
(79, '26408', '20', 25, 0, '2018-12-19 12:38:22'),
(80, '63422', '2', 26, 1, '2018-12-19 16:04:09'),
(81, '82036', '1', 26, 0, '2018-12-19 16:04:09'),
(82, '19919', '3', 26, 0, '2018-12-19 16:04:09'),
(83, '53486', '1', 26, 0, '2018-12-19 16:04:10'),
(84, '41605', 'ok', 27, 1, '2018-12-20 12:43:38'),
(85, '72148', 'ok', 28, 1, '2018-12-20 12:43:38'),
(86, '44462', '2', 29, 1, '2018-12-20 15:49:42'),
(87, '49008', '100', 29, 0, '2018-12-20 15:49:42'),
(88, '11658', '200', 29, 0, '2018-12-20 15:49:42'),
(89, '11263', '300', 29, 0, '2018-12-20 15:49:42'),
(90, '21342', '4', 30, 1, '2018-12-20 15:49:42'),
(91, '72923', '100', 30, 0, '2018-12-20 15:49:42'),
(92, '591', '200', 30, 0, '2018-12-20 15:49:43'),
(93, '84184', '300', 30, 0, '2018-12-20 15:49:43'),
(94, '41180', '2', 31, 1, '2018-12-20 16:03:43'),
(95, '70344', '1', 31, 0, '2018-12-20 16:03:44'),
(96, '28181', '3', 31, 0, '2018-12-20 16:03:44'),
(97, '29872', '4', 31, 0, '2018-12-20 16:03:44'),
(98, '64820', '2001', 32, 1, '2018-12-20 16:03:44'),
(99, '34483', '1', 32, 0, '2018-12-20 16:03:44'),
(100, '77956', '2', 32, 0, '2018-12-20 16:03:44'),
(101, '86334', '3', 32, 0, '2018-12-20 16:03:44'),
(102, '7216', '1', 33, 1, '2018-12-21 12:30:22'),
(103, '47836', '2', 33, 0, '2018-12-21 12:30:22'),
(104, '17534', '3', 33, 0, '2018-12-21 12:30:22'),
(105, '44162', '4', 33, 0, '2018-12-21 12:30:22'),
(106, '66990', 'Z', 34, 1, '2018-12-23 04:38:01'),
(107, '47201', 'XA', 34, 0, '2018-12-23 04:38:01'),
(108, '35035', 'AS', 34, 0, '2018-12-23 04:38:01'),
(109, '33576', 'AS', 34, 0, '2018-12-23 04:38:01'),
(110, '83820', 'Z', 35, 1, '2018-12-23 04:38:33'),
(111, '7793', 'XA', 35, 0, '2018-12-23 04:38:33'),
(112, '87504', 'AS', 35, 0, '2018-12-23 04:38:33'),
(113, '14146', 'AS', 35, 0, '2018-12-23 04:38:33'),
(114, '28192', 'as', 36, 1, '2018-12-23 04:39:51'),
(115, '7309', '', 36, 0, '2018-12-23 04:39:51'),
(116, '51969', 'as', 36, 0, '2018-12-23 04:39:51'),
(117, '37920', '', 36, 0, '2018-12-23 04:39:51'),
(118, '94759', 'as', 37, 1, '2018-12-23 04:40:29'),
(119, '15222', 'asa', 37, 0, '2018-12-23 04:40:29'),
(120, '91831', 'sas', 37, 0, '2018-12-23 04:40:29'),
(121, '13493', 'as', 37, 0, '2018-12-23 04:40:29'),
(122, '29167', 'as', 38, 1, '2018-12-23 04:41:07'),
(123, '25516', 'asa', 38, 0, '2018-12-23 04:41:07'),
(124, '40079', 'sas', 38, 0, '2018-12-23 04:41:07'),
(125, '23851', 'as', 38, 0, '2018-12-23 04:41:07'),
(126, '99014', 'as', 39, 1, '2018-12-23 04:41:07'),
(127, '23522', 'as', 39, 0, '2018-12-23 04:41:07'),
(128, '20568', 'as', 39, 0, '2018-12-23 04:41:08'),
(129, '32274', 'as', 39, 0, '2018-12-23 04:41:08'),
(130, '81348', '1', 40, 1, '2018-12-23 04:46:26'),
(131, '97284', '2', 40, 0, '2018-12-23 04:46:26'),
(132, '42382', '3', 40, 0, '2018-12-23 04:46:26'),
(133, '20056', '4', 40, 0, '2018-12-23 04:46:26'),
(134, '73135', '', 41, 1, '2018-12-23 04:46:27'),
(135, '5509', '', 41, 0, '2018-12-23 04:46:27'),
(136, '8138', '', 41, 0, '2018-12-23 04:46:27'),
(137, '24163', '', 41, 0, '2018-12-23 04:46:27'),
(138, '96402', '', 42, 1, '2018-12-23 04:46:27'),
(139, '9527', '', 42, 0, '2018-12-23 04:46:27'),
(140, '58425', '', 42, 0, '2018-12-23 04:46:27'),
(141, '63545', '', 42, 0, '2018-12-23 04:46:27'),
(142, '26426', 'INVOICE ', 43, 1, '2018-12-23 04:47:48'),
(143, '88867', 'BUKAN ', 43, 0, '2018-12-23 04:47:48'),
(144, '65059', 'TIDAK ', 43, 0, '2018-12-23 04:47:48'),
(145, '58694', 'BEUM', 43, 0, '2018-12-23 04:47:48'),
(146, '98292', '', 44, 1, '2018-12-23 04:47:48'),
(147, '15384', '', 44, 0, '2018-12-23 04:47:48'),
(148, '82039', '', 44, 0, '2018-12-23 04:47:49'),
(149, '64049', '', 44, 0, '2018-12-23 04:47:49'),
(150, '48121', 'OK', 45, 1, '2018-12-23 04:48:35'),
(151, '67010', '', 46, 1, '2018-12-23 04:48:35'),
(152, '19453', 'OKE', 47, 1, '2018-12-23 04:50:26'),
(153, '9232', '', 48, 1, '2018-12-23 04:50:26'),
(154, '3289', 'OKE', 49, 1, '2018-12-23 04:51:21'),
(155, '65535', '', 50, 1, '2018-12-23 04:51:21'),
(156, '51067', 'OKE', 51, 1, '2018-12-23 04:51:47'),
(157, '4530', '', 52, 1, '2018-12-23 04:51:47'),
(158, '95484', 'as', 53, 1, '2018-12-23 05:23:39'),
(159, '7365', '', 54, 1, '2018-12-23 05:23:39'),
(160, '39200', 'X', 55, 1, '2018-12-23 05:24:45'),
(161, '43155', '', 56, 1, '2018-12-23 05:24:45'),
(162, '98177', '', 57, 1, '2018-12-23 05:24:45'),
(163, '61422', '', 58, 1, '2018-12-23 05:24:45'),
(164, '9554', 'S', 59, 1, '2018-12-23 05:26:39'),
(165, '28099', 'AS', 60, 1, '2018-12-23 05:26:39'),
(166, '42794', 'ok', 61, 1, '2018-12-23 05:29:01'),
(167, '33432', '', 62, 1, '2018-12-23 05:29:01'),
(168, '77956', 'A', 63, 1, '2018-12-23 05:30:45'),
(169, '14404', 'HAHAH    ', 64, 1, '2018-12-23 05:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `t_jawaban_user`
--

CREATE TABLE `t_jawaban_user` (
  `id` int(11) NOT NULL,
  `id_soal_user` int(11) NOT NULL,
  `id_jawaban` varchar(50) NOT NULL,
  `jawaban_essai` text,
  `dipilih` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jawaban_user`
--

INSERT INTO `t_jawaban_user` (`id`, `id_soal_user`, `id_jawaban`, `jawaban_essai`, `dipilih`, `created`) VALUES
(1, 1, '22365', 'Paris', 0, '2018-12-08 14:28:13'),
(2, 2, '91894', 'Berlin', 0, '2018-12-08 14:28:13'),
(3, 3, '91894', 'Berlin', 0, '2018-12-08 14:29:56'),
(4, 4, '92377', 'London', 0, '2018-12-08 14:29:57'),
(5, 5, '24776', NULL, 1, '2018-12-08 15:35:35'),
(6, 5, '22234', NULL, 0, '2018-12-08 15:35:35'),
(7, 5, '74279', NULL, 0, '2018-12-08 15:35:35'),
(8, 5, '49721', NULL, 0, '2018-12-08 15:35:35'),
(9, 6, '28145', NULL, 0, '2018-12-08 15:35:36'),
(10, 6, '84076', NULL, 1, '2018-12-08 15:35:36'),
(11, 6, '88497', NULL, 0, '2018-12-08 15:35:36'),
(12, 6, '58053', NULL, 0, '2018-12-08 15:35:36'),
(13, 7, '74963', NULL, 0, '2018-12-08 15:35:36'),
(14, 7, '24211', NULL, 0, '2018-12-08 15:35:36'),
(15, 7, '9820', NULL, 0, '2018-12-08 15:35:36'),
(16, 7, '88333', NULL, 1, '2018-12-08 15:35:36'),
(17, 8, '32062', NULL, 0, '2018-12-19 12:45:05'),
(18, 8, '61571', NULL, 0, '2018-12-19 12:45:05'),
(19, 8, '11673', NULL, 0, '2018-12-19 12:45:05'),
(20, 8, '32913', NULL, 1, '2018-12-19 12:45:05'),
(21, 9, '45222', NULL, 0, '2018-12-19 12:45:05'),
(22, 9, '33235', NULL, 0, '2018-12-19 12:45:05'),
(23, 9, '73650', NULL, 1, '2018-12-19 12:45:05'),
(24, 9, '26408', NULL, 0, '2018-12-19 12:45:06'),
(25, 10, '19919', NULL, 0, '2018-12-19 16:04:38'),
(26, 10, '82036', NULL, 0, '2018-12-19 16:04:38'),
(27, 10, '53486', NULL, 0, '2018-12-19 16:04:38'),
(28, 10, '63422', NULL, 0, '2018-12-19 16:04:38'),
(29, 11, '72148', 'A', 0, '2018-12-20 12:44:12'),
(30, 12, '41605', NULL, 0, '2018-12-20 12:44:12'),
(31, 13, '72923', NULL, 0, '2018-12-20 15:50:10'),
(32, 13, '21342', NULL, 1, '2018-12-20 15:50:11'),
(33, 13, '84184', NULL, 0, '2018-12-20 15:50:11'),
(34, 13, '591', NULL, 0, '2018-12-20 15:50:11'),
(35, 14, '11658', NULL, 0, '2018-12-20 15:50:11'),
(36, 14, '44462', NULL, 1, '2018-12-20 15:50:11'),
(37, 14, '49008', NULL, 0, '2018-12-20 15:50:11'),
(38, 14, '11263', NULL, 0, '2018-12-20 15:50:11'),
(39, 15, '72923', NULL, 0, '2018-12-20 15:50:12'),
(40, 15, '591', NULL, 0, '2018-12-20 15:50:12'),
(41, 15, '21342', NULL, 1, '2018-12-20 15:50:12'),
(42, 15, '84184', NULL, 0, '2018-12-20 15:50:12'),
(43, 16, '49008', NULL, 0, '2018-12-20 15:50:12'),
(44, 16, '11263', NULL, 0, '2018-12-20 15:50:12'),
(45, 16, '44462', NULL, 1, '2018-12-20 15:50:12'),
(46, 16, '11658', NULL, 0, '2018-12-20 15:50:12'),
(47, 17, '21342', NULL, 1, '2018-12-20 15:54:00'),
(48, 17, '84184', NULL, 0, '2018-12-20 15:54:00'),
(49, 17, '591', NULL, 0, '2018-12-20 15:54:01'),
(50, 17, '72923', NULL, 0, '2018-12-20 15:54:01'),
(51, 18, '49008', NULL, 0, '2018-12-20 15:54:01'),
(52, 18, '11658', NULL, 0, '2018-12-20 15:54:01'),
(53, 18, '11263', NULL, 0, '2018-12-20 15:54:01'),
(54, 18, '44462', NULL, 1, '2018-12-20 15:54:01'),
(55, 19, '41180', NULL, 1, '2018-12-20 16:06:45'),
(56, 19, '70344', NULL, 0, '2018-12-20 16:06:45'),
(57, 19, '29872', NULL, 0, '2018-12-20 16:06:45'),
(58, 19, '28181', NULL, 0, '2018-12-20 16:06:45'),
(59, 20, '64820', NULL, 1, '2018-12-20 16:06:45'),
(60, 20, '34483', NULL, 0, '2018-12-20 16:06:46'),
(61, 20, '86334', NULL, 0, '2018-12-20 16:06:46'),
(62, 20, '77956', NULL, 0, '2018-12-20 16:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `t_livestream`
--

CREATE TABLE `t_livestream` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `url` text,
  `id_guru` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_livestream`
--

INSERT INTO `t_livestream` (`id`, `nama`, `keterangan`, `created`, `url`, `id_guru`) VALUES
(5, 'as', 'a', NULL, 'https://www.youtube.com/watch?v=qWuu6YYhMuk', 0),
(6, 'pembahasan 1', 'ok', NULL, 'https://www.youtube.com/watch?v=coE3BF8vsEc\\', 25),
(7, 'TES', 'TES', '2018-12-22 13:32:52', 'https://www.youtube.com/embed/qWuu6YYhMuk', 25),
(8, 'TES', 'OK', '2018-12-22 13:37:34', ' https://youtu.be/FcOctsNXyjk', 25);

-- --------------------------------------------------------

--
-- Table structure for table `t_modul_soal`
--

CREATE TABLE `t_modul_soal` (
  `id` int(11) NOT NULL,
  `kode_soal` varchar(50) DEFAULT NULL,
  `jurusan` int(11) NOT NULL DEFAULT '0',
  `kelas` int(11) NOT NULL DEFAULT '0',
  `nama_modul` varchar(100) DEFAULT NULL,
  `max_nilai` int(11) NOT NULL DEFAULT '0',
  `id_mapel` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `min_nilai` int(11) NOT NULL DEFAULT '0',
  `jumlah_jawaban` int(11) NOT NULL,
  `waktu` int(11) NOT NULL DEFAULT '0',
  `status_aktif` int(11) NOT NULL DEFAULT '0',
  `jumlah_soal` int(11) NOT NULL,
  `keterangan` text,
  `password` varchar(50) DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `essai` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_modul_soal`
--

INSERT INTO `t_modul_soal` (`id`, `kode_soal`, `jurusan`, `kelas`, `nama_modul`, `max_nilai`, `id_mapel`, `created`, `min_nilai`, `jumlah_jawaban`, `waktu`, `status_aktif`, `jumlah_soal`, `keterangan`, `password`, `jenis`, `essai`) VALUES
(1, 'A0123', 1, 1, 'Matematika kelas 10', 100, 6, '2018-12-08 00:52:44', 75, 4, 10, 0, 3, NULL, 'password', 'UTS', 0),
(2, '01201', 1, 1, 'ESSAI MATEMATIKA', 100, 6, '2018-12-08 03:02:24', 75, 1, 100, 0, 2, NULL, 'password', 'ULANGAN', 1),
(3, 'A01', 1, 1, 'Bahasa Indonesia', 100, 3, '2018-12-19 12:31:56', 75, 4, 10, 0, 20, NULL, 'rahasia', 'UTS', 0),
(4, 'A01', 1, 1, 'Bahasa', 100, 3, '2018-12-19 12:32:40', 1, 4, 10, 0, 20, NULL, NULL, 'ULANGAN', 0),
(5, 'A01', 1, 1, 'Bahasa', 100, 3, '2018-12-19 12:33:29', 1, 4, 10, 0, 20, NULL, NULL, 'ULANGAN', 0),
(6, 'A01', 1, 1, 'oK', 100, 6, '2018-12-19 12:35:55', 1, 100, 100, 0, 10, NULL, NULL, 'ULANGAN', 0),
(7, 'A01', 1, 1, 'Bahasa', 100, 3, '2018-12-19 12:36:40', 1, 4, 10, 0, 20, NULL, NULL, 'ULANGAN', 0),
(8, 'TES123', 1, 1, 'k', 100, 6, '2018-12-19 16:03:46', 10, 4, 10, 0, 1, NULL, 'rahasia', 'UTS', 0),
(9, 'A012', 1, 1, 'OK', 10, 3, '2018-12-20 12:42:35', 1, 1, 1, 0, 10, NULL, 'rahasia', 'ULANGAN', 1),
(11, 'TES2019', 1, 1, 'TES2019', 100, 6, '2018-12-20 16:03:16', 1, 4, 10, 0, 2, NULL, 'rahasia', 'UTS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_soal_ganda`
--

CREATE TABLE `t_soal_ganda` (
  `id` int(11) NOT NULL,
  `id_modul` int(11) NOT NULL,
  `soal` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_soal_ganda`
--

INSERT INTO `t_soal_ganda` (`id`, `id_modul`, `soal`, `created`) VALUES
(1, 4, 'Ibukota Indonesai?', '2018-12-07 03:11:08'),
(2, 4, 'Ibukota Indonesai?', '2018-12-07 03:11:13'),
(3, 4, 'Ibukota Indonesai?', '2018-12-07 03:11:20'),
(4, 4, 'Ibukota Indonesai?', '2018-12-07 03:12:05'),
(5, 4, 'Ibukota Thailand?', '2018-12-07 04:40:06'),
(6, 4, '1 X 0 +0 =...', '2018-12-07 04:40:55'),
(7, 4, '1+1=...', '2018-12-07 06:44:30'),
(8, 4, '2+2=', '2018-12-07 06:44:30'),
(9, 4, '10+1=', '2018-12-07 06:44:31'),
(10, 6, '1+1', '2018-12-07 06:47:12'),
(11, 6, '2+10=....', '2018-12-07 06:47:35'),
(12, 6, '1+1=....', '2018-12-07 08:20:14'),
(13, 6, '10+1', '2018-12-07 08:20:15'),
(14, 6, '20', '2018-12-07 08:20:15'),
(15, 6, '10 X 1', '2018-12-07 08:20:49'),
(16, 6, '900-100', '2018-12-07 08:20:50'),
(17, 1, '1+1=.', '2018-12-08 00:55:39'),
(18, 1, '6+1=....', '2018-12-08 00:55:39'),
(19, 1, '100-1', '2018-12-08 00:55:40'),
(20, 1, '100+10=', '2018-12-08 00:55:40'),
(21, 2, 'Ibukota Prancis Adalah', '2018-12-08 04:39:07'),
(22, 2, 'Ibukota Jerman', '2018-12-08 04:39:07'),
(23, 2, 'Ibukota Indonesia', '2018-12-08 04:39:08'),
(24, 3, '1+1=', '2018-12-19 12:38:22'),
(25, 3, '4+1', '2018-12-19 12:38:22'),
(26, 8, '1+1=...??', '2018-12-19 16:04:09'),
(27, 9, 'TES', '2018-12-20 12:43:38'),
(28, 9, 'TES', '2018-12-20 12:43:38'),
(29, 10, '1+1=...', '2018-12-20 15:49:41'),
(30, 10, '2+2=...', '2018-12-20 15:49:42'),
(31, 11, '1+1=.....', '2018-12-20 16:03:43'),
(32, 11, '2000+1=..', '2018-12-20 16:03:44'),
(33, 11, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAmQAAAEhCAYAAADRbLz+AAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAZ25vbWUtc2NyZWVuc2hvdO8Dvz4AACAASURBVHic7N15eBzVmej/b1Xvi9TaWpK12ZJsyQuWbCzvBsNgCCRDYhKykBAICQlwE+Zm7iS/TDITJrk3ufPMJHmSTAJ3MmQlhrBMSAAbSDCL8W6wMba8YXmVJS/a1a3eu+v3R7tbaqm71dos2byf51GCu6tOnTrnVNVb55yqVv6m8Ita6QIb+dPNNL/Ti7s9iNmhp2KRjZ5zAU695QI0ckqNlDdk4XMFOdvoIeSPkFNuIr/STCgQwXU+QMcJHzqDQsl8G6pe5cjGTjQNcqaZKG+wE+iLcPZAH0F/hNxyE/kzzJw/4uHCe14AHMVGKhZn0dbkpavZj8mmo7Tejrc3xMkdvQCU1tnIm27G1xum46SXQF8Ed3uQkqts5JSbONvYh6crhN1pYNo8K+cPeWg75iOZgiozxXOsHHypk0gEFAXmfCAPnUHh6Bs9+FwhAKpXOQj5I5x6y4WiRP+tM6i07HMT8kUoqLaQU27i+NYePF0hDEaF2R/IQ4tonDvkoa89hNmhY9o8G77eMMe39STNj6LA7Bvz6Dnrp3V/X/zz2jW59LUHObPXnXQ98X6go/RDTsqdIc6ub+dUO+jLc6i7wYxxnLYQaXOxf0Mf3pGsZDBSflMOpU51fDLR5uKdDX34x5CEfUkBV83VR//R00fjn1wkO3IstbnMWW66WH4R3Lu7OLw/SAhQc63ULNVx5uXk644pT2MQ6vRw7LVeusYhU5mWU2Z51/Cf7uHwGz68kbHnbSgd+avzqK7UMbSlaXibujm8xT+mdpPKeNVdujIeKbU8h6tvMOPb3U7j/lDCd7mrC6mtDNH83520DNqYPttE7iwz+aUmsvNUVAJJlxsP1vn5XLXIgIqG50g3h7f7CdjNVN+YjeV0N4d3BwgBGAyU3pBLebGKe3cbjfvD458ZANXErE/lkt8TPcdYB5TT2dwB5XlAl3K5aDkp9P9vjIIS/0RBUYZ+lnJNpf/fyt8U3qsVzLQQ8oXpOuNHQ6PAWEFN4SLMkRzUkJGENLRBO5ns88GfZbrueKaVbtlU6ymDvku2zfHcl+HyM5L1xJUvVRsbTyNtY8PlIVl6o1lnJFIdx8PlYSKPufGuq/HIVybllGy54UzEeWqi28xYtj0Sl7I9JWvPwy03noY7xlItfynqcvC1fGBeU303gnwFNT99wS5Oet6hPXD6YmA2fFCmXF94rwYaigLWAh2FphlUBW9IzNzgHUm1g6k+G2kQk0qqwhnJeunSS3aCGk0eBy6Xap+HW3e4dMT7z0QHZKNpYyM99lIdi2PNx0CjCchSnU8kIJv8gGwyg7FMtj8S76eALNV2J/NadokCsoHe7XmZNv+pxB60hMxc/FRR0IOGLV9P+WI7Chq5B+pGt9XLTbrCzeSiMR7bT7eNgUGZEDET2Ts2EQbncyrmeyQ3d1PBVD4vXGnB2OVgJDf9qdaZKMmCnfeZ6dYFXPCdACU62B69tCsoSYIAFaC8wY6vN8SBlzsw+ByTX2hair+xrDdcWiPteRttHtOlkW45ISayHUyVNnYp8zGaY3Ys25pK6UzENiUYm7pSXWsuxQ3IaK+PVwi7Pg8NDU1LnFw5uDg0TUNVdRoGs4KrzU9Ei6BXxmuK8CU0ll6DTAO98ZBp1+fl1gsiLq2p1C7G8yR7qYOxSy3VTVymF6zJvKCly+dEXWzlJnViKAP+xITTK0Y0IvGgTBvSUzRg2VBIw+sKklthpK/n4jMqE1VRyeZGZbpcpgfgeM2/Gm7eWbrx52T5Ga8yTTaUOfgzmXcmLqV07WyqzSG5HE3l8pqIvA08fyU7d07l8rhUBpbLcNeWSxl4jde2rrA61rTIxbJRQdNAST50qYLGqbd6CAXClF1tm/jKGxyZayn+e6QRfKp1R1Kxo+maz2R8fCxj6OmGVwf/t7jyTYUnKzOR7vid6LvzZDcsmf5NNZdymHok5XMphqCSncOvsAv1hLscg7ErUIQImjZ8d7heQ8PTE+a9TV2oRo3peSlSTHUgJPt8JMuOdJmJ6MYeTX4nengh0zyNpPyFEJmb7ONosrefzKV+2Ci2valYFpNtuDKRAGnK0LQImqIQ0UBVlIs9nNqAXrIovYYWH9sM+ifohWxCiKlpoi50ww3TT/QFdjynCUyGS/mwwVQqp2R5kSfOU5tKdXc5STfKNAEjUBoRNE0BRUFLCMH6lwAFfXTmv4ZGhIg2Ia9YFkKMh8vt5JtuHuZkbj+T9SbLZD1sMFXb1lQIxKZ6+cjTqFNeRIugKAoKary3rH8uWX8F6mPBmHbx/yfEVG7QQlwuLscT63APvVyK7YvhTZVymir5GGyq5gumTt6mSj7SmaQ8RmMsFY3IxQBs4BMr/X1manS48uLfROb2cqgsIcT4S/6EtxBCvC8MjrNSxVpqfGEiQ15cJoQQ40KCMSHE+1T0/WOReCDW3wGW+AixGvtw6JdCCCGEEGJs+mOsgb1jmkbCv1W05AsKIYQQQoixSYix0ryPTNWiS0v/mBBCCCHEOIvHV8PEWmpC1KZJSCaEEEIIMW4uxlcDp4YlRlvRf6kk+VAIIYQQQoyH4X9iR2NAQCbzx4QQQgghxt/QGGtozKWSsgtNCCGEEEKMhZbwX6kjLf1EZkJRFWyF6TcRCWp4OkITmY0pQ29RyZ1upqPJSyQk4a8QQgghoiY0INObVK66vSDtMt6uEO8+3jaidBVVwWhTCQcihPyTG9gYLCqqXsHvGv6H2csa7BTX2zBYFFrf6Yt/bszSoUU0gn3yYl4hhBDi/WhCA7IYb1eI5h2upN+FAyMPQuxFBuZ9LJ9ze/s4uaV3rNkbk5k35uCoMPHWo+cJ+9PvS9sRL6pBpfO4L+HzhZ8txNMRZP9T7ROZVSGEEEJMUZckIAv7NTqP+YZf8ArXdyHI8de6JzsbQgghxPuCoipokcxG0kay7ES4JAFZJnIrTThKTXQ3++k+5Y9/brDoKF1kIxTUOLevj7JFdgz26MOhWaVGZqzKpqclQNcJH7ZCA84aC+1HvZhzDRTMMuM+H+TMrmjvnM6oUjjXQlaJEVWn4OsJc+FAX9I5bIqqUFBjwTHdiMGs4neHaTvoxXU2AICz1oLNacCcGy3CimV2tDCc2uZKWaGOMhO5M0xcOOTB0xGibGkWeoOCooDRrmPGqmx8rjDn3u0fzlQNCoVzrGSXGVH1Cn1tQc7t60sY3rQXGyiYaeHCIQ9Gm0pBrRWDNZrnC40e3OeDGCw6iuqt2J0GwgEN19kA5xs9k9r4hBBCiIlSsTILW76Bwxs60YaZVWQvMjLr5hzee6mLvgvBS5PBQaZMQNZ3PkT1DTnk11jY+3hbfPhv+qosCmotHH+9B51BIbs8GphANIjJLjfid0dL2ppnoHiBjaxyE7b86K7F3nVrztEzd20eRpsO9/kgkZCGc46FoqusNP2lm45j3nhedEaV2bfmkjXNSMAVwe8KkV9toXC2lVPbezm7pw9LvoHsciN6cyw4NIGmoSipXyBiK4zmr/dsAE9HiOwSI3qzAgrxfdN3huBdLu6fyuxb87DmG/C0B9E0KFlop3CulUN/6sDTGUrYb/s0A7YCA57OEDqjgqPchLPGwolNvZQvywIF/K4w9mId+bPMOMqMHHmxazyrUQghhJh0ql7BUWrCVmig9oN5HNnQlbIDwlZoYPaHc9GbVOzFhis8IFOjwcVgmka8pyfgCXNyi4uZaxxULM3ixJs9ZJcaKai10NPs58IBDwD7/tBO1jQj8z6WT8cRb9I5ZOYsHe+91EXXCT/axYis8joHBquOQ8910nMm2gNnduiY/0knM1ZnJwRkFSuyyJpm5MxOF2fecgPRnro5H8mlYlk2PSf9nN7Wy+ltMOfDeTgqTBz4Y8ewc8gGO/inDgCW/o9peLtCQ+aQVa/JxZJr4MiGTrpORPOcU2Gk9m/zqbzewYE/diQsrzer7H28DX9vNEAtnm9lxmoHVX/joO2Qh+Ov96JFNHRGlbm35ZFbZcaar3/fPOUqhBDi/SES0jj0XAdzbysgZ7qJmptzeO/l7iFBma3QwJwP56E3qZze5uL8Ps8k5XjIm/onhr3QwNWfKxryV/9pZ8Jy7Yc9dJ30U3iVFVuhgRmrHYSDGsdf7xnR9lr3uOk85osW/MWyv7Dfw3svd8WDMQBfT5jeM34MVhVjlg6I9lQ5Z1vwdATjwRhA0BumeacbRYWcSvMoSyJztkIDjjIjbUc88WAMoPt0gM7jPrKmGTFlJ8bT5/d74sEYwLn9HiIhDS0Cp7b0xhtiOBCh/Wg0ALXmT5lOUiGEEGLchPzRoMzTESS3yszMm3JA6f/eVmBg9ofz0JtVzux00brHnTqxS+CSXI39PWFa3x26o1qSjpnjr3dT/2knc9fmozMqnNjUkxBkZCKU5MnNWA+Y0a7DmqfDaNOBqmCwRgMxvUEhANicBlS9Qu+ZwJA0uk74eOvR80RCE/96CkeZCYCgJ0JWiTHhu3AgGlhZ8/X4e/sLMZKkOzbk1VD02pDXg4R90X/rzJckJhdCCCEuuaA3wqE/dzL3o/nkzzRDJIejr3RjydMz+yN5GMwqZ95yJ3TATJZLEpAFvZGMuwGDfRFadruZviIbT0eQ8/vHp/vQaNUxY3U2uRd7t4K+CFpIQ29JDEhi/w6keCfYSIclR8toi+ajdJGd0kX2pMvojUrSz4UQQggRFfRGOPSnTubclkd+jQVFp5A1zYjBotKy282Znclfy3WpTbnxKkVVcNZYAbDkGrAXG3GfG9pbNVKzPpiDvdDAiU09tB/pf1N+9Y05OGst8eUiFzvjdIYxb3JMYg8jnNrSi/t88gmG3m6Z+yWEEEIMJ+AJX5xTlk9edbRjpvUdN83bp0YwBpdoDtlIlDbYsRboObPTRTgYofoGB4pubGkas3RkFRvpPh3gwgFP2p8t8l0Mcsy5QyMyvUkhr9qMNW/i41hfTzQfigKus4GkfyGvvNlfCCGEyISqU1B1/SNLeuPUCoH05TdAKNT/R/M4pq6k+O8UrAUGShfZcJ8LcuZtN0FfhMrVDiqWZXNqW//TlLG5UoouxTaUQZ9fjFsMFiXhc6NVh6MkOldLU6Pr+HpCeDqC5FWZMOfo8PX0z18ruspG+fIsml7pwtMVupgXkuclmRT5i4Q1FF1i3rpO+phxTTbTFtq4cNhLyNcffNkKDNiKDPEnT1Pud7JtD5MXIYQQ4kpjytYxZ20+RruOC4c85JSbKJxnRYtonHhzYn/xp2QF6PWJfzpd9E9Vox0vigJ6m81GKBQiGAwSCk3MEJjRrlKxPCvpd5GQxpm33CiqwswbckCJTuQHON/owVlrpbjeRsdxX3zo0t8TRgtr5FaZ6WsP4e0O4WpNPawZ8IRxtQbIKjFSc0suPc1+jHYdhXOs8feI6U39UcnJrS7m/G0ecz9awNl33PhdYbJKjBRfFQ0W24/2/+qArycImKhYno3rrJ+OJh+R4MheturtCmErMFBytR1fb5jOJi8Bd4TmnW4qlmdR98kCWvf24e8NYc03ULLQTtAbpuM9L+ERbksIIYR4PzHadcz9SD6mLB3nD3o48XoPZoeOuWvzKZpvI6LBqc0TF5RZrVb0ej0GgwG9Xh//0+l0qKqKqqooioLeYrEQDAbR6XQEgxPzMjSjXUfJ1cknpof8Ec685Y4PVZ59101fe38+jr/RzfyPF1D9Nw72P9VGJBxdp2W3m7LFWVRd7+D8AU/agAzgvb92UXV9DnlVZvKqzETCGhcOegkHIpQusmMrMOJqjW63t9nPkQ0dTF/lYPrKbCDai9X+nodTW3oZ+ObXs3s95M4wUzjHQuEcC90nLxAJjuyp0OadLmo+kEvF8iy8XSE6m6JPhLbucRPyRShryGLGqmg+tAh0nfJx8s0eCcaEEEKINIx2lblr8zBl67hwOBqMQfS1Vwef62Tu2jym1dkgAqe2TkxQZjKZMBgM8b+BAVksKFMUBeXBBx/UgsEggUCAYDBI7d6vTEiGJoLerKI3q/h7wxn/BJDepGC06fC7whkFNEabDr1JSb+8AmaHnpA/Mup5XapBwWTXEXAn344pW49OD353ZFQ/yC6EEEK8nxitOuaszcOSq6ftiJdjG4f+lrQlVx8dyrSqtOyZmEn+Rxb8HIPBgNFoTBuQTbmnLEci5IskzK3KaB2/Rsif+dBsoC9MoG+YhbT+hwFGKxLU8HalTmPg+8aEEEIIkZrBosaDsfb3fBx7dWgwBtEpQ4ef62DO2nxKr7ajRZi012BMrUcMhBBCCCHGSNEpKCp0NPlo2thFyh+ZBjydIQ4910HQF0FnmLyn3C7rHjIhhBBCiMEC7jAHn+0k6IukDcZiPB0h9j/ZTqBvZHPAx5MEZEIIIYS44gQ8IwuuJjMYAxmyFEIIIYSYdBKQCSGEEEJMMgnIhBBCCCEmmQRkQgghhBCTTAIyIYQQQohJJgGZEEIIIcQkk4BMCCGEEGKS6d9+4jx6u8b05VYUw2RnJ43Yi90Gv0Q31edCCCESX4qZ7Pwp5873p8EvS5V2MOnUhk8XUVpv5/i2ifmV83GX2W+ICyHE+5tGf8ClDPhsILkIi1j7iLWXkRjNOiIlFSCnxITfNblvqL2iaGn+hBBiMqQKyoQAaR9TwOX500nDdbOnuwtM1tgUhu/WT5VeMoPTE0KIqSDdkGWqc1a686f0sF1Zkl27UtW5NmgZaSdjdvkFZMMFO8M1DJJ8n+6klEl6mZJGKYS4VAYPQ2VyIxmTbJ3xPBeKy0OyOh84DC7tZFxdvk9ZZnI3l6nhesQy3fZY8yGEEONp8PyxkVwcMzmHycX2yjZe1zFpJxm5/HrIIP0ExIl46nI8GpMEaEKIyTI4KBvJlI9MvxNXptE8iSvtZFQuz4AM0g9djnfwM9r0ZC6ZEGKyjOVCmmo9ubG8sg03ZSdT0k5G5fIdskxnvIOgwZMXR0oapxBiMky1c6GYuoZ71+do0hrt+u9Tl28PGSTvgRo4nDmwm36svVyjbVQSjAkhJkOyB5UyHapMts5Yz4ViakrXKzpcnSe73ko7GbXLJyAbSRe6kub7kf473baFEGKqS3f+SneuHGla4vKSaV2O5OnckaYtElyZQ5ZCCCGEEJcRPUB3qx9TVjQ2+9a7iyc1Q0IIIYQQV4q7785suYs/Lg5VKxxAZEIzJYQQQgghhtI3fLqIYDBIIBAgGJSATAghhBDiUpM5ZEIIIYQQk0wCMiGEEEKISSYBmRBCCCHEJJOATAghhBBikklAJoQQQggxySQgE0IIIYSYZBKQCSGEEEJMMgnIhBBCCCEmmQRkQgghhBCTTAIyIYQQQohJJgGZEEIIIcQk0/f09BAKhQgGg4RCocnOjxBCCCHEFcPtdqPX6zEYDOj1evR6PTqdDp1Oh6qqqKqKoijSQyaEEEIIMdn0DoeDYDBIIBAgGAxOdn6EEEIIIa4Ydrsdg8GA0WhM2UsmPWRCCCGEEFOABGRCCCGEEJNMAjIhhBBCiEkmAZkQQgghxCSTgEwIIYQQYpJJQCaEEEIIMckkIBNCCCGEmGQSkAkhhBBCTDIJyIQQQgghJpkEZEIIIYQQk0wCMiGEEEKISSYBmRBCCCHEJJOATAghhBBikklAJoQQQggxydS3nzjPgZc6CHjCk52XS2bz5s00NTWNW3rNzc08/fTTPP/88xkt//LLL3P27NmU37/66qucPn16vLI3pYVCIV5//XV+//vfT9g+Hzx4kJ07d05I2lNdODy5x7XL5eL5559n3bp1eL3eSc3L5Wb79u288MILdHZ2jnva4XAYv98/4vXWr1/P+vXrxyUP58+f58SJE+OSlhCXlUgA17kT7N+1mZ3H3PGP9Q2fLqLtlJvj27qpXm0f83a+//3vc/78+SGff+ITn2DVqlXDrt/V1UUkEiE/P3/MeYHoiae1tZXi4mIMBgMAb775JosWLWLmzJljTr+zs5N///d/Z+HChZSWlma0zl/+8hecTifTpk1L+v2rr76KyWSioqJizPmb6p555hn27dvHtddei9FoHJc0B7ehgwcP0tLSwtKlS8cl/fHK13hL1tb/8R//kU984hMsXrx4QraZjqZp/PjHP8ZsNlNXV4dOp7vkebic7dixgyNHjjBnzhzy8vLGJc3e3l6eeOIJ9u3bF2+Lt99+OwsXLsxo/Q0bNgDwt3/7t2POy7/927/h8Xj43ve+R0FBwZjTE+JyEOk6yrYDrYSM2ZjDfSi5/d/pAXJKTBzdPD530oFAgNWrV3PdddclfG6z2TJaf8OGDbjdbu6///5xyY/b7eb73/8+3/72tzMOmEbiyJEjWK1WPv/5z4972u8HjY2N3HLLLVx77bXjluZ4t6HxMtH5StbW77vvvglp95no6uqitbWV73znOxQXF09KHkSiRx55hJMnTzJ37lwKCgrYunUrv/zlL/mXf/kXCgsLL2lebrjhBjo7O8nJybmk2xXiUnN3tKHml+A0gmouYFZDNUU5Cq1vbaV5wHL6idi4zWZL2gsQDofZvn079fX1HDhwgPb2dqqqqpg7dy4QvSM8d+4cfr+fLVu2sHz5cnQ6HaFQiN27d9Pe3s706dO56qqrEtKbM2cO+/fvZ9q0adTW1sa319rayoEDBwB45513CAQCVFZWxr8/fPgwTU1NFBQUsHjx4oQ7+NbWVt59910sFgsLFixIetJoamri4MGDaJrGli1bqK6ujvd6tbS0sH//fgDmz5+f9qLY29sbH1Krr68f8v1o9x/gxIkT+P1+HA4He/fuxWQysWTJEuz2/t7Qs2fPsm/fPjRNY86cOUyfPn1IfR08eJC2tjYqKyuZN28ezc3N7Nu3D4vFMiS9VPkdKBAIsGvXLtxuN6dPn2bnzp3xHqxgMMiePXu4cOECTqeThoYG9Hp9fH98Ph8Gg4GjR4/ygQ98AFXtnwqZrA0N3M9UZZBJnodb9tSpU5w7dy6+H+FwmB07dlBZWcnp06eH5Kunp4f33nuP6upq9uzZw6JFiygoKEhZHzEnT57k0KFDGI1GFi5cSF5eXsq23t7eTm5uLhaLZdiy7ezs5OjRo8ybN49du3YRCASor69P2ZMLqdt5W1tbvE3v37+f7u5uZs+ePWR9j8fDnj176OrqIj8/n0WLFmEymQDYunUrJpOJWbNm8c477+Dz+ViyZAkWi4WdO3fi9XqZN29evCe5sbGR7u5u5s2bx7vvvovP52PRokU4nc749vx+P3v27KGzs5Pi4mIWLFgQP+63bt2K0WiktLSUPXv2sHr1arKysuju7mbPnj0EAgGqqqqoqalJWR7d3d00NjZSWFhITU0NPp+Pt99+m5kzZ1JcXMz58+c5evQo5eXlTJ8+HU3TaGxs5PTp0zgcDhYsWJDQLmNl9Prrr6NpGvX19Wl7WHfv3o3X642n4/V62b17N0ajkbKyMk6ePElhYSEPPvggiqLg8/nYtWsXx44diwdk7e3t7N27l1AoxIwZM5LWW0ysncyYMYOysrKE786dO5d0akhZWRkzZszA6XTicDiIRCL09vayb98+iouLCYfDNDU1UVhYSENDA4qipNy+EJeDluPHASdOmxEsuTj1elSCQ5abkIAslVAoxLp169i4cSOlpaWoqsqLL77IXXfdxbJly2hpaaGvr49QKERzczPLli0jEAjwwx/+EFVVqa6u5vHHH6euro477riDUCjE73+/jry8XPLz81mxYkXC9lwuF62trUD05DDwhLF582YOHDhAcXExr7/+OkeOHOHuu+8GoieZxx9/nKVLl9Lc3Mz69ev52te+NuQuv7u7m66uLoLBIM3NzRQVFQGwbds2nnjiCZYsWUIkEmH9+vXcddddLFmyZEiZuFwuvv/972O326muruYXv/gFbnf/mPLg/X/iiSeYP39+fP/XrVtHXl4eeXl5Q/YfoifoPXv2YDabmTlzJkePHmXTpk089NBD6HS6hH1VFIUf/ehHfOYzn2Hp0qXx9F977TWmT5+O3+9nw4YNLFmyhDNnzlBdXc3u3bt59dVX+c53voPBYEib34EikQjNzc2Ew2E6OjqwWq0A+Hw+fvCDHwBQW1vLX//6VzZu3Mg3vvENDAYDu3fvZteuXRgMBoqKirjpppsS0k3WhiAaYD/66KNJyyDTPA9XH9nZ2fz4xz9GURSWLFnCK6+8wmuvvcZ3vvMdtm/fPiRfZ86c4fe//z25ubkUFhYye/Zsjh8/nrI+ADZu3MgLL7zA0qVL8Xq9PP/883z1q18lEAgkbetPPfUUd955J/n5+cOW7ZkzZ1i3bh1Op5Oqqira2tpYv349Dz30UNLek3Tt3Ov1cu7cuXjZxwLCgS5cuMAPfvADXC4Xer2eUCjESy+9xDe/+U2sVivr1q3DYDBgMplwuVwAvPHGG1gslvgczBdeeIFvfetblJWV8corr8R7rD0eD0A8veLiYrq7u/nRj35EW1tbPA+VlZV87WtfQ6fTsW7dOvR6PXq9Hq/Xy8KFC2lpaeGRRx4hEAjE11mzZg233377kP0B4ulUVlbyjW98g8bGRtatW8eqVau488472bZtG3/5y1/44he/SHl5OY888giNjY3x9V944QX+4R/+IaG8f/nLX8a3v379er7+9a+nDJIPHDjAtm3b0DSNa665hgMHDrBu3Tquvvpq6uvr+fa3v43ZbI4HObF5fVlZWQDs3buXX/7yl4RCoXiaS5cu5Z577hmyrdh8stLSUr7+9a8P+b6pqYl169YN+fyDH/wgM2bM4Nlnn6W7u5v58+fT1tbGunXrEuoOojfNn/3sZ5PuqxCXi9rFS+PTSNKZkIBs165dnDx5Mv7voqIibrvttvi/V61axZo1awDiF9lly5bxsY99DK/Xi9vtjl8MN2zYQCQS4R//8R9RVZXrrruOb3/729xwww04HA6AlENetbW1FBcXs337dm655ZaEXqrS0lIeeOABFEVhrGnWKwAAIABJREFU5syZ/OEPf+Duu+/G7/fz+ONP8LnPfS4+r+I3v/kNL7744pBhyYaGBlwuFy6XK55fr9fLk08+yWc+85l478zMmTN5+umnqaurw2w2J6SxceNGrFYr3/zmN9Hr9Xg8Hv7pn/4p/v0rr7wyZP8feuihhP2/+eabhx3y+9rXvobVaqWrq4tvfvObtLS0UFFRQUtLC3feeWd8jpHZbGbTpk0J860+9KEPsWjRIgB+/etf09jYyP/9v/8Xk8mEx+Phf/2v/8XJkyeZNWtW2vwOvMiYzWbuuOMO3n77bVasWBHf/ssvv0w4HOaf//mf0ev1BAIBvve977Fx40ZuueUWAEwmE//8z/8c70kZKFkbguiFMlUZZJrn4eqjsLCQ22+/nT/+8Y+UlpbG24zVak2Zr0gkwpe//OX4BXbPnj0p68PlcvGnP/2J+++/n/nz5wPROXgvvPACf/d3f5eyrcdkUrahUIi777473nvz0EMP8e6773LjjTcmpDVcO6+oqOCWW27h7bffZu3atfG2OtDevXux2+3cdNNNrFmzhl/+8pfxG4jYfNNAIMBdd91FRUUFP/3pT+no6KCmpoYvf/nLvPDCC+zcuZPdu3cn3GytWLGC1atXs2HDBnbs2MHLL7/M5z73OZ599lna2tpYs2YNa9as4bnnnmP79u1s27aNa665Boj2INbW1jJnzhyys7N5+OGHCQQCfPGLX6SiooJf/OIXvPrqq6xatQqn05kwMV6v15OVlcW0adM4c+YM4XA4HmwdPHgQIH5erKmpYfv27TQ2NlJTU8Pdd9/NwYMHefzxx3n++ee599574+kuX76cNWvWsHHjRjZt2sTLL7/MPffcg9/vT3how2Kx0NDQwLZt2zhw4EA8IIPoucpkMiW0i4MHD7J//36Ki4uZM2dO/AYsHA5z//33U1JSwn/913+xc+dO6uvrufrqq+Pr7tq1i/Xr15Obm8uDDz445LwGsGDBAsrLy4Ho1I5nn32WnJwcrr/++iHLDizDb3zjG4RCIX7+85+zbds2br755oReTiGuVBMSkDmdTubMmRP/d3Z2dsL3VVVV8f/Oz8+P39kn09jYSG5uLjt27Ih/ZjabaWlpiZ/kB3eVZ6Kqqip+lxjrPQiFQhw7dgyfz0dfXx/btm0DQFEUmpub0yUX19TURCgUSugNW758OY8//jjNzc3MmjUrYfljx45RX18fHzayWq0JvQnjsf9FRUXxHqjc3FwURYn3ONx22220tbWxZcsWXC5XvIdpoNzc/lmHTqeToqKieDAUy28svXT5zWSOysGDBxOG0YxGIw0NDRw6dCgeNBQUFCQNxkZbBiPJ83DLrlq1it27d/Pv//7v1NfXs2DBgrT50ul0Cb0d6erj2LFjKIqSMJz64Q9/eEh9pZJJ2ep0uvgQqaIo5OXlJfTYxoy0nSdz0003sWzZMvbt28eGDRvo6ekBoj3PMYqi0NDQAEBJSQkdHR0sXbqUgoICamtr2blzJ729vQnpLliwAKfTyc0338yOHTviT+/GgqKsrKz4dASIDoPHAjJFUfjyl7+MoiicPXuWzs5OsrKycLvdHDx4EIfDwZkzZzh16hQ9PT38+Mc/jm831gtWU1PD2bNnaWlp4cCBA+Tn59PR0UFrayunT5+mpKSErKyseLBUUFBAY2MjmqYB0aHvgRYvXozT6eSmm25i06ZN8f352c9+ljAk+G//9m/U1tZit9s5cuQI4XCYQ4cOYTKZhgzBNzc38+ijj2IwGPj85z+PTqfj+PHjuN1uKisr4+12zZo1/Pa3v+XgwYPxgEzTNB577DEAHnjggZRzwOx2O3a7nZ6eHjZu3Iiqqtx7773x3rhkpk2bFp9WctVVV7F7926am5slIBPvCxMSkFVWVrJ69epxScvr9RAMBuMnL4B58+YlHQIZK03T8Hg86HQ6Dh06lPBddXV1Rmn4/X6sVmvCfDSdTofRaBxy4YBoT0O6Bx68Xu+Q/Z87d+647f+mTZtYv349y5YtIz8/f8SBTkzsYjLW/AYCgSFzaAYGfBNhJHnOZNm6ujoOHTqUdD7gcNLVRzAYxGKxJMypMZlMGdfZaMs2VrcDjbSdJ3PkyBF+9rOfYTQaqampiQ+fJdsekHIuUarlYwF4bAgsFri+/PLL8WUsFsuQ1z/EthNb3u128+c//zlhHY/HQ1VVVcKQeexGc/bs2WzatIk333wTl8vFl770JX71q1/x2muv4fV6473PsfTffvtt3nnnnXjaweDQuSWx76B/mHHp0qUJN7dmsxmdTkd9fT1bt25l27ZtdHd309DQkPAE8/nz5/mP//gPAoEA9913X3wOXmxYdGAbiZXh4KA/NqS5b9++tE+DRyIRfvWrX9Hb28vatWtH9GT74PoT4kp3SeeQjUZBgZOCggI+9alPDfluNO/RGY7T6SQcDnPHHXcMuXhlorCwELfbjcvlit8JdnV14ff7kz7anZ+fnzCnZbCCgoIJ3f8NGzbw8Y9/PN7ToWkaZ86cGXV66fKbCafTOeQdbWfPnp2wV0XAyPI83LLd3d08//zzLFmyhGeeeYa5c+fGLyyZSFcfBQUFuN1uvF5v/OLc0dHB+fPn4w/GpDOeZTvSdp7Mxo0bCYVC3H///Vx11VVs2LCBlpaWYddLFYDFxIbxYvsa6+HNy8ujo6ODr371q/FewIH5Hyz2qgm73c73v//9eFAzcJ2PfvSjQ9arqalBURS2b9+OyWSirq4uPkQJxB+8iaV/6623xoeE3W73kPNObH8uXLgAEO+RivXqDdbQ0MDWrVt54YUX4v+O6ezs5Cc/+Qlut5svfOEL1NXVxb+L9QafO3cOTdNQFCVeH4N7ij//+c/z9NNP89JLL8WHqJN5/vnnee+995g3bx4f+MAHki6TbF+hv/7G65UfQkwWT2cHal4x+cO82WlC3tQfCARwu90JfwMnxaZjMpno7u6OH5grV65k69atHD16FIie9P/zP/8zPryRSXoQvXClEzvHl5eXU15exh/+8AcCgQCapvHGG2/w17/+NaPtxZ6eevrppwmFQgSDQZ5++un454MtWbKEnTt3cvz4cSA6Cb+rqyv+/cqVK9m2bduo9384saGKcDjM2bNn2bx5M5FIZNTpjTW/1157LTt27ODYsWMAHD16lLfeeivlxSeZwW1oPPM83LKPPfYYtbW13HPPPRQUFPD000+PKF/p6mP69OkUFxfz5z//mUgkgs/n47HHHmP37t3x9CF1Wx+Pso0ZaTtPJhbgbNq0ib/85S+8/vrrwPAvsx0uIHvyySfjL6OF/oAk1mv/yCOP8Nxzz/H73/+eb33rWxw5ciRpOnl5eVx11VW4XC5++MMfsn79en7+85/zve99L20voM1mo7S0lHA4zNy5c9Hr9dTV1cX3K/aU5jXXXIOiKDz33HM8+eST/Pd//zff/e534+/6inniiSf485//zG9+8xuAYd8ZFhu27O3txWw2x4crg8EgP/nJT+jq6qKgoIDDhw+zbt061q1bxyuvvBLf37a2Nh5++GGeffZZXnrpJfR6PStXroynH3to5fbbbyccDvPb3/6WUChEIBDgn/7pn/iXf/mX+HBprDfSaDTy+OOPs27duvhUkGRiDwI89thjNDU1kZ2dndHwtxBTWfOxJk50+IZdbkJ6yF588UVefPHFhM9uvfXW+ET+dBoaGti8eTMPPvgg//qv/8rChQv5yEc+ws9//nNUVSUcDnP99dfjcDgy6iEym80sWLCARx55hNWrVyd9cg4gNhqiqir33Xcfv/vd7/j7v/97jEYjeXl5Gb9nTFEU7r33Xh599FG++tWvomkalZWVPPDAA0mXX7RoEYcPH+YHP/gBOp2O2bNnJ8yXWLhwIR0dHTz88MPx/b/uuusy3v/h3HHHHfz2t7/ljTfewOFwMH/+fHbs2JHwlNVIpMtvJubPn88tt9zCT3/6U1RVRVEU1q5dO6Lhv8FtaDzznG7ZrVu3cuzYMb773e+iKAp33nkn3/ve91i0aBHz58/PKF/p6kOv13Pffffx61//Ot625s2bx8c//nFg+LY+HmUbM9J2nszatWtpbW1l//79HDlyhLlz57J3796kL5YeiZycHF566SU0TWPBggXxQOzGG29EURRefPFFXnrpJRRFoa6uLu0czC984Qs888wz7Nq1i9OnT2M0Grn++uvTzoOCaFB05syZeA9UXV0dTz31FKWlpfEpClVVVXzlK1/hySef5I033gCige7g+pg1axZ//etfiUQiXH311WknxUP0HLZw4UI2b95MXV1dfM6g3++P97K1tbUl9MzPnDmTG2+8kbvuuiv+4E5jYyMOh4NPfvKTSed/Llu2jJ07d3Lo0CHWr1/PLbfcQiAQIBKJoGlawvy22JBsLB/JngiHaC/ukSNHaGtrw2azcffdd2f0dJoQU1ltwxIMQ158bmH6sg9QNWDah/Lggw9qwWCQt55qZe7fOvjd7353aXOaRCAQwO/3J5z0NE2jp6cHm802qgM0tm7s5JQJn89HIBAY8lBCpnp7e1FVNaOhT4/Hg6IoKedajXX/04lEIrhcroyDpkyMNb+hUIje3l4cDseo3vCerA0NZyR5Hu3+ZZKvTOojNq8m2XDocG19rGU72Ejaear1bTbbmPPy4x//mCNHjvC1r32N8vJyAoFAynLu6enBbDZnPP8uHA7T29tLVlbWiM4hmerr60PTtJRlGAt0kj3NOBE8Hg9+v5+cnJwRvQcs1gs40rpsamrihz/8IbW1tfz93/893d3d2O32CSlrIS612I2F0WjEYDDEX6+j0+nQ6XTxG2Q9QHerH1PW1PmdcaPROORndBRFGdMbnUcTbJjN5jGdAEcSyA03z2is+5+OqqrjGozB2POr1+vHNHckWRsazkjyPNr9yyRfmdRHuvYy3LpjLdvBRnvDMl7rJzPcww4jbe86nS7haePxNtwvmYzXz4plymq1jmjuY8x4/TyWvL1fvB/p337iPHo7VK1wAKOfOySEEJMpLy+PoqKiSx68iLEzGo0UFRXJBH7xvqZv+HQRwWCQQCBAMCgBmRDi8hT7pQ1x+amoqOC73/3uZGdDiEk1dcYphRBCCCHepyQgE0IIIYSYZBKQCSGEEEJMMgnIhBBCCCEm2bi+5OWl/31yPJMTQgghhJh0tzw0Y8K3IT1kQgghhBCTTAIyIYQQQohJJgGZEEIIIcQkk4BMCCGEEGKSXbYBmc6goDdl/qO34vKmNyroDFLf4sok57PMGCwqymV71RIivXF9ynIkam+I/lBvJKzR1x7k3GEPkZCW0brzP1xA8Vwb4UCE5j0ujr7RPZFZvaxc80ApB17soPOUb8K3Vb3KQWtjH97u0LimWzzHSiig0X7MC8Ccm/IoqbNzZq+LIxu7xnVbQlwKCz9eSMdxL6d3u4Z8l+p8NtJjOavQQH6lhZM7e8c171OBqlNYclcx5iwdR17t4uyBvpTLzr0ln6AnzNFNQ68L6epBiMmm7+npIRQKEQwGCYXG98KaiqJA1UoHhzd2EQlGKKy1Muu6XHb9/hzenvR5cJQYcc608NqPThMOaqh6uau8VKpXOQA4tqUHgFBAQ4tkFkSPRDikEQlH09WbVaYvyebVH50m6L38f2tV1Stc++VS3ny4ZcgNSLrvxtvgupyqaV7pxvN8FglDKHD5HiOLPlXIyR29dJwcGoA6Z1rQGRTe+I8zk5AzIcbG7Xaj1+sxGAzo9Xr0ej06nQ6dToeqqqiqiqIoqA6Hg+zsbLKysrDb7Zc0k2f3uzn1lou9f2yjtdHNvA/lJ3xfUG2h/OosbPkGAExZOqbNsxMOaBTWWFFUKKiyoDdH+7CLZlsxmFWK59oorbcPGeIanN5IGCwqZQvslC20Y7TpEr7LLjZS0ZCFc6ZlyHqKCtPm2RJOtEWzrRgs0Tyb7DrKr86ieK4NvXHAMnOs8fwrSjQNRQG9SaGwxoLdaaCkzjZsvrOKonkrqB6aN0epiYqGLPIrzcPua26FiZwyMzllZvKmR5fv6wgmBA6ptjVcvQzmd4UJ9IXRGRTK6u1EwhoFVRZM9v5yH2255laYsOT0dwwPThfAlm/AUWKM/ztvhjmers6gUDTbmrBsRUNWNC8Xk9GbFJwzo+mWLbBTWBNdXm9WmTbPhsWhZ9o8WzzNdN+peoVp82yULbBjdiTmcyBVr1BylY3SOjtGa3Rdc5aOotr+vNqdBvJnmJPWZdFsK5YcPWUL7egullfedDPTFw9tH3qjQsn8aJ7StY/BUrW3VPuYW27C4tDjnGWhoiELs0MX3c86G2UL7QnlVzTbitGmUlJno3huYl0kO15SnQtSHcvmrGh7KplvG9J+Y+1+8H7b8g2UX51FTpkpaXkMdz4bLNV2YsKBCN6u6A2tNU+PY5qRrEID5YuyEtrzYMnqU29WKazpLwOTXUdBlaU/7VIT+ZVm8ivNIyrj3HITZoeO/Coz5Vdnxeu7aI4Vu9NIQbUFW0FinVhy9DhnWtAi/efBWJ6SHeODpauHZGU6eP+EGCu73U5WVhbZ2dk4HA4cDgc5OTnk5OSQm5sb/5syo/Gn3urFWW2Jn+zq1hZQuTwbk13H4s8URU9URhVztg7VoGAvMKAoCvM+lI8tN3qBnX9rAYvuKCK7yEhpnZ1FdxTF00+WXqZs+QZW3FuCNdeAvcDINQ+Uxi/ipXV2FnzUid6sUrXSQf1tzoR1tQhUX5MTvzBac/XUr3USDmo4Soysuq8Ea54eZ7WFlfeVYrRFq6R+rROjNboNVa+w4GNOVL2COVvPgo8VUn+bk+yi5Cf6mIqGLBZ9shCjTUfVcgcNA8qjaoWD+o8UYLTqmHltTjwYTrWvlmw9RquK0RqtA4gOJdqdxmG3la5ekilbmMW0uTZUnYI1V4+igr3AEA8UxlKu1StzEk6+NTfkkj0t8WJlztIx70MFQDQYvvoThZTWRW9W8qssTF+cDUDxXCuLPlWI3hQNNpfePS26/sU6qvtIARaHnjk35THz2hx0eiV+cbLlG9ANCCaTfac3qaz6UgmFNVasuQZWfKGEvBlDLxA6o8KKe6PfZRUZWfnFUow2HQFPhNo1uRTNtqLqFRruKEJvUpPW5fxbC1j8mSLyZ1jQ6RXm3JRHzfW5qAaV2WvyqPmb6BQDo1Vl1f2l5E03Y8s3sOpLJdjyDUnTHChVe0u3j5UrHCy5q5iCKgu5ZWZWfrGURZ8sJKvAiHOmlRVfKIlfnOffWsCiTxWR5TRSuTybhk8XJ9TFwOMl1bkg1bFsK4geE+bsaFCy8ksl8fNU2UI7Cz/mxGBRqV2Ty6zVOUA0mF3++WnY8g1ULnckPd8Mdz4bKNV2BsopMzHz4ufOagtXf6KI6mtysDr0LPnstKSBXKr6tOXqmXtL/w1y9jQjNRenmTirLTTcUcSs63Kx5OhHVMYzljlYcmcxhbOs5JSaWPWlUvQmFXu+AZ1BwezQY7QkXpYMZhVTlg69KVpOKKQ9xgdKVw+pynTw/glxqUyZ1hboixAJa5iydBgtOvKmm9n0H2fQNHBdCFC1ysGux85xtrEPS44+6fwAgPde66LzlA+jVeWGf6hAUcExzZQ0vfbj3ozypuoVDrzYEZ/TlFVkoKDKQss+N86ZFprfcXFiey+n3+qlcpljyPqt+9wUz7Vx9kAfRbOtnL84X652TR5HN3Vz+u3ofIZ5t+RTvTKHQ3/tTJsfTYOdvztLyJ96WEvVK8xek8e2X7XibgsCsOr+UgprrXQc9zJzdQ6bH2nB2xPi+DaFFV+YhqpX0u5r9rToybZ1f1/G27pwxAMkrxdtmNGVoC/CyZ29lNbbk9b3eJdrTOdpH7ZcPQazSvY0I57OEEW1Vk7u7CV/upkLR6P7FA5q7Hn6QnSfFfjAN6f3ByMKvPPMBUIBDVdbgBlLHTS92c2xzd1Ur3JwbHM34WB//fnd4SHfzbw2B9eFAO/+qQ2A3vN+5tyUx9b/ak3I7/TF2bjOB2hc3xHdtAoVi7JoerOb/c+3U3+bk9xyD91n/Jy/WB/J6rJxQwedF4eL3O1Bjr7ZTcgXoeu0jwUfdfLea11MX5JNx0lffFt+d5jCWisntvWkbB86g5Kyvc1Ymp12H5t3uzi+LToEeu2XSzl3yEPznmi9rvl6BdZ8A33t0TZ38MUOes4GUHUK1/3PMnLKTIT8kYTjJac09bkg1bGcN91Mz9lAfG7XjGXZGCwqWiQczeujrXg6Q5x+28X1Xy2naXM3M6/J4chrXTRfnKu05M7iIe2sryM47PkMQNWRcjvpjiFPd5C9f4yWq96kUlBlGTIfLVV9dp5If250twXY+btzQLTnNdMyBjjb2Bff35yyUhwlRo5t6aFojo3W/W66mv0J2+o9F+D8EQ+FNdb4epke46nqIV2ZDt4/IS6VKROQqbpoMBAOaGRXG9EbVZbcFT14dIboHVIm/O4wQDRYUaLrZpeMPj0A1/kABVXZVK90oOiid2nnD0UvbCd29rLwY06cM620HfMmnVDbst8d7SExKBTNtnHs4kHvKDZy8KWO+HLtJ71ULh0a0A0WCUbSBmMQ7WUJh7R4gATQedKLY5oRb1eQkD8Sn68XCWls+UXrsPs6mm3FArJk9RLyj22+y3iXa4wWgY4TPvIqzeTPsNC0uZvZa3IxWFTyZpjjF7nuZj8zV+fgmGYCBRSdgs4QvUuPBCOEAlp8n9MNqaSSXWykfcCFseOEjwUfMw4JZh3TTGRPM7L07ou9Qll6uluiF7WuZj9tx7yUX20fdv6N3xWO/3dXs495H8zH4tCjM/Q/4ZpVaKTtWH+eMplAbs3Vp2xv6fYRSGgjQV8En6t/jmkoEEE/YPgwVt6RsEZPqx97gYHuFn/C8ZLuXJDqWD5/qI/SOjur7iuhrclL8zsufL1h7E4DOqPK/FsL4nlQdQoWhx6700D3X/uDn+AY2ro1L/V2PF2p59zGjrlYWemStMFU9emYlnqIc3DaQMZlnCxfeuPIB2oyPcZT1UO6Mk22f0JcClMmIMudbsLnCuN3h4kENXrPB2h8oT3+vTaGOc5jTS82L2r3U+cJ+TWu/mRh/LueFj9v/OwMjhITZfV2lt0zjc3/ryVhfb8rTHeLn7IFdmz5hvidYtAXSRi20htVIrFJ8mOc0x30hRPSBtAZVQKeCEF/JOU8rnT7OpptTaTRlGum9X7hqIeCSgv5lWYOv9LJ+XITpXV29EaVvo5o4DnvQ/l4OkPs/N1ZNA1u/EbFuO7f4P3QGRXQhu5DOBjh3IE+mt/pf3IsFpwoKuSWm4mENLKLjXScyOyJvSWfLWb/8+20NXnJKjKy5M6ipHnKaD/StLdM93GkdAY1oQcyJt25INWxHPBE2PGbs1jz9BTVWll5bwmb/7OFcFAjEtLY/3x7wja8PaFxfeAo3XbGKlV9jqX4x/v8nUzac+cAqephIstUiNGaEnPIHCVGrvpgAcfejPZwdJz04Sg2ouoUPF0hIhENR0n6+VLpZJqe3hSdtzOYNUePrydMyK9hsutwFEd7RABW3FtCUY2VnhY/R9/owlZgQNUNPQG07HNTc0Me5w71xXs32o56qWiIzkdSVChbYOfCe9EeJZ8rhN0ZnVOUahJvOr6eMJ7uEKX10blPJruOwhorF4568PWECfRFn26F6KTZNf9fBTqDknZfI2Et6YU13bbSMZjVlBOeMzXScvUPKFdzlg5rijkibU1eSursuNuiDy6cP+Rh5rU5tDX175M114C7PYCmRetIZ1DjZZVK7KnUZL0Vg79ra/JQWp8VL/PpDdm0HfUMuVq2H/finGUh0BfG0xWKzuu52CMx89ocPB1B9jxzgfkfLoinnaouITrEaLLpcF0IAFyc3B1dtuOEN+EBhvm3FsTnLaVrH6naW6b7mInYXEBrnp6cMhPdZ/xDlkl3Lkh1LM+5KY/ZN+bh6QxxYnsvPlcYW4EBb3cIX2+I3Aoznq4Qvt4wBdWWiz2sXsoWZAHR80pW4cgfJIpJt52xSlWfflcYk02H4eIDBnkVmZ+DRnv+1sJaxr1l6Y7xhLykqIeJLFMhRmtSe8hWPVCKQvSupOnNblr2uYHowXLgxQ6WfLaYgCeMwaLjwIvt6RNLI9P0Zq3ORVHh4MuJ8xBO73ax+M5irvufZYT8Gj5XCHNWtOgOvNTBwo85mbk6B4tDz+FXOuOvbBjo3KE+rvpQPmcb++fXHHm9i/q1TlZ/pQxVr9BxwsupXdEhg6Y3u1nwUScBT4S+zuCQ9DLx7rMXqP9oIZXLHRitKk1vdtN7NnqR3fvsBepvc1JzXQ56k8re/24jHNTS7mv7MS8Nny5CZ1TY9+f2jLeVSnaxkdob89j2aGva5dIZabmeequXxXcWU1RrJRKO3mkn43eH8XQE43Ouulv8hIMabU39wztNb3ZTt7aAWdfl4ukMEvCEMWfrEob+BouEo+W46r5Sdj91gZ4Wf8rvzh304Jhm4tqvlBEORAh6I+x55sKQNFv395FdHF0u6IughTX2PHOB7GIj05dks+X/teBzhek44WPOTXk0ru9IW5fhoEbT5m6ueaCMoC9M12k/BouKqldo3d9HbrmZ1Q+WEw5G6D0X4PjW6ByvdGmmam+Z7mMmyurtVK/KwZyt473Xu/D29AffMenOBamO5WNbu7n640Vc8z9KMVpU2k/46LzY07j3j23Uf9RJ1QoHBosan6v03htdNNwRPY7CQY1QinaWqVTbGatU9RkJaZzZ6+bar5QRCWm429MfywON9vx9/oiHq24twGhVOfVW+v1Ld4wPlK4eJqpMhRgt5cEHH9SCwSCBQIBgMMjvfve7USf20v8+OX45u8hoUwl6IuPW5T2W9AwWNeW7sEx2HQFPOOUdltGmsuLeEt746dB5PDqDAgqEA4PeS6VTUPUMO18sk3yH/JGkeTNao/s0uDxS7avOqKBGxZ0EAAAgAElEQVRFSPmerHTbGqxyeTbWXAMHXuwYfuEURlOuKNHeufF4r5miRodnR3rBNVpTD+kO/k5Ro5Oyh8uvokb3K9Oh4uHqUmdQ0LTk36u66LDg4IB2uDRTtbdM9zGVNV+vYPuvz+LrDUW3n+SmaEheUpwLUh3LRmt0GDTZUKjBosaHwRI+Nw8to7FItZ2xSlWfepNCJJRZeSYz0vOtwRI9ljJdPuUxPjjdNPUwUWUqriy3PDRj1OvefffdGAwGjEZjyneRKYoydeaQpRLoG98+5LGkl+5ikW4SaGGNlRnLsjmd4q4v2QkeoifByDjMLU2X71QX71TrDHfiG8kFNeTXOPLa6N+8P9pyRRtZPtPRIoyq9yNd0DT4Oy2SWX61SPp0BxuuLlOWH9HevEh46LaGSzNV/jLdx+Gky/OQvKQ4F6Q6ltOVbaq8j2cwlm47Y5WqPsd6MzjS8+1I9y/T+k5XD1fCC6fFlWHKB2RXApNdx9nGvvjj+iJqrOUh5SpijrzWhb9PnowTQly+JCC7BCRgmBhSriJG5v8IIS53U+IpSyGEEEKI9zMJyIQQQgghJpkEZEIIIYQQk2xc55CN5bFQIYQQQoj3hwjBoIJO1/+zYvq3nziP3q4xfbkVZfQvkxZCCCGEEGkF6Tm5nwMnOvGjohgclNc1sKDUhtrw6SJK6+0c3zb8jwQLIYQQQojRifQcp/FYgKKG67n55pu4dq6FC3v3ccpzcQ5ZTokp7c+9CCGEEEKIkXN3tNFz8RcQXec68OWVU5VrBFTsZVWUmjtpbfPLpH4hhBBCiInScvw4zR1BIILPF8BgsdA/Q8yOzQpet19eDCuEEEIIMVFqFy/FYDAAYSIR0A3qC1NVCEfC0kMmhBBCCDHZJCATQgghhJhwumhvGIk/aB+JgE4d3G8mhBBCCCEmgIrZbCTY5yUY/8xLnwcsdpMEZEIIIYQQE8XT2YHrYgSWVZiPubuZ493RD/znjnHO56DEaZJJ/UIIIYQQE6X5WBMRLYd8mxE1t4q50w9y4K03aNUrRCI2SuoWU24d559OEkIIIYQQ/WoblmAwGi/+y0DuzEVcV6MRiigYrFZMqoqCTOqfUs6dO8f999/Pvn37xj3tUCjE7t272bBhA5s2baK9vX3EaWzZsoX7778fv98/7vkTQgghrkiKMvQz1YDJZGLgL1bqAbpb/ZiyJDa7UnV2dvKTn/wEj8fDjBkzuHDhAs888wz33HMPixYtmuzsCSGEEO97F39cHKpWOGDQo5jiyvDUU0+hKAr/5//8HywWC+FwmIcffpg//OEPEpAJIYQQU4C+4dNFBINBAoEAweD4BGTnzp3j7bffxuVyUVRUxPLly7FYLAD09fWxdetWFi9ezOHDhzl58iQ2m42VK1eSn5/Pvn37OHToEGazmYaGBkpLS+Ppbtq0icrKSlwuF42Njej1epYsWUJ5eXnC9j0eD9u2baOtrQ2n08mKFSuwWq0AnDlzhoMHD9LQ0MCbb76J0+lk5cqVw6432KFDh+jq6mLFihXxz/bt24eiKMyfPz/+2ZYtW8jPz+f/b+++46SqDv6Pf26Z2d6BpXdEOsIiKIKgRlSCioXHWKL+1Fix+5hEbI8xiRpje4zJY4+IBUGiGAQFpAQLYKG3pXfY3qfc+/tjmGGH3YUFZ1nE7/v1mtcyd+4998y5y2u/r3POPbdbt24ABINBFi1axIYNG0hKSmLgwIE0a9YsqmzHcfjyyy/ZuHEjqampDB48mPT09FrrsXXrVlauXMnw4cOx7f1TAhcsWEBaWho9evRg48aNDBs2LHINLMsiJyeHFStWUFpaSnJycqRuixcvZsOGDXi9Xvr27UuHDh3quswALFy4kJKSEoYOHRp1fhEREam/mP8FXbhwIa+//jpdu3alSZMmfP7558ycOZMHHniAxMRESkpKmDx5MnPmzCEzM5PU1FS++eYb5s2bR79+/Vi5ciXt2rVj2bJlzJgxg9/97ne0bt0agKlTp+L1erEsi7Zt27JhwwZmzpzJ2LFjI4Fn586dPPPMMyQkJNC+fXtmzZrF7NmzGTduHAkJCeTm5jJ58mRmzZqF1+ulZ8+e9TruQPn5+bz99tv079+fuLg4ACZMmIBlWZFAVllZydtvv81VV10FgM/n47nnnmPXrl10796d3NxcZsyYwd133x0VfN59911SU1Np0qQJixcv5osvvuCBBx6oNZR5vV4mTZpEdnY2vXv3BqCkpIS33nqLa665BoAnnniixnEFBQUkJyeTlJQUqdszzzzDzp076d69O8XFxUyfPp3Ro0czYsSIWq/1Z599xuTJk7nuuusUxkRERH6EmP8VnTlzJkOHDuWyyy4DoLCwkN/97ncsWrSIoUOHRvYbOHAg559/PgCbN2/mj3/8I+vWreOhhx7Ctm18Ph+///3vWbBgAWPGjIkcl56ezl133YVt21RVVfHUU08xadIkxo0bB8Bbb71FVlYWd999N7ZtU15ezsMPP8xnn30WOR/AyJEjGTJkSOR9fY8L69mzJ47jsGbNGnr16sWmTZsoKysjEAiwdetWWrduzdq1a3FdNxL6Pv30U3bs2MGDDz5IRkYGruvy/PPP88EHH3DfffdFlX3llVdG2u/RRx/l008/jbRpdc2aNaN9+/YsXrw4Esi+//57PB4Pffv2rfUaFRcXM3v2bM4880yMfZMNP/nkE7Zu3coDDzxA8+bNI9umTJlCz549o3oqIdTzN2nSJK644gpycnJqPY+IiIjUT8xn8v/2t7/lsssuw3VdioqKCAaDJCUlkZeXF7Vfp06dIv9u06YNlmXRp0+fSE+L1+slOzub/Pz8qOM6dOgQ2ScuLo4zzjiDrVu3UlJSQkFBAbm5uZxxxhmRfRITE+nZsyerV6+OKufkk0+O/PtQx5WUlDB79uzIa/Xq1aSlpdGmTRtWrVoFwHfffUf37t3p0qULP/zwAwCrV6+mbdu2pKamAqHew/79+5ORkQGAYRicfPLJrF+/nkAgEKlPOFhBKID279+fFStWADBv3rxIPb7++msABg0axJIlSyJlfPvtt/Tp0yfSc1ed3+/npZdeIjMzk7PPPjuyfeHCheTk5ETCGMCIESPwer0sXrw4qoxvv/2Wt99+m9GjR0eFWhERETkyMe8hy8vL4/3332fZsmXExcXh9XopKyvDdd06jzEMA8MwsCyrxnbHOfi8tqZNmwKhIUSfzwfAK6+8wiuvvBK1XzgE1Sa8BERdx5WVlUWFEr/fT9euXenZs2dkiYolS5bwi1/8gsrKShYsWMDIkSMjvWcAruuSl5fHvHnzmDdvXo06FBQU1Fm/Jk2a8NVXXwGh4Bf+nhkZGQwcOJCcnBwmTpzIihUr6Ny5M2vWrOHmm2+uUY7rurz66qsUFhZy//33R9rbcRzy8/PJzs6O2t+2bbKysmoskTF+/Hhc1z3k/DIRERGpn5gHshdffJHU1FQef/zxyJyn6sNxsVZZWQlAfHx8pIfoqquuom3btlH7HRj2qjNN86DHNW/enHvvvbfGcb169WLatGls2LCBHTt20KtXL/x+P++99x7btm1jy5YtXH755cD+0Dl06NBae5UyMjLqXBussrKS+Ph4AG6//fYanycnJ9O9e3cWL15MWVkZiYmJkTl11b3zzjusW7eOe++9N2o+mmmaWJaF3++vcUxlZWWNnrZhw4axfft2Xn31VcaNG0dKSkqt9RYREZH6iWkgKykpYfv27ZxzzjmRP/jBYDBqOO7HOrDHbO3atZGenNTUVCzLIj8/P3LnJIQWRT1YIGvZsuURHdehQweSk5N577336Ny5c+RuxXbt2vHBBx+QnJxMu3btIvu3bt2a7du317gr1O/3R02KD4fMsHXr1tXovTrQoEGDGD9+PCUlJeTk5NSo99SpU/n666+5++67o4Ylw9q3b8+qVasYNWpUZNvOnTvJz8+v0RN2/vnn4/P5ePzxx3nttde4/fbbI3PRRERE5PDFdA5ZUlISSUlJzJ07l61bt7Jx40b+/ve/U1FRUWvvy5H46quvWLp0KWVlZSxatIgvvviCwYMHY9s2CQkJDBkyhOnTpzN//nxKS0vZtm0bTz/9NFOmTKmzzCM9zjAMunXrxsaNG6Mm0J900kmsXLmSHj16RAWVc845h3Xr1vH++++Tn59PQUEB48eP569//WtUuR9++CErV65k7969fPLJJ6xbt45hw4YdtF369OkDwIoVKxg4cGDUZ/Pnz2fq1KmMGjUK0zTZsmVL5BWe23fOOeeQm5vLhx9+SGFhIVu2bOHVV18lKyur1kn7KSkpXHfddaxatYpPP/30oHUTERGRg4tpD5lpmlx//fW88cYb/OEPf8A0TQYPHkyLFi3YuXNnTM7RqVMnPvroI7Zs2QJAt27duPDCCyOfX3rppZimybvvvsv48eMxDIPevXtz1llnHbTcIz2uV69eLFy4MCqQ9evXjylTpkStRwahoHbVVVcxZcoUZs2aBUCrVq24+uqro/Y79dRTee211ygpKcE0Tc4888xD3slo2zb9+vVj7dq1tG/fPuqzOXPmAPDBBx/UOK5///7ccMMN9OrVi6uvvprJkyczffp0ALp06cKNN96IN/IMrmhdunThggsu4KOPPqJLly507tz5oHUUERGR2hljx4519y8M6+fNN9/80YW6rktBQQEJCQm1ruF1pO677z4GDhzIJZdcQllZGcFgMHIH44H8fj+FhYUkJycfVh2O9LjD4bou+fn5eL3eOudfOY5DQUEBiYmJDVaPg503ISGhzkVxRUREpH6uvvpqPB4PXq8Xj8eDbdvYto1lWViWhWmaGIYR+0n9EBrKy8zMbIiiI8ILmtbF4/FE7sA8HEd63OEwDIOsrKyD7mOa5iH3aQiNdV4REZGfMz1RXERERKSR2UVFRQQCAfx+f0zvhmwIN910U51DlCIiIiLHmtLSUmzbrnO4MjJkmZaWRvU5ZMey6qv7i4iIiBzrkpOT6zWHTEOWIiIiIo1MgUxERESkkSmQiYiIiDQyBTIRERGRRqZAJiIiItLIFMhEREREGpkCmYiIiEgji+mjk3Jzc2NZnIiIiEijOxrroMY0kGnhVhEREZHDpyFLERERkUamQCYiIiLSyBTIRERERBqZApmIiIhII1MgExEREWlk5qIJu1g+LQ9febCx6yIiIiLy81C1i+9mT2P6dztxADPn8mxa9Ulm/YLixq6aiIiIyM+An12rlrOjwiUYdIB9Q5bpLeOoKlEPmYiIiEgslebtocgfvc2/dzWr8tJp1XT/crCaQyYiIiLSQLatX8+WvGqJLFDA6hV7SenRgxbVlueP6Ur97s6d+O6/n+CMGbBrVyyLFtkvOxvr7LPxPvEERvPmjV0bERGROnUdMBCPx7PvnUPBuhXsTTmR07LjKK0WlWIayHx3303w3XdjWaRITbt2EXzrLXx+P3ETJjR2bUREROrFKcpl5e5EupzWnDigtNpnMQ1kwWnTYlmcyEHp901ERH4ynGJyV24jkNIZK38nOy0oqgjiuPls2x4f20BGUVFMixM5KP2+iYjIT0XAh5OQQQoFbN9ejGEY+MtdMArYvs2OcSATERERkYjy/DzMzOZkJTWha58W2LaNbdtYlkXekpl863ZiwIBWCmQiIiIiDWVL7jocN52sJO9B9zuqgczo1Qv7yisxe/YEw8BZsoTA+PG4y5YdzWqIiIiIHBVdc07G4609jDU76WzOM00MjtY6ZJaF9+mnSfjuOzxjx2JkZ2M0a4bn9ttJ+P57vE89BaaWRBMREZHjjGHUazcToHB7FXEpDReIvE8+iX3XXQT+8Q/K27ShMieHypwcytu0IfB//4d9zz14n3iixnFG9+54/vSnWl8kJuL5n/8hbuLEBqt3XezrryfRcbB++csan1mjRxP/xRcYrVv/6PMYnTsTP3cu1ogRP7qsmLAs4j7+GM/99zd2TURERI4r9qIJu7CToeOpaYAT8xMYPXti33EHgf/9X3y3345xwgm4eXmhz7Ky8N1yS6gid92F/803o4YvzTZtsK+8slptbYzsbCgqwv/YYxgJCZCYGPM6/yheb6hOdgxGg207VFYdXZ1HnWFgJCVBfPzB97MsErZuJfjxx/h+85sjPp3n/vux77yTymHDcFevPuJyREREjnXW6OvPeCSrYxyYLo7jMHr06CMuzP/oozW2ee65Bysnh8pRozDatiVh+fLQiYcNI+699wi89x7BTz7Bc8cdUFGB8/nnkWPd3FwCzzwTeRlNm2INHozvpptwFi3CdRzcFStwV67EzMnBPO00KC3FHjMGkpJwN2+utZ5Ghw7YY8Zg9u6Nu3MnlJZCfDzWJZeAYWANHoyRmIi7YwdkZWGNHo01YADu7t1QUoLZrx/WqFEEJ03C7N0bs18/3HXrwO8H08TNzcVZtiz02ZAhuBs3gs8XquPQoaF6VVVhDhyIdcEFGFlZuLm5tVwdC3fHDpxly6CqCuuiiyAYxOzVC2vECNz8fCgsDO1r21gjR2KdcQYEAqHvtY95yilY55+P0awZ7vr14LqQnHzw8po2xb70UswBA3CLiyE/H1wXt6oK54cfIm1rDhuGNXIkRosWoe/gOFiXXIJ9zTXg9+Ns3Ii7YQNY1v76uW6obQFz0CDMU07B3bsXe8wYjPbtcVevxjjhBOyrr8bMycHdvBknNxfKymr+fj388MF/KUVERBrRlClTsCwr8jJNM+plGAaGYTT8pH6zVy+c5cuhoAC3oAD/Y4/h2Rfc/I88grtmDQDOihWYvXvXXc5pp+G5916CU6YQ+Oc/AfA+8ghmz56UT56Mfd112DfeCMXFoR4crxffHXcQeOGFqHKs884j7sMPoaAg1PP0zDNUDBgAJSWhVd9374ZmzfA/8ABucTHxc+diNGkCjgM+H5VDhkTKivvHP2Bfj1HwyiupGjECe8wYPA88QGW/fpi9euF9+WWqLr2U4KRJeF94AbNjR8onTsTzyCN4HnooFDKSkgh++CFVF19co+3iJkwIPQFh6lTiJkzA3b4dIzMz9B0LCijv3BlKS4n/4gvMU06B8nJISMB3440EXnkF71NPYd9zD1RUQEICzvz5VJ51Fkbz5nWWZ2RmkrBwIfh8uCUleF96iaqLLiI4bRpxEyYQ/Ne/qJo/H+9zz2HfdhvODz9gduuG8+23VA4divexx0L179sXz9ixVM2ZQ/y0aZhnnhkKv0lJ+O6+m8Bzz+G59VasK64I1SMrC+Li8P/P/+Bu24Z19tmh63z//Thff42jx3GJiMhxquFn0htGqEemNgdur2u/lBTi3nwT9u6l6sYbD3q6ypEjKd/XE+QdN65mdZo0IfDii5SfcAKVl14KqanYl1yyvwpFRVSdey6Bf/4Tz4MPYmRlUdG1KxWdO4PHg33TTZF9fY89RnlSEs7MmVhnnQUJCVHnCnz4Ifj9oWCRno6Zk0Ng8mSM5s3xjBtH8F//ojwtDd9dd2GNHh0KVIfgLF5MeXIy/gcfhIwMrEGDsK+6CvOUU6j69a8pz8zE+f57PPfcg9GpE/Y99xCcNIny5GR8t9yCedpp2L/+9UHLs0aMgPR0qq69loqcHAIvv4zRrFmNutiXXYbz9ddU9u9P1fXX4yxZAk2aUNGzJwDBqVOpuvBCrEsuwTzzTHz33Ud5WhrBDz/E+4c/QOTZXlA1ejTlTZtCYSHWOecQePll/M8+C0DF8OE48+Ydsm1ERER+qho8kDlLloSWucjIwDjhBDwPPoj/kUfw7+shMk44AbKyMLt3Dw3N1cL77LMYHTqEwtiePQc9n5ubC8XFBGfPhqZNISMj6vPAtGkQH0/8nDmhkAeQmhr5PDhrFsHp03G3bcPs0wd3w4bQa+vWUKi59db959o3/Ods3BiaX5WWFl2Z/HyCM2dijRgRCmyWRWDiRMy+fcE0Mfv2Jf4//8Fzww0AmN27H7I93fXrwXFwNm0KbUhLi/QsOjNnhnrxBgygomdPzJNOCn2njz8G1yUwaVLoPAMGHLS84LRpuDt3EjdlCvEff4yzfj2Bt96qURf/669jDhpEwubNWL/4BYGXX671ofJmv34AeH79a+IXLMA8+WRISsJo2zayj7N2LZSW4ubl1WxHERGR41yDD1kGxo/Hc++9eB99FN/tt1PRo0dkmDLw7ru4a9bgffFF8HoJjB9f43jrgguwr72WwBtvEPzXv+p9XiMlJdTjVlkZtT3uhRewRo2ictQoMAziP/us7kJKSyEzc3+ZJ55Y6zymgwm+/z7Wa6/hGTsWd9cunDlzMAcPBsBZtIjgJ5/s3/errw6r7DC3dN/jSffd4GC0aQMpKbgFBdHbU1JC+x/ikUPupk1UdO+Ode652KNH433ySczmzfH99rdR+/nHjQt9v5EjsX/zG+xLL6W8Q4fQfLPq9tUv8NFHUXPlwjd3iIiI/Nw1eA+Zu3QpgRdewL7tNrzPP49brYfL3bMH7/PPY998M4HnnsNdujT64GbNiPu//wPHwV21CvummyKvA3u+wuw778S6/HKs887DWbw4NHeqGqNNm9B8MMPYv5xEHWugBT//HKN1a+y778a+7TYSfvgB++abD+v7B6ZMCU3oHzKE4KRJEAziLF6Mu317aDL7vsBoXXwx7vbth1V2pJ77QqXnwQex/uu/iJ8/n7hXX8VZuBD27sVzxx1YF1yA909/Cu3/6acHLc/71FMk7t0L+fn4H38cAgGMFi2id8rKIrGsDM+4cQQmTCA4YwYkJGCkp0MwGPrOPXpgDh8eCp2Og3X66bh792K0aBHqpQvfQFCXfdfOvvjimucXERE5jhyV1Vh9995L4PnnsW+9lcRt24j/5hviFy4kcds27FtuIfDMM/j++79rHGcNHRoadjRNPH/+M96//S3yMlq1qvVc1hlnEDd+PG5JSWRJjer8jz4KPh/xn32G1b8/OA5m+/a1luV/6imC77+P98kn8T7/PM6sWfj//OfD+/KFhaGwAgTCa6aVlVE1ejQUFBA3YQLel1+GkpLQXZpHwJkzB/+4cdhjxhD3zjtQXo7vppuguJjKyy6DhATiPvwQa+RI/L/9Lc6sWQctz/fYYwT//W/ipk0j/rvvcFaswPfII9E75eVRdcUVWKefTsLatdi/+lVoMv6aNaHh0ddewzjxROKeew5n8WJ8/+//YZ54InEffYTn97/H3bbtkN8rMGUK5OfjefhhzFNPPaK2ERER+Skwxo4d6/r9fnw+H36/nzfD86qOQPkhVtuPPDqpd+/Q3KulSwm89VZMHp3kfekl7BtvpKJlS1yfL3QXZV03Cdh2aN7YgUNrdUlICL3qu//hyMgIBbHwsOOPYVnQpEnoTtEDv3tWVqhNnMNYay49PdR7eLDvbdsYTZvi7t1bM1BmZYW+V1XV/m3Z2aF6+Hz1q4PHA2lpsHdvrR8nHs73EREROcquvvpqPB4PXq8Xj8cT9XDx8DIYR2XZi+rcpUvxH41V3g8VnAKBwwtXFRU1hj5jJjzPKxaCwVon1QNwJPO1DjWkCKE1z/atKVavcx7u0hV+f51hTERE5HhxVANZQwpMnIizenVoEVMRERGRn5DjJpA5s2Ydcm6UiIiIyLEotpP6tX6UHE36fRMRkeOEXVRURCAQwO/3EwgEflRh1rnnEnz33RhVTeTgrHPPbewqiIiIHFRpaSm2bdc5oT8yqT8tLY3qd1n+GN6//hWfxxNa5kHPHZSGkp2NdfbZeJ94orFrIiIiclDJyclH/y5Lo3nz/Y8jEhEREZF6OSoLw4qIiIhI3RTIRERERBqZApmIiIhII1MgExEREWlkCmQiIiIijUyBTERERKSRKZCJiIiINDIFMhEREZFGpkAmIiIi0sgUyEREREQamQKZiIiISCNTIBMRERFpZApkIiIiIkeVg9/vRG2xF03YhZ3s0u6URAxPI9VLRERE5Ljnp2jjUpZvyKcKE8OTRpveOfRtlYSZc3k2rfoks35BcWPXUkREROS45RStZ1muj+yc4ZxzztkM7Z7A7u+XsKl835Bless4qkqCjV1PERERkeNKad4eivyhf5fszKMysw0dM7yASXLrjrSKz2f7nirNIRMRERFpKNvWr2dLnh9wqKz04UlIYP8MsWSSEqGitAq78aooIiIicnzrOmAgHo8HCOI4YB3QF2aaEHSC6iETERERaWwKZCIiIiINzgr1hhG93IXjgGUe2G8mIiIiIg3AJD7ei7+sAn9kWwVl5ZCQHKdAJiIiItJQyvPzKNmXwFKaZRFfuIX1haENVTtz2VmZRsumcZrULyIiItJQtuSuw3HTyUryYmZ0pHu7FSxf+AXbbQPHSaJl7wG0SUSBTERERKShdM05GY/Xu++dh4zO/Rl2gkvAMfAkJhJnmhgokImIiIg0HMOouc30EOexsKpvAijcXkVciqaTiYiIiDSGfQ8Xh46npsEBt2KKiIiISMOzcy7Pxu/34/P58PsVyERERESONs0hExGRmHFd96DvpX6MA+YdHfge1NZHQ32uQ6wokImISEyEA4HrulGv6p/JwYX/4BuGEfVyXTcqDKitG1Z9r0MsKZCJiEjMuK6L4ziRn9X/mEn9hNvONE0Mw4j8rGu/kj0+Zj+7lc1fl1JeEGyEGh+fEjMs2g5MZvidrUlp6q3zOsSKApmIiPxo4R4ax3EIBkOhwDR19/6RCAdYx9k/rzvcluFemuptPfMvW1jzeXFjVfe4VV4QZNWnRTgBl1F/bA9EX4dY0/8WERGJiXBI8Pv96hGLAcMw8Pv9kR7H6qq39cYvSxuphj8PG78srfM6xJICmYiIxITrugSDQQWyGAkHsmAwWGsgC7e1r0wrJDQkX5lT53WIJQUyERH50aoPowUCgcauznEjEAhEemYOnMivtj56arsOsaZAJiIiMVN9Dpn8eMFgMGouWXVq66PnYNchVhTIREv7INMAAA7hSURBVEQkJsI9Bw39h+vnJNyWda05prY+Ouq6DrGkQCYiIjGjNbBir642VVsfXQ3d3gpkIiIiIo3MLioqIhAI4Pf7NTlQREREJIZKS0uxbRuPx4Nt29i2jWVZWJaFaZqRBWfttLS0ag8X9zd2vUVERESOG8nJyXg8Hrxeb52hzDAMDVmKiIiINDY9OklERI5pmzdvZtOmTaSnp9O1a1e8Xm+NfTZt2kRVVRUnnHBC1Pbt27ezd+9eTjzxxKjj1qxZg2VZdOrUKbLNdV1WrlxJXl4ezZs3p0uXLjXKOVCLFi3IzMxk+fLlNGnShJYtW8biKx9VCek2TTsn1NheURigLN9Pk44JFG6roniHL/JZdtdE4lIstn5fSqs+ydS1DnDehkqcoLu/fBcqigMUbK4kUFVzknxKtoeMNvFs/b4UJ1Dz86QsD636JBP0OexYXkZ5QWiqVbMuCcSn1R5pyvb6ydtYeahmaHQKZCIickzy+/08++yzzJ07F8uyCAaDNGvWjN///vdRQSoYDDJu3DiKi4t5/fXXyczMjHw2depUPv74Y8aMGcNVV10V2f7cc8+RlJTEk08+CcCePXt47LHH2LBhA7ZtEwgE6NGjBw8++CBJSUmRcg503XXXcdZZZ/HAAw9w4YUXct111zVgizSMNiclc8mzXWpsXzO7kI/HrWf0k52oKAry8iVLcYOQ0TaOa97uzsavi3nv1jVc/veuWN7aE9nUhzZQWRyoUX5lcZAZf97Esk/yoraf/d/t6HpWBpPvXceqzwqiPjv5yuYMv7M1pmmAAYEqh08f38jSj/M44642dBycVmsdlvxrL1Mf2nA4TdIoFMhEROSYNHHiRObOncudd97JsGHDKCgo4KGHHuIvf/kLL730UmS/hQsXUlhYiG3bzJ49m4svvrhGWZMnT+b000+nbdu2tZ7rmWeeIT8/n7/85S907dqVlStXMm7cOF5//XVuu+22yH7//Oc/iY+Pj7z3eDxUVh77vS/1MfnedeTOL4q8d4IuQZ/LzKe3cP4fOzLgV9l8M34Xv7ivLa4D0/+0CYC/Dv0WgJRmXm76qBfz/7GdL1/fAUDA59BlaHpU+clNPPzy0Q6MfLQDuf8poqIw1MuVkGbT+fR0gj6X3hc0jQpkbfuncNZ9bfj2/d18/vQWAEY+3IHzHurA5sUlTLxzLaYVCoU3fNCToh0+3h+7JvQ9aulpOxZpDpmIiByTZsyYQe/evTnzzDOxLIsmTZpw8cUX4/F4ooYPZ82aRadOnRg0aBAzZ86stazU1FT+9re/1bqW1LZt21i6dCkjR46ka9euAHTr1o2zzz67xjBlfHw8CQkJkZdtHz/9GoEqF3+FE3kFfaG2WvZJHpsWljDkplb0GpVF56HpfPnqdgq3VgHsP6Yy9NSAoH9/OW6wZvkFW6r4fspeLI9Bk477w22P87IwDJj39210OjWN5CaeyGcnXdwUX7nD509vIVDpEKh0+M/L29i7oYJmXRMJ+qqd0wU3uP990K9AJiIickRKS0vJy8ujXbt2UdvPPPNMnn/+eZo0aQJAUVER33zzDUOHDuX0009ny5YtrFmzpkZ5119/PcuXL2fGjBk1Ptu8eTMA7du3j9p+44038sgjj0RtmzlzJtOnT2f69OnMnz//R3zDY0/HU1Lpe1HTyMuO2z8M+enjG/EkmIx6rCMFm6tY8NqOIz6PYULHQam4QSjYF+oA+lzYhPX/KeK7D/bgui49f5kV+axJ5wQKtlQSqNz/ZIK96yt5dcxy1s4uPOK6HEuOn2gvIiLHjfAwYEJCzcnm1c2ePZtgMMiQIUNIT08nOTmZzz77rMbk/pycHE477TTeeOMNBg4cGPVZVVUoFFQfiqzL5MmTMfbNYG/evDmnnXZavb/Tsa7bOVl0GbY/8KyZXUCgKjScmLehkl2rymnRM4nVMwuOqNfp3HHt8FW0ITHDxpto8dmTmyjdHVpuK7trItknJvLVmzuoKAqw/sti+lzYlK/e2AmAN8GiLO/4XppLgUxERI45KSkpAJSUlERtd10Xx3GwLAsI9Vilp6czd+5cADIzM5k3bx433HBDjbsxb7jhBm655RZeeeWVqO1JSUlAqFeuuvDzC01z/2DSiy++eMiQ+FP1ycMbWDe39t6mbmdn0qJnEmX5Afpf1oxF7+2KuuuyPtbNK2Lv+gpOvqI5xTt8LHp3d+Sz3heEejwz28Qz8NfNcQMuWR3iadk7ie1LyqgsDtR6F6VhETUs+lOmIUsRETnmxMXF0aZNG1asWBG1/Z133uHCCy8kLy+PdevWsXHjRjweD59//jmff/45fr+fsrIyvvzyyxplZmZmcs011zBnzhx27twZ2d65c2cMw6hxrnHjxnHzzTc3zBf8CfEmmvzivrbsza3grWtXYloGI37b7tAHHmDtnEIWvr2LuS9tJfvERHqcF7ob1rQNepyXRUVhgO7nZdH34qZkdojHCbj0uaApANuXlZHZNo6kzP2hrG3/FH737YBIOT916iETEZFj0kUXXcRzzz3HSy+9xPDhw9m9ezcff/wxffv2JSsri4kTJ2JZFs8++yypqalAqFfr2muvZebMmZx++uk1yhwxYgSzZs1i5cqVkW0ZGRkMHz6c6dOn07JlS7p27coPP/zA0qVLueKKK6KO//777/F49k82b9WqVaQ3b/fu3SxatCjyWVpaWtRaZse65icm4gT3D0WWF/jZuaKc029tTXIzD/96IJf8jZV89eZOBt/Qgi7D049o/tbSqXmccm0LTr+1NStnFNBlWDqJGTbv376WdXP2l3fhE53oPiKTz57cxKJ3dtH3oqZc+GRn5v19G5ZtMPzONpTl+aPuDP0pUyATEZFj0llnnUVpaSnvv/8+//73vzFNk0GDBnHzzTfj9/uZO3cu/fr1i4QxCA0vDhkyhI8++og9e/bUKNMwDG699VbuuOOOqO233HILtm3z2muvEQgESEhI4NJLL2XMmDFR+/3xj3+Men/FFVfwy1/+EoAFCxawYMGCyGd9+vThD3/4w49uh6Nl6K2tot5v/KqYmX/dQs6vslk1o4BN34SGjxe8up3e52cx4v52bPyqGH+FU1txdXNhzt+2cfHTnel3SVM6Dk6jsjjI+v9EB6tl/86j+zmZdD0rk+Wf5PHe2DWc87t2XPnqieCGes3eu20NlcXHx5ilMXbsWLf6syzffPPNxq6TiIj8xDiOQyAQoLKyktLSUpo2bRrTsouKikhNTY3MHWso4SHPtLS0yOT9xrRnzx6Sk5OJj4/Htm1M06zR1m+cu62xq3lUJaTZ+KucqDsuG9o101rVuA71dfXVV9frWZbqIRMRkWOaaZpkZGQclXN5PB7S09OPyrnkyFQUBRq7Cg1Ck/pFREREGpm9aMIu7GSXdqckYngOfYCIiIiIHJm9y+eydHcQwzAir5SOQ7BzLs9mz6ZS1i8opNPpyY1dTxEREZHjlI+qSj8JrU+iT+sETNPENE3spOTQkGV6yziqSo6PuxREREREjhWleXsoijxkwEdFFSRmNCE9PZ2MjAwyMjJIjTM1qV9ERESkoWxbvx5oStMkL1BFlc+D4dvNlvVVBONSyG7VkjSv1iETERERaTBdBwzcv5iw34cv4KNw/QbIiCdYuo7Va7fS97SBCmQiIiIiR4Unm16nZGKmpJHgsbCMYlbN/5pVa3Zr2QsRERGRo8PETkrCG05fZgZtsxPxFxcrkImISOyEV7d3nKO3ivrxKtyGdT0xILzdk9j4TxQ4noXbNxZPbnAKNrF83R581bb5XQdMjwKZiIjERvgPlmmaVFRUNHJtfvoqKioij+g5MAxUb+vmJ2n2UUNqfpJd53Woj/L8PEr23WVpeqF001rW7A1FMqd8B7nbKkltnqU5ZCIiEjumaWJZFkVFRQQCARITE/dPaJZ68fv9lJeXU1ZWRmJiYp3PTQy3db/rknEoYs+SIL4S9ZbFijfFpWnvUPuGnzl5JLbkrsNx08lK8kJSG3p2q2DpktlMN7wYjkFq677065ysQCYiIj9eeMVx0zQjD07Oz89nx44d+P1+HMfBdd3GruYxLdx+Ho+HuLg4EhMTIw+yDrdveL/qbZ2WHc8pdzpUVVWprWOg5nWIr/U61FfXnJPxeL373pkktezJaW17E/AFsBKTSbBNDLTshYiIxIhhGFiWhcfjIT4+HgDbtgkEAppTVk/hkOX1eomPj8fj8WBZVq1DlmrrhlPf61AvtR5jEpeQgFWt080GKNxeRVyKppOJiMiRq96zAGBZFnFxcQSDQYWEegoPQ9q2jcfjwePxRHpmqlNbN6z6XodY2vdwceh4ahqgiygiIoev+iTz8HvLsnAcJxIQNIx2cNXbMBwIwv8+cMgyvF/4vdo6dup7HWLNzrk8G7/fj8/nw+9XIBMRkSMX7rkJ/wwHAwWE+jlwntjBAoDauuEcznWIFc0hExGRmDAMA9d1o/54KRwcmbqWuaj+Xm3d8A51HWJJgUxERGLmaP4B+7lTWx9fNJNfREREpJEpkImIiIg0skggU1eniIiISOzVJ2Oph0xERESkkUUFMvWSiYiIiMROXTdfHLjdDG8Mf1BZWXk06iciIiJyXAtnqgPXMautA8w8cIdly5Y1fA1FREREjnPLli07ZBALsw9chfbNN98kEAjQs2dPkpOTj0Z9RURERI4bpaWlLFu2jLfffrvGav91hbJIIAs/p6m4uJgXXniBQCBAIBCIPKjUcRxc19WjGURERORn78DHK1V/9qVt25FX9QeTH3hMdXZRURHBYDASwMIhrK4wpkAmIiIiP3e1Pe/ywFB2YDgLvw/vVz2o2Ws+rsJxHRwCOG5w/4vQT5d9YQwHl3AIUxgTERGRn7t9oQwDAxPDcDFwMY3QXZOm4e57OZg4mIaJaQQwsUJBDHPf0WBHCnBNDFwMw8XEDWUuA1zXwDVcXEzA3R/KlMlERETk58oI/zAIRzIDA8MwMbEwDRPDMENBDTMSwIzwgRhRhYXmkLkGJiYYoSDmGGDuO4GLQziIubgQHqrUkmUiIiLyc2fsD2NEApkZFczMfUEsNMwZfu07fN/P/w82AxZS54grjgAAAABJRU5ErkJggg==\" data-filename=\"Screenshot from 2018-12-06 21-40-40.png\" style=\"width: 612px;\"><br></p>', '2018-12-21 12:30:21'),
(34, 11, '<p><br></p>', '2018-12-23 04:38:00'),
(35, 11, '<p>AS</p>', '2018-12-23 04:38:33'),
(36, 11, '<p><br></p>', '2018-12-23 04:39:51'),
(37, 11, '<p><br></p>', '2018-12-23 04:40:29'),
(38, 11, '<p><br></p>', '2018-12-23 04:41:07'),
(39, 11, '<p><br></p>', '2018-12-23 04:41:07'),
(40, 11, '<p>TES</p>', '2018-12-23 04:46:26'),
(41, 11, '<p><br></p>', '2018-12-23 04:46:27'),
(42, 11, '<p><br></p>', '2018-12-23 04:46:27');
INSERT INTO `t_soal_ganda` (`id`, `id_modul`, `soal`, `created`) VALUES
(43, 11, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdUAAAGFCAYAAACrN2kAAAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAZ25vbWUtc2NyZWVuc2hvdO8Dvz4AACAASURBVHic7N13eE33H8Dx973ZSwaxIiG2xKbUqL03rdHSGtWaRYeapVRbRdHxU5Xas7VH7ZXYsYkkRoYQQYjs5Cb33vP7I0FwbwSXhH5ez+N55N5zvuNzzvl+7jn33PNVtW7fUUlPTyc9LZ39e3YihBBCiOejRsntJgghhBBvBnVuN0AIIYR4U0hSFUIIIUxEkqoQQghhIpJUhRBCCBORpCqEEEKYiCRVIYQQwkQkqQohhBAmIklVCCGEMBFJqkIIIYSJSFIVQgghTESSqhBCCGEiklSFEEIIE5GkKoQQQpiIJFUhhBDCRCSpCiGEECYiSVUIIYQwEUmqQgghhIlIUhVCCCFMRJKqEEIIYSKSVIUQQggTkaQqhBBCmIgkVSGEEMJEJKkKIYQQJmKe2w14XnExMURdvkTSrSj0SUkAqO3ssStchKJly5HPyen5CtbeIuhUGHF6UKktcChSlnLudiSGnSX4lgYly6LqfCWo6lUYywfr3cC6XBU8HZ/8rJIQfobA21aUqF6BQuaP1oNKjYV9EUqXd8fRLJt1ANBw89xBjgRGo3MsRa2Gb+Fhm7X5QZy6YU25Kp48bIaWW0GnCIvTP1xQZYd7pUq42T5ZXrH0MM4G30LzaGcpUdWLwppwzgTexqpEdSpkNkp7K4hTYXFkdMUC+yKlKe/uiNlTYiIAfTRndx0hqVwr6pawzO3WmIw++iy7jiRRrlVdHu+WPmo1Xw48TN25P9O1yKvZLwy3R8fNkzs4FuNO/aaVyP8ymqKPYvWXAzlcdy6zuhZ5yqKvPi5PNsLw/qi7eZIdx2Jwr9+USs8YqDzRr1epdbuOSrNWbZSGTZoreV1Y4AXl+KyflGt9uiq3mr6l3Db2r1ktJaJvN+XErz8r1y5ffqY6dJG/KU0sLRQXTy/Fu3wJxcXGSak2YoOyeUITpZK3l+KZ30JR27sp5by9lWp9lig3dFnWs3ZTPtmW+mSh2iBlal1bxdyiqNJnY/yT9XiXVzzz2yj5Kg5S1kfpjK6jKKnK0W9rKc6uVZS2PborrbxdFPvKXyv7Ex+0Xon8rYli7faJ8kgzdJHKb00sFQsXT8XL21vx9vZWvKt0Vn4NTDJY3q5dE5UmlbwVL8/8ioXaXnEr5614V+ujLLmRpgRNravYmlsoRftsVOKz1Glp4aJ4enkr3uU9lfw2+ZSKg9YrkdeyiYnIkLpbGeRho7T445aiy+22mFDq7kGKh00L5Y9bBnp1z19ZPmu54n8vN9ujUyI3DlIqFaiofLouQtG+rIq1V5Rp9eyUetOu5GDRaUo9u3rKtCsvrTVPZ2B/1EVuVAZVKqBU/HSdEvE8TcuF7Z2bXosz1csnT6AsnYdzwBnscrKComAdEYZHRBjKxlUEVauNdZ8BeHpXzFmF6oK8O+cU81pZcNOnPWW/WEzsjT2cm6Rhz+CytAv8nJ17R+CRww9d2gurWB1Yh37vX2PTym3c69ANx0fqsUJ/24d2np+zdO90On1gZ3AdZ10kB3afw6HjWlb6tMHu9km2H9XgkbNOUfDdOZya1wqr+y/pQplhoLxSdeqy59y3aPYMpmy7QD7fuZcRHmrQnuPb1YHU6fc+1zatZNu9DnRzvh+yd5lzah6trPTc9mmH5+dL2ftF45wFSGQvMYyDO/0IjNbiWLoeLRuXx0kNpN4kKDiRgl6OXN+5Df8oc0q804am5Z0efq+jieLkrr2cvqHBoeTbNGvsRX4zAA03g4NIci2FcmYTR83q06tRceN1oeFWcBAJrhUpEO3L9kOhaApUo0XrmhR51hNsWw+qN3TAOfMKiz7uCgd2HST4jhaHErVo1qwyBe+PTC+lPXpubf+KDp/4UuuPbczp7M6DC0Ta25zbtRv/6xocy7xDi0alM47VjGBy4/gOdp+5hdrtbVq3rJQZSwAtt8/tYrf/dTSOZXinRSNKO2JQYthBdvoFEq11pHS9ljTOur0ekUTkhUvcy1eGiu722cQi+3I1t4IJSnClYoFofLcfIlRTgGotWlMzBxtOf2s7X3X4BN9af7BtTmfcc9Bfzc1ggpJcKaWcYdNRM3p2q/DI9oZEwg7uxC8wGq1jaeq1bEx5pzfnDDZP9yQhPp7z343HcdRgnAPOPFcZKqDA6WPYjejP2Rk/kpKSkoO19CRFhxF66SwHT0eQ5lKIws99ZU7LqZVrCanbnXEDOuC8exWbb2dehlWSCNr8G7Nm/cz3E5ZwyrIqtSpZGV/HzIM27zdHv/RdSnk15L3xG7nhXBz3nH3SIGbPD/To0oUuXbrw7gAfzivPVp721ErWhtSl+7gBdHDezarNt7l/QVlJCmLzb7OY9fP3TFhyCsuqtaj05lzNzDX6G2v4uEZN+s7Zz/kLR1g0pD41eq8iUg+6iEX0q/cefXp244vlBzm+6xc+qlWHz7beRQ/oo7fzZd0qdJm2g4DLp1k3phmVW87gZCqgu87S/vXpNaAvXYfPZc2hiGzrQn+TFQPr02PQJ3QfNp99x/fjM6QhdQZtJOYZ+6S7toT+7/RnyTUd+qjV9KtRj6FLDnIh0J+137SmaqtZnNdm3/fnb4/C3X3j6dhnK5V+/Ze573k8TKhxB5jYqCrtvt9OwKUTrPq8AdXem88lHcA99o6pT43uP7Pt2CFWft2EKu1/43waQBwHJjaiarvv2R5wiROrPqdBtfeYn7Fi1q3JjTUfU6NmX+bsP8+FI4sYUr8GvVdFoudxGi7M6UGd9j9wPNky+1hkW66emysGUr/HID7pPoz5+46z32cIDesMYuNTNpxydx/jO/Zha6Vf+Xfue3g8DFQ2/dVxfWl/6vcaQN+uw5m75tAj2xv9DdZ8XIOafeew//wFjiwaQv0avVkV+WQEXld59kz16sVg1JNHU/jWDZOUp1L0FN2+ntuBZ7D8djpFPLI5v9PfZuPnTdhvZY59sVqM/WsCTayML54tzRFWrr2IXZEj/LwqFvPUPazacJMP2gGkE335NCcjg9mx9S7tVx9nZCVz0BwwuE6vT4viNXgTFzuf4cDefezdsIiRLTYTceA4k2s+bVOqsClRh7bty2MGqBwq4Ko2p3COy9NwZOVaLtoV4cjPq4g1T2XPqg3c7NU/4+30aC6fPklk8A623m3P6uMjqZQ+5zmDJu5LCgom8e1vWOEzgrcsQevvStUm81j3QzcGm5lhlh7M7crHOfBNJSxJYEt/b96ftYrRrQYQ8/s45uoH4LfrO2pYAfea0rvSx0z5pzfre1liZZnO2YjKHDo5gWpWkLBnitG6PnMzx1ytJSiqAif2fU0Fc7i37F1KjtrMkdSOtLV+vv6l+v7DRrOebF4zk/qWQPJJVsy9iCYp+74/X3sUEo5OofPHiyky1Q+f7iWyDIA6gv4Yxaykfuw9OoWaVqC/24helT/nx3+781fpPxj7PzVDj+xhnLc5+ptL+OCd2Sw59ClTC//BqFlJ9Nt7lCkZK9KoV2U+//Ffuv/lnXVrEhScyNvfrMBnxFtYosXftSpN5q3jh26fUTRLWyJWD+Tdn3R8tWkxfctZkrAnm1i4Z1fuEMzN1WiDoqhwYh9fZwSKd0uOYvORVDoa2XBKwlGmdP6YxUWm4ufTnRJZhgNdUDb9XdgOSytL0s9GUPnQSSZUs0IXMj1LCIIITnybb1b4MCKjI7hWbcK8dT/Q7TP3vH2Wl0N5MqmGnD6Fw7dfoUpKNHnZZnoFK5unjADqwnywLIR5rZ43kz6U7Lec9Te8aNyrDIUtFJrXO8G8VWuIaK0GlRMNvljAvObXmdOmFhPmrCa03WCKGlunZw3+/nAioZ3n878PP6d5c0cCd40i7JoWnppUwaZUYz7sm+Xyb+ohfvogh+Ul+7F8/Q28GveiTGELlOb1ODFvFWsi+vIeoHJqwBcL5tH8+hza1JrAnNWhtOn0wuH7z3NoOp4lFU6yZ81f/O92PMkxF9Ho4rgbqwdbwLwUjVuUy7hZDgfq162MsuM8wekxhB27SOF3fqbi/Q3u3JAGVVKZcuw89CqHCnOKN2qBt1UO6nIDMKNMo+aUydw1bIsWwTn5NokvcJJh6V0T79sz+XJgfgZ1bUuLhjX44IsaGW+auj3ao/wwOIryRTQc37yXkA9LUe7B1ZQ4jhw8h0OpHsQe2c9+AKzx8Ihh06EA4u4c5kKhOkzPrExd+CNWXf4IgJgFBznnUIoesUfYn7Ei1h4exGw6REBa1qTqQNPxS6hwcg9r/voft+OTibmoQRd3l1g9mUlVT/yBcbw7IYQP/t7MsCoZ10yz3Tbu2ZfrApiVaUTzh4GiiHMyt40HiqM/DCaqfBE0xzezN+RDSj0MFHFHsutvO9xVYF68ES28DYyfDk0Zv6QCJ/es4a//3SY+OYaLGh1xd2PRI0n1pbh6MRiHSSNfSkLVFCuB7fT/4eLqavKyAdBH4tPaGh8AzCj26Rr+l7aFe/XHM3X8QNzUkNbgLlta/M3fYd0frmdWiv7Tv2RZ/Ul8tbgR/Q4YWed6bxrXs2Xe0EoUm1gIi3vRqGuPYlVz68ea4UNra5/Msovx6WY/KqEn0qc1919GZUvnpWGMzkF5AIm7V7DlXn3GTx3PwIxGcXdLC/7++wrvZblcbFaqP9O/XEb9SV+xuGYTU0b3P0hP9L/DafLhBpw6fkCL8q7YqM0yBp77d2ar8+Hk9PCWcUs7W8w1ySRpE4lPVLDLZ//w8iZW2NtZkJSYkPm3CjsHh8yBLAd1ocLcMss1fZXqhXtoXmkkm3a78cucFfz26U8MSChCi8//h883TTDbZuL2qEszcK0vUzx2M6BRP7qO9GDPrJa4qgF9IvGJWhIvrebnadt5UJJzE2q4qUmIT0RvY4/tE6O+nsT4RLSJl1j98zS2P1yRJjXcHh1g9dH8O7wJH25wouMHLSjvaoPaLKPAB11KP8PsUZewSPMgKV39oI5st00OylWZW/IwUiqyj5Sa0gPX4jvFg90DGtGv60g89syiZUagctRflZ0DDgYypD76X4Y3+ZANTh35oEV5XG3UmD26UV97eSqpJsTHo548GtWDgz57irk5aW7FuGfvjFoFjvGxWEZGoNI9/l1GzhOquuhQ9qQMNfKuFU3nXMXQt7LqokPZozG0XifisvxlWe9nLmb8AogxKcMevl55LIfjx2b80e+60XUot4nLQ6KJiIhBcXKneEHbLJ/u1BQdugeDzWitwXCvjJdn1XQOV+931mMh1x9tFD8/aNQeHobMkspjD3O/K/2z9FE8q7tsnLOI+K6rOfpnK+wA7cnxLP0p9OEi+jjuxeogM3UmxsaRnq8IzhbOuOY3J+b2HXRkHuj6OO7EpONcpcDz1fVSqHGp0YtJ83sxiWTCNo+mywef8XOL3ZQzdXvUrhT3tMOsYGd+XXGJti0/oqfnLjaNqIy12pmC+W3IV3YcG7LezJcpbpkLFjG3iX4QTC0J0fdQObriXDA/NvnKMm7DPJ64uKULYd/9/9/dyJxF8XRdfZQ/W9kBWk6OX8ojXTLzoM+KfXx0uAst+39OvYPz6FDwKdsmJ+U+W6BwLe6JnVlBOv+6gkttW/JRT092bRpBZWt19v1FT2Q2Jd/dOIdF8V1ZffRPMpp6kvFLf+Jl72WvUp462w7/ZRrWT/kOVUEhxqsKN776Fod1uyk2/x8q/fIn3rP/pNiCv7Fbu4vIz78hrqzXgw8/L/0M9RVTW7tSomw5PB9JqHmnPPHsdOkppCQlkXT/X7IGnV5Br1NQqcwyzizSQvn7jw1E6NJJT8tcURvCjg0nSQLQR7Fj5xlsq79NZUtHGraoTdLuNey8m3GZTxu2lo2nXGnYrMqTDchJXc/eK9JTUh72KSmJZI3ukfeDF/Sny9itZDTRFs+6dShjm44mRf8S2vOQbY2RLJ/XlvAJXRm6/gZ67GjYqi5JO1ew5WZGvPQxuxnVqi1Tj2lweKcZtZN2s3pHNHpAd3URH3hVY/jOJOwatqJu0k5WbLmZccORPobdo1rRduoxNFnq1Ct6dIoKlVnG6V1a6N/8sSECXXo6D7qkLoBHiYK8NWYhkz03M3jgUsLTs982OSr3+QPFyOXzaBs+ga5D13NDT477+yQ9il6HolKR0dQ0Qv/+gw0ROtJNsVHziDxzpnr55AkK+e7MdhmdozN3B3+Fd9PmRpexs7enatv20LY95//dhMv2jdhOmPrGJFTxJkpjz7AS2Gc9qbdswMyLe+jRvxvT+31InUsVsY9JpsKXkxjg35slX4yg3Hh3sKhIhTvf0vgdBUfNRU7cqMmkrV1wRo1z/9lMP9iF/tVrU7mCLZHnQnD++E8mtbSHR66FAOoCtM+mrgpzv36Obu1hWAl7Hu3WTJIX3P/LjNL16mMzvQ9VtlTAq4gZdy8Go2n5I1MauOEQY+L2PNphinb5nRUXW9Oyfw883bYyrvdMZh/uyuCa1fmftwv3LgSQ2uAH1la1Qm31MT9P3U+XvlWpWq4YmstXsOr4P35tbYfarDczZx+m6+CaVP+fNy73LhCQ2oAf1lbFiusPayzQnv7dptPvwzpcqmhPTHIFvpw0AP/eS/hiRAX+yBoo8/IM9pmJX70R9PutGguz3Tajsym3HN+VecFIFe3C7ysu0rplf3p4urF1XHb9zT7mBdr3p9v0fnxY5xIV7WNIrvAlkwb403vJF4yoMI9fepXP8nXF60nVul1HJV2bTnpaOvv3ZJ/UXqbwBe9h9084aA2fK6UWdcfqx18p6Ob2ahsmRC5LjgrgXHg6hcpXxtPZDG10MKfC9LjZ/0v3WhvpeHY/Ay0ucOGGOuMpWY/8JEpHXHgAQVHpOJWqRPmC2Q97xuryqOxF4ee8w/ep9IlEBl4gPBYcPb3xcrN/cMXk1bdHT2LkBQLCE7F1r4i3h8Mjg7z2Xijng26hKupFpRKOj7ynT4zkQkA4ibbuVPT2wMFgdkgmKuAc4emFKF/ZE2czLdHBpwjTe1DZqzDZdSn7WDx/uc8rZ/0F3ZVpNKi6iU5nfRlZygySowg4F056ofJU9nTGTBtN8Kkw9B6V8XppO9mrkyeSakToBYqEVUV305akDcXR33s0sDpHJ9S/LpSEKkQWupDpNKyykY73Bysh8hj97UD2LhvLR9/qmBK2mX75c7tFL1+e+Aot9soyVKgwL5yCQ59LWJR7eGlKQeHukK8loQrxGJW1GxVrV8TN+sXvwhXiZUg/v4ppm1Jp+d1YuvwHEirkkaRa3HLrg/+rrfXYdQ7DukkkqPXc86qKd5Nmr64x+lhCz4cRe/8nXIkRnA+IJPlV1J18jePb17J6w27ORhn/yj/RfxlLj8YZff/56YkNPU/Yw84TcT6AyKd2PhH/ZUs5GpeI/7Jl+Cc+/t7jrwlTULt9wNw9c/nALU8cxkI8warpZHbu387C4XV4zilOXju5fjTGx93DVhv8yGsqFdjUjsb+gxA0HXP2BIGgS2Ev9C85OTNzpPkx5aMfOZIOJB5neq9++ISpn+E7CS0XZg5j+nltjtcA0AYv4P3GvfjfkWvcCtnD9x3r02/VVQNL6on3X87So3EGHm32otLwm/IRP2Z0nuPTe9HPJwz10zqvj8d/+VKOxpnhWqoUrhZZ37Qw8JoQQryZcv3u35vXAimpMvzDX3OPdMrUydlD2YeuKfhC7RjyVhBdmtd48LeSdIZfPxlLeJ/F/Nq+CGr9DTaPHcb8qzaYJ2p5a/RcRpbfwYiRx3CxvkfQlXAsu81hfqPT/PTXOgL9S1J7aiX2j5vDJStbUpI96DdrEm/5jWC0n4q7iV58P38QFc0B7rF28i9YjtrLosxrJIN7tGDVGQ36G5sZO2w+V23MSdS+xei5I/FE4d6BmQw8E0lYpCMf/fEnPW23PrFc+R0jGHnMBet7QVwJt6TbnEX0tV7NZwMXcNvVg/Kueu5Un8KcXkUf/XSlJHHm108YG96Hxb+2p4haT4zfNIbPOoPKWoOu4nB+HfMOCas/Y9CiWDzKFCPmjp56+niOr1wJJT3x/24sfqq7JJb/krYX10Dp2ni65vpnOCGEeKlyfZRLjr9m9L0kymFjY2v0fVOKuJPl93NpoSzr1YUlXj8yq5Nbxh1+6XdJLTOYOcuXsmJkYdb67CVVF8PlgwnUnraAlT7tublqGzdKdqR1leK0HjUQp8UTOVjrZ5YsWMzvTQP4/n+n0N4LwTe5OYsX30+oQFoQJ4JK0rjpwy8d1G6N+aBtWdLvplJm8ByWL13ByMJr8dmbCui4k68JPy/6m2Xv3+W3ef4kGVhOF3OZgwm1mbZgJT7tb7JqWxgnfWZzrfNi/lk6nRp393HpzuNn1GmELutFlyVe/DirE25mgC6Yud/6UXv2CpYs/5X6R7/DJ/AEPrMi6Dh/GX9ObYFrvB5I5/aVK9xOh3shviQ3X8ziQaUfvCaEEG+6XD9T1acZ/24wJq3QK7sOH5vyMBRKShLu/b/H9ZdxzDi+jrFv2YFWw82Dv/PlkTU4J5/ijrYWesCsWFnK2YA6nwN22jtZJvbWEhqWgGePIqhR41KmBBq/cNKKmVG8ghcOWStXW2ChTiU5RQ+PTeat1dzk4O9fcmSNM8mn7qCtpQfM8azghS1qrEp5oNsbSarB5cwoVrYcNqjJ52CH9k4yEZHpFG/ighoLatUsh/njD59SUkhy78/3rr8wbsZx1o19C7v0EEKuhnN9bG+OqSFZY0nFm1e5nlqMBvnVYFGa0u6P3X1qVpwKXg7war6NFkKIPCHXk2peoSgPL0GrHCvRqO37NC1/j659B7Di78W8G+TDYvOPOejTFs2GPuxecX9hYyWqcS1gwZ1bSYAVqTejURcoiDmgUj92gcDcm2Z1o5iy+AIfj6qEFZAa8Adfry1B6xuLMf/4ID5tNWzos5uMavXcjb6NjtKk3bkHTnYc8TG03OPNU5HPXiE2Jh1QcTUiCt3jN1WrHKnUqC3vNy3Pva59GbDibxa/V4hChbxo+NtSPnLREx0airrQdSapYolJBVQ3iLqtx+2xuh7vphBCvOlyPamqLR0x+DBdwMXy1itrh7XFk9/rWnoN5K/JF+nYewJFZlfD7cwfjB6zBz0qioRsZl1wXQMlmVGkUCqzZ/5FowF9MZvwKV+GluD6YT19Z9fF8oCh2m1pNOk3jvX/lGadylPeKZbL4fZ0m9WTssfcOPPHaMbs0YOqCCGb1xFYU4/2/HLGfbORa3uv0XZafbzPu/HdY8s92TxLar/bjMnfDGTMmQLEXwCVsV8qWXox8K/JXOzYmwnF1tHnYys+/WgQQeUTORVake9WfEb39t/yRd/hnHJL44JKocibMyWiEEI8l1x/+MOlgEOUvGX4ZiRFZYm2zu0cfa/a9Iekpy6TnffKBDCoa+1sl9HFRXI10R4PNwc0NyNIcChBYUMTemvvEXlDi3MxV2zTY7h6LREHDw9cnjppt57k6GvcTHHAzcMl85FfOuIir5Jo74Gbg4abEQk4lCiMHXoSo65yz8YddyfzbJZ7lCZoE6uuFOWdWqW4Nasdv1bZysr3HXMUI23sNa7G2eJWPH/m3dBaYq9FkOxUnKLGHqcihBD/Ibl+plrY3QvlpgqVgTuAz2jsiTvrR7O3Wz21nGZFzz91mTMxJbiT6mDwvWL5n/6bDzNHN0pm5h/zwiWeSFgPmDvjdn8OdCsXipd2eWrZGdTYuhan5KO14uhWkoxqzSlcwu7BsvZFPLF/6nKPsnC24dra6XzzD+ic+jOuc84SKoC5kzulHvmS2xwn95L/md+fCSHE0+R6Us3n6EyceXnsdEEPXlMUWJ5SirnJFaiY6ksznp5Ux/R5O9v39Xo9XX+JNfiegkIFz//GA/fVhZszfpHxCQmEEEI8vzxxK8nVtDYP/p+gN2dU/Fv8L9kbHWrOpl1m94n9L1zHniMXiE0x/DBxF9s0SpUo9sJ1CCGE+G/LE0nVqXQvFEXhYno++sY24EB6kUfe/yl8CZE3s59nNTupqaksPWX8wZPV80egUsnzU4UQQryYPJFUPUp6szW9O5/GvUOk3v6J92N18Xx2+PvnSqw6nY7JK0OJTDD83aGCQpvqT9YphBBCPKs8kVQB3Ip/SZpi/A7Sa+m36HNwPDuP78txmVG3bvHF/FCORRU3ukx5l2iqepd+7NVErp49xtFj54hINPAA+5f+0H15CL0QQryO8kxSrelVjRZ2tbJdJlYXz7iQ3+m/fgxbj+56+BD8LPR6PWcvnueHLb/S/cAX+JutRa8ylu4UPq5n4MH3ursE7FrL1H49+T0g9ckH2L/wQ/efRh5CL4QQr6Ncv/s3q1GNBhKwM5Qb2jvGF1KpOJt2mbNXL2MR8RfFLAvhrM34+UiiuYZIXTRJ2ixPk7A/QZLldWxvDsUs3f2Rolp4hlGjUqUn6zArTtuvviXmyC6y+6HOkw/dj8Fv2nBmnVFhrdFRcfivjGkAa0aM5JiLNfeCrhBu2Y05i/qRb+tYhs2/io15Itq3RjN3ZBm2DxstD6EXQojXWJ4asfM55OOnal/gYJGzh+inKzrCNDc4pbvMKd1lLmkiHk2omRTLmyQVm0ya/WEUMn4P6+l4l+Gdyzx/Yw08dF8XPJdv/Woze8USlv9an6Pf+XBJpyPm8kESak9jwUof2t9cxbZrGu6mlmHwnOUsXTGSwmt92JsqD6EXQojXXZ46UwUoX7IsMzRf8GXgTBK1JnwYuzqN1IJ/orO6Qsm0DvzQzQ5r6+e/YGvoofvpISFcDb/O2N7HUJOMxrIi0TrArBhlM566j4OdljuadDQ3D/L7l0dY45zMqTtaMp5/Lw+hF0KI11meS6oA1StU5Q+r8Yw6PTP7S8HPSgWlCl7h53csKVjA0E9s9NzaMgUfBjG2jY6EZEtsbAyfzBt66P57hQpRyKshvy39CBd9NKGhajwsdVx4/Kn7aX74LDbn44M+tNVsoE+Wp/PLQ+iFEOL1lSeTKmScsS51uWG2ZAAAIABJREFUnca0/XPZkeSfzWwwOaNGTWeHBnzR7FMsLY09hFdNgdKunPqwGx+sUHHF7AMWepuDwYfgZ8j60P1i6/rwsdWnfDQoiPKJpwit+B0rRpZ4ciWzslRzO8Mfo8eQ8fz7EDavC6bBi3VRCCFELsv1B+rnxInA08y7tJpTmouonjW7KiresqnAoArdqVTWO2frpN7l6k0tBTwKYffMZ45aYq9dJc7WjeL5s7m8rIsj8moi9h5uOGhuEpHgQAmDT+cXQgjxungtkup9QSEX2Ry4m+O6YMJTo4yfvSpQ3Lootc3L08G7OeU8X+CGJCGEECKH8uzlX0MqlCpHhVLlAIiJvceliCtExd4iMS3jxh57S1uKOBWiXPEyODvK3ClCCCFerdcqqWbl4uTM205v5XYzhBBCiAfkXlMhhBDCRCSpCiGEECYiSVUIIYQwEUmqQgghhIlIUhVCCCFMRJKqEEIIYSKSVIUQQggTkaQqhBBCmIgkVSGEEMJEJKkKIYQQJiJJVQghhDARSapCCCGEiUhSFUIIIUxEkqoQQghhIpJUhRBCCBORpCqEEEKYiCRVIYQQwkQkqQohhBAmIklVCCGEMBFJqkIIIYSJSFIVQgghTESSqhBCCGEiklSFEEIIE5GkKoQQQpiIJFUhhBDCRCSpCiGEECYiSVUIIYQwEUmqQgghhIlIUhVCCCFMRJKqEEIIYSKSVIUQQggTkaQqhBBCmIgkVSGEEMJEJKkKIYQQJiJJVQghhDARSapCCCGEiUhSFUIIIUxEkqoQQghhIpJUhRBCCBORpCqEEEKYiCRVIYQQwkQkqQohhBAmIklVCCGEMBFJqkIIIYSJSFIVQgghTMQ8txvwX6WPvcLxs9dJUR59XWXmSsN3vHOnUdoAlk34i/DaIxjfsUTutOEF6KMDORwYjZVHDd7ytAcg+epJTkQ5ULlWWZz+4x8hJT65QB/LleNnuf7kgY5rhXp4F5Sgv3Fat+uoNGvVRmnYpLkiXh2N35eKt4OtYmtrrVioVYrK3EqxtbVV7Ar2zL1GpWxUehe0UCp8fTT32vACkv7prjipVIqF90jlYJKiKIpWufBdTcXKY5CyOzW3W5f7JD65QOOnfOntoNja2irWFmpFpTJXrGxtFVu7gkrPf5JeYUO0StDM1kq14TsV2dQvl3xMyiWW78wgID6JpJitDChuhkOXJUQnJZF4awk3fWcz5N0WNG7Snn4/bOeaDkg7wbxhA/lx7W58PutC8xZdGb0mBC2gv7WfGR+3o2nL7oxZvoJZgwYxyzcB9LfYP+Nj2jVtSfcxy1kxaxCDZvmSAOhv+jJ7yLu0aNyE9v1+YPs1XS5HxETMHbG//idj5wTxeI8M9jkzrj/8s4Vf+rejRaehLDxziU0TetCy+XuMXheGzti6r6NnjQ9pnJg3jIE//MOWX/rTrkUnhi48w6VNE+jRsjnvjV5HWEaA8J09hHdbNKZJ+378sP3aE+X/J1m+w4yAeJKSYtg6oDhmDl1YEp1EUuItlrWNYM3E3rRr2oR2vSexKTSNnMY77aQPwwdOZumamXzSoRkte4xjfagW0BsYP3RcXDmKwb/sJnj7LEbM9Sctt+PyBpOkmtckbGVi/0nstu7EyD6FOPJtTz5fdRf0kRxaNZ9pX0/jpFstisfsZMaAsayNTeDfMb0Ys/YOXu+2x3nzJMb9uZBdl7Uk/DuGXmPWcsfrXdo7b2bSuD9ZuOsyWhLYOrE/k3Zb02lkHwod+Zaen6/ibm733RTUNeg3oAKnZnzDihtZh3Ujfc6M6/RvfAgvWZxE37l81v5DVukqUTR6GzOGTWW35g2K17PGBz2Rh1Yxf/o3+ISXpHiiL3M/a8+Hq3RUKhrNthnDmLpbQ8LWifSftBvrTiPpU+gI3/b8nFWvZYBelST2jelIrwUJNP96FC0S59Or188EaHMWb/31g6z8aypj59+hbpe30e+fRt/PFhIZZ2j8iMGhiDNW6QoWLp5ULO6EWW53/w0mSTWvcWjD1PVL+ap6HKdCkrFUJxB2ORItKlQqsGs8gpmjRzNtQD3ME64Seu0sfodvYdlwEN992osvRvegnDmoVOkE+B3mlmVDBn33Kb2+GE2PjDdQ4UCbqetZ+lV14k6FkGypJiHsMpHa3O68KagoOeB7+jpuYfKUPSQ++CrLWJ8z4mrfaDBTx06m79vmaPI1ZvjE0Yx7vwrqOxFEJL1J8XrW+IAqI0AMnjqWyX3fxlyTj8bDJzJ63PtUUd8hIiIJhzZTWb/0K6rHnSIk2RJ1QhiXX88AvRppp9i0LRRcrInx9yfGtgC6k9vYcU2Xo3ijUqFSudBq2AT69hnPiLYFSDrqh7+VofEjioJ1m1HZUYV12ZZ83LqsJNWXSJJqHqM9N5U2dXvw+1kVBYsVwFYNinJ/5FORL39+zAFrW2vMUNDr09Ckg7m1DZaAysYa68yl0zLewCbjDaytH1TC1DZ16fH7WVQFi1EgoxKUxxvzmlLZNWLsxA4kLh7PvFAVKnhKn1U4ODmhwgZrK1Dlc8LJTIWVtTUqFPSaNytezx4fUDk44aQCm4wA4eRkhsrKGmsVKHoN56a2oW6P3zmrKkixAraoUVBe1wC9CkoyyckKZubmqACzsp0ZPaEXNWxUwNPinXmFQWWHo6MaUGFnawNpKSSdzW78EK+CJNU8JvXscc6mFKXJJ5/T1SOdu+mg1aYbX8G8DF5lrEg+toXNYbc4+fe/BGgBzCnjVQar5GNs2RzGrZN/82+A9n4lHD+bQtEmn/B5Vw/SMyohm1peMyoKd5/MyFpXWPH3ebTwYn1+4+Jl4viQytnjZ0kp2oRPPu+KR/pd0tGS3W77n2dRnorlrEhXedL563EMbeaBtZM7xQo8w5Csv8axgyGkpYVw5GQUKs9yeAQZHz9UKkhL1ch33S+ZJNU8xqZeG5oXCGdOB3eqTlLTpqUTwfOGMOWQkRFKXZSeE8bRVLWB3lVq8XVEUcqYg0qlpmjPCYxrqmJD7yrU+jqCohlvgE092jQvQPicDrhXnYS6TUucgucxZGrgq+3sy2RWnoHfD6SUTpNxtvUifTa27pSDL7sXL09O4zPlUA4Ks6Fem+YUCJ9DB/eqTFK3oaVTMPOGTOGg3BFjmLo4fb4byztRM2hUuizebcay7Y4dBZ7lR47q/Oh2fEi54nWYeLooXUd9wtsNjIwfR4tSuaILies+pVLv5a/n/QCvCVXrdh2VdG066Wnp7N+zM7fbIwBdwjUuXYOi5dxxVGK5EanB0b0QdgY/AukI3TaHFQFudB3eBTe/wVRps5HGG0L5s/xe5qwIwK3rcLq4+TG4Shs2Nt5A6F9tsNIlcC2jEtwdFWJvRKJxdKeQ4UreDC/S5/9CvF6ojzoSrl3iGkUp5+6IEnuDSI0j7oXs5JN7djR3Cb1yCwqXpGR+66cvnyl1Ux+Kv3ecvgeOMdL1KvfsSlG6UMb6RscP/T3CL0WhKlqW4k7yiIKXRZLqa09PzN6JdOo5k/NmhbCOjyFfmxmsX9Kf8ol7mdipJzPPm1HIOp6YfG2YsX4J/b0sc7vRQogXkJFU/elz4Aw/1ZbjOS+RpPqm0MZyLfw2OkcPPFyts5wdaIm9Fs5tnSMeHq5Yy2mDEK+/xBtcjEjBqWQpCuX8BFe8AnIN4E1h7oR7aSdDb+DkXhpD7wghXlP2RSnnlduNEIbIeYsQQghhIpJUhRBCCBORpCqEEEKYSJ5MqvroQA76+nI8LPHBa8lXT+J39BKx+lxsmEnpiQ48iO/RK29Qn14z2gCWjR3BlI3hud2SvC9LrPL6wwOyHz+0BCwby4gpG19Ze7QByxg7Ygobw3MSuYz2Df9ufY7L18fsZdaIz5l7JPUZ63pd6IkOOoTvkUvc02f8HXvlKL6+/oTEZS4RHchB36NcumdoMH24zV84LDk4DvJkUk3dP5n2jRtTr/1kDiUD6AhfOpAW3Wdz8o15Sksq+ye3p9n7szj1xvTpNaMNZff8Oaw4fCu3W5L3ZYlVXh+unzZ+qM0tsbJ6dfdoakN3M3/OCg7fylnk1OaWWFvmvH1K3GnWz/mDzYGpoDbH0soK8zw5sj8vPTf/GUbzpp/x92096O+welhTGjduxlcbYwAdoQv606TlGLbFG1pfS+ju+cxZcZjHN4EueBZtqo9glyaHTcnBcZB3Q5/NFFXaiF3MGNSFFk2a0aH3N6w8n2iwiNeXoembspvuyXhM0k7MY9jAH1i+ciJd3/2Rw6mGpujKnG7qx7Xs9vmMLs1b0HX0GkJes+ehZxef5OA1TOzdjqZN2tF70iZCH3/Sj5Gpy0wS87zwVCH9NTZNHsDg2X4kZPl7yK8Hicls7z9bfqF/uxZ0GrqQM5c2MaFHS5q/N5p1YY8dgfqb7PhpCIO/30KenQEvu/EjJZ64xFQgjZM+wxk4eSlrZn5Ch2Yt6TFuPaEvcb/X39zBT0MG8/2Wa+hIJnjNRHq3a0qTdr2ZtCmUtMz2xSamZC5veMpBffRBZn/Snmat3uebrTce9lGbQnxcIqlGp+N7HY91c8o2rIe79ixH/VMh9RiHz1hR0FXHiYP+aEjE/3ggeDWggdmBp0zRqOfmjp8YMvh7Nu5fzqjBv7A7eDuzRszloO9vDBo4lR3RetDfZde0gQz69SDJxpr12HFwf4zJu0nV2BRVmqN827Ejk4+60OGTnnhd+5PerYewPib3mmpyRqZ/MzrdU4rxmOgjD7Fq/nTG/BZAsWqlsdhhaIquzOmmpn3NtJNu1Coew84ZAxi7Nja3I/FMjMYnYR9jOvZiQUJzvh7VgsT5vej1cwBZxxFjU5eZIuZOeeEoUxehSOph/vr+D7bFgj5qM7//tIwgimGd2d5vfMIpWTwR37mf0f7DVegqFSV62wyGTd3Nww/yKZya1oOeM4Mp2aYx7nl1uhOjU9xpCfddzoLV/oCe6wdX8tfUscy/U5cub+vZP60vny2MfDltSjnFtB49mRlckjaN3UndN4aOvRaQ0PxrRrVIZH6vXvwckEq473IW/nMM49PxJbJj3AeMXH0H724dyH9oC6czd2ZtuC/LF6zG/7bOyD79eh7rVjUbUccxhlPHAkm+cIQTCVXp0bU8d/0PE5R0Fv+zyRSrW4vw77KfojHl1DR69JxJcMk2NCnrhrNVOoqFC54Vi2MbsouFC7ZwLl4BJZ5zWxawcMdFI3PPPnYcpD4cY/Lw71Qzp6ja0JbJU/awvEjGq+mn1rA6QE1jn+kMfd+ZVKfDLOuwnnW+qXTu/Ib8Cjpz+rc1O84/Ov1b9SzTPbWGggfm02mDH4eOXjQak9ZmKlRY0vjrJczqZAf6ZqxfuoYd5x+doqt6xrxyjJg5mjap+fFbNoSrobfgdfqFq8pIfI4Fsi0UXGrE4O+vwraAjpPbdnBtWJkHq2ZMXfZkXIyW+SwxzxPMqf5RT2rOnsmGXfdoGL+NYxaN+bmrB+rjKlTY02jwVMbWXMKB+YOJaDyciaPzk+/f5UyMiCCJQgDE7h5J94BrNF10ji+q5ZW+GWJ4/HhiKZUKlUsrhk3oS2sKcmB+Jzb4+cMnnU3cnlh2j+xOwLWmLDr3BdXs0jiwaRuhuFAjxh9/lS0FdCfZtuM65R+skzkd35odnM86HV/yBfYfvIllg0l82+99HKoHsXTNtCdqNLZPv5bHum09Gr5lyVr/owQWOkqYZ126t7rFmoVHOHDOgZPX8lGncUM6NjEQr/ufnmN3M7J7ANeaLuLcF9VwUKfRrLIjk06VpeXHrSmz6o8cN+fx4yDtwKYHY0xe+AxtlKEpqpSEBJKxxN7BBgC1vR02pJCUnFevQz27bKd/MzDdU2r8U2Kiyk/hIhaANtspulT58pM/Y145rM1A0b+Gd1AZik9SMsmKGebmKsCMsp1HM6FXDTJn2eJpcXmxmOcdZmV78lGDNPZs3MSW7YexaPY+nYtkDgEqB5wy5hrDChX5nJwwU1lhnTHXWOblRT0xd61xKxaL39qdROXx3cPgFHcGl3MkY/PakbF5U03fGH0Md63dKBbrx9qdUehRSE5ORjEzJ2O3LEvn0RPoVcP6YTuNTcenpJCcqmBpa48VoLK1w/aJCt+wY11dgIYNK6OcPcD8AwE4vlWHKnXrUcM8gAPzDxBgXotGb1/JdvpCfcxdrN2KEeu3lp1Gd149GTPrpaM1ekn8yeNASX44xuTppGpoiirzijWpbJvISb+jJOhTCdp3hOtmFalR9TU9S02PIeT0CU6cyPh3Mvhm9tO/GZjuqXy1nMbkPzBFl6H4VKxIOat0VJ6d+XrcUJp5WOPkXoyHs2ylZR+XF4p5HqJ2p2vvFrB7KjP8rGnTsx35n60ASnafzrqZ72O5aRzjN0WTt4diA1PcGaC/doyDIWmkhRzhZJQKz3JljCz5AtQl6T59HTPft2TTuPFsijajfMVyWKWr8Oz8NeOGNsPD2gn3YgUeDsrGpuOz8KRUcTOSzh/jdJKOG34HCH7inOJNO9bNKNGgPp6xO1i1K4VqdWth61iHOl4xbF+zn5TKDWlofz7b6QvVJbszfd1M3rfcxLjxm4jWc38+PDQ6MLOzw0Z/lXPnYtCE7WJfkPFbkR4/DszKPxxj8nhS5YkpqtRFP+KnaZ1RFrbF08ODetOiaPjtLIZ659Uvd7Kni1zFp3Xe4q23Mv7V6bPQ6PRvUwN1Bqd7quGR05gYm6JrKoFvyom+ofiU6sN3Y98hakYjSpf1ps3YbdyxK5Dluw8ro1OXHUo3UmaOY5635G/fh/YWl7ls1ZYPWjg+RwkqXNpPYmLrZFaMmsTeOJM30bQen+LOAHV+HTs+LEfxOhM5XbQroz6p9nLaonKh/aSJtE5ewahJ+3Hq8x1j34liRqPSlPVuw9htd7DLOvebsen4frjK+yP6UDL8F1qWLEWHzVrczRTSNFnPsN+8Y92yWkPquSYQm1yet+u4gJkndWt7kBKXQsn6DSlh//TpC1Uu7Zk0sTXJK0YxaW8SJSpXxCVxHZ9W6s3ayu/TtWQMK98vjlvXUxSubIGSpiHV4CfHR4+D/U4Pxxhat+uoNGvVRmnYpLnyOtHGX1cCz11Qrt5Lz+2mvBTa+Agl8EKEEqtVFCX9nhIZflO5s763UtDCSxl1NEG5ExKgXL6Z8tg6OYmJVomPCFQuRMQqGUVHKuE3ExXdy+zMK5KyMfv4pN4JUQIuhCh3UgytbTguSU8p83XbD9PDFyqdC1sp3qOOKKm53Zhcl6Js7F1QsfAapRxNuKOEBFxWbhrcN16mVOVOSIByIeSOklF1vLKqm5NiUe6rjLe18UpE4AUlImMgUO5Fhis3EzOO1tRbl5ULV24pxpv85h7rRmUTL8PSlZiwC8qF8HtKuqIoSkqUEhwQotzRPF/1qXdClDx8o1L2zBzcqFDJLbeb8dKYObhT4cEDs50oWhxSzwIoKFiSv6T3E5fuchYTMxzcK/Cw6KIUN12z8wDj8bHKXxJvo9c7Dccl9Sllvj77oY7g6c2p880BFO9hrPqyFla53aS8QlHAMj8lje8cL5FVlv1Ky8lJTem/NokiH9XNeNvMAfeHAwFORR8erVYFS+NVMLuy3/Rj3YBs4mWYOc4lvHC+/6d1Ycp5P3/1VvlLytRvrxWZ7il7LyM+b1DMdQk3CI9WUah4Eezz9lXqVybxxkUiUpwoWaoQeWHzpsVcJeyeNe6lChm4+Ui8DiSpCiGEECaS929UEkIIIV4TklSFEEIIE5GkKoQQQpiIJFUhhBDCRCSpCiGEECYiSVUIIYQwEUmqQgghhInk6hOVQkNDc7N6IYQQwqTkTFUIIYQwkVw9Uy1ZsmRuVi+EEEKYlJypCiGEECYiSVUIIYQwEUmqQgghhIlIUhVCCCFMRJKqEEIIYSKSVIUQQggTMStTtvy3er0evU5Pn94f5nZ7MiSFc+LoGS6FhRN+9SrXomLR2xfA2eZN+wyg5dbpf1m72Y8L0Ta4l3TFRgWgJyZgO2s3HeBisgueHk5Y3F8l+SqnT97CqmiBzGUBzQ38t6zl34OBxNi44+lqjeqJuoyVmUbUiS2s+/cIodpClCrmgJmhlt46zb9rN+N3IRob95K42qiyb2cuSr56mpO3rChawCYzDsbiLHIumaunT3LLqigFXpPgPbkfGNuPNUQe28zafw8RFGuLe4kCWD/o4uvX7zeW9han/13LZr8LRNu4U9I1Y7vqYwLYvnYTBy4m4+LpgVOWQejJfSBn453hdY3tP4/Kk1lKe2UhA7p+xe8rV7Fq5UqW/jmJbrVq0HdlOLrcbpzJ6Ahd0I0G/ZZzJT6Gk7M70+jLPQDEbP2M5n0WczkxhiOT29Np9gW06Ina9xMftGhM8/dmcCz9fjFX+LNrC0btT8RKFYZPj4aM2BH3RG2Gy9QR8ld3Gg1cQ1jcdTYPb0aP+aFPxFgXuoBuDfqx/Eo8MSdn07nRl+xJMFZmLtJHse+nD2jRuDnvzThGRoiMx1nkjD5qHz990ILGzd9jxoMdLw8zuB8Y24+1XPilPU0+/5drSTGcmNaBhl/tI5HXsN9vMl0oC7o1oN/yK8THnGR250Z8mTEI8VnzPiy+nEjMkcm07zSbC1qMjgU5Ge+ebf95YmVo3a6j0qxVG6Vhk+ZKXpF+ZoJSo9JI5Yjm4WtJ2z9VStX5XgnS5l67TCtBOfj7WGXhmYxOaoN/VOpXHqkouijlz7bllQHbkxRFURRdxG9KC+9hyn6NRjm08DdlX+Bcpa1nf2VramYx9+Yr7dw+VNanKIqiaJXAKXUUr88PKJqsVRkrM/miMrW+t/LFwYyl0wMmK2/X+EY5nf5YSw/+roxdeCajTG2w8mP9ysrIQ1eNtPNlxCqHNIeUhb/tUwLntlU8+29VMkJkJM7/RdrrytbJHyptW3dS+kxcrMwe0k/58/xJZcHYuYp/qqIoulvKth+HKrN8ExRFUZTEA78pE1YGK8mHFiq/7QtU5rb1VPo/2PHyMIP7gbH9OErZ/9v3yprLGQNL+rmJyltVxyj+6YqiMVW/Y48rPsO7Kq2bNVfa9PhKWXouMePlUwuVr95vr7Ru3VHpPXG9cuU1CG2uSTio/D52oZJxGGuV4B/rK5VHHlKi/myrlB+wXUlSFEXRRSi/tfBWhu3XGN4HtDkb755p/zny+ICnUfLkmeqTtNyOuEGaozMOb8wVGHvqDfmePlUsAT2Rh45xr2JNSA8mIMSDSpWtAVAXrEg5bTAXYsyp22cojQo/drHC4W2aeQezbtlBzp/ZwSrfNOo18sIy6zJGy9SQprXCxipjNzAvUhjnsECC0h5rab0hfN+nCpaAPvIQx+5VpGaJUCNl6k0fqpyyrEufoY14NERG4vwfFLdxDIN3luOHNWv4uXEQy5afJjI9PymnfFh7Jg0S9rH0752s+fsQGjQcXfkXwfqC2NTtw9BGhY1eJstzDO4HRvbjcoVpOHQs77rHERF8jHUL9kHDxpQ1B0uT9FtH6F8j+U0/kOU7trHw0/wc3OxPYtJuxvaai/ngRWzZ8Cutroyh36/Bb9CVOBOzr8eQ7/uQcRhHcujYPSrWLEFwQAgelSpjDaAuSMVyWoIvxKA3uA+k52i8e6b9p4zl4yvn7mMKs6O/uox+dfdiCyiaBJId32HCnN64vSYfA3JOQ8g/w+m9sBg//P0esJ3kVCts7n9/rLLG2kpDUpJieHUzd+q1L8XqRT8xZXsqEfq6fFkxH/qY8+z1u0KCYkZhb8Vwmamlad3SnJ6Tp1FtsBdRG5dyTu9ESvR5dp/OXLd6K+oUz9hxNCH/MLz3Qor98Dfv5T/DzmdpZ657PM7/NWkEHjlDwWYj8bI1w7xeF5qX2A3qwjRr5MCgg9dJjvUjpnl/vC8cIiC5EH5nPGj6jWNuN9zkHt2PM17TXlnNxOGLOBnjSucfvbA3WW0qHEt4kLrof/w8L452rYcyp7E92mOjOeTQhmVvu6A2d6HTuzX5asFh4kaWx8Vkdb+BNCH8M7w3C4v9wN/v5efMzlSsbGwyv8dUYW1thSYpCYOjkFkZg+OdJj2G87v9uJKgYFa4Oh3qFH9KE7LuP3pizu/F70oCillhqreqkze/UwVQF+/FgsMnOOG/g4m1zXFuPpBelW1zu1kmloD/jHfpvsiDHzf8QoeialA545wvkdjYzDM+JZbY5Hy4OBv+vKwL/I3h892Zvmczf6/ZxZbekYwav5742Csc8/PF19ePczfzGSnThprfbGRBJx0n/IJw7NaN2gULUiA5y7o3Mr4lTfCfwbvdF+Hx4wZ+6VAU9TO2M3cZiPN/jkJqigZLm/s3XVhjaQlgRslm9dEc3c9h38uUaPQh7+Q7g+/pfRyza0Rz1zcrVk/sx5mvm3sPYOHuI5xe25zDA0ezMdFUNarJ/64P++d3wyV0PWNbVqHx5EMkpiSTamWLbeaVN5W1FRaaFDS5eKEnz0vwZ8a73Vnk8SMbfulAUbUKZ+d8JMbGkhE2hdjYZPK5OBu5umBpcLxzNY/lyjE/fH198Tt34ylNeHz/UYi9cgw/X198/c5xQ5vLD9TPEXV+2k2ZyN8NhzO9w16+qWaV2y0yET2RKwcy2LcVi9YNpaJ15suWFalbNYplftGMKFGI9DO+nCpYm+FGThgUTQqpmGOeeXCamalJS9WgKtmVcTM7Zy6VwHqDZeoI3bWZ27W/5sd+FkSv6c20qq2pV6EzHR6sC/rIlQwc7EurResYer+hz9jO3GMkzv855ngUL0TkpctoKI31vbOcD9dTETCv1Iy3bs7gtxgXuo50pV6wns9m7ibtnZ9wz4ufkZ6Twf1YG8Csd78m5Zv1jK1phcrWFuu0JJJNdh1Wx7VD27nu2ZUvpnVn+KCfad5pI0EfVsDzhj8ByVDSQUdkYAiWpd/H5c36DGM6+khWDhyMb6tFrBtaMeNyL5ZUrFuVqGV+RI8oQaH0M/jvKlocAAAgAElEQVSeKkhto4OQ4fHubbuS5B83k85G1nrYBAP7D2aU7DyOmVlXfj1uVNIpEYu6KKXqTVFOvylf5muDlR/ftlUKlKmu1KhRI+Nf3S8VRVGU9MB5yntVqyutunVW6lV6Rxm1646iSz+lTO/0llKjmqfiZOWqlK1eQ6kzYpOSqA1Rlvb+P3t3HhdVuT9w/MPiDKCiqOAOigsu5G4qmuKCiohpKpXVlWtdNbWwrDRbLFu8pdc07+1naYpZuS+lmGQukOSGwhVRRBbREBUckWVgYGae3x/gks6o3UYh+75fr16v29Nzno0zz3fOOXPPt7PqODBUTZr4hHqkQz81Kyr3lu4stqlM6mJEmOra5hE1ekyQ6tI5RH1x/OYH70aVNKe7cqnTQnW6Os7OfmpaRJGVNitO6ZG5anjXzqpj05pK695SdercQ03ddMTqOv/VGE+vVE+2aqX6/+1Z9eSIkaqPT2f1dnypUqpQff/3hqpq30/UaZNShv3T1UMuvuq1/QalVKk6Mne46tq5o2paU6vcW3ZSnXtMVd8XVPRsrLN4Hnx/xcp5XKBSV45VHVo+rIY9Pkr1a9tC+c+KUrk2m7dRnf42VHXuHKT+Pul59Uy/zqrfu3tVgcpVP7/bX3Xq+zc1ecII1a3LGLX85M2/mBFXGZPmqO4udVSLTp2vfY79pkUoVXpcfTGqg+o0OESN6PmQemT6DpVjsnYO5N3Ffvd7z5+imw9WdoFDH1WlxlJKS0rZs/PHP/h1QtiMIZvUlBycvHxoWO1OX19N5Gcmk35ZQ/0WzXC3djFvpU3TlbMkZxqp27wptW5+7m7TcYoKV3yB5NQr1PAysnRQKCWf/cK77Sv/Dat7zVxwjuQ0HVXqt6CZ1Q/Q/8505SwnT+eiqd+C5h7Xb5foLySTlqOlkY8XNeXP8D8ykJ2aQo6TFz4Nq93xmeYf2u/uggRVIf6KTCf4oPdYCapC2Jh8moT4K3JoxsSv12BuIFuAELYknygh/pI01G7atKIHIcQDRx6CCSGEEDYiQVUIIYSwEQmqQgghhI3IM1UhhBCVn1nHscht7M/S0qp/EL28XMqLjxG5bT9Z2lb0D+qFl4UX71muY0Z3LJJt+7PQtupPUC8vbjnUSp9Wy5ErVSGEEJWejm0vBBC64hQFun3MDh7OgkSj9dRvvznUcp07p6600qfV8nKV8Y1KQgghxFWmrM9VUKsJqizTpEmdWTRQtX1xl8qwlvotb4+aP3WxOmQwWU4Pt8t66sq8PfPV1MWHrPS5RxVZKb/6bia5UhVCCFGplSYdI9XzIcoyTdrj4euDMekosQlWUr85uOHVqhGuDqWW08MdjSXBSupKBzcvWjVytdJnIheOWy6/mvVSnqkKIYSo3PR6irXOXM806YTWoOdyoZXUby7teGxCO8BAit5CHf1lCq2krnRp9xgT2oFhm6U+CykstFJenm9OrlSFEEJUanZubrgW5HI902QuetdaNKp9p9Rv1tLDNaL2HVJXWuuzVh3L5VcPlaAqhBCiUtP4+tEhK4bobDNgID7qCB7detHNrwNZMdGUFZenfut+Y+q38vRwN9fp1Q2/DlnERGdTVlyWuvLGQy332R13K+VXD5Xbv0IIISq36sHMePlbxgwJIsIrnxM5A5i/qTU1a8zg5W/HMCQoAq/8E+QMmM+m1g6YLyxmSPsonk1bxehgS3VqUmPGy3w7ZghBEV7kn8hhwPxNtHYwc2HxENpHPcv5VSMs9ulQ3cdyeflQJUuNEEKIPwVDdiopOU54+TTkeqbJu0n9ZqXOXaSutNyn9XIJqkIIIYSNyDNVIYQQwkYkqAohhBA2IkFVCCGEsBEJqkIIIYSNSFAVQgghbESCqhBCCGEjElSFEEIIG5GgKoQQQtiIBFUhhBDCRir03b9paWkV2b0QQghhUxUaVL29vSuyeyGEEMKm5PavEEIIYSMSVIUQQggbkaAqhBBC2IgEVSGEEMJGJKgKIYQQNiJBVQghhLARCapCCCGEjTi0aNnqHbPZjNlkJnTsMxU9njKFp4ndH09y+mlOZ2RwNisXc7U6uDk/aN8BjFyIi2DDlmgSs51p7O2Osx2AGd2x7Wz4/mdO6mvR1LMmVa4eos8g7vAFtA3qlNcFDOc4uHUDEXuPo3NuTFN3J+xu6ctamyVkxW5lY8Q+0ox1adaoOg5WRqvPiOPwBS0N6jiXt2/g3MGtbIjYy3GdM42buuN0a8f33a3jtLbO4u7pyYg7zAVtA+r8SRbv1vMAjBfiiNiwhejEbJwbe+PubAcYyDywhQ0RMZzIdaFxkzo3nMd/vnk/sIwXiIvYwJboRLKdG+PtXvZ3NeuOsX3D9/x8Uk+tpp7UrHL9kFvPgT+y31k7f36rUkYpY8pyJox+hX+vWs3qVatY+fm7hDzcmb+vOo2pogdnMybSloXQe9w3pOTpOLxgBP7TdgKg2/YCAaErOFWgY9/sYIYvSMSImazdHzFmYF8CRs3jQOnVZlL4fPRApu8pQGuXzpIn+jA18sotvVlu00Tq0sfxn7ie9Cu/siVsAE98mXbrGpuz2P3RGAb2DWDUvAOUlo8/5fPRDJy+hwKtHelLnqDP1Ehu7fk+sjJOa+ss7o45azcfjRlI34BRzLt24lViFs8DMKUtI6T3OL5JyUN3eAEj/KexM99I4sJg+r0UwdlCHbEfD6PPK7sp4E847weZKY1lIb0Z900KebrDLBjhz7Sd+aDbxgsBoaw4VYBu32yChy8g0YjVveB/3++snT+3HFyxb1S6Hbu6/Xhl0cd015T9uz5yAu3e/ZZTITNpZe2rxZ9KEVlFrXk9fBah7TWYghX+IZFgbsv6z3bR9YPDvDvIBfNIJwIDvyBm0lyqZFRl/JLp5AXFXm8mP5qtRzoRtnYKw51MPHw+glGRCZQM6oXmah3zecttTpjM/hWnGLrwCO/01GAMhEfGhpMwdjYdbjwzjOlkVB3Pkul5XO86n+itR+gUtpYpw50wPXyeiFGRJJQMopeGimFxnFbWmf4VNMgKZMrkhw9f5z/78nF/eAQdcqJwnjiZKqsO4fv2BLpWucj2j98jyW8OU3tXo3Dvv/n41wBmeGZQdfwSpucFEXvnXiqexfMAirKKaP16OLNC26MxBaP8Q4hMyCHIwZ8Pv5rOyOYOGAP0+P1tByeMfWmfbqN5X4ll6ayP2ZiYi12d9jw58x2efqgqV+LCeX/uRhJz7fF4OJS3Xh9OM+0fnfwDqiiLotavEz4rlPYaE8HKn5DIBM6nfsaurh9w+N1BuJhH4hQYyBcxk1jYw8I5YEpl/f+831k5fxJL6N/9xg3PWDmvVG9l5OKZc5TUcKP6A3MHpho9J39AaHsNYCYz5gCXfbtAaRLHUj15qJ0TAPYevvgYk0jUOeIXOgX/ejd9o6jenQFtk9j49V4S4iNZHVVCT/82/CauWW3TQIlRi7O27DRwrF8Pt/TjnCi5aagaP0Kn+PPbrqvTfUBbkjZ+zd6EeCJXR1HS0582FRVQwco4razzX9CV715n0o8+fLh+Pf/qe4Kvv4kjs7Q2RUeWsCG+BPJ3s3LNj6xfE4MBA/tXLSXJ7IGzXyhT/OtZvU1W6Vg8D6Baz8l8ENoeDWDOjOHAZV+6+NSjz5SZjGx8hTNJB9i4bDf06UtLR9DYZN4m0pa+yiLzRL6J/IHl42uzd8tBCgp/YubTi3GcFM7WzZ8yOOV1xn2a9ADdibOxaj2Z/EEoZR/jTGIOXMa3SxOSjqXi+VA7nADsPfD1MZKUqMNs8Rwo/QP7nZXzp8XNG56m8l6pmjO+ZpzfLlwAZchHX+MR3v5sLA3/JF8D7p6B1LVhjF3eiA/XjAK2oy/W4nz1+bGdE05aA4WFyvLhDo3pGdyMdeEf8f72Ys6Y/Zjm64pZl8Cu6BTylQP12irLbRY3J3CQI0/N/piOk9qQ9d1KjpprUpSdwE9x5cd2GkwPL0uR0oHGPYNpti6cj97fTvEZM37TfHG9F0tkEzev819NCcf3xeMx4FXauDjg2PMxApr8BPb1GOBfnef3/oo+NxpdwHO0TYzhmL4u0fGe9H+rRkUP3OYMqWsJG7ucRh+uYVTtsjJjyjpmhYVzWOfOiDltqGaz3uyo0cST4vD/8K8vrjA0cAqf9a2G8cAMYqoP4evutbB3rMXwkV14ZdkvXHm1FbVs1vcDyJDK2rCxLG/0IWtG1Sb+x2K0zs7lV4d2ODlpMRQWYnG3dGhhcb8zlOpI+CmalHyFQ71ODOvhdYch3Hj+mNEl7CI6JR/lUI9Og3tU3itVe6+nWfZLLLEHI5nVzRG3gIk83c6loodlY/kcnDeSx8M9mbN5IcMa2IOdG26uBeTmmsuqqFxy9a7UcrP8fdl0fBFhXzZm7s4trFm/g61jM5n+5ibyclM4EB1FVFQ0R8+7WmnTmS5vfcey4SZio09QIySEbh4e1NHfcOw5o+Whm46zKOxLGs/dyZY169mxdSyZ099k0y3PGCoDC+v8l6MoLjKgcb76owsnNBoAB7wH9MKwfw+/RJ2iif8zPOIaT1Tcbg5U9SfA/cFaq/yD8xj5eDieczazcFiDaxugY9sJLP9pH3EbAvhl4gy+K7BVj/bUHrmEPV+GUCttEzMHtafv7BgKivQUa11wKb/zZuekpYqhCIPZVv0+gPIPMm/k44R7zmHzwmE0sLfDzc2VgtxcypZNkZurx7WWm5W7CxqL+527Yy4pB6KJiooi+ui5Owzh5vNHkZtygOioKKKij3LOWMFZau6KfW2Gvj+LNX3CmDtsF291fFAeOpjJXDWRSVGDCd84BV+n8mKNL34dsvg6OpupTepSGh/FEY9uhFm5YFCGIopxxLH8w+ngYE9JsQE779G8MX9Eea18Nlls00Taji1c7PYac8ZVIXv9WD7uEEjP1iMYdu1YK5SBomJwvN4x9iXFlXBTsLLOfzmOeHrVJTP5FAaa43T5vyScNuMLOD40gK7n57FIV4vRr7rTM8nMC/N/ouSRj2j8p7nne2fmzFVMnBTF4PCNTLl6IhiP8cnI1yh6axMzu2ixc3HBqaQQvc3uw5o4G7OdX5uO5uWPHyfs+X8RMPw7TjzTmqbnDnJMD97VTWQeT0XT/ElqPVjfYWzHnMmqiZOIGhzOxim+Zbd70eDr14Gsr6PJntqEuqXxRB3xoJu1zRLL+133qt7UfmM+d9jxLJ8/OOA94g1+s10GDn1UDRg8RPXpF6Aqi9L4t1Xnh15V+wxXS0zqTPhjqlnP91VccUWOzIaMSWpOdxdVp0Un1blz57J//KYppZQqPf6FGtWhkxocMkL1fOgRNX1HjjKVHlFzh3dVnTs2VTW17qplp86qx9TvVYExVa0c21l1HBiqJk18Qj3SoZ+aFZV7S3cW21QmdTEiTHVt84gaPSZIdekcor44brj12CNz1fCunVXHpjWV1r2l6tS5h5r6/RWVunKs6txxoAqdNFE98UgH1W9WlLq15/vH4jg3HbG6zn81xtMr1ZOtWqn+f3tWPTlipOrj01m9HV+qlCpU3/+9oara9xN12qSUYf909ZCLr3ptv0EpVaqOzB2uunbuqJrW1Cr3lp1U5x5T1fcFFT0b66ydr0lzuiuXOi1Up6vnQWc/NS2iQKWuHKs6tHxYDXt8lOrXtoXynxWlcm02b6M6/W2o6tw5SP190vPqmX6dVb9396oClat+fre/6tT3b2ryhBGqW5cxavnJ0nuxHA8EY9Ic1d2ljmrRqfO1z7HftAilSo+rL0Z1UJ0Gh6gRPR9Sj0zfoXJM1s6BvD+031k+f4puPljZBQ59VJUaSyktKWXPzh//4NcJYTOGbFJTcnDy8qFhtTt9fTWRn5lM+mUN9Vs0w93axbyVNk1XzpKcaaRu86bU+p0/NDLlZ5KcfhlN/RY0s9qxqDSKL5CceoUaXkaWDgql5LNfeLd95b9hda+ZC86RnKajyj06j01XznLydC6a+i1o7nH9don+QjJpOVoa+XhRU/4M/yMD2akp5Dh54dOw2h2faf6R/e5uSFAV4q/IdIIPeo+VoCqEjcmnSYi/IodmTPx6DeYGsgUIYUvyiRLiL0lD7aZNK3oQQjxw5LdmQgghhI1IUBVCCCFsRIKqEEIIYSPyTFUIIUTlZ9ZxLHIb+7O0tOofRC8vl/LiY0Ru20+WthX9g3rhZeHFe5brmNEdi2Tb/iy0rfoT1MuLWw610qfVcuRKVQghRKWnY9sLAYSuOEWBbh+zg4ezINFoPfXbbw61XMdyOsy76NNqebnK+EYlIYQQ4ipT1ucqqNUEtb1QKaVM6syigarti7tUxudBqtWE7aqs+IxaNLCtenGPQam8PWr+1MXqkMGksizV2ZWhPg9qpSaUNahMZxapgW1fVGWHzldTFx+y0uceVWSl/Oq7meRKVQghRKVWmnSMVM+HKMteaY+Hrw/GpKPEJlhJ/ebghlerRrg6lFpOD3c0lgSL6TDNOLh50aqRq5U+E7lw3HK5rvy95/JMVQghROWm11OsdeZ69kontAY9lwutpH5zacdjE9oBBlL0FuroL1NoJcWmS7vHmNAODNss9VlIYaGV8vJ8c3KlKoQQolKzc3PDtSCX69krc9G71qJR7TulfrOWHq4Rte+QYtNan7XqWC6/eqgEVSGEEJWaxtePDlkxRGebAQPxUUfw6NaLbn4dyIqJpqy4PPVb9xtTv5Wnh7u5Tq9u+HXIIiY6m7LisnSYNx5quc/uuFspv3qo3P4VQghRuVUPZsbL3zJmSBARXvmcyBnA/E2tqVljBi9/O4YhQRF45Z8gZ8B8NrV2wHxhMUPaR/Fs2ipGB1uqU5MaM17m2zFDCIrwIv9EDgPmb6K1g5kLi4fQPupZzq8aYbFPh+o+lsvLhypZaoQQQvwpGLJTSclxwsunIdezV95N6jcrde4ixablPq2XS1AVQgghbESeqQohhBA2IkFVCCGEsBEJqkIIIYSNSFAVQgghbESCqhBCCGEjElSFEEIIG5GgKoQQQtiIBFUhhBDCRiSoCiGEEDZSoe/+TUtLq8juhRBCCJuq0KDq7e1dkd0LIYQQNiW3f4UQQggbkaAqhBBC2IgEVSGEEMJGJKgKIYQQNiJBVQghhLARCapCCCGEjUhQFUIIIWzEoUXLVu+YzWbMJjOhY5+p6PH8hj4jjsMXtDSo44wdAEYuxEWwYUs0idnONPZ2x9kOwEDmgS1siIjhRK4LjZvUwcnuWiPEHb6AtkGd8rqVibX5mNEd286G73/mpL4WTT1rUuXqIZbmYzjHwa0biNh7HJ1zY5q6O3HrVK21WUJW7FY2RuwjzViXZo2q42BltL/9e+jJOLyP+OR0Tp8+zenTp8k4W0ATLw/bLtF9Yr4QySf/d5IePVvavG1jfDiztpno1bGB1bWt7GR9Koas+00s7H8lWbFs3RjBvjQjdZs1orrD7coNnDu4lQ0Rezmuc6ZxU/frseKWrm6OP2C8EEfEhi1EJ2bj3NgbdwtBpXJeqZqz2P3RGAb2DWDUvAOUAmAibVkIvcd9Q0qejsMLRuA/bSdgJHFhMP1eiuBsoY7Yj4fR55XdFGAma/dHjBnYl4BR8zhQWrFTupW1+YBu2wsEhK7gVIGOfbODGb4gEaO1+ZhS+Hz0QKbvKUBrl86SJ/owNfLKLb1ZbtNE6tLH8Z+4nvQrv7IlbABPfJmG6eaDLf09zHkc37GG1atXl/2z/B3GPvnJPVyve0vpYtmw7uBtahSwefJzrLho/t1tl5yKZPn2FIz/+/Aq3J3XBwo2T+a5FRf5vSv0IKzPvSLn5VWW9z9T6lIe95/I+vQr/LoljAFPfEmayVq5iZTPRzNw+h4KtHakL3mCPlMjuWW3tBh/wJS2jJDe4/gmJQ/d4QWM8J/GzvybjjWlVewblawyppNRdTxLpucRFHu1sIisota8Hj6L0PYaTMEK/5BIMHckx8GfD7+azsjmDhgD9Pj9bQcnjD0pzajK+CXTybveSCVibT5tWf/ZLrp+cJh3B7lgHulEYOAXxEyaSxVL88mPZuuRToStncJwJxMPn49gVGQCJYN6oblax3zecpsTJrN/xSmGLjzCOz01GAPhkbHhJIydTYcbzwxLfw/7egTO+A+BABQQ/Wog52a+fh/WzbqSg58z61g33hvXAUfzObb8cwV2oS/hsWU2h2s05cS6dZwwNWfUG3OY0LUGpswf+PD1/7Av34Oufathxg0Ac3YMn73zCRHJeVTx7MeL703DO+ZNPt68gyr6uTSY9yo9L6zl/Q++4fAlRxr3ncisVwbR2OEKceHvM3djIrn2Hjwc+havD2/2mzEaU9fzwVclhMwcQ2ttZVif6Qz8dQnvHK5B0xPrWHfCRPNRbzBnQleq3fX6vEbf4g28+fFmdlTRM7fBPF5ouMPC+sCVuHDen7uRxFx7PB4O5a3Xh1O/kqzPvXLv1v1uz8sHYd2NpN+y/5lIXb+CU0MXcuSdnmjKNjDCE57m6UhL5Y/SZOsROoWtZcpwJ0wPnydiVCQJJYPopbmxK0vxB4qyimj9ejizQtujMQWj/EOITCyhf3fNjZUq6ZWqxo/QKf7U+809iWr0nPwBoe01gJnMmANc9u0C9rXoM2UmIxtf4UzSATYu2w19+tLSUYNf6BT861XWGxtW5lOaxLFUTx5q5wSAvYcvPsYkEnWOludTvTsD2iax8eu9JMRHsjqqhJ7+bbjxHLHepoESoxZnbdlp4Fi/Hm7pxzlRctNQLf49rjMmfMo7sYN477nmf3hV/ghj6k5W7Tld9s3bfIkj339P3OUSMnZ/weyvChi3bCsrntbzr+fnE2e8wnevT+anVnNYv34e/c/vJ8kEYOS/S2ez1f0V1kZu4r0mW5k46yfq+PejbTVvBoeF0sMphrefmk/x35cQsXkevQ6/xIQvTpP300yeXuzIpPCtbP50MCmvj+PTpOvX/er8VqaO/T/MAwIrZOOyvD5mjBm7+WL2VxSMW8bWFU+j/9fzzI+79DvWJ5LSRv70a1sN78FhhHZMsLg+psKfmPn0YhwnhbN186cMTnmdcZ8mXbszUtHrc6/cu3W/u/PywVh3y/t5aYkRrbO2LJA51qeeWzrHT5RYKdfSfUBbkjZ+zd6EeCJXR1HS0582mpu7srzfVes5mQ9C26MBzJkxHLjsS5cWmpsrVdIr1dsykLo2jLHLG/HhmlHXSo0p65gVFs5hnTsj5rShWgWO8Pe5eT7b0RdrcXYu/75j54ST1kBhobJ8uENjegY3Y134R7y/vZgzZj+m+bpi1iWwKzqFfOVAvbbKcpvFzQkc5MhTsz+m46Q2ZH23kqPmmhRlJ/BTXPmxnQbTw+vms+5Gl/l+3joaTt5F+9tVq1AOtH/0aTrU0MCQYXQN+5LD2cc5Ge9O/9da4+LgiN+IATTZAeBIx1e/Ys62H/hq4c/kZpSSl/Urxho+uDhocK3rjlPSInYVtWTclX1s+QGcvd05vCOK2IwYqg/5mu617HGsNZyRXV5h2S9XmFwd0B/hwzE/o2ZuYtYjbhW8HrdyaP8oT3eogYYhDOsaxpcHDqH/Heuj1/SnposDGte6uGWst7A+MeR2SiCm+hC+7l4Le8daDB/ZhVeW/cKVJlT69blX/ui63/m8fJDX3YEWgYNwfGo2H3ecRJus71h51ExNA1bLG/cMptm6cD56fzvFZ8z4TfPF1awjYVc0KfkKh3qdGNbD67a9GlLXEjZ2OY0+XMOo2mZ0CbuITslHOdSj0+AelfRK1ap8Ds4byePhnszZvJBhDa4P37HtBJb/tI+4DQH8MnEG3xVU4DDvmoX52Lnh5lpAbm75MxKVS67elVpuli8TTccXEfZlY+bu3MKa9TvYOjaT6W9uIi83hQPRUURFRXP0vKuVNp3p8tZ3LBtuIjb6BDVCQujm4UEd/Q3Hnrv9Exfzxc2sjO3KmCGV8AN59XuInSNVq1ct+9/2WpwcSyg26Ck2aHC6+isFJ03Z1b35Ehv/0Zfx3xdSv01XerStj+aGpgDMRXqKTHmcS00hJSWFM7WHMv3J1hj0xWhdXMp/1GCHk7YKhiIDZhSFMT+wT3+ZpOMXqTSP969Nyg7HqtUpWyF7tE6OlBQVUGTT9WmLXZGeYq0LLuVN2jlpqWIowmCupOtzr8i624ymy1t8t2w4pthoTtQIIaSbBx7uVSyX10phUdiXNJ67ky1r1rNj61gyp7/JprxcUg5EExUVRfTRc7ftL//gPEY+Ho7nnM0sHNYAexS5KQeIjooiKvoo54wVnKXm9zGTuWoik6IGE75xCr5O5cXGY3wy8jWK3trEzC5a7FxccCopRH/Lr20qGyvz0fji1yGLr6OzmdqkLqXxURzx6EZYDcutKEMRxTjiWP6BcXCwp6TYgJ33aN6YP6K8Vj6bLLZpIm3HFi52e40546qQvX4sH3cIpGfrEQy7duztFe/dRXyrQfi5/KHFsAl7TRXMhQUYAKeCZE5lmvABUAbSTyZjpAP2F1PJsPdkhHsTiuud4+QpAzR34crRY5w2twXjCfYecifku4k81sxI3C/TyDOXbV12KBTg2LwNze1y6DL+VR6rCYXx3/GDqRm+OU05d/AYerypbsrkeKqG5k/Wwl5nR9WAN9jyb2emBjzHuz128mHP+38vxer6oDCknyTZCB3sL5KaYY/nsDY0+Z3rgx0oZW19vHCt15qm5w5yTA/e1U1kHk9F0/xJatnvrRTrc6/c63W//Xn5YK+7KW0HWy5247U546iSvZ6xH3cgsLsTprTtt5Z3gxPF4Hh9s8S+pBiDnTej35jPnXY8c+YqJk6KYnD4RqZc27Ad8B7xBte2S3Nm5Qyqxrh5jJ6wmoycVM4VnMIv7j16v/kf6n66mYxLSYT2Ci+rqPUnNuYjHh3twcinenOgoycFx/6L+R9LCa4ax7wRE1idkUPquQJO+cXxXu9Z7PgkuPybYc3EspYAACAASURBVAUznWKlxfnMI3jGy3w7ZghBEV7kn8hhwPxNtFZW5jPv77zUPoQJwVn08C4mYb+OZxcG89sYXN1ymw52XDInMickgFUdXEk/WZUXV46i9k1Dtfj3mLWDT4KdyEo9g1Ojxjjfl0W7PU33QHrMeJ9RY3fjYYA8DzCZADsN5tiFPPd8VYwJuyh9NpwBVZvSfqIfg6YNY+yGeuiL86lnZwZNW/r3vsS08c+R3KCQKi38aJ+8hgWR79K8URLLJr+Jx9zpzAxdy/NDnyCyjZnjR4oZuXQwo8a8wTMbwnh07I+0zI/lcIPX+aaPFjaVjc/OYxjzPt1L/0nT8Pvp/xjqfn9vFFldH+zQmGNZ+NzzVDUmsKv0WcIHtaK+/nesz7YBjGveiKRlk3nb4x2L6+PYcAxvPLOBsEfH8mPLfGIPN+D1b/qg/e/SSrE+98o9Xfe7OC8fiHU3xjFv9K37X+RMBxLnhBCwqgOu6Sep+uJKRtUGO7P51vK6Lfn1pfaETAgmq4c3xQn70T27kOAaN3dlab/bzsTET9mccYmk0F6EA6DFf/ZO5g1xunas6dRK7AKHPqpKjaWUlpSyZ+eP93GVbMtccI7kNB1V6regmXulfdp+9wzZpKbk4OTlQ8NqdzrJTeRnJpN+WUP9Fs2wOn0rbZqunCU500jd5k2pVWmfi94dc0EmJ08XU+faOuhZ96QP64efYKnfWc6a6+PjVfPa/y+v+EIyqfk1adbcg+sfDT3nT6ZT4NaM5h4arpxJRlfVm6baHE5mKuo1a0gNRyi+mEJKjgMNmjWllvb6sReS08jRNsLHq2al+9Z66/qAft2T+Kwfzomlfpw9a6a+jxc1yxfod61P7RLOncxE1WtGwxqOVtYH9BeSScvR0sjHi5qVbYHukXu67nd1Xj7A6266wtnkTIx1m9P0xg3MSrkpP5Pk9Mto7lGseGCCqhCW6Vn3ZEvWDU9m7eOV4B51JaRf9yQt1w0nee3jyArdP7LuDyYJquIBZyY/M4W8as1pWKMS3taqBMz5maTkVaN5wxp/tl8u/qnJuj+YHqSbAEJYYE/1hi2pXtHDqMTsqzekpSzQfSfr/mCSL0hCCCGEjUhQFUIIIWxEgqoQQghhI/JMVQghROVn1nEschv7s7S06h9ELy+X8uJjRG7bT5a2Ff2DeuFl4afUluuY0R2LZNv+LLSt+hPUy+vWX2Fb6dNqOXKlKoQQotLTse2FAEJXnKJAt4/ZwcNZkGgE3TZeCAhlxakCdPtmEzx8AYk3v1nVSh3L6TDvok+r5eUChz6qBgweovr0C1BCCCFEZWPK+lwFtZqgthcqpZRJnVk0ULV9cZfK+DxItZqwXZUVn1GLBrZVL+4xKJW3R82fulgdMphUlqU6uzLU50Gt1ISyBpXpzCI1sO2LquzQ+Wrq4kNW+tyjiqyUG8rHKleqQgghKrXSpGOkej5EWfZKezx8fTAmHSU2IRXPh9qVvXXK3gNfHyNJiTrMDm54tWqEq0MpSccs1DkaS4LFdJhmHNy8aNXI1UqfiVw4brlcV56vRJ6pCiGEqNz0eoq1zlzPXumE1qDncmExWmfn8ueYdjg5aTEUFqJc2vHYhHaAgRS9hTr6yxRaSbHp0u4xJrQDwzZLfRZSWGilvDyvhFypCiGEqNTs3NxwLcjlevbKXPSutWhU25WC3FzKihW5uXpca7lxPVGmHW5uluo0ovYdUmxa67NWHcvlVw+VoCqEEKJS0/j60SErhuhsM2AgPuoIHt160c2vA1kx0ZQVxxN1xINu3W9MO6PB11KdXt3w65BFTHQ2ZcVl6TBvPNRyn91xt1J+9VC5/SuEEKJyqx7MjJe/ZcyQICK88jmRM4D5m1pTs8YMXv52DEOCIvDKP0HOgPlsau2A+cJihrSP4tm0VYwOtlSnJjUspsM0c2HxENpHPcv5VSMs9ulQ3cdyeflQ5YX6Qggh/hQM2amk5Djh5dOQ69krDWSnppDj5IVPw2pWbr9aqXMXKTYt92m9XIKqEEIIYSPyTFUIIYSwEQmqQgghhI1IUBVCCCFsRIKqEEIIYSMSVIUQQggbkaAqhBBC2IgEVSGEEMJGJKgKIYQQNiJBVQghhLCRCn33b1paWkV2L4QQQthUhQZVb2/viuxeCCGEsCm5/SuEEELYiARVIYQQwkYkqAohhBA2IkFVCCGEsBEJqkIIIYSNSFAVQgghbESCqhBCCGEjDi1atnrHbDZjNpkJHftMRY/nGuOFOCI2bCE6MRvnxt64O9sBZnTHtrPh+585qa9FU8+aVLl2hJ6MuMNc0DagjrNdWZHhHAe3biBi73F0zo1p6u6E3S09WWuzhKzYrWyM2EeasS7NGlXHwfaz5EJcBBu2RJOY7Uxjb3fKhn6beeoziDt8AW2DOlyd5v2apz4jjsMXtDSo44wdejIO7yM+OZ3Tp09z+vRpMs4W0MTLw7ZLJMSfXeFpYvfHk5x+mtMZGZzNysVcrQ5uzvaAkfjwWWwz9aJjA9vvMA8U4wXiIjawJTqRbOfGeLs7YweYdcfYvuF7fj6pp1ZTT2peDwo37VkABs4d3MqGiL0c1znTuKk7TrdullbbLMmKZevGCPalGanbrBHVLfzJKuWVqiltGSG9x/FNSh66wwsY4T+Nnfmg2/YCAaErOFWgY9/sYIYvSMQImLN289GYgfQNGMW8A6XljaTw+eiBTN9TgNYunSVP9GFq5JVb+rLcponUpY/jP3E96Vd+ZUvYAJ74Mg2TbWdJ2rIQeo/7hpQ8HYcXjMB/2s7bjMlM1u6PGDOwLwGj5nF1mvdlnuYsdn80hoF9Axg17wClAOY8ju9Yw+rVq8v+Wf4OY5/8xKYrJMSDwJiynAmjX+Hfq1azetUqVn7+LiEPd+bvq05jooRTkcvZnmKs6GFWbqY0loX0Ztw3KeTpDrNghD/TyoICLwSEsuJUAbp9swkevoDEsqBw656FiZTPRzNw+h4KtHakL3mCPlMjuWW3tNKmKXUpj/tPZH36FX7dEsaAJ74k7dbNEgKHPqoGDB6i+vQLUJVF/t5/q5nL45VBKaWMSWpOr3bq1ZgM9XlQKzVhe6FSSinTmUVqYNsX1R6DUoaY5WrR7uNqcVBT9dy24rJGLn+phjZ8Rm0qUkopozr+fg/V5qWfy9q8ypRluU39SfXPXm3Vy3vLapcem626d35LxZXadJZq779nquXxZX0Yk+aoXu1etT4mg0HFLF+kdh9frIKaPqeuTvO+zNMQo5Yv2q2OLw5STZ/bporVzfJV1Cu9VPB/Ttlsdf4npotq76JJauTgASogeJyasyNTmZRSuYeWqLDRgWpAwBD1xCsr1dGCih1mhbGyPir3kFoSNloFDghQQ554Ra38yy7QvVEa/7bq/NCrat8NH8rC7eNVsx4fqBPGQrX2iUbq0XcXq7CRA9WAYRPVZwcuK6WUMv66Tb33t6EqcPiz6sMNq9W89zZW0Awqgfy96t8zl6uy7dKokub0Uu1ejVFZnwepVhO2q0KllDKdUYsGtlUvlgUFC3vWZfXl0IbqmbLNUhmPv696tHlJ/fzbzdJKm3p18p+9VNuX95btraXH1OzundVbt26WqlJeqVbrOZkPQtujAcyZMRy47EuXJmkcS/XkoXZOANh7+OJjTCJRZ0bjF8oU/3q/vW1ZvTsD2iax8eu9JMRHsjqqhJ7+bdDcWKc0yUqbBkqMWpy1ZcvjWL8ebunHOVFi01nSc/IHhLbXAGYyYw5w2bfLbcbkiF/oFPzr3XS/4X7MU+NH6BR/bu76KmPCp7wTO4j3nmv+h1fljzD+dymzt7rzytpINr3XhK0TZxFZmMbSVxdhnvgNkT8sZ3ztvWw5WFCh46woFten2ETa0ldZZJ7IN5E/sHx8bfZuOchfc4XuFyMXz5yjpIYb1e0Aijh4sIi/L9/Cimf0fDLpE+KNV9g8YxKRLd5n3Zq3afHje7y/Mb6iB15xqvVk8gehlG2XmcQcuIxvlyYkHUvF86F2OAHYe+DrYyQpUYfZ4p5Vne4D2pK08Wv2JsQTuTqKkp7+tPntZmm1TUOJEa2ztuz2rmN96rmlc/zWzbJi3/17J4bUtYSNXU6jD9cwqnY8PxZrcXYu/x5g54ST1kBhobJ8sENjegY3Y134R7y/vZgzZj+m+bpi1iWwKzqFfOVAvbYKvaU2i5sTOMiRp2Z/TMdJbcj6biVHzTUxlOpI+CmalHyFQ71ODOvhZYtZkro2jLHLG/HhmlHAdstjuo/zLMpO4Ke48mM7DaaHl8Zy3wBc5vt562g4eVfZCV+BHDu+yldztvHDVwv5OTeD0rwsfi2qQRPPYsL/8y++uDKUwCmf0bdapfwuec9ZXB+9HQ838aQ4/D/864srDA2cwmd9q1XO50J/YuaMrxnntwsXQBny0dd4hLc/G0tDe0A50GHYU7SvroEhw+j64jJis49z8r/1GPS6L1U1Dgx7eiBvx1b0LCoBQyprw8ayvNGHrBlVm/gfi9E6O5efr3Y4OWkxFBZiebd0oHHPYJqtC+ej97dTfMaM3zRfXM06EnZd3dfb4qi31GYxzQMH4fjUbD7uOIk2Wd+x8qiZmoZSdAk/EZ2Sj3KoR6fBPSrvZyf/4DxGPh6O55zNLBzWAHs7N9xcC8jNNZdVULnk6l2p5Wb58sl0fBFhXzZm7s4trFm/g61jM5n+5ibyclM4EB1FVFQ0R8+7WmnTmS5vfcey4SZio09QIySEbh4euDvmknIgmqioKKKPnrPFLDk4bySPh3syZ/NChjWwh0owzzr6G449d/tnPeaLm1kZ25UxQ9xssB5/hJlLG/9B3/HfU1i/DV17tKW+BrCrzcgle/gypBZpm2YyqH1fZscUVvBYK4KV9VH21B65hD1fhlArbRMzB7Wn7+wY/oordC/Zez3Nsl9iiT0YyaxujrgFTOTpdi5l/9HOEedqVcsrOuFUpYRig54igzMuLmW/orF3rU41Cz+o+UvJP8i8kY8T7jmHzQuH0cDeDjc3Vwpycynb2RS5uXpca7lZ/rGl6TiLwr6k8dydbFmznh1bx5I5/U025d24r5+32qZzl7f4btlwTLHRnKgRQkg3DzzcHclNOUB0VBRR0Uc5Z6zgLDXWmDNXMXFSFIPDNzLFt+yWJRpf/Dpk8XV0NlOb1KU0PoojHt0Iq2G5DWUoohhHHMtPRAcHe0qKDdh5j+aN+SPKa+WzyWKbJtJ2bOFit9eYM64K2evH8nGHQLpX9ab2G/MZYbnL3ztLMldNZFLUYMI3TuHqNCvDPHu2HsGw+Xc3y+K9u4hvNQg/lz+0GDZg5MTeQ7iHfMfEx5phjPuFaXlmlOksMdt/penol/n48TCe/1cAw787yts9e1T0gO8zK+uDibMx2/m16Whe/vhxwp7/FwHDv+PojJ70qOA7Dw8k+9oMfX8Wa/qEMXfYLt7qCCgD6SeTMdIB+4upnLbzZIR7YwrcM0lLL4UmVcg+dIQ0c6eKHn3FMWeyauIkogaHs3GKb9mtWTT4+nUg6+tosqc2oW5pPFFHPOhmfbOkqBgcr2+W2JcUY7DzZvQN+3r+JsttmtJ2sOViN16bM44q2esZ+3EHArtXxbv2G9y4XVbCoGri1MpP2ZxxiaTQXoQDoMV/9k7+OeNlvh0zhKAIL/JP5DBg/iZaOxiJmzeaCaszyEk9R8EpP+Le682s7S/wUvsQJgRn0cO7mIT9Op5dGMxvl7s6wRbbtOOSOZE5IQGs6uBK+smqvLhyFLVtOs1TrPx0MxmXkgjtVTZLtP7ExsyzPCYVx7wRE1idkUPquQJO+cXxXu9Z7Jj393s+T2PcPEZPWE1GTirnCk7hF/cevWft4JNgJ7JSz+DUqDHOtlyb/4mGtv17c2naeJ5LbkBhlRb4tU9mzcKfePTX/xD2RTu6eRpJOmjHyIXtKnqwFcDK+izYRttWmwgL+4J23TwxJh3EbuRC2klAvWfs643inzPX0PeFeQTvfAmzcqZKwqc8N9EFY8JOjM+tYEDVZrR59iGGhI1ifC93iouN1K+09xXvPdOplXy6OYNLSaFc3y5nE/PPGbz87RiGBEXglX+CnAHz2dTawcqetZ0XXmpPyIRgsnp4U5ywH92zCwm+KQZXD7bcpt0lM4lzQghY1QHX9JNUfXElo27dLLELHPqoKjWWUlpSyp6dP96P9fljDNmkpuTg5OVDwzs+GzORn5lM+mUN9Vs0w137+9o0XTlLcqaRus2bUut+bzJ/lXnamP78SdIL3GjW3APNlTMk66ri3bQmRWdPcjpXQ/0WzfFwunM7DyrL61MbhytnOXk6F039FjT/Ky9QhTFx5Wwymeb6+HjVxAEz5w99z+HiZjRt3AjP9Ld5eI4Px3+cUtEDrYQMZKemkOPkhU/DO/8ewJSfSXL6ZTT1W9DM+mZpuU3TFc4mZ2Ks25ymVjbLP19QFUKIB56Z89+9yLA3j9GsawMuxGXQ/v1NfBIkL1ep7Crh7V8hhPirs6feo/9m/4ALnM4sorpnE9zlBsKfggRVIYSopOyr1sW7ZUWPQvwef+FH30IIIYRtSVAVQgghbESCqhBCCGEjlfqZqj4jjqPFXjzsUwt79GQcjiU933ztv9s51KHPI74VOEIhhBD3jT6DuKPFeD3sQy17oDCD2MPpFJSHBTsHd1r3bIvHTZeLhswDbPsxnkvaZvQa0o9WNe0pS4cZybb9WWhb9Seolxe3vMPGrONY5Db2Z2lp1T+IXl4uty+nsl6pSqoxIYQQ11hOfVly7DPGjZvNV1fjwvoYMm56s6oxcSHB/V4i4mwhutiPGdbnFXYXWE8lep2ObS8EELriFAW6fcwOHs6CRONtystUzitVYzoZVcezZHoeQVdfIm1fj8AZ/yEQgAKiXw3k3MzXK26MQggh7hMj6RlVGb9kOnlB1zMLqNxcDO3HsWjx01S9sXp+FJ+8ncQjH/0D7xwH/D/8iukjm+NgDEDv9zd2JP7Kqc920fWDw7w7yAXzSCcCA78gZtJCOu37hLeTHuFfjx7hs11d+eDwuwxyMTPSKZDAL2KY+PpJi+WTFvZBQ2W9Uv2TpBoTQghxP2gspr40XcnDkB/PZzOn8HzYu3x16FLZi/Ad3PBq1QhXB3tq9ZnCzJGNuXImiQMbl7GbPvRtmmI1laiDmxetGrlSmnSMVM+HKKtij4evD8akRC4ct1yuK78FXTmD6m1dTTU2ucJTjQkhhKg4qlZrHm7ggnvfJwh5uJAvnxjNghMmcGnHYxOCaHk1BhtTWDdrClPfj6JGYCBtqumtpth0afcYE4Jagl5PsdaZ61Wc0BoKKSy0Ul6eb65y3v69jaupxsb/X0WnGhNCCFGRqg54m7UDrv5bF4p2t+LTPTlMbV33t1eMjm2ZsPwnJpjSWTS4HzO2LKXe1XSYNe2vpcP0uSHFpp2bG64FuVyvkove1YdadSyXXz30T3elWpZqrHclSDUmhBCi4pg4veFNpq88WfYDI/N5UjPs8Kh340v1jRz75FGGfBiLAcDOBRenEgpLffDrkEVMdDZmwFCeDrP7DRlrNL5+dMiKITrbDBiIjzqCR7fuuFspv3popbxS/XOkGhNCCHFfGOOYN/rW1Jc/jPcibdQwem/sQN2L/+VUgxmsHlIV84XFDGkfxbNpq3js0dF4jHyK3gc64llwjP+a/8HS4Eb4dbaUDtPMhcVDaB/1LOdXjWDGy98yZkgQEV75nMgZwPxNrXGo7mO5vHyokqVGCCHEn5cxlzPJZyio5oWPZw0s/r7VXMC55DR0VerTopk71xK+3UWKTUN2Kik5Tnj5NOTGKtbKJagKIYQQNvKne6YqhBBCVFYSVIUQQggbkaAqhBBC2IgEVSGEEMJGJKgKIYQQNiJBVQghhLARCapCCCGEjUhQFUIIIWxEgqoQQghhIxJUhRBCCBuRoCqEEELYiARVIYQQwkYkqAohhBA2IkFVCCGEsBEJqkIIIYSNSFAVQgghbESCqhBCCGEjElSFEEIIG5GgKoQQQtiIBFUhhBDCRiSoCiGEEDYiQVUIIYSwEQmqQgghhI1IUBVCCCFsRIKqEEIIYSMSVIUQQggbkaAqhBBC2IgEVSGEEMJGJKgKIYQQNiJBVQghhLARCapCCCGEjUhQFUIIIWxEgqoQQghhIxJUhRBCCBuRoCqEEELYiARVIYQQwkYkqAohhBA2IkFVCCGEsBEJqkIIIYSNSFAVQgghbESCqhBCCGEjElSFEEIIG5GgKoQQQtiIBFUhhBDCRiSoCiGEEDYiQVUIIYSwEQmqQgghhI1IUBVCCCFsRIKqEEIIYSMSVIUQQggbkaAqhBBC2IgEVSGEEMJGJKgKIYQQNiJBVQghhLARCapCCCGEjUhQFUIIIWzEsaIHIIQQohIx55J88L+cK4ZqTTrTpUk1i9WM8eG8e+Qh3hrXGc19HZ+OY5Hb2J+lpVX/IHp5uZSVG85x8Icfic/R0LTnEPq3rmnlqlFPRtxRir0exqdWeQ3jBeJ+2M6h84549hzCwDZuFo41ciHuB7YfOo+jZ0+GDGyDmz2AGd2xSLbtz0Lbqr9cqQohhLiBOYtDm1fy6bQQ/vHlKYxWqpWcimT59hSr//12CjZP5rkVFzH/7iN1bHshgNAVpyjQ7WN28HAWJBrBlMLnowcyfU8BWrt0ljzRh6mRV2452py1m4/GDKRvwCjmHSgtKzSlsSykN+O+SSFPd5gFI/yZtjP/piNNpC0Lofe4b0jJ03F4wQj8p+0sG9G2FwgIXcGpAh37ZgfLlaoQQogbOLbmqX9+Rgu7WJ4HwEx2zGe880kEyXlV8Oz3Iu+9NoCaNxxiTF3PB1+VEDLzCerE3lq3gfEwS98/RJUav7AhownesZvZUUXP3AbzeLVDEp+98wkRyXlU8ezHi++9xoAGlq/3zOfX89murnxw+F0GuZgZ6RRI4BcxTHo3la1HOhG2dgrDnUw8fD6CUZEJlAzq9ZuraGN6BlXHL2F6XhCxVwuLsihq/Trhs0JprzERrPwJiUyE/t1vOLKIrKLWvB4+i9D2GkzBCv+QSDC3Zf1nu+j6wWHeHeSCeaSTXKkKIYS4DeN/WTp7K+6vrCVy03s02TqRWZHF1/6zOr+VqWP/D/OAQFo7WKlr/pWfl3/I2uJn+Ocrf6df22p4Dw4jtEc1/rt0NlvdX2Ft5Cbea7KVibMiKbYylNKkY6R6PkQ7JwB7PHx9MCYloqvanQFtk9j49V4S4iNZHVVCT/82t9yW1viFMsW/Hg43FlbryeQPQmmvAcyZxBy4jG+XFjcdWY2ekz8gtKwSmTEHuOzbBUqTOJbqyUNlA8Lew1euVIUQQtyGY0de/WoO2374ioU/55JRmkfWr3qoCeiP8OGYn1EzNzHrETfAzXJd7LCjNUOf60+bukZyXBzQuNbFvZqWuq9+xZxtP/DVwp/JzSglL+tX9ICTpbHo9RRrnXEuvxy0c3JCayik0L4xPYObsS78I97fXswZsx/TfKuhS/iJ6JR8lEM9Og3ugdftHv4aUlkbNpbljT5kzajamHUJ7IpOIV85UK/TYHp4aQADqWvDGLu8ER+uGQVsR1+sxfn6gORKVQghhGV2gPnSRv7RdzzfF9anTdcetK2vARSgKIz5gX36yyQdv0jpbeuCnUM1XKvfFHLMl9j4j76M/76Q+m260qNtfa4fYWE8bm64FuSSW/4wVuXmonethevJRYR92Zi5O7ewZv0Oto7NZPqbm/g15QDRUVFERR/l3O0e/uYfZN7Ixwn3nMPmhcNoYA8qN4UD0VFERUVz9JwRyOfgvJE8Hu7JnM0LGdbAHuzccHMtIPf6gORKVQghBGA+z4oxQex9YidLhjuQmanDtVMtzCdWcsg9hO8mPkYzYxy/TMvDrADsqBrwBlv+7czUgOd4t8dO3lF7rdS9iR0oBRhPsPeQOyHfTeSxZkbifplGnlkBJnTpSeS5taZJzeuBWOPrR4esr4nOnkqTuqXERx3Bo1sYrobVFOOIo11ZPQcHe0qKS/Ea8QbzR9xp3pmsmjiJqMHhbJzie+0K2cF7BG9cO9hM5qpnmBQ1mPCNU/C9Wknji1+HLL6OzmZqk7qUxkdJUBVCCAHYezB4TA8WTO3LoM8dSM0awtyPGuPk1J/el6Yx/rlkGhRWoYVfe5LXLGD7yLKrMzuPYcz7dC/9J03Db/1Qel969Za62/p0uqEjBxo3b0TSssm86fEm/XtfYtr450huUEiVFn60T17Dgm0P47moP5F/O8v6J12uH1o9mBkvf8uYIUFEeOVzImcA8ze1Rlvz77zUPoQJwVn08C4mYb+OZxcGU+M3EzQSN280E1ZnkJN6joJTfsS915s3/1OXTzdncCkplF7hZTW1/rOJmTfk+qGmU6z8dDMZl5IIvV6J2Jh5BM94mW/HDCEowov8EznYBQ59VJUaSyktKWXPzh9t/ncSQgjx52G8fJqT5xQNfJridvWyS3+ek+kFuDVrjofmCmeSdVT1bkptS88o76puAedOZqLqNaNhjRLOn0ynwK0ZzT00XDmTjK6qN9UiXuFfjebyz37aW7owZKeSkuOEl09Dql27kDWRn5lM+mUN9Vs0w/3Ww+4dQzapKTk4eflIUBVCCFHZmMg8dBhjx4fx+pPdT/2TDVcIIcSDz4GGXR+u6EH8T+TXv0IIIYSNSFAVQgghbESCqhBCCGEjElSFEEIIG5GgKoQQ91LhaQ4fSuN63hM9Z+L2cvRciY06MBIf/hbLDt99e4asw2z9ZimLP1/G2p0nuGyyTfvG+HBmrYi/KXNNIacPHyLt5sQvlZqJ3LTDHIjP4IrFVDq3zkl/Jo69R89JUBVCiHvJmLKcCc8uJrEEoJhj//cUw16JorCqrbKQlnAqcjnbU+4uCZt+3zv07/8KEelFqNKLxHz8KN3GribTah62u2/frlZzOjSrhd2NhcYUJ+pcnQAACCRJREFUlk94lsWJtvoSca+ZyAh/iqBp69i5+iWCnl7B/7d3/1Ex53scx58z05aUqzhFcYspOXRI7i5arnN36YeWWNmS0La6pNxlrWF1l7Bn/Vjp6pac8usa+VVhqZVws6HrHuTu9SOpiWqupPyoaIyame4f4dJtWu5ZP/bu5/HfzPmcM+/ve86Z93zm+53vq7xlb1ock/bieoL95pFbbyGGqiAIwqvxkCubpzI1zY2E9IV4dATQULAriuAx3vh8GMaqbDV6oOF0ElHrd7HhU3+8PMcRkXSGWsBQnUdC5ARGeXrhN20lRytafNobqslLiGTCKE+8/Kax8mhFi8xSHQWZB3j40Vriv/wDM2d9QdzedFZ7dUXXBGgK2BUVzBhvHz4MW0W2uuUWVkdJ+lKid1ym9uxG5gT44un1AUGKFC7Ug77qMqcLq2lz49tWrfoSUqOXk/n4uPSl7Fm6gswbOqrzEoicMApPLz+mrTxKhQF0P2wmesM+UuYH4jPSl9CYXKoNxvqg44fN0WzYl8L8QB9G+oYSk1vdSmGNXMy7zpC5y4iaH4hj2QXUbXyfeHhlM1OnpuGWkM5Cj45iqAqCILx8esp3hhG8tRexaYsZZt380as9sZjgWC2hG77j25hh5H82g+RSPbqyYyQvU3L/k81kbp2MZs1MYv+h/dGYNN0/N7Is04Z5qdns+6oHmeHRZD+To2aC89DB3FPOYU7sLo5dvInWoj9jp/4OR5mWE4uDidWGsuG7b4kZls9nM5IpfTIhm6jMnEPIegMjvczYqIjHEL6d7KwtTO98kozT99Fdy2FnzrXnCi5vtdbGbnSp2cHq7Sr0gL54F2sz7tPV+kKrkXK68uNs+nobNUFJ7E8JoWFdNMpSvZE+6Cg/vomvt9UQlLSflJAG1kUrW6nMlAG/sSYv/nOCArdhN382Q4z8qKAv30lY8FZ6xaaxeJg1UsQ5VUEQhJeuSb2N6KRitPfucK/x8bM6LmXn8MBFTu2pDLLOmyO3yedIngYAmdtYJg/oiKm9L37v3OBsfj3uCiUrBhaijEsks6yRuormmLTHTNwVKFcMpFAZR2JmGY11FfxL82wtVr4J5O4Ow/7afpaHDEEuH8q09Wep1V0iO+cBLvJaTmVkcd5cjk3+ER6Vg+bcciYtbyJqRzS/7dSRHg5ajq1bQ/KBq8hnJRL1nuUL9aT1WtsxNNQfTdp2Lur0XNlzEMn4ybi3a46fG1ioJC4xk7LGOioeHZjUbQxT3K0ws3GnX/daqqv1bfRBituYKbhbmWHj3o/utS13qgbu5sXwqfIGdeeKGZa8nxgfNXELlBS13H43qdkWnUSx9h53/vOmiqEqCILwsklsA0nMyWV3aCmKSTHkawAMPNA8QF9XQYlKhUpVTufRCwhylQISTCw6YAGAFLN2JjRoKklvMybNwO29v+e96Qeot+vLOx6u2LWaoyajy6BgouJ3ciT/GkUZk6haPpvEgvtoHuipqyhBpVKhKu/M6AVBuEqBpnrysk6huVtIQVUjSDvjv+F7NgV04uq+KLzd3mNZXv0LdMR4rSb9phBkmcH2v19gz+H2TJjkgsRopJwEqYkpbz3qk0TSRFNbfZBIMTFtXo1UgqSpRXN051gzN4tBm05waFEHti5Oo/Cokn21NtjLnl2KxJbAxBxyd4dSqphETP6jIf8CXRAEQRD+F2bmmEvNcI3cSmyfHUwJT6XcYIJzX2ckVm8zXaFAoYjA08UBuaM50MTDa1co0gGGKkrKpDj8upK/nbEhYGE44709sK5RP4pJe0zH5ZNnsAlYSPh4bzysa1DXGZ6dqYabpM8ajeLgnSfnWi179MHRUo9e6kxfZwlWb09HoVCgiPDExUGOozkgscDzjxkcWCVnW9hS8mrV5B0qpftHc/lGeYSsCBl79p9/gYa0UatMTlCwI3/9ZgmHOwcQ4CBDd7k5Um5h+Hi8PaypURuJlHvePhijr6aqvgsOdu1wmBLHApMVvB+hwm+2F/+9D28OJzdzjWRrbB92TAkntdwg7v0rCILwykht8Y1NoXjsOCYu7UaWIoqPU2cyemI2fQ0FnNP6s9GneadqajhLXNhMLHQXyGmcxl9GDaT6SGsxacPpD4ApriOGc/vz6YQV2VP/Vi/edSti99qDeC/xxVEGSG0YEeDBlohhDE8agNOvGrh+qQiJ/2oW9e2GRdTHpM4czcTsvhgKzqH134jPk62XBFu/GP58cgQR85z5QpfI7OT+DHbQUXhagn9cf6gwctyGUpQhgzhs3vzQxCmUuJDh3J5vpFb/yfRZFIl+7Ra6SgFXI/FzQa1dsmysD4dodfnTzIYzLfhPTP8ggO/7mVFe0oXBvavZPjuUO598yYqJLq29qdj6xpJSPJZxE5eKlBpBEITXS0uVSsUtmT1OPTthBmjSguidPo7LG99FrTZg19sRKxmAptWYtJ5P5appKq9w7b41Ts62mNaWU3THAnnPzjxzrY2hnptXr1L5wAzbnk7YWT7126a2CpXqFjJ7J3p2ais/TU+t+gqlNabY9XLGtl0bS40wVqtJWTLj/C8w53g877d/svj54+eetw9GaKtUlNxtj6OzPZayBu6WXuVuBzlyYy/2FDFUBUEQ3jCatCBc0sZRlBpI+x9f/n+kgVPxM4jeUkivJXuJ97P72Z2jFENVEAThDWO4dx1VnSXO3X55/3usr7zKLdPuOHb6qW6O8WqJc6qCIAhvGGmHbrh0eN1VvB4WXeWPrnr+efqlfQkSBEEQhJdGDFVBEARB+In8G+VMCK+bR262AAAAAElFTkSuQmCC\" data-filename=\"Screenshot from 2018-12-08 17-08-21.png\" style=\"width: 469px;\"></p><p><br></p><p>APAKAH INI?&nbsp; &nbsp;&nbsp;</p><p><br></p>', '2018-12-23 04:47:48'),
(44, 11, '<p><br></p>', '2018-12-23 04:47:48'),
(45, 2, '<p>TES</p>', '2018-12-23 04:48:34'),
(46, 2, '<p><br></p>', '2018-12-23 04:48:35'),
(47, 2, '<p>SATU DATA&nbsp;&nbsp;&nbsp;&nbsp;</p>', '2018-12-23 04:50:26'),
(48, 2, '<p><br></p>', '2018-12-23 04:50:26'),
(49, 2, '<p>TES</p>', '2018-12-23 04:51:21'),
(50, 2, '<p><br></p>', '2018-12-23 04:51:21'),
(51, 2, '<p>TES</p>', '2018-12-23 04:51:47'),
(52, 2, '<p><br></p>', '2018-12-23 04:51:47'),
(53, 2, '<p>TES?</p>', '2018-12-23 05:23:38'),
(54, 2, '<p><br></p>', '2018-12-23 05:23:39'),
(55, 2, '<p>TES</p>', '2018-12-23 05:24:45'),
(56, 2, '<p><br></p>', '2018-12-23 05:24:45'),
(57, 2, '<p><br></p>', '2018-12-23 05:24:45'),
(58, 2, '<p><br></p>', '2018-12-23 05:24:45'),
(59, 2, '<p>TE</p>', '2018-12-23 05:26:39'),
(60, 2, '<p><br></p>', '2018-12-23 05:26:39'),
(61, 2, '<p>OKAY</p>', '2018-12-23 05:29:01'),
(62, 2, '<p><br></p>', '2018-12-23 05:29:01'),
(63, 2, '<p>AAS</p>', '2018-12-23 05:30:45'),
(64, 2, '<p>HAHAH&nbsp; &nbsp;&nbsp;<br></p>', '2018-12-23 05:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `t_soal_user`
--

CREATE TABLE `t_soal_user` (
  `id` int(11) NOT NULL,
  `id_modul` int(11) NOT NULL,
  `soal_id` varchar(40) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  `benar` int(11) NOT NULL DEFAULT '0',
  `dijawab` int(11) NOT NULL DEFAULT '0',
  `status_pengerjaan` int(11) NOT NULL DEFAULT '0',
  `is_click` int(11) NOT NULL DEFAULT '0',
  `stoped_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_soal_user`
--

INSERT INTO `t_soal_user` (`id`, `id_modul`, `soal_id`, `id_soal`, `created`, `id_user`, `benar`, `dijawab`, `status_pengerjaan`, `is_click`, `stoped_at`) VALUES
(1, 2, '1544279293', 21, '2018-12-08 14:28:13', 30, 1, 1, 2, 1, NULL),
(2, 2, '1544279293', 22, '2018-12-08 14:28:13', 30, 0, 1, 2, 1, NULL),
(3, 2, '53397', 22, '2018-12-08 14:29:56', 31, 1, 1, 2, 1, NULL),
(4, 2, '53397', 23, '2018-12-08 14:29:56', 31, 0, 1, 2, 1, NULL),
(5, 1, '81597', 19, '2018-12-08 15:35:35', 30, 1, 1, 2, 0, NULL),
(6, 1, '81597', 18, '2018-12-08 15:35:35', 30, 1, 1, 2, 0, NULL),
(7, 1, '81597', 20, '2018-12-08 15:35:36', 30, 1, 1, 2, 0, NULL),
(8, 3, '53545', 24, '2018-12-19 12:45:05', 30, 1, 1, 2, 0, '2018-12-19 12:45:25'),
(9, 3, '53545', 25, '2018-12-19 12:45:05', 30, 1, 1, 2, 0, '2018-12-19 12:45:25'),
(10, 8, '58873', 26, '2018-12-19 16:04:38', 30, 0, 0, 2, 0, '2018-12-19 16:14:41'),
(11, 9, '72348', 28, '2018-12-20 12:44:12', 30, 1, 1, 2, 1, '2018-12-20 12:45:13'),
(12, 9, '72348', 27, '2018-12-20 12:44:12', 30, 1, 0, 2, 1, '2018-12-20 12:45:13'),
(13, 10, '51854', 30, '2018-12-20 15:50:10', 30, 1, 1, 2, 0, '2018-12-20 15:54:19'),
(14, 10, '51854', 29, '2018-12-20 15:50:11', 30, 1, 1, 2, 0, '2018-12-20 15:54:19'),
(15, 10, '32784', 30, '2018-12-20 15:50:12', 30, 1, 1, 2, 0, '2018-12-20 15:54:19'),
(16, 10, '32784', 29, '2018-12-20 15:50:12', 30, 1, 1, 2, 0, '2018-12-20 15:54:19'),
(17, 10, '24453', 30, '2018-12-20 15:54:00', 30, 1, 1, 2, 0, '2018-12-20 15:54:19'),
(18, 10, '24453', 29, '2018-12-20 15:54:01', 30, 1, 1, 2, 0, '2018-12-20 15:54:19'),
(19, 11, '90962', 31, '2018-12-20 16:06:45', 30, 1, 1, 2, 0, '2018-12-20 16:07:50'),
(20, 11, '90962', 32, '2018-12-20 16:06:45', 30, 1, 1, 2, 0, '2018-12-20 16:07:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_absen`
--
ALTER TABLE `tb_absen`
  ADD PRIMARY KEY (`id_absen`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD KEY `tb_admin_ibfk_1` (`id_user`);

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  ADD PRIMARY KEY (`id_materi`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_jurusan` (`jurusan_id`),
  ADD KEY `id_kelas` (`kelas_id`);

--
-- Indexes for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_siswa` (`id_siswa`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_jurusan` (`id_jurusan`),
  ADD KEY `tb_nilai_ibfk_2` (`kode_soal`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `guru_id` (`guru_id`),
  ADD KEY `jurusan_id` (`jurusan_id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `mapel_id` (`mapel_id`);

--
-- Indexes for table `tb_ulangan_pil`
--
ALTER TABLE `tb_ulangan_pil`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_kelas` (`kelas_id`),
  ADD KEY `id_mapel` (`mapel_id`),
  ADD KEY `id_jurusan` (`jurusan_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  ADD PRIMARY KEY (`id_uts`),
  ADD KEY `guru_id` (`guru_id`),
  ADD KEY `jurusan_id` (`jurusan_id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `mapel_id` (`mapel_id`);

--
-- Indexes for table `tb_uts_pil`
--
ALTER TABLE `tb_uts_pil`
  ADD PRIMARY KEY (`id_ulangan`),
  ADD KEY `id_guru` (`guru_id`),
  ADD KEY `id_kelas` (`kelas_id`),
  ADD KEY `id_mapel` (`mapel_id`),
  ADD KEY `id_jurusan` (`jurusan_id`);

--
-- Indexes for table `t_jawaban`
--
ALTER TABLE `t_jawaban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jawaban_user`
--
ALTER TABLE `t_jawaban_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_livestream`
--
ALTER TABLE `t_livestream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_modul_soal`
--
ALTER TABLE `t_modul_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_soal_ganda`
--
ALTER TABLE `t_soal_ganda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_soal_user`
--
ALTER TABLE `t_soal_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_absen`
--
ALTER TABLE `tb_absen`
  MODIFY `id_absen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_guru`
--
ALTER TABLE `tb_guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  MODIFY `id_materi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_ulangan_pil`
--
ALTER TABLE `tb_ulangan_pil`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  MODIFY `id_uts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tb_uts_pil`
--
ALTER TABLE `tb_uts_pil`
  MODIFY `id_ulangan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_jawaban`
--
ALTER TABLE `t_jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `t_jawaban_user`
--
ALTER TABLE `t_jawaban_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `t_livestream`
--
ALTER TABLE `t_livestream`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_modul_soal`
--
ALTER TABLE `t_modul_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_soal_ganda`
--
ALTER TABLE `t_soal_ganda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `t_soal_user`
--
ALTER TABLE `t_soal_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_absen`
--
ALTER TABLE `tb_absen`
  ADD CONSTRAINT `tb_absen_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `tb_siswa` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD CONSTRAINT `tb_admin_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD CONSTRAINT `tb_guru_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_materi_tugas`
--
ALTER TABLE `tb_materi_tugas`
  ADD CONSTRAINT `tb_materi_tugas_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_materi_tugas_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_materi_tugas_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD CONSTRAINT `tb_nilai_ibfk_3` FOREIGN KEY (`id_siswa`) REFERENCES `tb_siswa` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_4` FOREIGN KEY (`id_kelas`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_nilai_ibfk_5` FOREIGN KEY (`id_jurusan`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD CONSTRAINT `tb_siswa_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_ulangan_eesay`
--
ALTER TABLE `tb_ulangan_eesay`
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_ulangan_eesay_ibfk_4` FOREIGN KEY (`mapel_id`) REFERENCES `tb_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_uts_eesay`
--
ALTER TABLE `tb_uts_eesay`
  ADD CONSTRAINT `tb_uts_eesay_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `tb_guru` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_2` FOREIGN KEY (`jurusan_id`) REFERENCES `tb_jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_3` FOREIGN KEY (`kelas_id`) REFERENCES `tb_kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_uts_eesay_ibfk_4` FOREIGN KEY (`mapel_id`) REFERENCES `tb_mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
