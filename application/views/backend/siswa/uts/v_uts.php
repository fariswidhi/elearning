<?php if ($this->session->flashdata('selesai')): ?>
  <div class="alert alert-success">
    <?php echo $this->session->flashdata('selesai'); ?>
  </div>
<?php endif ?>
<div class="row clearfix">
  <?php if ($soal->num_rows() >0 ): ?>
    <?php foreach ($soal->result() as $s): ?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    <div class="card">
      <div class="header">
        <?php echo $s->essai==1 ? '<label class="badge bg-teal">Essai</label>':'<label class="badge bg-purple">Pilihan Ganda</label>'  ?>
          <h2>
              <?= $s->nama_modul; ?>
          </h2>


      </div>
      <div class="body">
        <table class="table table-striped">

          <tr>
            <td>Waktu(menit)</td><td><?php echo $s->waktu ?></td>
          </tr>
          <tr>
            <td>Min Nilai</td><td><?php echo $s->min_nilai ?></td>
          </tr>
          <tr>
            <td>Max Nilai</td><td><?php echo $s->max_nilai ?></td>
          </tr>
        </table>
<!-- Button trigger modal -->
<!-- <?php print_r($s) ?> -->

<?php 
$userid = $this->session->userdata('id');
$d = $this->db->get_where('t_soal_user',['id_modul'=>$s->id,'id_user'=>$userid]);

$num = $d->num_rows();
// print_r($d->num_rows());
 ?>

 <?php if ($num==0): ?>
<button type="button" class="btn btn-primary form-control btn-modal" data-toggle="modal" data-target="#exampleModal" data-source="<?php echo base_url().'siswa/Uts/detail/'.$s->id ?>">
  Mulai
</button>

<?php
 else: 
  ?>   
  <div class="alert alert-danger"> 
    <center>    Soal Sudah Dikerjakan</center>
  </div>
 <?php endif ?>


      </div>
    </div>
  </div>
    <?php endforeach ?>
  <?php endif ?>

</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Info Ujian Pilihan Ganda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary continue">Lanjutkan</button>
      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#uts').addClass('active');
  });

  $(document).on('click','.btn-modal',function(){
    var ds = $(this).attr('data-source');

    $.get(ds,function(res){
      console.log(res);
      $(".modal-body").html(res);
    });
  })

  $(document).on('click','.continue',function(){
    var password = $("#pss").val();

    // alert("OK");
    // console.log(password);
    $.ajax({
      url: "<?php echo base_url() ?>siswa/Uts/check_pass",
      method: "POST",
      data:{
        password: password,
        id: $("#id").val()
      },
      dataType:"JSON",
      success:function(res){
        if (res.status==0) {
          alert("Password Salah, Silahkan Coba Lagi");
        }
        else{
          alert("Berhasil");
          location.href="<?php echo base_url().'siswa/ujian' ?>"
        }
      }
    })
  });
</script>


