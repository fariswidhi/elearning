<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller
{


  protected $table="tb_mapel";
  protected $folder = "backend/admin/mapel/";
  protected $primaryKey= "id";
  protected $base = "Mapel";

  function __construct()
  {
    parent::__construct();
  }


  public function forms(){



  $forms  = [

      1=>[
        [

        'name'=>'Nama Mata Pelajaran',
        'showAsTable'=>true,
        'list'=>[
          'type'=>'text',
          'field'=>'nama_mapel'
        ]

        ],
      

       
        
      ],

    ];

    // print_r($forms);
    return $forms;

    // echo json_encode($forms);


  }
  public function index()
  {
    $this->model_security->getsecurity();
    $data    = $this->model_all->get_all($this->table)->result();
    $judul   = "Guru";
    $content = $this->folder."table";
    $forms = $this->forms();

    $this->load->view("backend/admin/home", compact('data','judul','content','forms'));
  }
  
  public function edit($id){

    if ($this->input->post()) {
      # code...

$nama_mapel = $this->input->post("nama_mapel");

$datane = array(
"nama_mapel" => $nama_mapel,
);
$where = array("id_mapel"=>$id);
$this->db->update("tb_mapel",$datane,$where);



      alert_success('Mapel Berhasil Diubah');
      return redirect('admin/'.$this->base);
    }
    else{
    $judul   = "Update Data".$this->base;
    $content = $this->folder."edit";
    $forms = $this->forms();


    $data  = $this->model_all->get_where($this->table,["id_mapel"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Edit Data ";
      $content = $this->folder.'edit';
    $this->load->view("backend/admin/home", compact('content','judul','forms','data'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }

    }

    // print_r($data);
  }


  public function detail($id){

    $forms = $this->forms();


    $data  = $this->model_all->get_where($this->table,["id_mapel"=>$id]);
    if ($data->num_rows()==1) {
      # code...

      $data = $data->result_object()[0];
      $judul = "Detail Data ";
      $content = $this->folder.'detail';
    $this->load->view("backend/admin/home", compact('content','judul','forms','data'));
    }
    else{
      echo "Data Tidak Ditemukan";
    }



  }
  public function dump_insert(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$this->db->insert("'.$table.'",$datane);';

  }


  public function dump_update(){

    $table = $this->table;
    $forms = $this->forms();
    $text = "";
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $text .= '$'.$x['list']['field'] .' = $this->input->post("'.$x['list']['field'].'");<br>';
      }
    }


    $act2 = '$datane  = array(<br>';
    foreach ($forms as $f) {
      # code...

      // print_r($f);
      foreach ($f as $x) {
        $act2 .= '"'.$x['list']['field'] .'" => $'.$x['list']['field'].',<br>';
      }
    }
    $act2 .= ");";



    echo $text;
    echo "<br>";
    echo $act2;
    echo "<br>";
    echo '$where = array("'.$this->primaryKey.'"=>$id);<br>';
    echo '$this->db->update("'.$table.'",$datane,$where);';

  }
  public function create(){

    if ($this->input->post()) {
      # code... 
      $post = $this->input->post();

      // foreach ($post as $key => $value) {
      //   # code...
      //   echo $key."<br>";
      // }


      // print_r($userid);
$nama_mapel = $this->input->post("nama_mapel");

$datane = array(
"nama_mapel" => $nama_mapel,
);
$this->db->insert("tb_mapel",$datane);


      // $data  =  $this->model_all->
      alert_success('Data Berhasil Ditambahkan');
      return redirect('admin/'.$this->base);


    }
    else{

    $content = $this->folder."create";
      $judul = "Tambah Data ";


  $forms = $this->forms();
   $this->load->view('backend/admin/home',compact('content','judul','forms')) ;
    }
  }

  public function delete($id){

    $where  = array("id_mapel"=>$id);
    $this->model_all->del_id($this->table,$where);

          return redirect('admin/'.$this->base);

  }

}
