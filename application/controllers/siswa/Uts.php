<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uts extends CI_Controller {

	public function index()
	{
		$this->model_security->getsecurity();
    	$judul   = "Uts";
    	$content = "backend/siswa/uts/v_uts";
    	// $isi['data']    = $this->model_all->join2table_group('tb_uts_pilgan');
    	// print_r($this->session->userdata());
    	$userdata =$this->session->userdata();
    	$userid = $userdata['id'];

    	// print_r($userid);
    	$siswa = $this->db->get_where('tb_siswa',['id_user'=>$userid])->result()[0];

    	$jurusan  = $siswa->jurusan;
    	$kelas = $siswa->id_kelas;


    	$soal = $this->db->get_where('t_modul_soal',['jurusan'=>$jurusan,'kelas'=>$kelas,'jenis'=>'UTS']);

    	// print_r($soal->result());
    	// print_r($soal->);
        
    	// foreach ($siswa[0] as $key => $value) {
    	// 	# code...
    	// 	echo $key."<br>";
    	// }


        // $this->session->set_flashdata('selesai', 'OK');

        // if (condition) {
        //     # code...
        // }

        // print_r($this->session->flashdata('selesai'));
    	$this->load->view("backend/siswa/home", compact('content','judul','soal'));

    	// print_r()
	}



	public function detail($id){
        // $data = $this->db->get_where('t_modul_soal',['id'=>$id]);

        $data = $this->db->query("SELECT * FROM t_modul_soal a inner join tb_mapel b on a.id_mapel = b.id_mapel WHERE a.id='".$id."'");
		$this->load->view('backend/siswa/uts/info-soal',compact('data','id'));
	}

    public function check_pass(){
        $pass = $this->input->post('password');
        $id = $this->input->post('id');

        $data = $this->db->get_where('t_modul_soal',['id'=>$id,'password'=>$pass]);

        if ($data->num_rows()>0) {
            # code...

        $datane = $data->result_object()[0];

        $jumlah_soal = $datane->jumlah_soal;
        $query = $this->db->query("SELECT * FROM `t_soal_ganda` WHERE id_modul ='".$id."' order BY rand() limit ".$jumlah_soal);
        // print_r($datane);
        // print_r($query);
        $userid = $this->session->userdata('id');

        // $userid = 
        $soalid = $this->db->query("SELECT FLOOR(RAND() * 99999) AS random_num FROM t_soal_user WHERE 'random_num' NOT IN (SELECT soal_id FROM t_soal_user) LIMIT 1");

        if ($soalid->num_rows() == 0) {
            # code...
            $soalid = time();
        }
        else{

        $soalid = $soalid->result()[0]->random_num;
        }
        // print_r($userdata);
        foreach ($query->result() as $q) {
            # code...
            // print_r($q);
            $id_soal = $q->id;

            // print_r($id_soal);
            // $data = $this->db->get_where('t_jawaban',['id_soal'=>$id_soal])->result();
            $qry = $this->db->query("SELECT * FROM t_jawaban WHERE id_soal='".$id_soal."' ORDER BY RAND()");

            // print_r($qry);


            $insertData  = [
                'id_modul'=>$id,
                'id_soal'=>$id_soal,
                'soal_id'=>$soalid,
                'id_user'=>$userid
            ];

            $this->db->insert('t_soal_user',$insertData);
            $lastId = $this->db->insert_id();
            // echo ;
            // print_r($this->session->userdata());
            foreach ($qry->result_object() as $qd) {
                # code...
                // print_r($qd);
                $uid = $qd->uid;


                $dataInsert = [
                    'id_soal_user'=>$lastId,
                    'id_jawaban'=>$uid,
                    'dipilih'=>0
                ];
                $this->db->insert('t_jawaban_user',$dataInsert);


                // print_r($uid);
            }

            // echo $id_soal."<br>";

        }
        $array = array(
            'modul' => $id,
        );


        


        $this->session->set_userdata( $array );

                $modul = $this->session->userdata('modul');
        $user = $this->session->userdata('id');

        // $this->db->update('t_soal_user',['status_pengerjaan'=>1],['id_modul'=>$modul,'id_user'=>$user]);

        $this->db->query("UPDATE t_soal_user SET status_pengerjaan ='1' WHERE id_modul ='$modul' AND id_user='$user' AND status_pengerjaan <>'2'");
        $status=1;


        }
        else{
            $status=0;
        }
        echo json_encode(['status'=>$status]);
    }



}
