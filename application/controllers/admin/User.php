<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
    	$this->model_security->getsecurity();
		$isi['data']		= $this->model_all->get_all('tb_user')->result();
		$isi['judul'] 	= "User";
		$isi['content'] = "backend/admin/dt_user/v_user";
		$this->load->view("backend/admin/home", $isi);
	}

	public function t_user()
	{
   		$this->model_security->getsecurity();
		$isi['judul']		= "User / Tambah User";
		$isi['content'] = "backend/admin/dt_user/t_user";
		$this->load->view("backend/admin/home", $isi);
	}

	public function insert_user()
	{
    	$this->model_security->getsecurity();
		$this->form_validation->set_rules('username', 'Username', 'required',
			array('required'	=> 'Username Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[100]',
			array(
				'required'		=> 'Password Tidak Boleh Kosong',
				'min_length'	=> 'Minimal Panjang 8 Huruf'
			)
		);
		$this->form_validation->set_rules('level', 'Level', 'required',
			array('required'	=> 'Level Tidak Boleh Kosong')
		);

		if($this->form_validation->run() == FALSE)
		{
			return $this->t_user();
		}
		else
		{
			$data = array(
						'username'	=> $this->input->post('username'),
						'pass1'			=> hash_string($this->input->post('password')),
						'pass2'			=> $this->input->post('password'),
						'level'			=> $this->input->post('level')
			);
			$this->model_all->insert('tb_user',$data);
			alert_success('User Berhasil Ditambahkan');
			return redirect('admin/User');
		}


	}

	public function u_user()
	{
    	$this->model_security->getsecurity();
		$id_user	= $this->uri->segment(4);
		$where		= array('id_user' => $id_user);
		$data			= $this->model_all->get_where('tb_user',$where)->result();
		foreach ($data as $dt) {
			$isi['id_user']	 = $dt->id_user;
			$isi['username'] = $dt->username;
			$isi['password'] = $dt->pass2;
			$isi['level']		 = $dt->level;
		}
		$isi['judul'] 	= "User / Update User";
		$isi['content'] = "backend/admin/dt_user/u_user";
		$this->load->view("backend/admin/home", $isi);
	}

	public function update_user()
	{
    	$this->model_security->getsecurity();
		$this->form_validation->set_rules('username', 'Username', 'required',
			array('required'	=>	'Username Tidak Boleh Kosong')
		);
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]',
			array(
				'required'		=>	'Password Tidak Boleh Kosong',
				'min_length'	=>	'Minimal Panjang 8 Huruf'
			)
		);
		$this->form_validation->set_rules('level', 'Level', 'required',
			array('required'	=>	'Level Tidak Boleh Kosong')
		);

		if ($this->form_validation->run() == FALSE)
		{
			return $this->u_user();
		}
		else
		{
				$id_user	= $this->input->post('id_user');
				$where		= array('id_user'	=> $id_user);
				$data	= array(
						'username'	=> $this->input->post('username'),
						'pass1'			=> hash_string($this->input->post('password')),
						'pass2'			=> $this->input->post('password'),
						'level'			=> $this->input->post('level')
				);

				$this->model_all->update('tb_user', $data, $where);
				alert_success('User Berhasil Diupdate');
				return redirect('admin/User');
		}
	}

	public function delete_user()
	{
    	$this->model_security->getsecurity();
		$id_user = $this->uri->segment(4);
		$where	 = array('id_user' => $id_user);
		$this->model_all->del_id('tb_user', $where);
		alert_danger('User Berhasil Dihapus');
		return redirect('admin/User');
	}
}
