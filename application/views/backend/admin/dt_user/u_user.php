<!-- Input Group -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
          <h2>
              Update User
          </h2>

      </div>
      <div class="body">
        <div class="row clearfix">
          <div class="col-md-6">
            <form action="<?= base_url(); ?>admin/User/update_user" method="post">

              <label for="username">Username</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">person_add</i>
                </span>
                <div class="form-line">
                    <input type="hidden" name="id_user" value="<?= $id_user; ?>">
                    <input type="text" class="form-control date" required name="username" value="<?= $username; ?>" placeholder="Username">
                </div>
                <b class="text-danger"><?= form_error('username'); ?></b>
              </div>
              

              <label for="password">Password</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">lock</i>
                </span>
                <div class="form-line">
                    <input type="password" class="form-control date" required="required" name="password" value="<?= $password; ?>" placeholder="Password">
                </div>
                <b class="text-danger"><?= form_error('password'); ?></b>
              </div>

              <label for="level">Level</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                  <select class="form-control show-tick" name="level" data-live-search="true">
                      <option value="">-- Pilih Level --</option>
                      <option value="1" <?php if($level == 1) { echo "selected";} ?>>Admin</option>
                      <option value="2" <?php if($level == 2) { echo "selected";} ?>>Guru</option>
                      <option value="3" <?php if($level == 3) { echo "selected";} ?>>Siswa</option>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('level'); ?></b>
              </div>

              <div class="input-group">
                  <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                  <button type="submit" name="button" class="btn btn-success waves-effect waves-light pull-right"><span>Update User</span> <i class="material-icons">update</i> </button>
              </div>

            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_user').addClass('active');
  });
</script>
<!-- #END# Input Group -->
