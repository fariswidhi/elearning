<!-- Input Group -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
            Tambah Mata Pelajaran
        </h2>
      </div>
      <div class="body">
        <div class="row clearfix">
          <div class="col-md-6">
            <form action="<?= base_url(); ?>admin/Mapel/insert_mapel" method="POST">

              <label for="mapel">Nama Mapel</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control date" required="required" name="mapel" placeholder="Nama Mapel">
                </div>
                <b class="text-danger"><?= form_error('mapel'); ?></b>
              </div>

              <label for="jurusan">Jurusan</label>
              <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">local_offer</i>
                </span>
                <div class="form-line">
                  <select class="form-control show-tick" required="required" name="jurusan" data-live-search="true">
                      <option value="">-- Pilih Jurusan --</option>
                      <?php
                        foreach ($data as $dt) {
                          echo "<option value='".$dt->id_jurusan."'>".$dt->nama_jurusan."</option>";
                        }
                      ?>
                  </select>
                </div>
                <b class="text-danger"><?= form_error('jurusan'); ?></b>
              </div>

              <div class="input-group">
                <button type="button" onclick="window.history.go(-1)" class="btn btn-danger waves-effect waves-light"><i class="material-icons">arrow_back</i><span> Kembali </span> </button>
                <button type="submit" name="button" class="btn btn-primary waves-effect waves-light pull-right"><span>Tambah Mapel</span> <i class="material-icons">send</i> </button>
              </div>

            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/back_end/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#data_master').addClass('active');
    $('#data_mapel').addClass('active');
  });
</script>
<!-- #END# Input Group -->
