<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->model_security->getsecurity();
    $isi['data']    = $this->model_all->get_all('tb_jurusan')->result();
    $isi['judul']   = "Jurusan";
    $isi['content'] = "backend/admin/dt_jurusan/v_jurusan";
    $this->load->view("backend/admin/home", $isi);
  }

  public function t_jurusan()
  {
    $this->model_security->getsecurity();
    $isi['judul']   = "Jurusan / Tambah Jurusan";
    $isi['content'] = "backend/admin/dt_jurusan/t_jurusan";
    $this->load->view("backend/admin/home", $isi);
  }

  public function insert_jurusan()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
      array('required' => 'Jurusan Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {

      return $this->t_jurusan();

    } else {

      $data = array('nama_jurusan'  => $this->input->post('jurusan'));
      $this->model_all->insert('tb_jurusan', $data);
      alert_success('Jurusan Berhasil Ditambahkan');
      return redirect('admin/Jurusan');
    }
  }

  public function u_jurusan()
  {
    $this->model_security->getsecurity();
    $isi['judul']   = "Jurusan / Tambah Jurusan";
    $isi['content'] = "backend/admin/dt_jurusan/u_jurusan";
    $id = $this->uri->segment(4);
    $where = array('id_jurusan' => $id);
    $data = $this->model_all->get_where('tb_jurusan', $where)->result();
    foreach ($data as $dt) {
      $isi['id_jurusan']    = $dt->id_jurusan;
      $isi['nama_jurusan']  = $dt->nama_jurusan;
    }
    $this->load->view("backend/admin/home", $isi);
  }

  public function update_jurusan()
  {
    $this->model_security->getsecurity();
    $this->form_validation->set_rules('jurusan', 'Jurusan', 'required',
      array('required'  => 'Jurusan Tidak Boleh Kosong')
    );

    if ($this->form_validation->run() == FALSE) {

      return $this->u_jurusan();

    } else {
      $where = array('id_jurusan' => $this->input->post('id_jurusan'));
      $data = array('nama_jurusan' => $this->input->post('jurusan'));
      $this->model_all->update('tb_jurusan', $data, $where);
      alert_success('Jurusan Berhasil Diupdate');
      return redirect('admin/Jurusan');
    }
  }

  public function delete_jurusan()
  {
    $this->model_security->getsecurity();
    $id_jurusan = $this->uri->segment(4);
    $where = array('id_jurusan' => $id_jurusan);
    $this->model_all->del_id('tb_jurusan', $where);
    alert_danger('Jurusan Berhasil Dihapus');
    return redirect('admin/Jurusan');
  }
}
