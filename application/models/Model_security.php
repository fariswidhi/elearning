<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_security extends CI_Model {

	public function getsecurity() 
	{
		$user = $this->session->userdata('id');
		if(empty($user))
		{
			$this->session->sess_destroy();
			redirect('Login');
		}
	}
}
